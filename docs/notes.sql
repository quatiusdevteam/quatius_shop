-- Note for selecting spec and value.

SELECT id name, specValue FROM (SELECT `label`as name, `value` FROM `dictionaries` WHERE `group` = 'specification_name') specname LEFT JOIN (SELECT id ,`label` as specValue, `value`, SUBSTRING_INDEX(SUBSTRING_INDEX(`value`, '_', 1), '_', -1) as specId, SUBSTRING_INDEX(SUBSTRING_INDEX(`value`, '_', 2), '_', -1) as valId FROM `dictionaries` WHERE `group` != 'specification_name') as sepvale on specname.value = sepvale.specId
