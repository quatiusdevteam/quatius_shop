<?php

use Quatius\Shop\Providers\ShopServiceProvider;

class ShopperGroupTest extends TestCase {
    
    protected function getPackageProviders($app)
    {
        return [
            ShopServiceProvider::class
        ];
    }
    
    protected function getPackageAliases($app)
    {
        return [
            ///'Cache'            => Illuminate\Support\Facades\Cache::class,
            // 'Acme' => 'Acme\Facade'
        ];
    }
    
    /**
     * Resolve application Console Kernel implementation.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    /* protected function resolveApplicationConsoleKernel($app)
    {
        //$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();
        //$app->singleton('Illuminate\Contracts\Console\Kernel', 'Acme\Testbench\Console\Kernel');
    } */
    
    /**
     * Resolve application HTTP Kernel implementation.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    /* protected function resolveApplicationHttpKernel($app)
    {
        $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();
        //$app->singleton('Illuminate\Contracts\Http\Kernel', 'Acme\Testbench\Http\Kernel');
    } */
    
    /**
     * Setup the test environment.
     */
    public function setUp()
    {
        parent::setUp();
        
        //$this->loadLaravelMigrations(['--database' => 'testbench']);
        //$this->withFactories(__DIR__.'/factories');
        // Your code here
    }
    
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
       /*  $app['config']->set('database.default','mysql');
        $app['config']->set('database.connections.mysql', [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST', 'localhost'),
            'database'  => 'clients_soniq_solut',
            'username'  => 'root',
            'password'  => 'pass',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ]); */
    }
    
	/** @test */
	public function example()
	{
	    //$user = App\User::find(3);
	    //$this->assertTrue(!!$user);
	    //$prod = factory(Product::class)->make();
	    //dd($prod);
	    //$this->assertTrue($prod);
	    //dd(user());
		//dd(app('ProductHandler')->makeModel()->find(1));
	    //$secret = config('app.debug');
	    //echo $secret;
		//$this->assertTrue(config('app.debug'));
	}
	
	/** @test */
	public function ShopperHandlerRepository(){
	    
	    $repo = app('ShopperHandler');
	    
	    $this->assertTrue($repo instanceof \Quatius\Shop\Repositories\ShopperRepository); 
	    
	    $this->assertTrue($repo->model() == "Quatius\Shop\Models\ShopperGroup");
	    
	    //$this->assertDatabaseHas($table, array $data);
	}
}
