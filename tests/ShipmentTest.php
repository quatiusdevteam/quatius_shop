<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Quatius\Shop\Models\OrderSession;
use Quatius\Shop\Models\OrderItem;
use Quatius\Shop\Models\ShipmentMethod;


class ShipmentTest extends TestCase {
    
    use DatabaseTransactions;//, WithoutMiddleware, DatabaseMigrations;
    
    /**
     * Setup the test environment.
     */
    public function setUp()
    {
        parent::setUp();
        //$this->beginDatabaseTransaction();
    }
	
	/** @test */
	public function ShipmentHandlerRepository(){
	    
	    $repo = app('ShipmentHandler');
	    
	    $this->assertTrue($repo instanceof \Quatius\Shop\Repositories\ShipmentRepository); 
	    $this->assertTrue($repo->model() == "Quatius\Shop\Models\ShipmentMethod");
	    
	    
	    $order = $this->getAnOrder();
	    
	    $this->assertNull(
	        $repo->getCost($order, collect([new ShipmentMethod(['name'=>'user','execute_level'=>'total', 'selectable_by'=>'system'])]))
	        ,'Cannot autoselect shipment method - only system selection');
	    
	    
	    $order->method('getShipmentMethod')
	    ->willReturn(new ShipmentMethod(['name'=>'user','execute_level'=>'total', 'selectable_by'=>'system']));
	    
	    // Select a shipment automatically
	    $total = $repo->getCost($order, collect([]));
	    
	    // should use set of shipCost = 8.50 ;
	    $this->assertEquals(17, $total,
	        'set shipCost of 8.50 and qty 2'); 
	    
	    
	    $orderProds = collect($order->getProducts());
	    	    
	    $this->assertEquals(8.50, $orderProds->first()->shipCost);
	    
	    $repo->resetItemCost($order);
	    $this->assertFalse(isset($orderProds->first()->shipCost));
	    
	    $shipMet = new ShipmentMethod(['name'=>'user','execute_level'=>'total', 'selectable_by'=>'system']);
	    $distance = $shipMet->getDistancePostcode('3000', '3195');
	    $this->assertEquals(24015, round($distance));
	    /* $shipMethod = Mockery::mock(ShipmentMethod::class);//$this->createMock(ShipmentMethod::class);
	    
	    $shipMethod->shouldReceive('getAttribute')
	    ->with('selectable_by')
	    ->andReturn('systems');
	    
	    $shipMethod->shouldReceive('getTotalCost')
	    ->with($order)
	    ->andReturn(15); */
	    
	}
	
	private function getAnOrder(){
	    
	    $order = $this->createMock(OrderSession::class);
	    
	    $orderItem = new OrderItem();
	    $orderItem->shipCost = 8.50;
	    
	    $product = new Quatius\Shop\Models\Product();
	    $product->params= '{"pronto_data":{"New Stock":"AU","JB PREPAID?":"","FreightDefault":"   45.4545","Freight SA":"   54.5454","Freight WA":"  118.1818","Freight NT":"   63.6363","Freight QLD":"   63.6363","Freight NSW":"   54.5454","Freight ACT":"   54.5454","Freight VIC":"   45.4545","Freight TAS":"   72.7272","Cship":"TV:X:COMBINE:X"}}';
	    
	    $orderItem->setProduct($product, 2);
	    
	    $order->method('getProducts')
	    ->willReturn([$orderItem]);
	    
	    return $order;
	}

}
