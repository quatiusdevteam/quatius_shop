<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Quatius\Shop\Models\UserDetail::class, function(Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_1' => $faker->phoneNumber,
        'address_1' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->randomElement(['VIC','ACT','QLD','NSW','TAS','WA','SA','NT']),
        'country' => 'Australia',
        'postcode' => $faker->postcode
    ];
});
    

$factory->define(Lavalite\Page\Models\Page::class, function(Faker\Generator $faker) {
    $description = $faker->sentence();
    $name = $faker->words(1,true);
    $sku = $faker->regexify('[A-Z0-9]{6,12}');
    $type = 'category';
    return [
        'name' => $name,
        'title' => $name,
        'description' => $description,
        'content' => $description,
        'slug' => $type,
        'view' => $type,
        'abstract' => $type,
        'status' => 1,
        'keyword' => $faker->words(5,true),
    ];
});

$factory->define(Quatius\Shop\Models\Product::class, function(Faker\Generator $faker) {
    $description = $faker->sentence();
	$sku = $faker->regexify('[A-Z0-9]{6,12}');
	
    return [
        'description' => $description,
    	'sku' => $sku,
        'slug' => $sku,
    	'barcode' => $faker->isbn13,
    	'params' => '{}'
	];
});

$factory->define(Quatius\Shop\Models\Category::class, function(Faker\Generator $faker) {
    $page = factory('Lavalite\Page\Models\Page')->create();
    $page = Lavalite\Page\Models\Page::find($page->id);
    return [
        'page_id' => $page->id,
        'slug' => str_slug($page->name),
        'parent_id' => 0,
        'status' => $faker->numberBetween(1,2,3)
    ];
});

$factory->define(Quatius\Shop\Models\ProductPrice::class, function(Faker\Generator $faker) {
    
    return [
        'price' => $faker->randomFloat(2,8,200),
        'override'=> 0,
        'override_price'=> 0,
		'price_group'=>"RRP",
		'discount_group'=>"",
    ];
});


/*
$factory->define(Quatius\Shop\Models\Specification::class, function(Faker\Generator $faker) {
    $name = $faker->word;
    
    return [
        'name' => $name,
        'status' => $faker->numberBetween(1,2,3)
    ];
});
*/