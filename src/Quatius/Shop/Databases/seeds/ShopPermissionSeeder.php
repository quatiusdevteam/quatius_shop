<?php

namespace Quatius\Shop\Databases\seeds;
use Illuminate\Database\Seeder;
use DB;

class ShopPermissionSeeder extends Seeder
{
    public function run()
    {
        $roleRepo = app(\Litepie\Contracts\User\RoleRepository::class);
        
        try{
            DB::table('permissions')->insert([
                [
                    'slug' => 'order.add_to_cart',
                    'name' => 'Add To Cart',
                ]
            ]);
            
            
            $guest = $roleRepo->findWhere(['name'=>'guest'])->first();
            if ($guest){
                $permissions = $guest->permissions;
                $permissions['order.add_to_cart'] = "1";
                $roleRepo->update(['permissions'=>$permissions], $guest->id);
            }
            
            $registered = $roleRepo->findWhere(['name'=>'registered'])->first();
            if ($registered){
                
                $permissions = $registered->permissions;
                $permissions['order.add_to_cart'] = "1";
                
                $roleRepo->update(['permissions'=>$permissions], $registered->id);
            }
        }
        catch (\Exception $e){
            
        }
    }
}
