<?php
namespace Quatius\Shop\Databases\seeds;

use Illuminate\Database\Seeder;
use DB;

class ShopperGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE shopper_groups;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        DB::table('shopper_groups')->insert([
            [
                'id'         => 1,
                'name'       => 'Guest',
                'price_group'=> 'GUEST',
                'default'    => 1,
                'ordering'   => 1
            ],
            [
                'id'         => 2,
                'name'       => 'Registered',
                'price_group'=> 'REGISTER',
                'default'    => 2,
                'ordering'   => 2
            ],
            [
                'id'         => 3,
                'name'       => 'Wholesale',
                'price_group'=> 'RRP',
                'default'    => 3,
                'ordering'   => 3
            ]
        ]);
    }
}
