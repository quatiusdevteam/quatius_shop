<?php

namespace Quatius\Shop\Databases\seeds;
use Illuminate\Database\Seeder;
use DB;

class ShopInitialSeeder extends Seeder
{
    public function run()
    {
        $this->call(ShopPermissionSeeder::class);
        
        $max_menu_id = DB::table('menus')->select(DB::raw('MAX(id) AS "id"') )->first()->id;
		if($max_menu_id==0) $max_menu_id = 100;
		DB::statement("ALTER TABLE `menus` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=$max_menu_id;");
						
        $adminShopId = DB::table('menus')->insertGetId(
            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/shop',
                'name'        => 'Shop',
                'description' => null,
                'icon'        => 'fa fa-cube',
                'target'      => null,
                'order'       => 11,
                'status'      => 1,
                'role'        => null,
            ]
        );
        
        DB::table('menus')->insert([
            [
                'parent_id'   => $adminShopId,
                'key'         => null,
                'url'         => 'admin/shop/product',
                'name'        => 'Products',
                'description' => null,
                'icon'        => 'fa fa-cubes',
                'target'      => null,
                'order'       => 11,
                'status'      => 1,
                'role'        => null,
            ],
            [
                'parent_id'   => $adminShopId,
                'key'         => null,
                'url'         => 'admin/shop/order',
                'name'        => 'Orders',
                'description' => null,
                'icon'        => 'fa fa-file-text-o',
                'target'      => null,
                'order'       => 12,
                'status'      => 1,
                'role'        => null,
            ],
            [
                'parent_id'   => $adminShopId,
                'key'         => null,
                'url'         => 'admin/shop/payment',
                'name'        => 'Payment Methods',
                'description' => null,
                'icon'        => 'fa fa-credit-card',
                'target'      => null,
                'order'       => 13,
                'status'      => 1,
                'role'        => '["superuser"]',
            ],
            [
                'parent_id'   => $adminShopId,
                'key'         => null,
                'url'         => 'admin/shop/shipment',
                'name'        => 'Shipment Methods',
                'description' => null,
                'icon'        => 'fa fa-truck',
                'target'      => null,
                'order'       => 14,
                'status'      => 1,
                'role'        => null,
            ],
            [
                'parent_id'   => $adminShopId,
                'key'         => null,
                'url'         => 'admin/shop/shoppers',
                'name'        => 'Shopper Groups',
                'description' => null,
                'icon'        => 'fa fa-users',
                'target'      => null,
                'order'       => 15,
                'status'      => 1,
                'role'        => null,
            ],
            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => '#',
                'name'        => 'Products',
                'description' => "{!! view('Shop::menu.category',compact(['menu']))!!}",
                'icon'        => null,
                'target'      => null,
                'order'       => 22,
                'status'      => 1,
                'role'        => null,
            ],
            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => '#',
                'name'        => 'Cart',
                'description' => "{!! view('Shop::menu.cart',compact(['menu']))!!}",
                'icon'        => 'fa fa-shopping-cart',
                'target'      => null,
                'order'       => 23,
                'status'      => 1,
                'role'        => null,
            ]
        ]);
        
    }
}
