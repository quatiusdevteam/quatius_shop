<?php
namespace Quatius\Shop\Databases\seeds;

use Illuminate\Database\Seeder;
use App\Modules\Account\Models\UserDetail;
use App\User;

class UserDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserDetail::truncate();
        
        User::get()->each(function($user){
            $user->setBillingDetail(factory(UserDetail::class)->make());
            for ($i=0; $i < rand(0,3); $i++)
            {
                $user->setShipmentDetail(factory(UserDetail::class)->make());
            }
        });
    }
}
