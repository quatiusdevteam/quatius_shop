<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('account',20)->nullable();
            $table->string('address_type',2)->default('BT');
            $table->string('company', 64)->nullable();
            $table->string('title',32)->nullable();
            $table->string('first_name',64)->nullable();
            $table->string('middle_name',64)->nullable();
            $table->string('last_name',64)->nullable();
            $table->string('phone_1',24)->nullable();
            $table->string('phone_2',24)->nullable();
            $table->string('address_1',64)->nullable();
            $table->string('address_2',64)->nullable();
            $table->string('city',32)->nullable();
            $table->string('state',64)->default('VIC');
            $table->string('country',64)->default('AU');
            $table->string('postcode',15)->nullable()->default('3000');
            $table->timestamps();
            
            $table->foreign('user_id')
	            ->references('id')
	            ->on('users')
	            ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
