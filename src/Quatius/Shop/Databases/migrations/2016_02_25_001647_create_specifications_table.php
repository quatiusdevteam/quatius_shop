<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unit_type',100);// String, Int, Float //Now link to data dictionaries
            $table->string('unit_display',50)->nullable()->default('');
            $table->enum('search_by',['field','value','both',''])->nullable();
            
            $table->tinyInteger('status')->default(2); // 1=unpublished, 2=published, 3=maintained
            $table->integer('ordering')->unsigned()->default(0);
            $table->timestamps();
        });
        
        Schema::create('specification_values', function (Blueprint $table) {
            $table->integer('specification_value_id')->unsigned();
            $table->string('specification_value_type');
            $table->integer('specification_id')->unsigned();
            $table->string('value',100);
            $table->tinyInteger('status')->default(2); // 1=unpublished, 2=published, 3=maintained
            $table->foreign('specification_id')
            ->references('id')
            ->on('specifications')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specification_values');
        Schema::dropIfExists('specifications');
    }
}
