<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_histories_id')->unsigned();
            $table->string('shop_histories_type');
            $table->string('status',20)->default('pending');
            $table->string('value',150)->nullable();
            $table->string('comments')->default('');
            $table->text('params')->nullable();
            $table->timestamps();
            $table->string('link_type')->nullable();
            $table->integer('link_id')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_histories');
    }
}
