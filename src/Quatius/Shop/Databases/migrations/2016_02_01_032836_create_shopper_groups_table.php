<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Quatius\Shop\Databases\seeds\ShopperGroupSeeder;

class CreateShopperGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopper_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('default')->default(0);
            $table->string('account', 20)->nullable();
            $table->string('price_group', 20)->default('RRP');
            $table->string('reference', 20)->nullable();
            $table->integer('page_id')->unsigned();
            $table->integer('ordering')->default(0);
        });
        
        $shopSeed = new ShopperGroupSeeder();
        $shopSeed->run();
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopper_groups');
    }
}
