<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price', 15, 4)->default(0);
            $table->boolean('override')->default(false);
            $table->decimal('override_price', 15, 4)->default(0);
            $table->string('price_group', 20)->nullable();
            $table->string('discount_group', 20)->nullable();
            $table->string('currency', 10)->default('AUD');
            $table->dateTime('publish_up')->nullable();
            $table->dateTime('publish_down')->nullable();
            $table->integer('qty_start')->default(0);
            $table->integer('qty_end')->default(0);
            $table->integer('product_id')->unsigned()->index();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
