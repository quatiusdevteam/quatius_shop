<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->string('destinations')->nullable();
            $table->string('address_1',64)->nullable();
            $table->string('address_2',64)->nullable();
            $table->string('city',32)->nullable();
            $table->string('state',64)->default('VIC');
            $table->string('country',64)->default('AU');
            $table->string('postcode',15)->nullable()->default('3000');
            $table->float('lat', 10,6)->nullable()->default(0.0);
            $table->float('lng', 10,6)->nullable()->default(0.0);
            $table->tinyInteger('status')->default(2); // 1=unpublished, 2=published, 3=maintained
            
            $table->integer('ordering')->default(0);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('warehouses');
    }
}
