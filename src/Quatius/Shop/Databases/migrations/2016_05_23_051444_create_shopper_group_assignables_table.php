<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Quatius\Shop\Databases\seeds\ShopperGroupSeeder;

class CreateShopperGroupAssignablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 		Schema::create('shopper_assignables', function (Blueprint $table) {
			$table->integer('shopper_assignable_id')->index();
            $table->string('shopper_assignable_type');
            $table->integer('shopper_group_id')->unsigned();
            $table->primary(['shopper_assignable_id', 'shopper_assignable_type','shopper_group_id'],
            				"shopper_assignables_id_type_shopper_group_id_primary");
        });
 		
	    $shopSeed = new ShopperGroupSeeder();
	    $shopSeed->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopper_assignables');
    }
}
