<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status',20)->default('pending');
            $table->string('order_number',20)->unique()->nullable();
            $table->string('payment_method',20)->default('');
            $table->dateTime('delivery_date')->nullable();
            //$table->string('cust_reference', 32)->nullable();
            
            $table->string('currency',4)->default('AUD');
            $table->decimal('discount', 16, 4)->default(0);
            $table->decimal('shipping_cost', 8,4)->default(0);
            $table->decimal('shipping_cost_ex_tax', 8,4)->default(0);
            $table->decimal('subtotal', 16, 4)->default(0);
            $table->decimal('total', 16, 4)->default(0);
            $table->decimal('tax_total', 8,4)->default(0);
            //$table->string('order_desc')->nullable();
            //$table->string('rep', 5)->nullable();
           //$table->string('rep_name')->nullable();
           // $table->string('order_type', 5)->nullable()->default('R');
            $table->string('comments', 255)->nullable();
            $table->text('params')->nullable();
            $table->integer('pronto_order_id')->unsigned()->nullable();
            $table->timestamps();
        });
        
        Schema::create('order_details', function (Blueprint $table) {
       		$table->increments('id');
       		$table->integer('user_id')->unsigned()->nullable();
       		$table->integer('order_id')->unsigned();
       		$table->string('address_type',2)->default('BT');
       		$table->string('account',20)->nullable();
       		$table->string('company', 64)->nullable();
       		$table->string('title',32)->nullable();
       		$table->string('first_name',64)->nullable();
       		$table->string('middle_name',64)->nullable();
       		$table->string('last_name',64)->nullable();
       		$table->string('email', 255)->nullable();
       		$table->string('phone_1',24)->nullable();
     		$table->string('phone_2',24)->nullable();
       		$table->string('address_1',64)->nullable();
       		$table->string('address_2',64)->nullable();
       		$table->string('city',32)->nullable();
       		$table->string('state',64)->default('VIC');
       		$table->string('country',64)->default('Australia');
       		$table->string('postcode',15)->nullable();
       		$table->foreign('order_id')
        		->references('id')
        		->on('orders')
        		->onDelete('cascade');
        		 
        	$table->foreign('user_id')
        	->references('id')
        		->on('users')
        	->onDelete('set null');
        });
        
        Schema::create('order_items', function (Blueprint $table) {
        	
        	$table->increments('id');
        	$table->integer('order_id')->unsigned();
        	$table->integer('product_id')->nullable()->unsigned();
        	$table->string('description',200)->nullable();
        	$table->string('sku',64)->nullable();
        	$table->string('barcode',64)->nullable();
        	$table->integer('qty')->default(1);
        	$table->integer('shipped_qty')->default(0);
        	$table->decimal('price', 16, 4)->default(0);
        	$table->decimal('discount', 8,4)->default(0);
        	$table->decimal('tax', 8,4)->default(0);
        	$table->decimal('total', 16, 4)->default(0);
        	$table->text('params')->nullable();
        	//$table->timestamps();
        	
        	$table->foreign('order_id')
        	->references('id')
        	->on('orders')
        	->onDelete('cascade');
        	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('orders');
    }
}
