<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AlterPageTableAddParams extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Fixed: Unknown database type enum requested error;
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    
        Schema::table('pages', function ($table) {
            $table->string('name', 250)->change();
            $table->string('params')->default('[]')->after('abstract');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Fixed: Unknown database type enum requested error;
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        
        Schema::table('pages', function ($table) {
            $table->string('name', 50)->change();
            $table->dropColumn('params');
        });
    }
}
