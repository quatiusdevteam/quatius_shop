<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Quatius\Shop\Databases\seeds\ShopInitialSeeder;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description',200)->nullable();
            $table->string('type',20)->default('generic');
            $table->string('slug',200);
            $table->string('sku',50);
            $table->string('barcode',50);
            $table->string('keyword',2000)->nullable();
            $table->string('specs',2000)->nullable();
            $table->integer('page_id')->nullable()->unsigned();
            $table->integer('manufacturer_id')->unsigned()->default(0);
            $table->text('params')->nullable(); //json format
            $table->tinyInteger('status')->default(1); // 1=unpublished, 2=published, 3=maintained
            $table->softDeletes();
            $table->timestamps();
        });
        
        $shopSeed = new ShopInitialSeeder();
        $shopSeed->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
