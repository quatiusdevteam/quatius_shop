<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShipmentSupport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_shipment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150)->default('');
            $table->string('label',150)->default('');
            $table->string('description',250)->nullable()->default('');
            $table->string('customer_note',250)->nullable()->default('');
            $table->string('order_note',250)->nullable()->default('');
            $table->string('type',50)->default('');
            $table->text('params')->nullable();
            $table->tinyInteger('status')->default(1); // 1=unpublished, 2=published, 3=maintained
            $table->integer('ordering')->unsigned()->default(0);
            $table->enum('selectable_by',['default','user','manual','system'])->default('user');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::table('user_details', function (Blueprint $table) {
            $table->text('state')->nullable()->change();
        });
        
        Schema::table('order_details', function (Blueprint $table) {
            $table->text('state')->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_shipment_methods');
        
    }
}
