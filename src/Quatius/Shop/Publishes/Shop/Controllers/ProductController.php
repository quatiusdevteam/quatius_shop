<?php 
namespace App\Modules\Shop\Controllers;

use App\Http\Controllers\PublicWebController;
use Quatius\Shop\Traits\Controllers\PublicProduct;


class ProductController extends PublicWebController {

    use PublicProduct;
    
}
