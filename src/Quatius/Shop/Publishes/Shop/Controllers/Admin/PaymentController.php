<?php

namespace App\Modules\Shop\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use Quatius\Shop\Traits\Controllers\Admin\ManagePayment;

class PaymentController extends AdminWebController
{
    use ManagePayment;
}
