<?php

namespace App\Modules\Shop\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use Quatius\Shop\Traits\Controllers\Admin\ManageOrder;

class OrderController extends AdminWebController
{
    use ManageOrder;
}
