<?php

namespace App\Modules\Shop\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use Quatius\Shop\Traits\Controllers\Admin\ManageProduct;

class ProductController extends AdminWebController
{
    use ManageProduct;
}
