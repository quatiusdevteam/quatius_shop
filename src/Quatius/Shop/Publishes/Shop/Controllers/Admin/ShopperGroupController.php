<?php 

namespace App\Modules\Shop\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use Quatius\Shop\Traits\Controllers\Admin\ManageShopperGroup;

class ShopperGroupController extends AdminWebController
{
    use ManageShopperGroup;
}
