<?php

namespace App\Modules\Shop\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use Quatius\Shop\Traits\Controllers\Admin\ManageShipment;

class ShipmentController extends AdminWebController
{
    use ManageShipment;
}
