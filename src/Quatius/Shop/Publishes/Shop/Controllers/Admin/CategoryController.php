<?php

namespace App\Modules\Shop\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use Quatius\Shop\Traits\Controllers\Admin\ManageCategory;


class CategoryController extends AdminWebController
{
    use ManageCategory;
    
}
