<?php

namespace App\Modules\Shop\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use Quatius\Shop\Traits\Controllers\Admin\ManageSpecification;

class SpecificationController extends AdminWebController
{
    use ManageSpecification;
}
