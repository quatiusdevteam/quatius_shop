<?php

namespace App\Modules\Shop\Controllers;

use App\Http\Controllers\PublicWebController;
use Quatius\Shop\Traits\Controllers\UpdateWishList;

class WishListController extends PublicWebController
{
    use UpdateWishList;
}
