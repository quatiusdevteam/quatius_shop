<?php 
namespace App\Modules\Shop\Controllers;

use App\Http\Controllers\PublicWebController;
use Quatius\Shop\Traits\Controllers\UpdateCart;

class CartController extends PublicWebController {

    use UpdateCart;
    
}
