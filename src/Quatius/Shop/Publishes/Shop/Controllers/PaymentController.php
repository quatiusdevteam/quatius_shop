<?php

namespace App\Modules\Shop\Controllers;

use App\Http\Controllers\PublicWebController;
use Quatius\Shop\Traits\Controllers\DoPayment;


class PaymentController extends PublicWebController {
    
    use DoPayment;
    
}
