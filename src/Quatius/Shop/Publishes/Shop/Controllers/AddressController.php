<?php

namespace App\Modules\Shop\Controllers;

use App\Http\Controllers\Controller;
use Quatius\Shop\Traits\Controllers\AddressUpdate;

class AddressController extends Controller
{
    use AddressUpdate;
}
