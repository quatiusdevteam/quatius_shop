/**
 * 
 */
var shopBaseUrl = 'admin/shop/';
var disableCategoryPost = false;

function saveForm(formId, targetId, callback){
	$.post($(formId).attr('action'),
		$(formId).serialize(),
		function(responseData){
			if (responseData.view && targetId)
			{
				$(targetId).empty();
				$(targetId).html(responseData.view);
			}
			if (callback)
			{
				callback(responseData, $(formId));
			}
		},
		'json'
	);
}

function getCategoryDialog(){
	if ($('#edit-cate-dialog').length > 0)
		return $('#edit-cate-dialog');

	$('<div id="edit-cate-dialog" class="modal fade" tabindex="-1" role="dialog">').appendTo('body');
	return $('#edit-cate-dialog');
}

function deleteCategory(itm){
	$(itm).parents('.modal').modal('hide');
	var cateId = $(itm).parents('.category[data-id]').attr('data-id');
	$.post(getCateUrl('', itm), 
		'_method=delete',
		function(result){
			$('.category-item[data-id="'+cateId+'"]').remove();
	},'json');
}

function editCategory(itm){
	var dialog = getCategoryDialog();
	$.get(getCateUrl('/edit',itm), function(content){
		dialog.empty();
		dialog.html(content);
		dialog.find('[name="selected"]').val($(itm).parents('[data-selected]').attr('data-selected'));
		dialog.modal('show');			
	});
}

function createCategory(itm){
	var dialog = getCategoryDialog();
	$.get(getCateUrl('create'), function(content){
		dialog.empty();
		dialog.html(content);
		dialog.find('[name="selected"]').val($(itm).parents('[data-selected]').attr('data-selected'));
		dialog.modal('show');		
	});
}

function getCateUrl(extra, itm){
	if (itm!=null){
		var cateId = $(itm).parents('.category[data-id]').attr('data-id');
		extra = cateId+extra;
	}
	return shopBaseUrl+'category/'+extra;
}

function getProdUrl(extra, itm){
	if (itm!=null){
		var prodId = $(itm).parents('.product[data-id]').attr('data-id');
		extra = prodId+extra;
	}
	if (extra){
		return shopBaseUrl+'product/'+extra;	
	}
	
	return shopBaseUrl+'product';
}

function getSpecUrl(extra){
	if (extra)
		return shopBaseUrl+'specification/'+extra;
	return shopBaseUrl+'specification';
}

function updateCategory(itm){
	if (disableCategoryPost)
		return;
		
	var inputs =  $(itm).parents('.category-list').find('input:checked');

	$.post(getProdUrl("/assign/categories", itm), 
		inputs.serialize(),
		function(result){
			var prodId = $(itm).parents('.product[data-id]').attr('data-id');
			updateTableData(prodId, 'category_ids', result.category_ids);
		
	},'json');
}

function newProduct(itm){
	var inputs =  $(itm).find('input');
	$(itm).find('button[type="submit"]').prop('disabled', true);
	$.post(getProdUrl("new"),
		inputs.serialize(),
		function(result){
			oTable.ajax.reload();
			$('[data-toggle]').popover('destroy');
		}
	);
	return false;
}

function updateProduct(itm, field, targetId){
	var posting={
		_method:"patch"
	};
    posting[field] = itm.value;
    
	$.post(getProdUrl("",itm), posting,function(result){
		if (result.view)
			$(targetId).html(result.view);
	},'json');
}

function assignProductToPage(itm, page_id, targetId){
	$.post(getProdUrl("/page/assign/"+page_id,itm), '',function(result){
		var prodId = $(itm).parents('.product[data-id]').attr('data-id');
		updateTableData(prodId, 'status', result.status);
		if (result.view)
			$(targetId).html(result.view);
	},'json');
}

function removeProductFromPage(itm){
	$.post(getProdUrl("/page/remove",itm), '',function(result){
		var prodId = result.product_id;
		updateTableData(prodId, 'status', result.status);
		
		if ($('.child-row.product').length > 1){
			$('.child-row.product[data-id="'+prodId+'"]').remove();
		}else{
			$('#box-area').empty();
		}
	},'json');
}

function removeImage(itm){
	$(itm).parents('.media-tile').css('opacity','0.5');
	$(itm).prop('disabled',true);
	$.post(getProdUrl("/image",itm), "_method=delete&"+$(itm).parents('.media-tile').find('input').serialize(),function(result){
		$(itm).parents('.media-tile').remove();
	});
}

function replaceRemoveButton(itm){
	$(itm).replaceWith('<button style="position: absolute; right: 0;top: 0;" class="btn-link remove-indicator" onclick="removeImage(this)" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>');
}

function completedSelect(itm, addedbutton, mediaSelect){
	$(itm).find('input.ordering').val($(itm).parents('.media-selection-area').attr('data-size'));
	replaceRemoveButton(itm.find('.remove-indicator'));
	$.post(getProdUrl("/image",itm), "_method=put&"+$(itm).find('input').serialize(),function(result){
		
	});
	mediaSelect.css("opacity", "0.5");
	return true;
} 

function completedDrag(itm){
	var inputMediaId = $(itm).parents('.media-selection-area').find('input.media_id');
	inputMediaId.attr('name', 'medias[][media_id]');
	replaceRemoveButton(itm.find('.remove-indicator'));
	$.post(getProdUrl("/image/ordering",itm), "_method=put&"+inputMediaId.serialize(),function(result){
		
	});
}

function updateStatus(itm, value){
	var status = value;
	$.post(getProdUrl('/status',itm), 
			"_method=put&status="+status,
			function(response){
				var prodId = $(itm).parents('.product[data-id]').attr('data-id');
				updateTableData(prodId, 'status', status);
				$(itm).parents('.product').removeClass('status-unpublish').removeClass('status-published');
				if (status==2){
					$(itm).parents('.product').addClass('status-published');
				}else{
					$(itm).parents('.product').addClass('status-unpublish');
				}
	    	}, 'json'
		);
}

function assignCategory(itm){
	var cateId = $(itm).parents('.category-item[data-id]').attr('data-id');

	$.get(getProdUrl('/category/assign/'+cateId,itm), function(result){
		
	});
}

function checkUnloadSpec(itm){ //unused
	var selItm = $(itm);

	if (selItm.hasClass('')){
	}
}

function createSelection(itmId){
	var selItm = $(itmId);
	var specTable = selItm.parents('.spec-group').find('.spec-table');
	selItm.removeClass('unload');

	selItm.empty();
	selItm.append("<option value=''>Add Specification</option>");
	selItm.append("<option value='0'>&lt;New Specification&gt;</option>");
	var specArray = new Array();
	$.each(specList, function(index, data){
		specArray.push(data);
	});
	
	$.each(specArray.reverse(), function(index, data){
		if (data){
			var disable = specTable.find('[data-id="'+data.id+'"]').length == 0?'':'style="display:none"';
			selItm.append("<option value='"+data.id+"'"+disable+">"+data.name+"</option>");
		}
	});
}

function getEditTemplate(specGroup, specId, setValue)
{
	if (specList[specId] != undefined)
	{
		var specRow = specGroup.find('.spec-template-value').clone().removeClass('spec-template-value').addClass('spec-item');
		specRow.attr('data-id', specId);
		
		specRow.find('[data-toggle="popover"]').popover();

		specRow.find('input[name="spec_id"]').val(specId);
		specRow.find('input[name="spec_name"]').val(specList[specId].name);
		specRow.find('input[name="unit_display"]').val(specList[specId].unit_display);

		var specValues = specList[specId].values;
		
		specRow.find('.opt-values').empty();
		$.each(specValues, function(key, value){
			var valSel = $('<li><a data-value="'+key+'">'+value+'</a></li>');
			valSel.on('click', function(){
				 loadSpecValue(this, $(this).find('a').text(), $(this).find('a').data('value'));
			});
			
			specRow.find('.opt-values').append(valSel);
		});

		if (setValue != null)
		{
			if (setValue.indexOf(" ") != -1){ /* multiple values*/
				var valList = setValue.split(" ");
				
				var tmpView = specRow.find('input[name="label[]"]');
				if (valList.length > 0){
					loadSpecValue(tmpView, specValues[valList[0]], valList[0]);
				}
				for(var i = 1; i < valList.length; i++){
					tmpView = cloneSpecValue(tmpView,specValues[valList[i]], valList[i]);
				}
			}
			else{
				try{
				loadSpecValue(specRow.find('input[name="label[]"]'), specValues[setValue], setValue);
				}
				catch(e){}
			}
		}
		return specRow;
	}
	return null;
}

function selectSpec(itm){
	var specId = $(itm).val();
	var specGroup = $(itm).parents('.spec-group');
	var specTable = specGroup.find('.spec-table');
	
	if (specId == '0')
	{
		specTable.append(
			specGroup.find('.spec-template-new').clone().removeClass('spec-template-new').addClass('spec-item'));
		$(itm).val('');
	}
	else if(specId != '')
	{
		if (specTable.find('[name="spec_id"][value="'+specId+'"]').length > 0 || specTable.find('[data-id="'+specId+'"]').length > 0)
			return $(itm).val('');
		
		specTable.append(getEditTemplate(specGroup, specId));
		$(itm).val('');
	}
}

function saveSpec(itm){
	var specRow = $(itm).parents('.spec-item');
	$.post(getSpecUrl(), 
		specRow.find('input').serialize(),
		function(response){
			specList[response.data.data.id]=response.data.data;
			createSelection('.spec-select');
			specRow.replaceWith($(response.view));
			iconList = response.icons;
    	}, 'json'
	);
}

function updateSpecData(itm){
	var jJtm = $(itm);

	var data = new Object();
	data[jJtm.attr('name')] = jJtm.val();
	
	var specRow = $(itm).parents('.spec-item');
	var specId = specRow.find('input[name="spec_id"]').val();
	$('.btn-spec-setting').popover('hide');
	
	$.post(getSpecUrl(specId+'/update'), 
		data,
		function(response){
			specRow.find('.btn-spec-edit').click();
			specList[response.data.data.id]=response.data.data;
			createSelection('.spec-select');
			iconList = response.icons;
    	}, 'json'
	);
	
}

function removeSpecValue(itm){
	var specRow = $(itm).parents('.spec-item');
	var specGroup = specRow.parents('.spec-group');
	$('.btn-spec-setting').popover('hide');
	
	specRow.css('opacity','0.5');
	var specId = specRow.data('id');
	
	$.post(getSpecUrl(), 
		specGroup.find('.spec-template-delete input').serialize()+specRow.attr('data-value')
		+"&spec_id="+specId,
		function(response){
			specRow.remove();
    	}, 'json'
	);
	
}

function removeSpec(itm){
	var specRow = $(itm).parents('.spec-item');
	var specId = specRow.find('input[name="spec_id"]').val();
	$('.btn-spec-setting').popover('hide');

	$.post(getSpecUrl(specId), 
		{_method:"DELETE"},
		function(response){
			if(response.deleted){
				delete specList[parseInt(response.deleted)];
				createSelection('.spec-select');
				$('.spec-item[data-id="'+response.deleted+'"]').remove();
			}
    	}, 'json'
	);
}

function editSpec(itm){
	var specRow = $(itm).parents('.spec-item');
	var specGroup = specRow.parents('.spec-group');
	var specId = specRow.attr('data-id');
	var value = specRow.attr('data-value');
	
	var specEditRow = getEditTemplate(specGroup, specId, value);
	specEditRow.addClass('spec-editing');
	specRow.after(specEditRow);
	specRow.hide();
	
	specEditRow.find('.btn-spec-setting').popover({
		trigger : 'click',
		html:true,
		content : function (){
			return $('.spec-setting-tmpl').html();
		}
	});
	specEditRow.find('.btn-spec-setting').on('shown.bs.popover', function () {
		var groupArea = $(this).parent();
		
		if (specList[specId] != undefined){
			groupArea.find('select[name="unit_type"]').val(specList[specId].unit_type);
			groupArea.find('select[name="search_by"]').val(specList[specId].search_by);
			groupArea.find('input[name="ordering"]').val(specList[specId].ordering);

			if(specList[specId].params)
			{
				$.each(specList[specId].params, function(key, val){
					groupArea.find('[name="params['+key+']"]').val(val);
				});
			}
			
		}

		groupArea.find('.btn-spec-delete').popover({
			html:true,
			content : "<div class='text-center'><button class='btn btn-warning btn-xs' type='button' onclick='removeSpec(this)'> Yes </button> <button class='btn btn-default btn-xs' type='button'> No </button></div>",
			trigger : 'focus',
		});
	});

	specEditRow.find('.btn-spec-cancel').on('click', function(){
		$(this).parents('tr.spec-item').prev().show();
		$(this).parents('tr.spec-item').remove();
		$('.btn-spec-setting').popover('hide');
	});
}

function cloneSpecValue(itm, setValue, code){
	var inputValue = $(itm).parents('.spec-item-value');

	var newInputValue = inputValue.clone();
	inputValue.after(newInputValue);

	newInputValue.find('.opt-values li').on('click', function(){
		loadSpecValue(this, $(this).find('a').text(), $(this).find('a').data('value'));
	});
	loadSpecValue(newInputValue, setValue, code);
	return newInputValue;
}

function loadSpecValue(itmSpecVal, label, code){
	var specItem = $(itmSpecVal).parents('.spec-item-value');

	if ($(itmSpecVal).hasClass('spec-item-value')){
		specItem = $(itmSpecVal);
	}
	specItem.attr('data-code',code);
	specItem.find('input[name="label[]"]').val(label);
	
// 	specItem.find('[name="media_id[]"]').val(0);
// 	specItem.find('[name="params[]"]').val("{}");
 	specItem.find('.btn-spec-value-media').css('opacity','0.5');
	$.each(iconList, function(key, valData){
		if (code == key){
			specItem.find('[name="media_id[]"]').val(valData.media_id);
			specItem.find('[name="params[]"]').val(valData.params);
			specItem.find('.btn-spec-value-media').css('opacity','1');
		}
	});

	createIconPopup(specItem, code);
}

function createIconPopup(specItem, code){
	specItem.find('.btn-spec-value-media').popover({
			trigger : 'click',
			placement: 'left',
			html:true,
			title:"Attach Icon",
			content : function (){
				$('.btn-spec-value-media').not(this).popover('hide');
				var popup = $('<button style="position: absolute; right: 0;top: 0;" class="btn-link" onclick="$(\'.btn-spec-value-media\').popover(\'hide\')" type="button"><i class="fa fa-times" aria-hidden="true"></i></button> <button style="position: absolute;right: 20px;top: 40px;z-index: 5;" class="btn-link" onclick="removeSpecValueIcon(this);" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button> <div class="icon-preview" style="width:123px; height:138px; background-color:gray" onclick="$(this).parents(\'.popover\').css(\'z-index\',100);getMedia(this,false, function(media, itm){setSpecValueIcon(media, itm);}, \'spec-icons\')"></div>'
						+'<select onchange="setSpecValueIconPos(this)" class="form-control icon-pos-select"><option value=".cnr-DT">Product Detail</option><option value=".cnr-TL">Top Left</option><option value=".cnr-TR">Top Right</option><option value=".cnr-BL">Bottom Left</option><option value=".cnr-BR">Bottom Right</option></select>');
			
				return popup;
			}
		});
	
	specItem.find('.btn-spec-value-media').on('shown.bs.popover', function () {
		if (iconList[code] != undefined){
			specItem.find('.icon-preview').append(iconList[code].view);
		}

		var params = new Object();
		try{
			params = JSON.parse(specItem.find('[name="params[]"]').val());
		}catch(e){}
		
		if (params.class !== undefined){
			specItem.find('.icon-pos-select').val(params.class);
		}
	});
}

function setSpecValueIconPos(itm){
	var specItem = $(itm).parents('.spec-item-value');
	var params = new Object();
	try{
		params = JSON.parse(specItem.find('[name="params[]"]').val());
	}catch(e){}

	params.class =  $(itm).val();

	specItem.find('[name="params[]"]').val(JSON.stringify(params));
}

function setSpecValueIcon(media, itm){
	$(itm).empty();
	$(itm).append(media);

	media.find('.remove-indicator').remove();
	var specItem = $(itm).parents('.spec-item-value');

	var code = specItem.attr('data-code');
	var hasIcon = false;
	$.each(iconList, function(key, valData){
		if (code == key){
			hasIcon = true;
		}
	});
	if (!hasIcon){
		var tmpIcon = new Object();
		tmpIcon.media_id = media.data('id');
		tmpIcon.params = "{}";
		tmpIcon.view = media.html();

		iconList[code] = tmpIcon;
	}

	specItem.find('.btn-spec-value-media').css('opacity','1');
	specItem.find('[name="media_id[]"]').val(media.data('id'));
}

function removeSpecValueIcon(itm){
	$(itm).parent().find('.media-tile').remove();
	var specItem = $(itm).parents('.spec-item-value');
	specItem.find('[name="media_id[]"]').val(0);
	specItem.find('.btn-spec-value-media').css('opacity','0.5');
}

function getRowData(tableId, index){
	var table = $(tableId); 
	var row = table.row(index); 
	return row.data();
}

function setStatusFilter(itm){
	$(itm).toggleClass('active');
	oTable.draw();
}

var tableFilteringStatusId = '';

function addFilteringStatus(onTableId){
	tableFilteringStatusId = onTableId;
	$.fn.dataTable.ext.search.push(
	    function( settings, data, dataIndex ) {
		    if (tableFilteringStatusId != settings.sTableId ||
				$('button.active[data-filter_status]').length == 0 
				){
			    return true;
		    }
		    return ($('button.active[data-filter_status="'+data[1]+'"]').length == 1);
	    }
	);
}

function addFilteringCategroy(onTableId){
	tableFilteringStatusId = onTableId;

	$.fn.dataTable.ext.search.push(
	    function( settings, data, dataIndex ) {
		    if (tableFilteringStatusId != settings.sTableId ||
		    	$('#filter-category').val() == "0" 
				){
			    return true;
		    }
		    if (dataSets.data[dataIndex].category_ids == null || dataSets.data[dataIndex].category_ids == ""){
			    return false;
		    }
		    
			var selIds =  $('#filter-category').val().split(',');
			
		    var pcateIds = dataSets.data[dataIndex].category_ids.split(',');

			var match = false;

			$.each(selIds, function(selIn, selData){
				selData = ""+selData;
				$.each(pcateIds, function(prodIn, prodData){
					prodData = ""+prodData;
					if (selData == prodData) {match = true;}
				});
			});
		    
		    return match;
	    }
	);
}

var prodIdGrouped = new Array();
var pageIds = new Array();

function setGroupFilter(itm){
	prodIdGrouped = new Array();
	pageIds = new Array();
	
	$.each(dataSets.data,function (index, data){
		if (data.status != 3 && data.page_id != null && data.page_id != 0 )
	    {
    		if (pageIds.indexOf(data.page_id) == -1)
    	    {
    			prodIdGrouped.push(""+data.product_id);
    	    	pageIds.push(data.page_id);
    	    }
	    }
	});
	
	$(itm).toggleClass('active');
	oTable.draw();
}
function addFilteringGroup(onTableId){
	tableFilteringStatusId = onTableId;

	$.fn.dataTable.ext.search.push(
	    function( settings, data, dataIndex ) {
		    if (tableFilteringStatusId != settings.sTableId ||
				$('button.active[data-filter_group]').length == 0 
				){
			    return true;
		    }

		    if (prodIdGrouped.indexOf(data[0]) != -1) {
				return true;
		    }
		    return false;
	    }
	);
}

function reset(itm){
	$('#product_filter .active').removeClass('active');
	$('#filter-category').val(0);
	oTable.draw();
}

function updateTableData(id, key, value){
	var item = searchData(dataSets.data, 'id', id);
	if (item != null){
		var index = searchDataIndex(dataSets.data, item);
		item[key] = value;
		
		if (index!=null){
			refreshRow(index,item);
		}
	}
}

function refreshRow(index, data){
	//table = $(tableId).dataTable();
    var table = oTable;
	var row = table.row(index); 
	
 	row.data(data);
    row.invalidate();
    table.draw(false);
    dataSets = table.ajax.json();
}

function refreshTable()
{
   	table = $('#main-list').dataTable();
   	datas = dataSets
    //var table = oTable;
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<datas.data.length; i++)
    {
      table.oApi._fnAddData(oSettings, datas.data[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
}

function searchData(list, key, value){

	var data = null;
	var found = list.find(function(element) {
		  return element[key] == value;
	});
	return found;
}

function searchDataIndex(list, key, value){
	var itm = null;

	if (value == null){
		itm = key;
	}
	else{
		itm = searchData(list, key, value);
	}
		
	if (itm != null){
		for (var i=0; i < list.length; i++){
			if(itm == list[i]) return i;
		}
	}
	return null;
}

function showCategoryDialog(itm)
{
	var rowTR = $(itm).parents('tr');
    var d = $('#main-list').DataTable().row( rowTR ).data();
    
	$('#changeCategory').attr('data-id', d.id)
	disableCategoryPost = true;

	$('#changeCategory .category-item[data-id-raw="9"]').attr('data-d');
	
	$('#changeCategory input').prop('checked',false).iCheck('update');
	if (d.category_ids != null && d.category_ids != ""){
    	var ids = d.category_ids.split(",");
    	for (var i = 0; i < ids.length; i++){
    		var hasdId = $('#changeCategory .category-item[data-id-raw="'+ids[i]+'"]').data('id');
    		$('#changeCategory .category-item input[value="'+hasdId+'"]').prop('checked',true).iCheck('update');
    	} 
	}
	disableCategoryPost = false;
	$('#changeCategory').modal('show');
}

function onSelectProductRow(itm){
	if($(itm).hasClass('set-category')){
		return;
	} 
	
	var rowTR = $(itm).parents('tr');
	rowTR.toggleClass("selected").siblings(".selected").removeClass("selected");
    var d = rowTR.parents('.dataTable ').DataTable().row( rowTR ).data();
    
	$('#box-area').load(getProdUrl(d.id));
}

function createModel(id, title, clickAction){
	clickAction = clickAction?clickAction:"";
	id = id?id:"";
	title = title?title:"";
	
	return '<div id="'+id+'" class="modal fade" tabindex="-1" role="dialog"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">'+title+'</h4></div><div class="modal-body"></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button><button type="button" onclick="'+clickAction+'" class="btn btn-primary">Apply</button></div></div></div></div>';
}

function showReferenceLink(id){
	var data = searchData(dataSets.data, 'id', id);

	if (!data){
		return;
	}
	
	var filtered = new Array();

	$.each(dataSets.data, function (index, dataObj){
		if (dataObj.status != 3 && 
			dataObj.sku != data.sku &&
			dataObj.description.indexOf("REFURBISHED") == -1){
			filtered.push(dataObj);
		}
	});
	
	var dialog = $('#product_select_dialog');
	if (dialog.length == 0){
		$('body').append(createModel('product_select_dialog', 'Select a product to link to this item', 'applyReference(this)'));
		dialog = $('#product_select_dialog');
		dialog.addClass('product');
	}
	
	$('#product_select_dialog').attr('data-id', data.id);

	if ($('#product_select_dialog table.dataTable').length > 0){
		$('#product_select_dialog table.dataTable').DataTable().destroy();
		$('#product_select_dialog table.dataTable').remove();
	}

	dialog.find('.modal-body').html('<table class="table table-responsive table-striped table-bordered" id="prod_select"></table>');

    $('#product_select_dialog').modal('show');

	var prodTable = $('#prod_select').DataTable({
		data: filtered,
		initComplete: function () {
			$('#prod_select_filter input').val(data.sku.substr(2, 7));
			$('#prod_select_filter input').trigger('keyup');
			
			if ($('#prod_select tbody td.dataTables_empty').length == 1){
				$('#prod_select_filter input').val('');
				$('#prod_select_filter input').trigger('keyup');
			}
		},
        columns: [
        	{ title: "Status", "data": "status" },
        	{ title: "SKU", "data": "sku" },
            { title: "Description", "data": "description" },
        ],
        "columnDefs": [
			{ "width": "30px", "targets": [0] },
        	{
        		"targets": [0,1],
        		"createdCell": function (td, cellData, rowData, row, col) {
            		
        			$(td).addClass('text-center');
        		}
        	},
        	{
            	"render": function ( data, type, row ) {
						switch(parseInt(data)){
						case 1:
							return '<span class="hide">1</span><i class="icon-unpublish" aria-hidden="true"></i>';
						case 2:
							return '<span class="hide">2</span><i class="icon-published" aria-hidden="true"></i>';
						case 3:
							return '<span class="hide">3</span><i class="icon-configure" aria-hidden="true"></i>';
							
						}
                },"targets": 0
            },
		]
	});
	
	$('#prod_select tbody').on( 'click', 'tr', function () {
		var rowTR = $(this);
		rowTR.toggleClass("active").siblings(".active").removeClass("active");
		var d = rowTR.parents('.dataTable').DataTable().row( rowTR ).data();
	});
}

function applyReference(itm){
	var selProdRow = $('#prod_select tbody tr.active');
	if ($('#prod_select tbody tr.active').length > 0)
	{
		var d = selProdRow.parents('.dataTable').DataTable().row( selProdRow ).data();
		setProductType(itm, 'reference', {product_id : d.id});
		
		$('#product_select_dialog').modal('hide');
	}
}

function setProductType(itm, type, data){
	if(!data){
		data = {};
	}
	
	$.post(getProdUrl("/type/"+type, itm), 
		data,
		function(response){
			updateTableData(response.product_id, 'status', response.status);
			$('#box-area').html(response.view);
		}, 'json'
	);
}

function deleteProd(id){
	$.post(getProdUrl(id), 
		{_method:"DELETE"},
		function(response){
			$('#entry-right').html('');
			$('#entry-left .box').removeClass('box-warning').addClass('box-default');
			$('#entry-left .box').find('button').prop('disabled', true);
    	    $('#entry-left .box').find('button.btn-delete').hide();
			$('#entry-left .box').find('button.btn-restore').prop('disabled', false).show();
    	}, 'json'
	);
}

function restoreProd(id){
	$.post(getProdUrl(id+'/restore'), 
		{_method:"PUT"},
		function(response){
			$('#entry-right').html('');
			$('#entry-left .box').removeClass('box-default').addClass('box-warning');
			$('#entry-left .box').find('button').prop('disabled', false);
			$('#entry-left .box').find('button.btn-delete').show();
			$('#entry-left .box').find('button.btn-restore').prop('disabled', true).hide();
    	}, 'json'
	);
}

function getTrashProds(itm){
	$(itm).toggleClass('active');
	oTable.ajax.reload();
}

function configTable(){
	return {
        "ajax": {
        	url: getProdUrl(),
        	"data": function ( d ) {
				d.deleted = $('.trash-filter.active').length;
			}
        },

		'processing': true,
        initComplete: function () {
			$('#product-list_filter input').val('{{$sku}}');
			$('#product-list_filter input').trigger('keyup');
		},
		
        "order": [[ 0, "asc" ]], // "desc" 
        "columns": [
            { title: "ID", "data": "product_id" },
    		{ title: "Status", "data": "status" },
    		{ title: "SKU", "data": "sku" },
            { title: "Product", "data": "description" },
            { title: "Category", "data": "category_ids" },
            { title: "Available", "data": "available" },
            { title: "Ordered", "data": "ordered" },
        ],
        "columnDefs": [
        	{ "width": "20px", "targets": 0 },
			{ "width": "30px", "targets": [1] },
			{ "width": "80px", "targets": [2] },
        	{
        		"targets": [0,1],
        		"createdCell": function (td, cellData, rowData, row, col) {
            		
        			$(td).addClass('text-center');
        		}
        	},
        	{
            	"render": function ( data, type, row ) {
						switch(parseInt(data)){
						case 1:
							return '<span class="hide">1</span><i class="icon-unpublish" aria-hidden="true"></i>';
						case 2:
							return '<span class="hide">2</span><i class="icon-published" aria-hidden="true"></i>';
						case 3:
							return '<span class="hide">3</span><i class="icon-configure" aria-hidden="true"></i>';
							
						}
                },"targets": 1
            },

        	{
            	"class": "set-category",
            	"render": function ( data, type, row ) {
                	var cateNames = " &nbsp;";
                	if (data == null || data==""){
                		
                	}else{
                    	var ids = data.split(",");
                    	
                    	var cateNames =  $('#changeCategory .category-item[data-id-raw="'+ ids[0]+'"] > label.shopper-current .category-name' ).text();
                    	if (ids.length > 1){
                        	for (var i = 1; i < ids.length; i++){
    							cateNames += ", "+ $('#changeCategory .category-item[data-id-raw="'+ ids[i]+'"] > label.shopper-current .category-name' ).text(); 
    						} 
    					}
					}
					cateNames = cateNames==''?'&lt;unassigned&gt;':cateNames;
            		return '<a href="#" style="display:block" onclick="showCategoryDialog(this)">'+cateNames+'</a>';
                },"targets": 4
            },
        ]
    };
}