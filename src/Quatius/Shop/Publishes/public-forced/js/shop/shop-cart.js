/**
 * 
 */
 var shopBaseUrl = baseUrl+'/shop/';
 var tempRes = null;
 
 function toggleWishList(itm){
	 var sku = $(itm).parents('[data-sku]').data('sku');
 
	 var menuItm = $('.wish-menu-item').parents('ul');
	 $(document).trigger( "wishlist-updating", [itm]);
	 $.post(shopBaseUrl+"wishlist/toggle",
	 {sku:sku},
	 function(jsonRespose){
		 if (jsonRespose.success){
			 $(document).trigger( "wishlist-updated", [itm, jsonRespose]);
 
			 $('.product-order[data-sku="'+sku+'"]')
				 .removeClass('product-saved')
				 .removeClass('product-unsaved')
				 .addClass(jsonRespose.action=='add'?'product-saved':'product-unsaved');
 
			 if (typeof menuItm.smartmenus == 'function')
			 {
				 try{
					 menuItm.smartmenus('destroy');
					 menuItm.smartmenus();
				 }catch(e){
				 }
			 }
		 }
	 },'json');
 }
 
 function removeProduct(itm){
	 var prodId = '';
	 if (itm!=null){
		 prodId = $(itm).parents('.product-order[data-id]').attr('data-id');
		 //extra = prodId+extra;
	 }
	 var cartItm =  $('.cart-menu-item');
	 cartItm.removeClass('open');
	 
	 $(itm).parents('.order-item').addClass('removing');
	 $(document).trigger( "order-updating", [itm]);
 
	 $.post(getCartUrl('remove'),
		 encodeURIComponent('prod_id')+ '=' +prodId,
		 function(jsonRespose){
			 updateCart(jsonRespose, itm, false);
			 $(itm).parents('.order-item').remove();
			 if ($('.order-table').length > 0 && $('.order-table .order-item').length == 0)
				 window.location.reload();
 
		 },'json');
 
 }
 
 function updateOrderQty(itm, sendIt){
	 var formItm = null;
	 sendIt = sendIt == undefined? true : sendIt;
	 if($(itm).is('form')){
		 formItm = $(itm);
	 }
	 else{
		 formItm = $(itm.form);
		 var min = 1;
		 if ($(itm).attr('min') != undefined){
			 min = parseInt($(itm).attr('min'));
		 }
		 if (min == 0) min = 1;
 
		 if ($(itm).attr('max') != undefined){
			 var stock = parseInt($(itm).attr('max'));
			 var qty = isNaN(parseInt($(itm).val()))? min: parseInt($(itm).val());
 
			 $(itm).val(qty);
 
			 if (qty > stock){
				 $(itm).parent().addClass('has-error');
			 }
			 else{
				 $(itm).parent().removeClass('has-error');
				 if (qty < min){
					 $(itm).val(min);
				 }
			 }
		 }
		 $(document).trigger( "product-qty-updating", [itm]);
	 }
	 
	 if (sendIt)
		 overrideCartProduct(formItm);
 }
 
 function addToCart(itm, qty){
	 var prodId = '';
	 if (itm!=null){
		 prodId = $(itm).parents('.product-order[data-id]').attr('data-id');
		 //extra = prodId+extra;
	 }
	 if (qty==null)
		 qty = 1;
 
	 var cartItm =  $('.cart-menu-item');
	 cartItm.removeClass('open');
 
	 setCartBtnState(itm, 'loading');
	 $(document).trigger( "order-updating", [itm]);
	 
	 $.post(getCartUrl('add/product'),
		 encodeURIComponent('prod_id')+ '=' +prodId
		 +"&"+encodeURIComponent('prod_qty')+ '=' +qty,
		 function(jsonRespose){
			 updateCart(jsonRespose, itm);
		 },'json');
 
 }
 
 function overrideCartProduct(formItm){
	 setCartBtnState(formItm, 'loading');
	 $(document).trigger( "order-updating", [formItm]);
	 $.post(getCartUrl('override/product'),
		 $(formItm).serialize(),
		 function(jsonRespose){
			 updateCart(jsonRespose, formItm, false);
		 },'json');
 }
 
 function setCartBtnState(itm, state){
	 var cartBtn = null;
 
	 if ($(itm).is('button') || $(itm).is('a') )
		  cartBtn = $(itm);
	 else if($(itm).is('form'))
	 {
		 cartBtn = $(itm).find('.btn-addtocart');
	 }
 
	 if (cartBtn)
		  cartBtn.button(state);
 }
 
 function updateCart(jsonRespose, itm, showPopup){
	 var cartItm =  $('.cart-menu-item');
	 var menuItm = cartItm.parents('ul');
 
	 setCartBtnState(itm, 'reset');
 
	 $(document).trigger( "order-updated", [jsonRespose, itm]);
 
	 if (jsonRespose.view){
		 $('.cart-menu-item .dropdown-menu').empty();
		 var newItems = $(jsonRespose.view);
		 $('.cart-menu-item .dropdown-menu').append(newItems.find('li'));
	 }
	 $('.cart-menu-item .order-count').text(jsonRespose.order.total_qty);
 
	 if ($('.order-table').length > 0)
	 {
		 $('.order-sub-total-val .value').text(jsonRespose.order.subtotal);
		 $('.order-shipping-val .value').text(jsonRespose.order.shipping_cost);
		 $('.order-total-val .value').text(jsonRespose.order.total);
		 
		 $.each(jsonRespose.order.products, function(index, prod){
			 var prodRow = $('.order-table .order-item[data-id="'+prod.product_id+'"]');
			 prodRow.find('.each-price .value').text(prod.price);
			 prodRow.find('.total-price .value').text(prod.total);
			 if (prodRow.find('input[name="prod_qty[]"]').length >0)
				 prodRow.find('input[name="prod_qty[]"]').val(prod.qty);
		 });
 
		 if(jsonRespose.order.params.discounts != undefined){
			 $('.discount-list > div').addClass('remove');
			 $.each(jsonRespose.order.params.discounts, function($type, $data){
				 $('.discount-'+$type).removeClass('remove');
				 if ($data.name)
					 $('.discount-'+$type+' .discount-name').html($data.name);
				 if ($data.value)
					 $('.discount-'+$type+' .value').html($data.value);
				 else
					 $('.discount-'+$type+' .value').html('0.00');
 
				 if ($data.description)
					 $('.discount-'+$type+' .discount-description').html($data.description);
 
				 if ($data.warning){
					 $('.discount-'+$type).addClass('discount-warning');
					 $('.discount-'+$type +' .discount-text-warning').html($data.warning);
				 }
				 else {
					 $('.discount-'+$type).removeClass('discount-warning');
					 $('.discount-'+$type +' .discount-text-warning').html("");
				 }
			 });
			 //remove all unupdated
			 $('.discount-list > div.remove').remove();
		 } 
	 }
 
	 if (!cartItm.hasClass('open')){
		 if (typeof menuItm.smartmenus == 'function'){
			 try{
				 menuItm.smartmenus('destroy');
				 menuItm.smartmenus();
				 //menuItm.smartmenus('refresh');
			 }catch(e){
				 //console.log('smartmenus '+e.message);
				 menuItm.smartmenus('destroy');
				 menuItm.smartmenus();
			 }
 
			 if (showPopup == null || showPopup){
				 menuItm.smartmenus('itemActivate', cartItm);
			 }
		 }else{
			 cartItm.addClass('open');	
		 }
	 }
 
	 if (!itm)
		 return;
	 indicateChanges(itm);
	 updateProductView(jsonRespose);
 }
 
 function indicateChanges(btn)
 {
	 var prod_ids = new Array();
	 var jItm = $(btn);
 
	 if (jItm.is("form")){
		 prod_ids.push(jItm.find('input[name="prod_id"]').val());
	 }
	 else{
		 var product = jItm.parents('.product-order');
		 if (product.length > 0)
		 {
			 prod_ids.push(product.attr('data-id'))
		 }
	 }
	 
	 $.each(prod_ids, function(index, id){
		 $('.cart-preview .cart-item[data-id="'+id+'"]').addClass('just-added');
	 });
 }
 
 function updateProductView(orderResponse){
	 if(orderResponse.success){
		 if (orderResponse.action == 'add'){
			 $.each(orderResponse.order.products, function(index, prod){
				 $('.product-order.not-ordered[data-id="'+prod.product_id+'"]').removeClass('not-ordered').addClass('has-ordered');	
			 });
		 }
		 else if (orderResponse.action == 'remove'){
			 return $('.product-order.has-ordered').filter(function(){
				 return searchData(orderResponse.order.products,'product_id',$(this).data('id')) == null;
			 }).removeClass('has-ordered').addClass('not-ordered');
		 }
	 }
 }
 
 function getCartUrl(extra){
	 return shopBaseUrl+'cart/'+extra;
 }
 
 function searchData(list, key, value){
	 var found = list.find(function(element) {
		   return element[key] == value;
	 });
	 return found;
 }
 