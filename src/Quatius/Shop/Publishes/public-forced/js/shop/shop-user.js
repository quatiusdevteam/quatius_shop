/**
 * 
 */
var shopBaseUrl = 'shop/';

function newAddress(type){
	var addr_type = type;

    $.getJSON(shopBaseUrl+'address/create', function (jsonResponse){
    	$('#addressModal .modal-body').html(jsonResponse.view);
    	if ($('#addressModal .modal-body form').length > 0)
    	{
        	$('#addressModal .modal-body input[name="address_type"]').val(addr_type);
    		
    		$('#addressModal .modal-body form').on('submit',function(){
    			$.post(	$('#addressModal .modal-body form').attr('action'),
    	    			$('#addressModal .modal-body form').serialize(),
        				function(result){
    						updateAddressView(result.view);
    					},'json'
    				);
    			$('#addressModal').modal('hide');
    			return false;
    		});
    		
    		$('#addressModal').modal('show');
    	}
    });
}

function removeAddress(itm){
	var jSection = $(itm).parents('.section');
	$.post(	shopBaseUrl+'address/'+jSection.find('[data-id]').attr('data-id'),
			{_method:"DELETE"},
		    	function(result){
					jSection.remove();
				}
			),'json';
}

function updateAddressView(contentHtml){
	var updatedAddress = $(contentHtml);
	var addressId = updatedAddress.attr('data-id');
	var findExisting = $('.section [data-id="'+addressId+'"]');
	
	if(findExisting.length == 1) //update
	{
		findExisting.replaceWith(updatedAddress);
	}
	else //add new
	{
		var divContainer = $('<div class="col-xs-12 section">');
		divContainer.append(updatedAddress);
		if (updatedAddress.attr('data-type')=='BT')
		{
			$('#billing').empty();
			divContainer.prepend('<span class="pull-right small"><button class="btn btn-default btn-xs" onclick="editAddress(this)"><i class="fa fa-edit"></i></button></span>');
			$('#billing').append(divContainer);
			
		}else
		{
			divContainer.prepend('<span class="pull-right small"><button class="btn btn-default btn-xs" onclick="editAddress(this)"><i class="fa fa-edit"></i></button> <button class="btn btn-default btn-xs" onclick="removeAddress(this)"><i class="fa fa-trash"></i></button></span>');
			divContainer.append('<hr style="border:1px dotted #ccc;" />');
			$('#shippings').append(divContainer);
		}
	}
}

function editAddress(itm){
	var jSection = $(itm).parents('.section');
    $.getJSON(shopBaseUrl+'address/'+jSection.find('[data-id]').attr('data-id')+'/edit', function (jsonResponse){
    	$('#addressModal .modal-body').html(jsonResponse.view);
    	if ($('#addressModal .modal-body form').length > 0)
    	{
    		$('#addressModal .modal-body form').on('submit',function(){
    			$.post(	$('#addressModal .modal-body form').attr('action'),
    	    			$('#addressModal .modal-body form').serialize(),
    	    			function(result){
    						updateAddressView(result.view);
						},'json'
					);
    			$('#addressModal').modal('hide');
    			return false;
    		});
    		
    		$('#addressModal').modal('show');
    	}
    });
}