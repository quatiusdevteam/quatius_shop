$(document).ready(function(){
	$( "input.search-filter").keyup(function( event ) {
		if ( event.which == 13 ) {
			event.preventDefault();
		}
		filterSpec(this);
	});
});


function searchItems(itm) {
	var data = getProducts();
	var matchObj = [], index=0;
    
    var jItm = $(itm);
    
    var searchValue = jItm.val();
    
    if (searchValue.trim().length < 2){
    	$(".search-item-result").hide();
    	return;
    }

    for(var i=0; i < data.length; i++) {
    	var keywords = data[i].keyword.toLowerCase();
    	if (findByKeyword(keywords,searchValue)){
    		matchObj.push(data[i]);
    	}
    }
    
    var prodInCate = getMatchInCategory(getRootCategories(), matchObj);
    var disp = jItm.parent().find('.search-item-result');
	disp.html(formatDisplay(matchObj, prodInCate, itm));
	disp.show();
}

function filterSpec(itm){
	var filterSpecs = $('.spec-filter').filter(function() { return $(this).val() != ""; });

	var searchWord = $('.search-filter').val().trim();
	
	if (filterSpecs.length == 0 && searchWord.length < 2)
		$('.listed-prod').show();
	else
	{
		$('.listed-prod')
			.removeClass('filtering')
    		.filter(function(){
    			var keyword = $(this).find('[data-keyword]').data('keyword');
    			if (searchWord.length > 1 && !findByKeyword(keyword, searchWord)){
    				return false;
    			}
    			
    			for (var s=0; s < filterSpecs.length; s++){
    				if ($(this).find('[data-spec-values~="'+$(filterSpecs.get(s)).val()+'"]').length ==0){
    					return false;
    				}
    			}
    			return true;
    		})
    		.addClass('filtering');
    		
		$('.listed-prod.filtering:hidden').show();
		$('.listed-prod:visible:not(.filtering)').hide();
	}
}

function findByKeyword(keywords, search){
	if (search.trim().length < 2){
    	return false;
    }
    var words = search.toLowerCase().trim().split(' ');
	keywords = keywords.toLowerCase();
	for (var word of words) {
		if (word.trim().length < 2) continue;
		
		if(keywords.indexOf(word.trim()) != -1){
			return true;
		}
	}
    
    return false;
}

function formatDisplay(matchObj, prodInCate, itm){

	var formObj = $(itm).parents('form');
	var content = $('<ul>');
	var item = null;
	
	for(var cate of prodInCate){
		if (cate.products.length == 0) continue;
		
		item = $('<li>');
		item.append($('<a>')
			.attr('href', baseUrl + "/"+ cate.url+"?"+formObj.serialize())
			.append($('<span class="badge">').text(cate.products.length))
			.append($('<span class="cate-name">').text(' '+cate.name))
		);
		content.append(item);
	}

	item = $('<li class="search-found">');
	item.append($('<a>')
		.attr('href', $(itm).parents('form').attr('action')+'?'+formObj.serialize())
		.text(matchObj.length+' item(s) found')
	);
	content.append(item);
	return content;
}

function getMatchInCategory(cates, matchList){
	
	var catesCopy = eval(JSON.stringify(cates));
	for (var cate of catesCopy) {
		cate.subIdList = cate.subs.split(',');
		cate.products = new Array();
	}

	for(var prod of matchList){
		var cIds = prod.category_ids==null?[]:prod.category_ids.split(',');

		for (var cate of catesCopy) {
			for (var cId of cIds){
				if (cate.subIdList.indexOf(cId) != -1){
					cate.products.push(prod);
					break;
				}
			}
		}
	}
	return catesCopy;
}

function getProducts(){
	return searchList.products;
}

function getCategories(){
	return searchList.categories;
}

function getRootCategories(){
	var root = new Array();
	var cates = getCategories();
	for (var c in cates) {
		if (cates[c].level != 0) continue;
		
		root.push(cates[c]);
	}
	return root;
}