<?php 

return [
    'payment-method'=>'Payment Methods',
    'payment-gateway'=>'Select A Payment Method',
    'payment-name'=>'Name',
    'payment-type'=>'Type',
    'payment-mode' => 'Mode',
    'payment-live-credentials' => 'Live Credentials',
    'payment-sandbox-credentials' => 'Sandbox Credentials',
    'payment-params' => 'Params',
    'payment-notes' => 'Notes',
    'payment-status' => 'Status',
    'payment-name' => 'Payment Method',
    'payment-manage' => 'Manage Payment Method',
    'buttons'=>[
        'empty-cart' => 'Clear Cart',
        'yes' =>'Yes',
        'no' =>'No',
        'checkout' =>'CHECKOUT',
        'confirm' =>'CONFIRM'
    ],
    'status' => 
		[
			'Cancelled'=>'Cancelled',
			'pending' => 'Pending', 
			'confirmed'=>'Confirmed', 
			'processing' => 'Processing In Pronto', 
			'refunded'=>'Refunded', 
			'refunded - partial' => 'Refunded Partially', 
			'picked'=>'Picked', 
			'Forward Order' => 'Forward Order',
			'In Progress' => 'In Progress', 
			'Held'=>'Forward Order', 
			'delivered'=>'Delivered', 
			'Finished'=>'Completed',
			'template'=>'Template'
		],

    'order'                  => [
    	'options'     => [
            'status'  => ['male' => 'Male', 'female' => 'Female', 'other' => 'Other']
		],
		
		'label' => [
			'order_number' => 'Order Number:'
		]
    ],
    'courier'   => [
        'LJW'   => 'https://auspost.com.au/parcels-mail/track.html#/track?id=',
    ]
];
