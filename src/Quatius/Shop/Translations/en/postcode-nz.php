<?php 

return [
  [
    "postcode"=> 110,
    "name"=> "Woodhill",
    "lat"=> -35.749,
    "lng"=> 174.327
  ],
  [
    "postcode"=> 112,
    "name"=> "Whau Valley",
    "lat"=> -35.693,
    "lng"=> 174.3001
  ],
  [
    "postcode"=> 114,
    "name"=> "Waro",
    "lat"=> -35.5909,
    "lng"=> 174.2815
  ],
  [
    "postcode"=> 116,
    "name"=> "Ruakaka",
    "lat"=> -35.892,
    "lng"=> 174.4644
  ],
  [
    "postcode"=> 140,
    "name"=> "Riverside",
    "lat"=> -35.7245,
    "lng"=> 174.3213
  ],
  [
    "postcode"=> 141,
    "name"=> "Otangarei",
    "lat"=> -35.6834,
    "lng"=> 174.3008
  ],
  [
    "postcode"=> 142,
    "name"=> "Sherwood Rise",
    "lat"=> -35.7546,
    "lng"=> 174.3681
  ],
  [
    "postcode"=> 143,
    "name"=> "Morningside",
    "lat"=> -35.7343,
    "lng"=> 174.3054
  ],
  [
    "postcode"=> 144,
    "name"=> "Tikipunga",
    "lat"=> -35.6866,
    "lng"=> 174.3266
  ],
  [
    "postcode"=> 145,
    "name"=> "Kensington",
    "lat"=> -35.7085,
    "lng"=> 174.3207
  ],
  [
    "postcode"=> 146,
    "name"=> "Maungakaramea",
    "lat"=> -35.8451,
    "lng"=> 174.2048
  ],
  [
    "postcode"=> 147,
    "name"=> "Woodhill",
    "lat"=> -35.7429,
    "lng"=> 174.308
  ],
  [
    "postcode"=> 148,
    "name"=> "Woodhill",
    "lat"=> -35.7512,
    "lng"=> 174.3152
  ],
  [
    "postcode"=> 149,
    "name"=> "Riverside",
    "lat"=> -35.7247,
    "lng"=> 174.3259
  ],
  [
    "postcode"=> 150,
    "name"=> "Hikurangi",
    "lat"=> -35.5979,
    "lng"=> 174.2863
  ],
  [
    "postcode"=> 151,
    "name"=> "Ruakaka",
    "lat"=> -35.8751,
    "lng"=> 174.4554
  ],
  [
    "postcode"=> 152,
    "name"=> "Maungatapere",
    "lat"=> -35.7549,
    "lng"=> 174.2081
  ],
  [
    "postcode"=> 153,
    "name"=> "Tutukaka",
    "lat"=> -35.6129,
    "lng"=> 174.522
  ],
  [
    "postcode"=> 154,
    "name"=> "Ngunguru",
    "lat"=> -35.6297,
    "lng"=> 174.5041
  ],
  [
    "postcode"=> 170,
    "name"=> "Puwera",
    "lat"=> -35.7762,
    "lng"=> 174.268
  ],
  [
    "postcode"=> 171,
    "name"=> "Mata",
    "lat"=> -35.8556,
    "lng"=> 174.3925
  ],
  [
    "postcode"=> 172,
    "name"=> "Parakao",
    "lat"=> -35.7039,
    "lng"=> 173.9353
  ],
  [
    "postcode"=> 173,
    "name"=> "Kiripaka",
    "lat"=> -35.6147,
    "lng"=> 174.4391
  ],
  [
    "postcode"=> 174,
    "name"=> "Whangarei Heads",
    "lat"=> -35.8069,
    "lng"=> 174.4823
  ],
  [
    "postcode"=> 175,
    "name"=> "Tahere",
    "lat"=> -35.6943,
    "lng"=> 174.4312
  ],
  [
    "postcode"=> 176,
    "name"=> "Moengawahine",
    "lat"=> -35.6306,
    "lng"=> 174.1057
  ],
  [
    "postcode"=> 178,
    "name"=> "Maungakaramea",
    "lat"=> -35.852,
    "lng"=> 174.2236
  ],
  [
    "postcode"=> 179,
    "name"=> "Maungatapere",
    "lat"=> -35.7562,
    "lng"=> 174.1728
  ],
  [
    "postcode"=> 181,
    "name"=> "Marua",
    "lat"=> -35.5606,
    "lng"=> 174.3839
  ],
  [
    "postcode"=> 182,
    "name"=> "Hukerenui",
    "lat"=> -35.4967,
    "lng"=> 174.1879
  ],
  [
    "postcode"=> 184,
    "name"=> "Whangaruru North",
    "lat"=> -35.3593,
    "lng"=> 174.3023
  ],
  [
    "postcode"=> 185,
    "name"=> "Ruatangata East",
    "lat"=> -35.6304,
    "lng"=> 174.216
  ],
  [
    "postcode"=> 192,
    "name"=> "Rukuwai",
    "lat"=> -35.7429,
    "lng"=> 174.4705
  ],
  [
    "postcode"=> 193,
    "name"=> "Waiotira",
    "lat"=> -35.9554,
    "lng"=> 174.196
  ],
  [
    "postcode"=> 200,
    "name"=> "Opua",
    "lat"=> -35.3073,
    "lng"=> 174.0975
  ],
  [
    "postcode"=> 202,
    "name"=> "Russell",
    "lat"=> -35.2557,
    "lng"=> 174.1233
  ],
  [
    "postcode"=> 210,
    "name"=> "Kawakawa",
    "lat"=> -35.3838,
    "lng"=> 174.0668
  ],
  [
    "postcode"=> 211,
    "name"=> "Moerewa",
    "lat"=> -35.3867,
    "lng"=> 174.0249
  ],
  [
    "postcode"=> 230,
    "name"=> "Kerikeri",
    "lat"=> -35.2336,
    "lng"=> 173.9593
  ],
  [
    "postcode"=> 241,
    "name"=> "Opua",
    "lat"=> -35.3124,
    "lng"=> 174.1206
  ],
  [
    "postcode"=> 242,
    "name"=> "Russell",
    "lat"=> -35.2628,
    "lng"=> 174.1228
  ],
  [
    "postcode"=> 243,
    "name"=> "Kawakawa",
    "lat"=> -35.3799,
    "lng"=> 174.0672
  ],
  [
    "postcode"=> 244,
    "name"=> "Moerewa",
    "lat"=> -35.3875,
    "lng"=> 174.0144
  ],
  [
    "postcode"=> 245,
    "name"=> "Kerikeri",
    "lat"=> -35.2289,
    "lng"=> 173.9487
  ],
  [
    "postcode"=> 246,
    "name"=> "Waipapa",
    "lat"=> -35.206,
    "lng"=> 173.9167
  ],
  [
    "postcode"=> 247,
    "name"=> "Paihia",
    "lat"=> -35.282,
    "lng"=> 174.0915
  ],
  [
    "postcode"=> 271,
    "name"=> "Haruru",
    "lat"=> -35.2851,
    "lng"=> 174.0397
  ],
  [
    "postcode"=> 272,
    "name"=> "Manawaora",
    "lat"=> -35.2844,
    "lng"=> 174.1759
  ],
  [
    "postcode"=> 281,
    "name"=> "Pokere",
    "lat"=> -35.4904,
    "lng"=> 174.0032
  ],
  [
    "postcode"=> 282,
    "name"=> "Karetu",
    "lat"=> -35.375,
    "lng"=> 174.1364
  ],
  [
    "postcode"=> 283,
    "name"=> "Waihaha",
    "lat"=> -35.3488,
    "lng"=> 174.1925
  ],
  [
    "postcode"=> 293,
    "name"=> "Puketona",
    "lat"=> -35.2621,
    "lng"=> 173.9724
  ],
  [
    "postcode"=> 294,
    "name"=> "Purerua",
    "lat"=> -35.1578,
    "lng"=> 174.0225
  ],
  [
    "postcode"=> 295,
    "name"=> "Waipapa",
    "lat"=> -35.1769,
    "lng"=> 173.876
  ],
  [
    "postcode"=> 310,
    "name"=> "Dargaville",
    "lat"=> -35.9542,
    "lng"=> 173.8785
  ],
  [
    "postcode"=> 340,
    "name"=> "Dargaville",
    "lat"=> -35.9413,
    "lng"=> 173.8697
  ],
  [
    "postcode"=> 341,
    "name"=> "Te Kopura",
    "lat"=> -36.0309,
    "lng"=> 173.923
  ],
  [
    "postcode"=> 370,
    "name"=> "Arapohue",
    "lat"=> -36.0004,
    "lng"=> 174.0199
  ],
  [
    "postcode"=> 371,
    "name"=> "Redhill",
    "lat"=> -36.0523,
    "lng"=> 173.8602
  ],
  [
    "postcode"=> 372,
    "name"=> "Kairara",
    "lat"=> -35.8194,
    "lng"=> 173.8585
  ],
  [
    "postcode"=> 373,
    "name"=> "Mamaranui",
    "lat"=> -35.8591,
    "lng"=> 173.7106
  ],
  [
    "postcode"=> 374,
    "name"=> "Arapohue",
    "lat"=> -35.9673,
    "lng"=> 173.9916
  ],
  [
    "postcode"=> 376,
    "name"=> "Tutamoe",
    "lat"=> -35.6467,
    "lng"=> 173.6147
  ],
  [
    "postcode"=> 377,
    "name"=> "Baylys Beach",
    "lat"=> -35.9516,
    "lng"=> 173.7818
  ],
  [
    "postcode"=> 379,
    "name"=> "Whatoro",
    "lat"=> -35.7555,
    "lng"=> 173.6729
  ],
  [
    "postcode"=> 381,
    "name"=> "Kirikopuni",
    "lat"=> -35.8384,
    "lng"=> 174.0082
  ],
  [
    "postcode"=> 391,
    "name"=> "Tangaihe",
    "lat"=> -36.2126,
    "lng"=> 174.038
  ],
  [
    "postcode"=> 405,
    "name"=> "Kaikohe",
    "lat"=> -35.401,
    "lng"=> 173.7896
  ],
  [
    "postcode"=> 410,
    "name"=> "Kaitaia",
    "lat"=> -35.1134,
    "lng"=> 173.2502
  ],
  [
    "postcode"=> 420,
    "name"=> "Taipa",
    "lat"=> -35.0008,
    "lng"=> 173.4958
  ],
  [
    "postcode"=> 440,
    "name"=> "Kaikohe",
    "lat"=> -35.4081,
    "lng"=> 173.7986
  ],
  [
    "postcode"=> 441,
    "name"=> "Kaitaia",
    "lat"=> -35.114,
    "lng"=> 173.2637
  ],
  [
    "postcode"=> 442,
    "name"=> "Mangonui",
    "lat"=> -34.9896,
    "lng"=> 173.5366
  ],
  [
    "postcode"=> 443,
    "name"=> "Rawene",
    "lat"=> -35.3932,
    "lng"=> 173.5056
  ],
  [
    "postcode"=> 444,
    "name"=> "Omapere",
    "lat"=> -35.5367,
    "lng"=> 173.3862
  ],
  [
    "postcode"=> 445,
    "name"=> "Opononi",
    "lat"=> -35.5046,
    "lng"=> 173.3924
  ],
  [
    "postcode"=> 446,
    "name"=> "Waimamaku",
    "lat"=> -35.556,
    "lng"=> 173.4759
  ],
  [
    "postcode"=> 447,
    "name"=> "Okaihau",
    "lat"=> -35.3201,
    "lng"=> 173.7713
  ],
  [
    "postcode"=> 448,
    "name"=> "Kaeo",
    "lat"=> -35.1006,
    "lng"=> 173.7805
  ],
  [
    "postcode"=> 449,
    "name"=> "Ahipara",
    "lat"=> -35.171,
    "lng"=> 173.1534
  ],
  [
    "postcode"=> 451,
    "name"=> "Awanui",
    "lat"=> -35.048,
    "lng"=> 173.2565
  ],
  [
    "postcode"=> 453,
    "name"=> "Kohukohu",
    "lat"=> -35.3582,
    "lng"=> 173.5461
  ],
  [
    "postcode"=> 460,
    "name"=> "Pukenui",
    "lat"=> -34.8149,
    "lng"=> 173.1178
  ],
  [
    "postcode"=> 462,
    "name"=> "Broadwood",
    "lat"=> -35.2592,
    "lng"=> 173.3896
  ],
  [
    "postcode"=> 472,
    "name"=> "Pakaraka",
    "lat"=> -35.3618,
    "lng"=> 173.9337
  ],
  [
    "postcode"=> 473,
    "name"=> "Waima",
    "lat"=> -35.4963,
    "lng"=> 173.5549
  ],
  [
    "postcode"=> 474,
    "name"=> "Tautoro",
    "lat"=> -35.5071,
    "lng"=> 173.8144
  ],
  [
    "postcode"=> 475,
    "name"=> "Marangai",
    "lat"=> -35.3278,
    "lng"=> 173.6987
  ],
  [
    "postcode"=> 476,
    "name"=> "Umawera",
    "lat"=> -35.2385,
    "lng"=> 173.6264
  ],
  [
    "postcode"=> 478,
    "name"=> "Kaeo",
    "lat"=> -35.0974,
    "lng"=> 173.7895
  ],
  [
    "postcode"=> 479,
    "name"=> "Saies",
    "lat"=> -35.0791,
    "lng"=> 173.6798
  ],
  [
    "postcode"=> 481,
    "name"=> "Manukau",
    "lat"=> -35.2366,
    "lng"=> 173.2382
  ],
  [
    "postcode"=> 482,
    "name"=> "Kaiaka",
    "lat"=> -35.1122,
    "lng"=> 173.4133
  ],
  [
    "postcode"=> 483,
    "name"=> "Lake Ohia",
    "lat"=> -34.9593,
    "lng"=> 173.3595
  ],
  [
    "postcode"=> 484,
    "name"=> "Te Kao",
    "lat"=> -34.6874,
    "lng"=> 172.924
  ],
  [
    "postcode"=> 486,
    "name"=> "Waiharera",
    "lat"=> -34.9757,
    "lng"=> 173.2179
  ],
  [
    "postcode"=> 491,
    "name"=> "Broadwood",
    "lat"=> -35.2764,
    "lng"=> 173.4408
  ],
  [
    "postcode"=> 492,
    "name"=> "Panguru",
    "lat"=> -35.4197,
    "lng"=> 173.3623
  ],
  [
    "postcode"=> 494,
    "name"=> "Paraoanui",
    "lat"=> -35.0088,
    "lng"=> 173.621
  ],
  [
    "postcode"=> 496,
    "name"=> "Pawarenga",
    "lat"=> -35.3437,
    "lng"=> 173.3046
  ],
  [
    "postcode"=> 505,
    "name"=> "Mangawhai Heads",
    "lat"=> -36.0858,
    "lng"=> 174.5842
  ],
  [
    "postcode"=> 510,
    "name"=> "Waipu",
    "lat"=> -35.9822,
    "lng"=> 174.4508
  ],
  [
    "postcode"=> 520,
    "name"=> "Rowsells",
    "lat"=> -36.1077,
    "lng"=> 174.3568
  ],
  [
    "postcode"=> 530,
    "name"=> "Ruawai",
    "lat"=> -36.1322,
    "lng"=> 174.0277
  ],
  [
    "postcode"=> 540,
    "name"=> "Mangawhai",
    "lat"=> -36.1258,
    "lng"=> 174.5746
  ],
  [
    "postcode"=> 541,
    "name"=> "Mangawhai Heads",
    "lat"=> -36.0946,
    "lng"=> 174.5874
  ],
  [
    "postcode"=> 542,
    "name"=> "Kaiwaka",
    "lat"=> -36.1642,
    "lng"=> 174.4428
  ],
  [
    "postcode"=> 543,
    "name"=> "Paparoa",
    "lat"=> -36.103,
    "lng"=> 174.2315
  ],
  [
    "postcode"=> 545,
    "name"=> "Waipu",
    "lat"=> -35.9842,
    "lng"=> 174.4453
  ],
  [
    "postcode"=> 547,
    "name"=> "Rowsells",
    "lat"=> -36.1102,
    "lng"=> 174.3537
  ],
  [
    "postcode"=> 549,
    "name"=> "Ruawai",
    "lat"=> -36.1365,
    "lng"=> 174.0233
  ],
  [
    "postcode"=> 550,
    "name"=> "Matakohe",
    "lat"=> -36.1298,
    "lng"=> 174.1825
  ],
  [
    "postcode"=> 571,
    "name"=> "Mareretu",
    "lat"=> -36.053,
    "lng"=> 174.2547
  ],
  [
    "postcode"=> 573,
    "name"=> "Kaiwaka",
    "lat"=> -36.1881,
    "lng"=> 174.429
  ],
  [
    "postcode"=> 582,
    "name"=> "Waipu",
    "lat"=> -36.0052,
    "lng"=> 174.4499
  ],
  [
    "postcode"=> 583,
    "name"=> "Marohemo",
    "lat"=> -36.1641,
    "lng"=> 174.2892
  ],
  [
    "postcode"=> 587,
    "name"=> "Ranganui",
    "lat"=> -36.1441,
    "lng"=> 174.4094
  ],
  [
    "postcode"=> 588,
    "name"=> "Maungaturoto",
    "lat"=> -36.0723,
    "lng"=> 174.3737
  ],
  [
    "postcode"=> 589,
    "name"=> "Huarau",
    "lat"=> -36.0601,
    "lng"=> 174.3167
  ],
  [
    "postcode"=> 591,
    "name"=> "Ruawai",
    "lat"=> -36.1195,
    "lng"=> 174.0709
  ],
  [
    "postcode"=> 592,
    "name"=> "Rehia",
    "lat"=> -36.0549,
    "lng"=> 174.0388
  ],
  [
    "postcode"=> 593,
    "name"=> "Pahi",
    "lat"=> -36.1962,
    "lng"=> 174.2067
  ],
  [
    "postcode"=> 594,
    "name"=> "Matakohe",
    "lat"=> -36.0672,
    "lng"=> 174.1446
  ],
  [
    "postcode"=> 600,
    "name"=> "Blockhouse Bay",
    "lat"=> -36.9158,
    "lng"=> 174.6922
  ],
  [
    "postcode"=> 602,
    "name"=> "Kelston",
    "lat"=> -36.9042,
    "lng"=> 174.6474
  ],
  [
    "postcode"=> 604,
    "name"=> "Huia",
    "lat"=> -36.9808,
    "lng"=> 174.5827
  ],
  [
    "postcode"=> 610,
    "name"=> "Te Atatu South",
    "lat"=> -36.8531,
    "lng"=> 174.6466
  ],
  [
    "postcode"=> 612,
    "name"=> "McLaren Park",
    "lat"=> -36.8988,
    "lng"=> 174.5918
  ],
  [
    "postcode"=> 614,
    "name"=> "Swanson",
    "lat"=> -36.8546,
    "lng"=> 174.584
  ],
  [
    "postcode"=> 618,
    "name"=> "Hobsonville",
    "lat"=> -36.7987,
    "lng"=> 174.6378
  ],
  [
    "postcode"=> 620,
    "name"=> "Forrest Hill",
    "lat"=> -36.7661,
    "lng"=> 174.7548
  ],
  [
    "postcode"=> 622,
    "name"=> "Belmont",
    "lat"=> -36.801,
    "lng"=> 174.778
  ],
  [
    "postcode"=> 624,
    "name"=> "Devonport",
    "lat"=> -36.8236,
    "lng"=> 174.7918
  ],
  [
    "postcode"=> 626,
    "name"=> "Chatswood",
    "lat"=> -36.8058,
    "lng"=> 174.7083
  ],
  [
    "postcode"=> 627,
    "name"=> "North Shore",
    "lat"=> -36.798,
    "lng"=> 174.7462
  ],
  [
    "postcode"=> 629,
    "name"=> "Marlborough",
    "lat"=> -36.7753,
    "lng"=> 174.7199
  ],
  [
    "postcode"=> 630,
    "name"=> "Browns Bay",
    "lat"=> -36.7215,
    "lng"=> 174.7447
  ],
  [
    "postcode"=> 632,
    "name"=> "Greenhithe",
    "lat"=> -36.7443,
    "lng"=> 174.6948
  ],
  [
    "postcode"=> 640,
    "name"=> "New Lynn",
    "lat"=> -36.9079,
    "lng"=> 174.6831
  ],
  [
    "postcode"=> 641,
    "name"=> "Glen Eden",
    "lat"=> -36.9118,
    "lng"=> 174.652
  ],
  [
    "postcode"=> 642,
    "name"=> "Titirangi",
    "lat"=> -36.9378,
    "lng"=> 174.6575
  ],
  [
    "postcode"=> 643,
    "name"=> "Green Bay",
    "lat"=> -36.9306,
    "lng"=> 174.6801
  ],
  [
    "postcode"=> 644,
    "name"=> "Blockhouse Bay",
    "lat"=> -36.9234,
    "lng"=> 174.7015
  ],
  [
    "postcode"=> 645,
    "name"=> "Glendene",
    "lat"=> -36.8869,
    "lng"=> 174.6446
  ],
  [
    "postcode"=> 650,
    "name"=> "Sunnyvale",
    "lat"=> -36.881,
    "lng"=> 174.6347
  ],
  [
    "postcode"=> 651,
    "name"=> "Te Atatu",
    "lat"=> -36.8422,
    "lng"=> 174.6515
  ],
  [
    "postcode"=> 652,
    "name"=> "Te Atatu South",
    "lat"=> -36.865,
    "lng"=> 174.6463
  ],
  [
    "postcode"=> 653,
    "name"=> "Swanson",
    "lat"=> -36.8655,
    "lng"=> 174.58
  ],
  [
    "postcode"=> 654,
    "name"=> "Te Atatu South",
    "lat"=> -36.8557,
    "lng"=> 174.6298
  ],
  [
    "postcode"=> 655,
    "name"=> "Swanson",
    "lat"=> -36.8639,
    "lng"=> 174.6011
  ],
  [
    "postcode"=> 656,
    "name"=> "Massey East",
    "lat"=> -36.8332,
    "lng"=> 174.6224
  ],
  [
    "postcode"=> 657,
    "name"=> "West Harbour",
    "lat"=> -36.8222,
    "lng"=> 174.6105
  ],
  [
    "postcode"=> 660,
    "name"=> "Waitakere",
    "lat"=> -36.8492,
    "lng"=> 174.5447
  ],
  [
    "postcode"=> 661,
    "name"=> "Te Atatu Peninsula",
    "lat"=> -36.8107,
    "lng"=> 174.6446
  ],
  [
    "postcode"=> 662,
    "name"=> "Brighams Creek",
    "lat"=> -36.7935,
    "lng"=> 174.6163
  ],
  [
    "postcode"=> 663,
    "name"=> "Massey East",
    "lat"=> -36.8185,
    "lng"=> 174.6292
  ],
  [
    "postcode"=> 740,
    "name"=> "Hauraki",
    "lat"=> -36.7875,
    "lng"=> 174.7729
  ],
  [
    "postcode"=> 741,
    "name"=> "Milford",
    "lat"=> -36.7728,
    "lng"=> 174.7661
  ],
  [
    "postcode"=> 742,
    "name"=> "Torbay",
    "lat"=> -36.6949,
    "lng"=> 174.7506
  ],
  [
    "postcode"=> 743,
    "name"=> "Forrest Hill",
    "lat"=> -36.7512,
    "lng"=> 174.7467
  ],
  [
    "postcode"=> 744,
    "name"=> "Devenport",
    "lat"=> -36.8303,
    "lng"=> 174.7969
  ],
  [
    "postcode"=> 745,
    "name"=> "Glenfield",
    "lat"=> -36.768,
    "lng"=> 174.7395
  ],
  [
    "postcode"=> 746,
    "name"=> "Birkenhead",
    "lat"=> -36.8123,
    "lng"=> 174.7254
  ],
  [
    "postcode"=> 747,
    "name"=> "Marlborough",
    "lat"=> -36.782,
    "lng"=> 174.7216
  ],
  [
    "postcode"=> 748,
    "name"=> "North Shore",
    "lat"=> -36.8017,
    "lng"=> 174.7446
  ],
  [
    "postcode"=> 749,
    "name"=> "Birkdale",
    "lat"=> -36.7925,
    "lng"=> 174.6872
  ],
  [
    "postcode"=> 750,
    "name"=> "Takapuna",
    "lat"=> -36.7969,
    "lng"=> 174.7782
  ],
  [
    "postcode"=> 751,
    "name"=> "Glenfield",
    "lat"=> -36.7513,
    "lng"=> 174.7139
  ],
  [
    "postcode"=> 752,
    "name"=> "Albany",
    "lat"=> -36.7313,
    "lng"=> 174.7075
  ],
  [
    "postcode"=> 753,
    "name"=> "Browns Bay",
    "lat"=> -36.7155,
    "lng"=> 174.7484
  ],
  [
    "postcode"=> 754,
    "name"=> "Mairangi Bay",
    "lat"=> -36.7384,
    "lng"=> 174.7534
  ],
  [
    "postcode"=> 755,
    "name"=> "Albany",
    "lat"=> -36.7246,
    "lng"=> 174.6952
  ],
  [
    "postcode"=> 756,
    "name"=> "Paremoremo",
    "lat"=> -36.7758,
    "lng"=> 174.6702
  ],
  [
    "postcode"=> 757,
    "name"=> "Forrest Hill",
    "lat"=> -36.743,
    "lng"=> 174.7248
  ],
  [
    "postcode"=> 758,
    "name"=> "Mairangi Bay",
    "lat"=> -36.7397,
    "lng"=> 174.7419
  ],
  [
    "postcode"=> 772,
    "name"=> "Karekare",
    "lat"=> -36.9656,
    "lng"=> 174.4894
  ],
  [
    "postcode"=> 781,
    "name"=> "Piha",
    "lat"=> -36.8981,
    "lng"=> 174.4855
  ],
  [
    "postcode"=> 782,
    "name"=> "Waitakere",
    "lat"=> -36.8333,
    "lng"=> 174.5226
  ],
  [
    "postcode"=> 792,
    "name"=> "Red Vale",
    "lat"=> -36.6884,
    "lng"=> 174.6927
  ],
  [
    "postcode"=> 793,
    "name"=> "Coatesville",
    "lat"=> -36.7222,
    "lng"=> 174.6236
  ],
  [
    "postcode"=> 794,
    "name"=> "Dairy Flat",
    "lat"=> -36.6687,
    "lng"=> 174.6549
  ],
  [
    "postcode"=> 800,
    "name"=> "Helensville",
    "lat"=> -36.6765,
    "lng"=> 174.4527
  ],
  [
    "postcode"=> 810,
    "name"=> "Huapai",
    "lat"=> -36.7726,
    "lng"=> 174.5488
  ],
  [
    "postcode"=> 812,
    "name"=> "Waimauku",
    "lat"=> -36.7661,
    "lng"=> 174.4924
  ],
  [
    "postcode"=> 814,
    "name"=> "West Harbour",
    "lat"=> -36.8061,
    "lng"=> 174.5993
  ],
  [
    "postcode"=> 820,
    "name"=> "Riverhead",
    "lat"=> -36.7582,
    "lng"=> 174.5898
  ],
  [
    "postcode"=> 830,
    "name"=> "Parakai",
    "lat"=> -36.6596,
    "lng"=> 174.434
  ],
  [
    "postcode"=> 840,
    "name"=> "Helensville",
    "lat"=> -36.6764,
    "lng"=> 174.4505
  ],
  [
    "postcode"=> 841,
    "name"=> "Huapai",
    "lat"=> -36.7754,
    "lng"=> 174.5548
  ],
  [
    "postcode"=> 842,
    "name"=> "Pahitoa",
    "lat"=> -36.7687,
    "lng"=> 174.4926
  ],
  [
    "postcode"=> 843,
    "name"=> "Kaukapakapa",
    "lat"=> -36.6164,
    "lng"=> 174.4933
  ],
  [
    "postcode"=> 871,
    "name"=> "Kaukapakapa",
    "lat"=> -36.6354,
    "lng"=> 174.5287
  ],
  [
    "postcode"=> 873,
    "name"=> "Parakakau",
    "lat"=> -36.5611,
    "lng"=> 174.5841
  ],
  [
    "postcode"=> 874,
    "name"=> "Parkhurst",
    "lat"=> -36.5652,
    "lng"=> 174.3028
  ],
  [
    "postcode"=> 875,
    "name"=> "Helensville",
    "lat"=> -36.6868,
    "lng"=> 174.4557
  ],
  [
    "postcode"=> 881,
    "name"=> "Woodhill",
    "lat"=> -36.7829,
    "lng"=> 174.4262
  ],
  [
    "postcode"=> 882,
    "name"=> "Waimauku",
    "lat"=> -36.7176,
    "lng"=> 174.5263
  ],
  [
    "postcode"=> 883,
    "name"=> "Pahitoa",
    "lat"=> -36.7604,
    "lng"=> 174.4673
  ],
  [
    "postcode"=> 891,
    "name"=> "Huapai",
    "lat"=> -36.7875,
    "lng"=> 174.5296
  ],
  [
    "postcode"=> 892,
    "name"=> "Kumeu",
    "lat"=> -36.7596,
    "lng"=> 174.571
  ],
  [
    "postcode"=> 900,
    "name"=> "Wellsford",
    "lat"=> -36.2914,
    "lng"=> 174.5168
  ],
  [
    "postcode"=> 910,
    "name"=> "Warkworth",
    "lat"=> -36.4018,
    "lng"=> 174.6626
  ],
  [
    "postcode"=> 920,
    "name"=> "Leigh",
    "lat"=> -36.308,
    "lng"=> 174.9148
  ],
  [
    "postcode"=> 930,
    "name"=> "Whangaparaoa",
    "lat"=> -36.6172,
    "lng"=> 174.7966
  ],
  [
    "postcode"=> 931,
    "name"=> "Orewa",
    "lat"=> -36.58,
    "lng"=> 174.6853
  ],
  [
    "postcode"=> 932,
    "name"=> "Red Beach",
    "lat"=> -36.6232,
    "lng"=> 174.7063
  ],
  [
    "postcode"=> 940,
    "name"=> "Wellsford",
    "lat"=> -36.2938,
    "lng"=> 174.5238
  ],
  [
    "postcode"=> 941,
    "name"=> "Warkworth",
    "lat"=> -36.3978,
    "lng"=> 174.6639
  ],
  [
    "postcode"=> 942,
    "name"=> "Snells Beach",
    "lat"=> -36.4204,
    "lng"=> 174.7235
  ],
  [
    "postcode"=> 943,
    "name"=> "Whangaparaoa",
    "lat"=> -36.6366,
    "lng"=> 174.748
  ],
  [
    "postcode"=> 944,
    "name"=> "Silverdale",
    "lat"=> -36.6178,
    "lng"=> 174.6778
  ],
  [
    "postcode"=> 945,
    "name"=> "Red Beach",
    "lat"=> -36.6056,
    "lng"=> 174.6978
  ],
  [
    "postcode"=> 946,
    "name"=> "Orewa",
    "lat"=> -36.5856,
    "lng"=> 174.6943
  ],
  [
    "postcode"=> 947,
    "name"=> "Leigh",
    "lat"=> -36.2905,
    "lng"=> 174.8047
  ],
  [
    "postcode"=> 948,
    "name"=> "Matakana",
    "lat"=> -36.3519,
    "lng"=> 174.7169
  ],
  [
    "postcode"=> 950,
    "name"=> "Waiwera",
    "lat"=> -36.5447,
    "lng"=> 174.7058
  ],
  [
    "postcode"=> 951,
    "name"=> "Puhoi",
    "lat"=> -36.5121,
    "lng"=> 174.661
  ],
  [
    "postcode"=> 960,
    "name"=> "Okiwi",
    "lat"=> -36.1556,
    "lng"=> 175.3935
  ],
  [
    "postcode"=> 961,
    "name"=> "Okupu",
    "lat"=> -36.2427,
    "lng"=> 175.462
  ],
  [
    "postcode"=> 962,
    "name"=> "Tryphena",
    "lat"=> -36.3015,
    "lng"=> 175.4883
  ],
  [
    "postcode"=> 963,
    "name"=> "Port Fitzroy",
    "lat"=> -36.1639,
    "lng"=> 175.3624
  ],
  [
    "postcode"=> 972,
    "name"=> "Wayby",
    "lat"=> -36.3001,
    "lng"=> 174.6126
  ],
  [
    "postcode"=> 973,
    "name"=> "Wharehine",
    "lat"=> -36.315,
    "lng"=> 174.447
  ],
  [
    "postcode"=> 974,
    "name"=> "Te Hana",
    "lat"=> -36.2485,
    "lng"=> 174.6008
  ],
  [
    "postcode"=> 975,
    "name"=> "Topuni",
    "lat"=> -36.1895,
    "lng"=> 174.5252
  ],
  [
    "postcode"=> 977,
    "name"=> "Wharehine",
    "lat"=> -36.3561,
    "lng"=> 174.3225
  ],
  [
    "postcode"=> 981,
    "name"=> "Woodcocks",
    "lat"=> -36.4243,
    "lng"=> 174.5979
  ],
  [
    "postcode"=> 982,
    "name"=> "Algies Bay",
    "lat"=> -36.4404,
    "lng"=> 174.7235
  ],
  [
    "postcode"=> 983,
    "name"=> "Pohuehue",
    "lat"=> -36.4709,
    "lng"=> 174.6573
  ],
  [
    "postcode"=> 984,
    "name"=> "Ahuroa",
    "lat"=> -36.501,
    "lng"=> 174.5307
  ],
  [
    "postcode"=> 985,
    "name"=> "Big Omaha",
    "lat"=> -36.3249,
    "lng"=> 174.7381
  ],
  [
    "postcode"=> 986,
    "name"=> "Tawharanui",
    "lat"=> -36.3544,
    "lng"=> 174.8003
  ],
  [
    "postcode"=> 991,
    "name"=> "Whangapara",
    "lat"=> -36.2,
    "lng"=> 175.4287
  ],
  [
    "postcode"=> 992,
    "name"=> "Silverdale",
    "lat"=> -36.5992,
    "lng"=> 174.6619
  ],
  [
    "postcode"=> 993,
    "name"=> "Red Beach",
    "lat"=> -36.6374,
    "lng"=> 174.693
  ],
  [
    "postcode"=> 1010,
    "name"=> "Surfdale",
    "lat"=> -36.8052,
    "lng"=> 174.9811
  ],
  [
    "postcode"=> 1011,
    "name"=> "Herne Bay",
    "lat"=> -36.8447,
    "lng"=> 174.7336
  ],
  [
    "postcode"=> 1021,
    "name"=> "Arch Hill",
    "lat"=> -36.863,
    "lng"=> 174.7432
  ],
  [
    "postcode"=> 1022,
    "name"=> "Point Chevalier",
    "lat"=> -36.8585,
    "lng"=> 174.7053
  ],
  [
    "postcode"=> 1023,
    "name"=> "Epsom",
    "lat"=> -36.8872,
    "lng"=> 174.7709
  ],
  [
    "postcode"=> 1024,
    "name"=> "Epsom",
    "lat"=> -36.8877,
    "lng"=> 174.7529
  ],
  [
    "postcode"=> 1025,
    "name"=> "Sandringham",
    "lat"=> -36.8844,
    "lng"=> 174.7238
  ],
  [
    "postcode"=> 1026,
    "name"=> "Waterview",
    "lat"=> -36.876,
    "lng"=> 174.6829
  ],
  [
    "postcode"=> 1041,
    "name"=> "Mount Roskill",
    "lat"=> -36.9125,
    "lng"=> 174.7335
  ],
  [
    "postcode"=> 1042,
    "name"=> "Hillsborough",
    "lat"=> -36.9214,
    "lng"=> 174.7393
  ],
  [
    "postcode"=> 1050,
    "name"=> "Remuera",
    "lat"=> -36.8768,
    "lng"=> 174.8042
  ],
  [
    "postcode"=> 1051,
    "name"=> "Ellerslie",
    "lat"=> -36.8917,
    "lng"=> 174.8071
  ],
  [
    "postcode"=> 1052,
    "name"=> "Point Resolution",
    "lat"=> -36.857,
    "lng"=> 174.7914
  ],
  [
    "postcode"=> 1060,
    "name"=> "Penrose",
    "lat"=> -36.9177,
    "lng"=> 174.8356
  ],
  [
    "postcode"=> 1061,
    "name"=> "Te Papapa",
    "lat"=> -36.9155,
    "lng"=> 174.7991
  ],
  [
    "postcode"=> 1062,
    "name"=> "OTAHUHU",
    "lat"=> -36.941,
    "lng"=> 174.8483
  ],
  [
    "postcode"=> 1071,
    "name"=> "Kohimarama",
    "lat"=> -36.8565,
    "lng"=> 174.8482
  ],
  [
    "postcode"=> 1072,
    "name"=> "Tamaki",
    "lat"=> -36.8873,
    "lng"=> 174.8528
  ],
  [
    "postcode"=> 1081,
    "name"=> "Ostend",
    "lat"=> -36.7932,
    "lng"=> 175.0362
  ],
  [
    "postcode"=> 1140,
    "name"=> "Mechanics Bay",
    "lat"=> -36.8472,
    "lng"=> 174.7692
  ],
  [
    "postcode"=> 1141,
    "name"=> "Newmarket",
    "lat"=> -36.8507,
    "lng"=> 174.7632
  ],
  [
    "postcode"=> 1142,
    "name"=> "Newton",
    "lat"=> -36.8478,
    "lng"=> 174.7577
  ],
  [
    "postcode"=> 1143,
    "name"=> "Mechanics Bay",
    "lat"=> -36.8451,
    "lng"=> 174.7675
  ],
  [
    "postcode"=> 1144,
    "name"=> "Freemans Bay",
    "lat"=> -36.8481,
    "lng"=> 174.7437
  ],
  [
    "postcode"=> 1145,
    "name"=> "Grafton",
    "lat"=> -36.8578,
    "lng"=> 174.7579
  ],
  [
    "postcode"=> 1147,
    "name"=> "Herne Bay",
    "lat"=> -36.846,
    "lng"=> 174.7339
  ],
  [
    "postcode"=> 1148,
    "name"=> "Auckland",
    "lat"=> -36.8609,
    "lng"=> 174.7695
  ],
  [
    "postcode"=> 1149,
    "name"=> "Parnell",
    "lat"=> -36.8731,
    "lng"=> 174.7773
  ],
  [
    "postcode"=> 1150,
    "name"=> "Auckland",
    "lat"=> -36.8641,
    "lng"=> 174.7621
  ],
  [
    "postcode"=> 1151,
    "name"=> "Point Resolution",
    "lat"=> -36.8568,
    "lng"=> 174.7811
  ],
  [
    "postcode"=> 1245,
    "name"=> "Western Springs",
    "lat"=> -36.8654,
    "lng"=> 174.7378
  ],
  [
    "postcode"=> 1246,
    "name"=> "Mount Albert",
    "lat"=> -36.8702,
    "lng"=> 174.7107
  ],
  [
    "postcode"=> 1340,
    "name"=> "Wesley",
    "lat"=> -36.9004,
    "lng"=> 174.72
  ],
  [
    "postcode"=> 1342,
    "name"=> "Balmoral",
    "lat"=> -36.8885,
    "lng"=> 174.7472
  ],
  [
    "postcode"=> 1344,
    "name"=> "One Tree Hill",
    "lat"=> -36.8918,
    "lng"=> 174.7745
  ],
  [
    "postcode"=> 1345,
    "name"=> "Royal Oak",
    "lat"=> -36.9117,
    "lng"=> 174.7763
  ],
  [
    "postcode"=> 1346,
    "name"=> "Sandringham",
    "lat"=> -36.8837,
    "lng"=> 174.733
  ],
  [
    "postcode"=> 1347,
    "name"=> "One Tree Hill",
    "lat"=> -36.9,
    "lng"=> 174.7734
  ],
  [
    "postcode"=> 1348,
    "name"=> "Avondale",
    "lat"=> -36.8878,
    "lng"=> 174.6845
  ],
  [
    "postcode"=> 1349,
    "name"=> "Epsom",
    "lat"=> -36.8823,
    "lng"=> 174.7619
  ],
  [
    "postcode"=> 1350,
    "name"=> "Owairaka",
    "lat"=> -36.8849,
    "lng"=> 174.715
  ],
  [
    "postcode"=> 1351,
    "name"=> "Balmoral",
    "lat"=> -36.893,
    "lng"=> 174.7363
  ],
  [
    "postcode"=> 1352,
    "name"=> "Mount Eden",
    "lat"=> -36.8724,
    "lng"=> 174.744
  ],
  [
    "postcode"=> 1440,
    "name"=> "Three Kings",
    "lat"=> -36.9032,
    "lng"=> 174.742
  ],
  [
    "postcode"=> 1443,
    "name"=> "Lynfield",
    "lat"=> -36.9262,
    "lng"=> 174.7231
  ],
  [
    "postcode"=> 1445,
    "name"=> "Waikowhai",
    "lat"=> -36.9202,
    "lng"=> 174.7367
  ],
  [
    "postcode"=> 1446,
    "name"=> "Mount Eden",
    "lat"=> -36.8801,
    "lng"=> 174.7495
  ],
  [
    "postcode"=> 1541,
    "name"=> "Greenlane",
    "lat"=> -36.8804,
    "lng"=> 174.798
  ],
  [
    "postcode"=> 1542,
    "name"=> "Ellerslie",
    "lat"=> -36.898,
    "lng"=> 174.8092
  ],
  [
    "postcode"=> 1543,
    "name"=> "Greenlane",
    "lat"=> -36.8823,
    "lng"=> 174.7849
  ],
  [
    "postcode"=> 1544,
    "name"=> "Ellerslie",
    "lat"=> -36.8992,
    "lng"=> 174.8053
  ],
  [
    "postcode"=> 1545,
    "name"=> "Panmure",
    "lat"=> -36.8961,
    "lng"=> 174.8324
  ],
  [
    "postcode"=> 1546,
    "name"=> "Ellerslie",
    "lat"=> -36.895,
    "lng"=> 174.8001
  ],
  [
    "postcode"=> 1640,
    "name"=> "Otahuhu",
    "lat"=> -36.9444,
    "lng"=> 174.8441
  ],
  [
    "postcode"=> 1641,
    "name"=> "Panmure",
    "lat"=> -36.9079,
    "lng"=> 174.838
  ],
  [
    "postcode"=> 1642,
    "name"=> "Mount Wellington",
    "lat"=> -36.9097,
    "lng"=> 174.8145
  ],
  [
    "postcode"=> 1643,
    "name"=> "Onehunga",
    "lat"=> -36.9205,
    "lng"=> 174.7861
  ],
  [
    "postcode"=> 1740,
    "name"=> "Saint Heliers",
    "lat"=> -36.8507,
    "lng"=> 174.8577
  ],
  [
    "postcode"=> 1741,
    "name"=> "Panmure",
    "lat"=> -36.9005,
    "lng"=> 174.8557
  ],
  [
    "postcode"=> 1742,
    "name"=> "Meadowbank",
    "lat"=> -36.8758,
    "lng"=> 174.8277
  ],
  [
    "postcode"=> 1743,
    "name"=> "Point England",
    "lat"=> -36.8791,
    "lng"=> 174.8552
  ],
  [
    "postcode"=> 1744,
    "name"=> "Mission Bay",
    "lat"=> -36.8493,
    "lng"=> 174.8288
  ],
  [
    "postcode"=> 1745,
    "name"=> "Orakei",
    "lat"=> -36.856,
    "lng"=> 174.8147
  ],
  [
    "postcode"=> 1746,
    "name"=> "New Windsor",
    "lat"=> -36.8975,
    "lng"=> 174.6973
  ],
  [
    "postcode"=> 1840,
    "name"=> "Oneroa",
    "lat"=> -36.7834,
    "lng"=> 175.0105
  ],
  [
    "postcode"=> 1841,
    "name"=> "Onetangi",
    "lat"=> -36.792,
    "lng"=> 175.0798
  ],
  [
    "postcode"=> 1842,
    "name"=> "Blackpool",
    "lat"=> -36.7938,
    "lng"=> 175.026
  ],
  [
    "postcode"=> 1843,
    "name"=> "Ostend",
    "lat"=> -36.796,
    "lng"=> 175.0465
  ],
  [
    "postcode"=> 1971,
    "name"=> "Onetangi",
    "lat"=> -36.7931,
    "lng"=> 175.0922
  ],
  [
    "postcode"=> 2010,
    "name"=> "Pakuranga Heights",
    "lat"=> -36.9104,
    "lng"=> 174.8925
  ],
  [
    "postcode"=> 2012,
    "name"=> "Eastern Beach",
    "lat"=> -36.8714,
    "lng"=> 174.8978
  ],
  [
    "postcode"=> 2013,
    "name"=> "East Tamaki",
    "lat"=> -36.9373,
    "lng"=> 174.8935
  ],
  [
    "postcode"=> 2014,
    "name"=> "Shelly Park",
    "lat"=> -36.898,
    "lng"=> 174.9308
  ],
  [
    "postcode"=> 2016,
    "name"=> "East Tamaki",
    "lat"=> -36.9647,
    "lng"=> 174.9241
  ],
  [
    "postcode"=> 2018,
    "name"=> "Beachlands",
    "lat"=> -36.8818,
    "lng"=> 175.0134
  ],
  [
    "postcode"=> 2022,
    "name"=> "Mangere",
    "lat"=> -36.9784,
    "lng"=> 174.7817
  ],
  [
    "postcode"=> 2023,
    "name"=> "Wiri",
    "lat"=> -36.969,
    "lng"=> 174.8822
  ],
  [
    "postcode"=> 2024,
    "name"=> "Mangere East",
    "lat"=> -36.9559,
    "lng"=> 174.8184
  ],
  [
    "postcode"=> 2025,
    "name"=> "Wiri",
    "lat"=> -36.9915,
    "lng"=> 174.8466
  ],
  [
    "postcode"=> 2102,
    "name"=> "Manurewa",
    "lat"=> -37.0234,
    "lng"=> 174.8856
  ],
  [
    "postcode"=> 2103,
    "name"=> "Weymouth",
    "lat"=> -37.0408,
    "lng"=> 174.8752
  ],
  [
    "postcode"=> 2104,
    "name"=> "Wiri",
    "lat"=> -37.0011,
    "lng"=> 174.8672
  ],
  [
    "postcode"=> 2105,
    "name"=> "Manurewa East",
    "lat"=> -37.0135,
    "lng"=> 174.9183
  ],
  [
    "postcode"=> 2110,
    "name"=> "Red Hill",
    "lat"=> -37.0609,
    "lng"=> 174.9656
  ],
  [
    "postcode"=> 2112,
    "name"=> "Takanini",
    "lat"=> -37.045,
    "lng"=> 174.9202
  ],
  [
    "postcode"=> 2113,
    "name"=> "Papakura",
    "lat"=> -37.0874,
    "lng"=> 174.9409
  ],
  [
    "postcode"=> 2120,
    "name"=> "Pukekohe",
    "lat"=> -37.2088,
    "lng"=> 174.8945
  ],
  [
    "postcode"=> 2121,
    "name"=> "Tuakau",
    "lat"=> -37.2674,
    "lng"=> 174.9395
  ],
  [
    "postcode"=> 2122,
    "name"=> "Waiau Pa",
    "lat"=> -37.1379,
    "lng"=> 174.6979
  ],
  [
    "postcode"=> 2123,
    "name"=> "Waiuku",
    "lat"=> -37.2484,
    "lng"=> 174.7359
  ],
  [
    "postcode"=> 2140,
    "name"=> "Pakuranga",
    "lat"=> -36.9121,
    "lng"=> 174.8708
  ],
  [
    "postcode"=> 2141,
    "name"=> "East Tamaki",
    "lat"=> -36.9311,
    "lng"=> 174.8971
  ],
  [
    "postcode"=> 2142,
    "name"=> "East Tamaki",
    "lat"=> -36.9322,
    "lng"=> 174.9122
  ],
  [
    "postcode"=> 2143,
    "name"=> "Botany Downs",
    "lat"=> -36.8983,
    "lng"=> 174.9069
  ],
  [
    "postcode"=> 2144,
    "name"=> "Eastern Beach",
    "lat"=> -36.8681,
    "lng"=> 174.904
  ],
  [
    "postcode"=> 2145,
    "name"=> "Shelly Park",
    "lat"=> -36.8937,
    "lng"=> 174.9315
  ],
  [
    "postcode"=> 2146,
    "name"=> "Shelly Park",
    "lat"=> -36.9041,
    "lng"=> 174.9392
  ],
  [
    "postcode"=> 2147,
    "name"=> "Beachlands",
    "lat"=> -36.8825,
    "lng"=> 174.9969
  ],
  [
    "postcode"=> 2148,
    "name"=> "Maraetai",
    "lat"=> -36.8804,
    "lng"=> 175.0343
  ],
  [
    "postcode"=> 2149,
    "name"=> "Whitford",
    "lat"=> -36.9456,
    "lng"=> 174.965
  ],
  [
    "postcode"=> 2150,
    "name"=> "Mangere",
    "lat"=> -36.9986,
    "lng"=> 174.7885
  ],
  [
    "postcode"=> 2151,
    "name"=> "Favona",
    "lat"=> -36.9422,
    "lng"=> 174.7875
  ],
  [
    "postcode"=> 2153,
    "name"=> "Mangere",
    "lat"=> -36.9702,
    "lng"=> 174.7993
  ],
  [
    "postcode"=> 2154,
    "name"=> "Mangere",
    "lat"=> -36.9699,
    "lng"=> 174.7856
  ],
  [
    "postcode"=> 2155,
    "name"=> "Papatoetoe",
    "lat"=> -36.9714,
    "lng"=> 174.8621
  ],
  [
    "postcode"=> 2156,
    "name"=> "Papatoetoe",
    "lat"=> -36.9797,
    "lng"=> 174.8541
  ],
  [
    "postcode"=> 2157,
    "name"=> "Wiri",
    "lat"=> -36.9805,
    "lng"=> 174.8767
  ],
  [
    "postcode"=> 2158,
    "name"=> "Mangere East",
    "lat"=> -36.9665,
    "lng"=> 174.8251
  ],
  [
    "postcode"=> 2159,
    "name"=> "Otara",
    "lat"=> -36.9611,
    "lng"=> 174.8732
  ],
  [
    "postcode"=> 2160,
    "name"=> "Wiri",
    "lat"=> -36.9774,
    "lng"=> 174.8968
  ],
  [
    "postcode"=> 2240,
    "name"=> "Homai",
    "lat"=> -37.0068,
    "lng"=> 174.875
  ],
  [
    "postcode"=> 2241,
    "name"=> "Wiri",
    "lat"=> -36.992,
    "lng"=> 174.8815
  ],
  [
    "postcode"=> 2242,
    "name"=> "Weymouth",
    "lat"=> -37.0332,
    "lng"=> 174.8692
  ],
  [
    "postcode"=> 2243,
    "name"=> "Manurewa East",
    "lat"=> -37.022,
    "lng"=> 174.8966
  ],
  [
    "postcode"=> 2244,
    "name"=> "Pahurehure",
    "lat"=> -37.0616,
    "lng"=> 174.9417
  ],
  [
    "postcode"=> 2245,
    "name"=> "Takanini",
    "lat"=> -37.0392,
    "lng"=> 174.9198
  ],
  [
    "postcode"=> 2246,
    "name"=> "Pahurehure",
    "lat"=> -37.0486,
    "lng"=> 174.9282
  ],
  [
    "postcode"=> 2247,
    "name"=> "Drury",
    "lat"=> -37.1023,
    "lng"=> 174.9514
  ],
  [
    "postcode"=> 2248,
    "name"=> "Clevedon",
    "lat"=> -36.9927,
    "lng"=> 175.0373
  ],
  [
    "postcode"=> 2340,
    "name"=> "Pukekohe",
    "lat"=> -37.1999,
    "lng"=> 174.9058
  ],
  [
    "postcode"=> 2341,
    "name"=> "Waiuku",
    "lat"=> -37.251,
    "lng"=> 174.7286
  ],
  [
    "postcode"=> 2342,
    "name"=> "Tuakau",
    "lat"=> -37.258,
    "lng"=> 174.9466
  ],
  [
    "postcode"=> 2343,
    "name"=> "Bombay",
    "lat"=> -37.1862,
    "lng"=> 174.9912
  ],
  [
    "postcode"=> 2344,
    "name"=> "Patumahoe",
    "lat"=> -37.1871,
    "lng"=> 174.829
  ],
  [
    "postcode"=> 2345,
    "name"=> "Waiau Pa",
    "lat"=> -37.1495,
    "lng"=> 174.7647
  ],
  [
    "postcode"=> 2440,
    "name"=> "Pokeno",
    "lat"=> -37.2457,
    "lng"=> 175.0215
  ],
  [
    "postcode"=> 2441,
    "name"=> "Meremere",
    "lat"=> -37.3231,
    "lng"=> 175.0694
  ],
  [
    "postcode"=> 2471,
    "name"=> "Kopuku",
    "lat"=> -37.2446,
    "lng"=> 175.1648
  ],
  [
    "postcode"=> 2472,
    "name"=> "Pokeno",
    "lat"=> -37.2224,
    "lng"=> 175.0053
  ],
  [
    "postcode"=> 2473,
    "name"=> "Kaiaua",
    "lat"=> -37.1222,
    "lng"=> 175.2316
  ],
  [
    "postcode"=> 2474,
    "name"=> "Mercer",
    "lat"=> -37.2798,
    "lng"=> 175.0728
  ],
  [
    "postcode"=> 2571,
    "name"=> "Waikopua",
    "lat"=> -36.9162,
    "lng"=> 174.991
  ],
  [
    "postcode"=> 2576,
    "name"=> "Whitford",
    "lat"=> -36.9781,
    "lng"=> 174.9855
  ],
  [
    "postcode"=> 2577,
    "name"=> "Drury",
    "lat"=> -37.1147,
    "lng"=> 174.9742
  ],
  [
    "postcode"=> 2578,
    "name"=> "Ramarama",
    "lat"=> -37.1313,
    "lng"=> 174.935
  ],
  [
    "postcode"=> 2579,
    "name"=> "Ararimu",
    "lat"=> -37.1436,
    "lng"=> 175.0191
  ],
  [
    "postcode"=> 2580,
    "name"=> "Weymouth",
    "lat"=> -37.1032,
    "lng"=> 174.8609
  ],
  [
    "postcode"=> 2582,
    "name"=> "Clevedon",
    "lat"=> -36.9738,
    "lng"=> 175.0179
  ],
  [
    "postcode"=> 2583,
    "name"=> "Paparimu",
    "lat"=> -37.0996,
    "lng"=> 175.1194
  ],
  [
    "postcode"=> 2584,
    "name"=> "Opaheke",
    "lat"=> -37.1003,
    "lng"=> 175.0012
  ],
  [
    "postcode"=> 2585,
    "name"=> "Whakatiri",
    "lat"=> -36.9896,
    "lng"=> 175.1648
  ],
  [
    "postcode"=> 2675,
    "name"=> "Ararimu",
    "lat"=> -37.185,
    "lng"=> 175.0338
  ],
  [
    "postcode"=> 2676,
    "name"=> "Paerata",
    "lat"=> -37.1735,
    "lng"=> 174.8913
  ],
  [
    "postcode"=> 2677,
    "name"=> "Pukekohe East",
    "lat"=> -37.216,
    "lng"=> 174.9333
  ],
  [
    "postcode"=> 2678,
    "name"=> "Mauku",
    "lat"=> -37.2305,
    "lng"=> 174.8412
  ],
  [
    "postcode"=> 2679,
    "name"=> "Waiau Pa",
    "lat"=> -37.1394,
    "lng"=> 174.7893
  ],
  [
    "postcode"=> 2681,
    "name"=> "Glenbrook",
    "lat"=> -37.2045,
    "lng"=> 174.7562
  ],
  [
    "postcode"=> 2682,
    "name"=> "Otaua",
    "lat"=> -37.3092,
    "lng"=> 174.7493
  ],
  [
    "postcode"=> 2683,
    "name"=> "Waipipi",
    "lat"=> -37.2288,
    "lng"=> 174.6664
  ],
  [
    "postcode"=> 2684,
    "name"=> "Pollok",
    "lat"=> -37.1186,
    "lng"=> 174.6161
  ],
  [
    "postcode"=> 2693,
    "name"=> "Te Kohanga",
    "lat"=> -37.3001,
    "lng"=> 174.8746
  ],
  [
    "postcode"=> 2694,
    "name"=> "Tuakau",
    "lat"=> -37.2626,
    "lng"=> 174.9591
  ],
  [
    "postcode"=> 2695,
    "name"=> "Kaawa",
    "lat"=> -37.4521,
    "lng"=> 174.8611
  ],
  [
    "postcode"=> 2696,
    "name"=> "Opuatia",
    "lat"=> -37.3556,
    "lng"=> 175.0016
  ],
  [
    "postcode"=> 2697,
    "name"=> "Te Kohanga",
    "lat"=> -37.3909,
    "lng"=> 174.871
  ],
  [
    "postcode"=> 3010,
    "name"=> "Ohinemutu",
    "lat"=> -38.1178,
    "lng"=> 176.2514
  ],
  [
    "postcode"=> 3015,
    "name"=> "Sunnybrook",
    "lat"=> -38.1483,
    "lng"=> 176.2067
  ],
  [
    "postcode"=> 3020,
    "name"=> "Mamaku",
    "lat"=> -38.0956,
    "lng"=> 176.0756
  ],
  [
    "postcode"=> 3025,
    "name"=> "Murupara",
    "lat"=> -38.4571,
    "lng"=> 176.709
  ],
  [
    "postcode"=> 3040,
    "name"=> "Ngapuna",
    "lat"=> -38.1377,
    "lng"=> 176.2507
  ],
  [
    "postcode"=> 3041,
    "name"=> "Ngongotaha",
    "lat"=> -38.0805,
    "lng"=> 176.2124
  ],
  [
    "postcode"=> 3042,
    "name"=> "Owhata",
    "lat"=> -38.1345,
    "lng"=> 176.2952
  ],
  [
    "postcode"=> 3043,
    "name"=> "Whakarewarewa",
    "lat"=> -38.1592,
    "lng"=> 176.2567
  ],
  [
    "postcode"=> 3044,
    "name"=> "Glenholme",
    "lat"=> -38.1448,
    "lng"=> 176.2375
  ],
  [
    "postcode"=> 3045,
    "name"=> "Ngapuna",
    "lat"=> -38.14,
    "lng"=> 176.2516
  ],
  [
    "postcode"=> 3046,
    "name"=> "Koutu",
    "lat"=> -38.1294,
    "lng"=> 176.2337
  ],
  [
    "postcode"=> 3047,
    "name"=> "Mangakakahi",
    "lat"=> -38.1291,
    "lng"=> 176.2195
  ],
  [
    "postcode"=> 3048,
    "name"=> "Tihiotonga",
    "lat"=> -38.1602,
    "lng"=> 176.2355
  ],
  [
    "postcode"=> 3049,
    "name"=> "Mamaku",
    "lat"=> -38.0953,
    "lng"=> 176.0768
  ],
  [
    "postcode"=> 3060,
    "name"=> "Reporoa",
    "lat"=> -38.436,
    "lng"=> 176.3411
  ],
  [
    "postcode"=> 3062,
    "name"=> "Murupara",
    "lat"=> -38.4562,
    "lng"=> 176.7041
  ],
  [
    "postcode"=> 3072,
    "name"=> "Tarukenga",
    "lat"=> -38.062,
    "lng"=> 176.1258
  ],
  [
    "postcode"=> 3073,
    "name"=> "Waimangu",
    "lat"=> -38.2904,
    "lng"=> 176.4731
  ],
  [
    "postcode"=> 3074,
    "name"=> "Tikitere",
    "lat"=> -38.0913,
    "lng"=> 176.4142
  ],
  [
    "postcode"=> 3076,
    "name"=> "Punaroma",
    "lat"=> -38.1809,
    "lng"=> 176.362
  ],
  [
    "postcode"=> 3077,
    "name"=> "Guthrie",
    "lat"=> -38.32,
    "lng"=> 176.2239
  ],
  [
    "postcode"=> 3078,
    "name"=> "Atiamuri",
    "lat"=> -38.4262,
    "lng"=> 175.9947
  ],
  [
    "postcode"=> 3079,
    "name"=> "Ruatahuna",
    "lat"=> -38.5567,
    "lng"=> 176.8892
  ],
  [
    "postcode"=> 3081,
    "name"=> "Tahorakui",
    "lat"=> -38.6353,
    "lng"=> 176.4422
  ],
  [
    "postcode"=> 3083,
    "name"=> "Tahorakui",
    "lat"=> -38.4805,
    "lng"=> 176.2263
  ],
  [
    "postcode"=> 3110,
    "name"=> "Omanu Beach",
    "lat"=> -37.6686,
    "lng"=> 176.2664
  ],
  [
    "postcode"=> 3112,
    "name"=> "Hairini",
    "lat"=> -37.7273,
    "lng"=> 176.1538
  ],
  [
    "postcode"=> 3114,
    "name"=> "Omokoroa Beach",
    "lat"=> -37.6348,
    "lng"=> 176.0467
  ],
  [
    "postcode"=> 3116,
    "name"=> "Omanu",
    "lat"=> -37.6536,
    "lng"=> 176.2044
  ],
  [
    "postcode"=> 3118,
    "name"=> "Papamoa Beach",
    "lat"=> -37.7146,
    "lng"=> 176.3294
  ],
  [
    "postcode"=> 3119,
    "name"=> "Te Puke",
    "lat"=> -37.7774,
    "lng"=> 176.3267
  ],
  [
    "postcode"=> 3120,
    "name"=> "Poroporo",
    "lat"=> -37.9555,
    "lng"=> 176.9225
  ],
  [
    "postcode"=> 3121,
    "name"=> "Ohope Beach",
    "lat"=> -37.97,
    "lng"=> 177.084
  ],
  [
    "postcode"=> 3122,
    "name"=> "Opotiki",
    "lat"=> -38.0138,
    "lng"=> 177.2788
  ],
  [
    "postcode"=> 3123,
    "name"=> "Taneatua",
    "lat"=> -38.0646,
    "lng"=> 177.0066
  ],
  [
    "postcode"=> 3127,
    "name"=> "Kawerau",
    "lat"=> -38.0905,
    "lng"=> 176.6964
  ],
  [
    "postcode"=> 3129,
    "name"=> "Katikati",
    "lat"=> -37.5499,
    "lng"=> 175.9175
  ],
  [
    "postcode"=> 3140,
    "name"=> "Tauranga",
    "lat"=> -37.6934,
    "lng"=> 176.1622
  ],
  [
    "postcode"=> 3141,
    "name"=> "Tauranga",
    "lat"=> -37.685,
    "lng"=> 176.1682
  ],
  [
    "postcode"=> 3142,
    "name"=> "Greerton",
    "lat"=> -37.7267,
    "lng"=> 176.1332
  ],
  [
    "postcode"=> 3143,
    "name"=> "Tauranga",
    "lat"=> -37.6739,
    "lng"=> 176.1649
  ],
  [
    "postcode"=> 3144,
    "name"=> "Tauranga",
    "lat"=> -37.6982,
    "lng"=> 176.1597
  ],
  [
    "postcode"=> 3145,
    "name"=> "Otumoetai",
    "lat"=> -37.6684,
    "lng"=> 176.1405
  ],
  [
    "postcode"=> 3146,
    "name"=> "Judea",
    "lat"=> -37.6898,
    "lng"=> 176.1352
  ],
  [
    "postcode"=> 3147,
    "name"=> "Bethlehem",
    "lat"=> -37.6959,
    "lng"=> 176.1094
  ],
  [
    "postcode"=> 3148,
    "name"=> "Hairini",
    "lat"=> -37.7162,
    "lng"=> 176.1793
  ],
  [
    "postcode"=> 3149,
    "name"=> "Omanu",
    "lat"=> -37.6553,
    "lng"=> 176.2004
  ],
  [
    "postcode"=> 3150,
    "name"=> "Mount Maunganui",
    "lat"=> -37.6381,
    "lng"=> 176.1836
  ],
  [
    "postcode"=> 3151,
    "name"=> "Kairua",
    "lat"=> -37.7006,
    "lng"=> 176.2838
  ],
  [
    "postcode"=> 3152,
    "name"=> "Omanu Beach",
    "lat"=> -37.676,
    "lng"=> 176.223
  ],
  [
    "postcode"=> 3153,
    "name"=> "Te Puke",
    "lat"=> -37.7849,
    "lng"=> 176.3275
  ],
  [
    "postcode"=> 3154,
    "name"=> "Omokoroa Beach",
    "lat"=> -37.6326,
    "lng"=> 176.0523
  ],
  [
    "postcode"=> 3158,
    "name"=> "Whakatane",
    "lat"=> -37.9524,
    "lng"=> 176.9959
  ],
  [
    "postcode"=> 3159,
    "name"=> "Poroporo",
    "lat"=> -37.9621,
    "lng"=> 176.9819
  ],
  [
    "postcode"=> 3160,
    "name"=> "Edgecumbe",
    "lat"=> -37.9752,
    "lng"=> 176.8264
  ],
  [
    "postcode"=> 3161,
    "name"=> "Ohope Beach",
    "lat"=> -37.9642,
    "lng"=> 177.0361
  ],
  [
    "postcode"=> 3162,
    "name"=> "Opotiki",
    "lat"=> -38.0057,
    "lng"=> 177.2847
  ],
  [
    "postcode"=> 3163,
    "name"=> "Taneatua",
    "lat"=> -38.0653,
    "lng"=> 177.0034
  ],
  [
    "postcode"=> 3164,
    "name"=> "Waimana",
    "lat"=> -38.1431,
    "lng"=> 177.0767
  ],
  [
    "postcode"=> 3166,
    "name"=> "Katikati",
    "lat"=> -37.5528,
    "lng"=> 175.9174
  ],
  [
    "postcode"=> 3167,
    "name"=> "Te Teko",
    "lat"=> -38.0362,
    "lng"=> 176.7976
  ],
  [
    "postcode"=> 3168,
    "name"=> "Matata",
    "lat"=> -37.8886,
    "lng"=> 176.7586
  ],
  [
    "postcode"=> 3169,
    "name"=> "Kawerau",
    "lat"=> -38.0867,
    "lng"=> 176.7012
  ],
  [
    "postcode"=> 3171,
    "name"=> "Lower Kaimai",
    "lat"=> -37.8097,
    "lng"=> 176.025
  ],
  [
    "postcode"=> 3172,
    "name"=> "Omokoroa Beach",
    "lat"=> -37.5886,
    "lng"=> 176.0675
  ],
  [
    "postcode"=> 3173,
    "name"=> "Oropi",
    "lat"=> -37.8386,
    "lng"=> 176.1706
  ],
  [
    "postcode"=> 3175,
    "name"=> "Upper Papamoa",
    "lat"=> -37.7412,
    "lng"=> 176.2216
  ],
  [
    "postcode"=> 3176,
    "name"=> "Whakamarama",
    "lat"=> -37.7138,
    "lng"=> 176.0076
  ],
  [
    "postcode"=> 3177,
    "name"=> "Athenree",
    "lat"=> -37.4922,
    "lng"=> 175.9169
  ],
  [
    "postcode"=> 3178,
    "name"=> "Katikati",
    "lat"=> -37.5826,
    "lng"=> 175.8646
  ],
  [
    "postcode"=> 3182,
    "name"=> "Te Puke",
    "lat"=> -37.8464,
    "lng"=> 176.2896
  ],
  [
    "postcode"=> 3183,
    "name"=> "Upper Papamoa",
    "lat"=> -37.8104,
    "lng"=> 176.2666
  ],
  [
    "postcode"=> 3186,
    "name"=> "Ohinepanea",
    "lat"=> -37.8915,
    "lng"=> 176.4704
  ],
  [
    "postcode"=> 3187,
    "name"=> "Papamoa",
    "lat"=> -37.74,
    "lng"=> 176.3135
  ],
  [
    "postcode"=> 3188,
    "name"=> "Ngawaro",
    "lat"=> -37.8957,
    "lng"=> 176.1919
  ],
  [
    "postcode"=> 3189,
    "name"=> "Paengaroa",
    "lat"=> -37.8464,
    "lng"=> 176.413
  ],
  [
    "postcode"=> 3191,
    "name"=> "Otangihaku",
    "lat"=> -38.0614,
    "lng"=> 176.9708
  ],
  [
    "postcode"=> 3192,
    "name"=> "Kawerau",
    "lat"=> -38.1218,
    "lng"=> 176.7807
  ],
  [
    "postcode"=> 3193,
    "name"=> "Edgecumbe",
    "lat"=> -37.963,
    "lng"=> 176.8355
  ],
  [
    "postcode"=> 3194,
    "name"=> "Awakaponga",
    "lat"=> -37.9481,
    "lng"=> 176.7675
  ],
  [
    "postcode"=> 3196,
    "name"=> "Waimana",
    "lat"=> -38.2072,
    "lng"=> 177.1206
  ],
  [
    "postcode"=> 3197,
    "name"=> "Toatoa",
    "lat"=> -38.172,
    "lng"=> 177.4113
  ],
  [
    "postcode"=> 3198,
    "name"=> "Waimana",
    "lat"=> -38.1269,
    "lng"=> 177.1553
  ],
  [
    "postcode"=> 3199,
    "name"=> "Omaio",
    "lat"=> -37.8634,
    "lng"=> 177.8537
  ],
  [
    "postcode"=> 3200,
    "name"=> "Te Rapa",
    "lat"=> -37.761,
    "lng"=> 175.2474
  ],
  [
    "postcode"=> 3204,
    "name"=> "Frankton Junction",
    "lat"=> -37.7974,
    "lng"=> 175.2604
  ],
  [
    "postcode"=> 3206,
    "name"=> "Melville",
    "lat"=> -37.8198,
    "lng"=> 175.2865
  ],
  [
    "postcode"=> 3210,
    "name"=> "Claudelands",
    "lat"=> -37.7462,
    "lng"=> 175.2707
  ],
  [
    "postcode"=> 3214,
    "name"=> "Hamilton",
    "lat"=> -37.7708,
    "lng"=> 175.2985
  ],
  [
    "postcode"=> 3216,
    "name"=> "Hamilton East",
    "lat"=> -37.7961,
    "lng"=> 175.3088
  ],
  [
    "postcode"=> 3218,
    "name"=> "Temple View",
    "lat"=> -37.8198,
    "lng"=> 175.2282
  ],
  [
    "postcode"=> 3225,
    "name"=> "Raglan",
    "lat"=> -37.8109,
    "lng"=> 174.8709
  ],
  [
    "postcode"=> 3240,
    "name"=> "Hamilton West",
    "lat"=> -37.7849,
    "lng"=> 175.276
  ],
  [
    "postcode"=> 3241,
    "name"=> "Te Rapa",
    "lat"=> -37.7571,
    "lng"=> 175.2475
  ],
  [
    "postcode"=> 3242,
    "name"=> "Hamilton West",
    "lat"=> -37.7881,
    "lng"=> 175.2644
  ],
  [
    "postcode"=> 3243,
    "name"=> "Frankton Junction",
    "lat"=> -37.7955,
    "lng"=> 175.2458
  ],
  [
    "postcode"=> 3244,
    "name"=> "Hamilton",
    "lat"=> -37.7852,
    "lng"=> 175.2802
  ],
  [
    "postcode"=> 3245,
    "name"=> "Melville",
    "lat"=> -37.8175,
    "lng"=> 175.2824
  ],
  [
    "postcode"=> 3246,
    "name"=> "Melville",
    "lat"=> -37.8224,
    "lng"=> 175.2876
  ],
  [
    "postcode"=> 3247,
    "name"=> "Hamilton East",
    "lat"=> -37.7914,
    "lng"=> 175.2944
  ],
  [
    "postcode"=> 3248,
    "name"=> "Claudelands",
    "lat"=> -37.7514,
    "lng"=> 175.2799
  ],
  [
    "postcode"=> 3249,
    "name"=> "Te Rapa",
    "lat"=> -37.738,
    "lng"=> 175.253
  ],
  [
    "postcode"=> 3251,
    "name"=> "Hamilton East",
    "lat"=> -37.7985,
    "lng"=> 175.3157
  ],
  [
    "postcode"=> 3252,
    "name"=> "Claudelands",
    "lat"=> -37.7723,
    "lng"=> 175.2934
  ],
  [
    "postcode"=> 3253,
    "name"=> "Newstead",
    "lat"=> -37.8017,
    "lng"=> 175.3223
  ],
  [
    "postcode"=> 3254,
    "name"=> "Newstead",
    "lat"=> -37.7899,
    "lng"=> 175.3261
  ],
  [
    "postcode"=> 3255,
    "name"=> "Hamilton East",
    "lat"=> -37.7876,
    "lng"=> 175.3166
  ],
  [
    "postcode"=> 3256,
    "name"=> "Claudelands",
    "lat"=> -37.729,
    "lng"=> 175.2734
  ],
  [
    "postcode"=> 3257,
    "name"=> "Claudelands",
    "lat"=> -37.7651,
    "lng"=> 175.2534
  ],
  [
    "postcode"=> 3260,
    "name"=> "Matangi",
    "lat"=> -37.8043,
    "lng"=> 175.395
  ],
  [
    "postcode"=> 3261,
    "name"=> "Whatawhata",
    "lat"=> -37.7974,
    "lng"=> 175.1535
  ],
  [
    "postcode"=> 3262,
    "name"=> "Horotiu",
    "lat"=> -37.6941,
    "lng"=> 175.19
  ],
  [
    "postcode"=> 3263,
    "name"=> "Te Kowhai",
    "lat"=> -37.7432,
    "lng"=> 175.1526
  ],
  [
    "postcode"=> 3264,
    "name"=> "Gordonton",
    "lat"=> -37.6696,
    "lng"=> 175.3041
  ],
  [
    "postcode"=> 3265,
    "name"=> "Raglan",
    "lat"=> -37.8003,
    "lng"=> 174.87
  ],
  [
    "postcode"=> 3266,
    "name"=> "Te Uku",
    "lat"=> -37.8289,
    "lng"=> 174.957
  ],
  [
    "postcode"=> 3281,
    "name"=> "Gordonton",
    "lat"=> -37.6905,
    "lng"=> 175.3075
  ],
  [
    "postcode"=> 3282,
    "name"=> "Rukuhia",
    "lat"=> -37.843,
    "lng"=> 175.2852
  ],
  [
    "postcode"=> 3283,
    "name"=> "Tamahere",
    "lat"=> -37.8514,
    "lng"=> 175.3865
  ],
  [
    "postcode"=> 3284,
    "name"=> "Eureka",
    "lat"=> -37.7576,
    "lng"=> 175.3961
  ],
  [
    "postcode"=> 3285,
    "name"=> "Aramiro",
    "lat"=> -37.905,
    "lng"=> 175.0829
  ],
  [
    "postcode"=> 3288,
    "name"=> "Te Kowhai",
    "lat"=> -37.7355,
    "lng"=> 175.174
  ],
  [
    "postcode"=> 3289,
    "name"=> "Whatawhata",
    "lat"=> -37.7794,
    "lng"=> 175.136
  ],
  [
    "postcode"=> 3290,
    "name"=> "Temple View",
    "lat"=> -37.8461,
    "lng"=> 175.1942
  ],
  [
    "postcode"=> 3293,
    "name"=> "Waitetuna",
    "lat"=> -37.8177,
    "lng"=> 175.0743
  ],
  [
    "postcode"=> 3295,
    "name"=> "Te Uku",
    "lat"=> -37.8322,
    "lng"=> 174.9795
  ],
  [
    "postcode"=> 3296,
    "name"=> "Ruapuke",
    "lat"=> -37.8766,
    "lng"=> 174.8145
  ],
  [
    "postcode"=> 3297,
    "name"=> "Raglan",
    "lat"=> -37.8324,
    "lng"=> 174.8054
  ],
  [
    "postcode"=> 3300,
    "name"=> "Morrinsville",
    "lat"=> -37.652,
    "lng"=> 175.5293
  ],
  [
    "postcode"=> 3310,
    "name"=> "Waitoa",
    "lat"=> -37.6,
    "lng"=> 175.6334
  ],
  [
    "postcode"=> 3320,
    "name"=> "Te Aroha",
    "lat"=> -37.5391,
    "lng"=> 175.7098
  ],
  [
    "postcode"=> 3330,
    "name"=> "Wharewaka",
    "lat"=> -38.7037,
    "lng"=> 176.0588
  ],
  [
    "postcode"=> 3332,
    "name"=> "Wairakei",
    "lat"=> -38.6195,
    "lng"=> 176.103
  ],
  [
    "postcode"=> 3334,
    "name"=> "Turangi",
    "lat"=> -39.0028,
    "lng"=> 175.8069
  ],
  [
    "postcode"=> 3340,
    "name"=> "Morrinsville",
    "lat"=> -37.6577,
    "lng"=> 175.5275
  ],
  [
    "postcode"=> 3341,
    "name"=> "Waitoa",
    "lat"=> -37.6015,
    "lng"=> 175.6281
  ],
  [
    "postcode"=> 3342,
    "name"=> "Te Aroha",
    "lat"=> -37.5429,
    "lng"=> 175.7124
  ],
  [
    "postcode"=> 3343,
    "name"=> "Te Aroha",
    "lat"=> -37.5523,
    "lng"=> 175.7039
  ],
  [
    "postcode"=> 3351,
    "name"=> "Taupo",
    "lat"=> -38.6868,
    "lng"=> 176.0718
  ],
  [
    "postcode"=> 3352,
    "name"=> "Taupo",
    "lat"=> -38.6828,
    "lng"=> 176.0709
  ],
  [
    "postcode"=> 3353,
    "name"=> "Turangi",
    "lat"=> -38.9887,
    "lng"=> 175.8087
  ],
  [
    "postcode"=> 3360,
    "name"=> "Tarawera",
    "lat"=> -39.0266,
    "lng"=> 176.5734
  ],
  [
    "postcode"=> 3371,
    "name"=> "Kiwitahi",
    "lat"=> -37.7378,
    "lng"=> 175.5598
  ],
  [
    "postcode"=> 3372,
    "name"=> "Motumaoho",
    "lat"=> -37.6875,
    "lng"=> 175.4797
  ],
  [
    "postcode"=> 3373,
    "name"=> "Tahuna",
    "lat"=> -37.4989,
    "lng"=> 175.4546
  ],
  [
    "postcode"=> 3374,
    "name"=> "Te Puninga",
    "lat"=> -37.5777,
    "lng"=> 175.5857
  ],
  [
    "postcode"=> 3375,
    "name"=> "Morrinsville",
    "lat"=> -37.602,
    "lng"=> 175.445
  ],
  [
    "postcode"=> 3377,
    "name"=> "Oruanui",
    "lat"=> -38.6226,
    "lng"=> 176.0015
  ],
  [
    "postcode"=> 3378,
    "name"=> "Waipahihi",
    "lat"=> -38.7041,
    "lng"=> 176.1355
  ],
  [
    "postcode"=> 3379,
    "name"=> "Rangitaiki",
    "lat"=> -38.887,
    "lng"=> 176.3747
  ],
  [
    "postcode"=> 3380,
    "name"=> "Waitoa",
    "lat"=> -37.5909,
    "lng"=> 175.6224
  ],
  [
    "postcode"=> 3381,
    "name"=> "Kuratau",
    "lat"=> -38.8184,
    "lng"=> 175.7096
  ],
  [
    "postcode"=> 3382,
    "name"=> "Turangi",
    "lat"=> -39.0284,
    "lng"=> 175.9471
  ],
  [
    "postcode"=> 3391,
    "name"=> "Manawaru",
    "lat"=> -37.6407,
    "lng"=> 175.7962
  ],
  [
    "postcode"=> 3392,
    "name"=> "Mangaiti",
    "lat"=> -37.5085,
    "lng"=> 175.6909
  ],
  [
    "postcode"=> 3393,
    "name"=> "Mangaiti",
    "lat"=> -37.4947,
    "lng"=> 175.6061
  ],
  [
    "postcode"=> 3400,
    "name"=> "Matamata",
    "lat"=> -37.8152,
    "lng"=> 175.7832
  ],
  [
    "postcode"=> 3401,
    "name"=> "Waharoa",
    "lat"=> -37.759,
    "lng"=> 175.7491
  ],
  [
    "postcode"=> 3410,
    "name"=> "Tirau",
    "lat"=> -37.9878,
    "lng"=> 175.7524
  ],
  [
    "postcode"=> 3411,
    "name"=> "Putaruru",
    "lat"=> -38.0505,
    "lng"=> 175.7793
  ],
  [
    "postcode"=> 3415,
    "name"=> "Arapuni",
    "lat"=> -38.0715,
    "lng"=> 175.6498
  ],
  [
    "postcode"=> 3420,
    "name"=> "Tokoroa",
    "lat"=> -38.2229,
    "lng"=> 175.8685
  ],
  [
    "postcode"=> 3421,
    "name"=> "Mangakino",
    "lat"=> -38.3696,
    "lng"=> 175.7737
  ],
  [
    "postcode"=> 3432,
    "name"=> "Leamington",
    "lat"=> -37.9096,
    "lng"=> 175.4781
  ],
  [
    "postcode"=> 3434,
    "name"=> "Leamington",
    "lat"=> -37.8866,
    "lng"=> 175.469
  ],
  [
    "postcode"=> 3440,
    "name"=> "Matamata",
    "lat"=> -37.8111,
    "lng"=> 175.7725
  ],
  [
    "postcode"=> 3441,
    "name"=> "Waharoa",
    "lat"=> -37.7591,
    "lng"=> 175.7525
  ],
  [
    "postcode"=> 3442,
    "name"=> "Tirau",
    "lat"=> -37.9782,
    "lng"=> 175.7568
  ],
  [
    "postcode"=> 3443,
    "name"=> "Putaruru",
    "lat"=> -38.0516,
    "lng"=> 175.7788
  ],
  [
    "postcode"=> 3444,
    "name"=> "Tokoroa",
    "lat"=> -38.2176,
    "lng"=> 175.8717
  ],
  [
    "postcode"=> 3445,
    "name"=> "Mangakino",
    "lat"=> -38.3704,
    "lng"=> 175.774
  ],
  [
    "postcode"=> 3450,
    "name"=> "Leamington",
    "lat"=> -37.895,
    "lng"=> 175.4719
  ],
  [
    "postcode"=> 3451,
    "name"=> "Hautapu",
    "lat"=> -37.8605,
    "lng"=> 175.4531
  ],
  [
    "postcode"=> 3471,
    "name"=> "Okauia",
    "lat"=> -37.7686,
    "lng"=> 175.8421
  ],
  [
    "postcode"=> 3472,
    "name"=> "Hinuera",
    "lat"=> -37.8526,
    "lng"=> 175.6993
  ],
  [
    "postcode"=> 3473,
    "name"=> "Kaimai",
    "lat"=> -37.8507,
    "lng"=> 175.8962
  ],
  [
    "postcode"=> 3474,
    "name"=> "Waharoa",
    "lat"=> -37.7272,
    "lng"=> 175.7514
  ],
  [
    "postcode"=> 3475,
    "name"=> "Walton",
    "lat"=> -37.7656,
    "lng"=> 175.6598
  ],
  [
    "postcode"=> 3481,
    "name"=> "Te Waotu",
    "lat"=> -38.1162,
    "lng"=> 175.6938
  ],
  [
    "postcode"=> 3482,
    "name"=> "Ngatira",
    "lat"=> -38.1275,
    "lng"=> 175.8195
  ],
  [
    "postcode"=> 3483,
    "name"=> "Putaruru",
    "lat"=> -38.0474,
    "lng"=> 175.8289
  ],
  [
    "postcode"=> 3484,
    "name"=> "Tirau",
    "lat"=> -37.9592,
    "lng"=> 175.7208
  ],
  [
    "postcode"=> 3485,
    "name"=> "Okoroire",
    "lat"=> -37.9758,
    "lng"=> 175.8676
  ],
  [
    "postcode"=> 3491,
    "name"=> "Kinleith",
    "lat"=> -38.2644,
    "lng"=> 175.9126
  ],
  [
    "postcode"=> 3492,
    "name"=> "Mangakino",
    "lat"=> -38.5067,
    "lng"=> 175.696
  ],
  [
    "postcode"=> 3493,
    "name"=> "Bruntwood",
    "lat"=> -37.8344,
    "lng"=> 175.4448
  ],
  [
    "postcode"=> 3494,
    "name"=> "Pukeatua",
    "lat"=> -37.9944,
    "lng"=> 175.5804
  ],
  [
    "postcode"=> 3495,
    "name"=> "Leamington",
    "lat"=> -37.9615,
    "lng"=> 175.4494
  ],
  [
    "postcode"=> 3496,
    "name"=> "Whitehall",
    "lat"=> -37.8541,
    "lng"=> 175.5666
  ],
  [
    "postcode"=> 3500,
    "name"=> "Thames",
    "lat"=> -37.1355,
    "lng"=> 175.5456
  ],
  [
    "postcode"=> 3503,
    "name"=> "Ngatea",
    "lat"=> -37.2735,
    "lng"=> 175.4893
  ],
  [
    "postcode"=> 3506,
    "name"=> "Whangapoua",
    "lat"=> -36.6993,
    "lng"=> 175.6601
  ],
  [
    "postcode"=> 3508,
    "name"=> "Tairua",
    "lat"=> -36.9977,
    "lng"=> 175.848
  ],
  [
    "postcode"=> 3510,
    "name"=> "Whitianga",
    "lat"=> -36.817,
    "lng"=> 175.6913
  ],
  [
    "postcode"=> 3540,
    "name"=> "Thames",
    "lat"=> -37.139,
    "lng"=> 175.542
  ],
  [
    "postcode"=> 3541,
    "name"=> "Ngatea",
    "lat"=> -37.2763,
    "lng"=> 175.4971
  ],
  [
    "postcode"=> 3542,
    "name"=> "Whitianga",
    "lat"=> -36.8358,
    "lng"=> 175.7045
  ],
  [
    "postcode"=> 3543,
    "name"=> "Coromandel",
    "lat"=> -36.7586,
    "lng"=> 175.4984
  ],
  [
    "postcode"=> 3544,
    "name"=> "Tairua",
    "lat"=> -37.002,
    "lng"=> 175.8482
  ],
  [
    "postcode"=> 3545,
    "name"=> "Turua",
    "lat"=> -37.2378,
    "lng"=> 175.5664
  ],
  [
    "postcode"=> 3546,
    "name"=> "Pauanui",
    "lat"=> -37.0206,
    "lng"=> 175.8587
  ],
  [
    "postcode"=> 3547,
    "name"=> "Colville",
    "lat"=> -36.6344,
    "lng"=> 175.4767
  ],
  [
    "postcode"=> 3574,
    "name"=> "Turua",
    "lat"=> -37.2269,
    "lng"=> 175.5535
  ],
  [
    "postcode"=> 3575,
    "name"=> "Tapu",
    "lat"=> -36.9856,
    "lng"=> 175.4922
  ],
  [
    "postcode"=> 3576,
    "name"=> "Waitakaruru",
    "lat"=> -37.2732,
    "lng"=> 175.3863
  ],
  [
    "postcode"=> 3577,
    "name"=> "Parawai",
    "lat"=> -37.131,
    "lng"=> 175.6008
  ],
  [
    "postcode"=> 3578,
    "name"=> "Nevesville",
    "lat"=> -37.1958,
    "lng"=> 175.6659
  ],
  [
    "postcode"=> 3579,
    "name"=> "Hikuai",
    "lat"=> -37.0865,
    "lng"=> 175.8125
  ],
  [
    "postcode"=> 3581,
    "name"=> "Ahimia",
    "lat"=> -36.8134,
    "lng"=> 175.4844
  ],
  [
    "postcode"=> 3582,
    "name"=> "Te Rerenga",
    "lat"=> -36.7533,
    "lng"=> 175.6141
  ],
  [
    "postcode"=> 3583,
    "name"=> "Kennedys Bay",
    "lat"=> -36.6805,
    "lng"=> 175.5537
  ],
  [
    "postcode"=> 3584,
    "name"=> "Colville",
    "lat"=> -36.6061,
    "lng"=> 175.4532
  ],
  [
    "postcode"=> 3591,
    "name"=> "Coroglen",
    "lat"=> -36.8959,
    "lng"=> 175.7144
  ],
  [
    "postcode"=> 3592,
    "name"=> "Coroglen",
    "lat"=> -36.9317,
    "lng"=> 175.6572
  ],
  [
    "postcode"=> 3597,
    "name"=> "Ngatea",
    "lat"=> -37.2926,
    "lng"=> 175.4673
  ],
  [
    "postcode"=> 3600,
    "name"=> "Paeroa",
    "lat"=> -37.3735,
    "lng"=> 175.6712
  ],
  [
    "postcode"=> 3610,
    "name"=> "Waihi",
    "lat"=> -37.3865,
    "lng"=> 175.8446
  ],
  [
    "postcode"=> 3611,
    "name"=> "Waihi Beach",
    "lat"=> -37.4039,
    "lng"=> 175.9432
  ],
  [
    "postcode"=> 3620,
    "name"=> "Whangamata",
    "lat"=> -37.1974,
    "lng"=> 175.869
  ],
  [
    "postcode"=> 3640,
    "name"=> "Paeroa",
    "lat"=> -37.381,
    "lng"=> 175.671
  ],
  [
    "postcode"=> 3641,
    "name"=> "Waihi",
    "lat"=> -37.3923,
    "lng"=> 175.8413
  ],
  [
    "postcode"=> 3642,
    "name"=> "Waihi Beach",
    "lat"=> -37.4117,
    "lng"=> 175.9424
  ],
  [
    "postcode"=> 3643,
    "name"=> "Whangamata",
    "lat"=> -37.2071,
    "lng"=> 175.8715
  ],
  [
    "postcode"=> 3671,
    "name"=> "Netherton",
    "lat"=> -37.3374,
    "lng"=> 175.585
  ],
  [
    "postcode"=> 3672,
    "name"=> "Netherton",
    "lat"=> -37.3715,
    "lng"=> 175.6157
  ],
  [
    "postcode"=> 3673,
    "name"=> "Tirohia",
    "lat"=> -37.4191,
    "lng"=> 175.6648
  ],
  [
    "postcode"=> 3674,
    "name"=> "Komata",
    "lat"=> -37.3376,
    "lng"=> 175.7408
  ],
  [
    "postcode"=> 3681,
    "name"=> "Waihi",
    "lat"=> -37.3714,
    "lng"=> 175.8798
  ],
  [
    "postcode"=> 3682,
    "name"=> "Waihi",
    "lat"=> -37.4057,
    "lng"=> 175.8178
  ],
  [
    "postcode"=> 3691,
    "name"=> "Whangamata",
    "lat"=> -37.1833,
    "lng"=> 175.8231
  ],
  [
    "postcode"=> 3700,
    "name"=> "Starrtown",
    "lat"=> -37.5596,
    "lng"=> 175.1583
  ],
  [
    "postcode"=> 3710,
    "name"=> "Te Kauwhata",
    "lat"=> -37.4039,
    "lng"=> 175.1466
  ],
  [
    "postcode"=> 3720,
    "name"=> "Ngaruawahia",
    "lat"=> -37.6691,
    "lng"=> 175.1522
  ],
  [
    "postcode"=> 3721,
    "name"=> "Taupiri",
    "lat"=> -37.6191,
    "lng"=> 175.1894
  ],
  [
    "postcode"=> 3740,
    "name"=> "Starrtown",
    "lat"=> -37.558,
    "lng"=> 175.1589
  ],
  [
    "postcode"=> 3741,
    "name"=> "Te Kauwhata",
    "lat"=> -37.4039,
    "lng"=> 175.144
  ],
  [
    "postcode"=> 3742,
    "name"=> "Ngaruawahia",
    "lat"=> -37.6659,
    "lng"=> 175.1482
  ],
  [
    "postcode"=> 3771,
    "name"=> "Pukekapia",
    "lat"=> -37.5358,
    "lng"=> 175.1096
  ],
  [
    "postcode"=> 3772,
    "name"=> "Ruawaro",
    "lat"=> -37.5172,
    "lng"=> 175.0108
  ],
  [
    "postcode"=> 3781,
    "name"=> "Waerenga",
    "lat"=> -37.3573,
    "lng"=> 175.2623
  ],
  [
    "postcode"=> 3782,
    "name"=> "Whangamarino",
    "lat"=> -37.3589,
    "lng"=> 175.1326
  ],
  [
    "postcode"=> 3784,
    "name"=> "Waiterimu",
    "lat"=> -37.4617,
    "lng"=> 175.2925
  ],
  [
    "postcode"=> 3791,
    "name"=> "Komakorau",
    "lat"=> -37.6355,
    "lng"=> 175.2398
  ],
  [
    "postcode"=> 3792,
    "name"=> "Orini",
    "lat"=> -37.5777,
    "lng"=> 175.2696
  ],
  [
    "postcode"=> 3793,
    "name"=> "Waingaro",
    "lat"=> -37.7088,
    "lng"=> 174.9803
  ],
  [
    "postcode"=> 3794,
    "name"=> "Pepepe",
    "lat"=> -37.6266,
    "lng"=> 174.9202
  ],
  [
    "postcode"=> 3800,
    "name"=> "Te Awamutu",
    "lat"=> -38.0195,
    "lng"=> 175.3305
  ],
  [
    "postcode"=> 3802,
    "name"=> "Pirongia",
    "lat"=> -37.9914,
    "lng"=> 175.2051
  ],
  [
    "postcode"=> 3803,
    "name"=> "Ohaupo",
    "lat"=> -37.9178,
    "lng"=> 175.3066
  ],
  [
    "postcode"=> 3840,
    "name"=> "Te Awamutu",
    "lat"=> -38.0104,
    "lng"=> 175.3237
  ],
  [
    "postcode"=> 3841,
    "name"=> "Kihikihi",
    "lat"=> -38.0377,
    "lng"=> 175.3454
  ],
  [
    "postcode"=> 3843,
    "name"=> "Kawhia",
    "lat"=> -38.0639,
    "lng"=> 174.8229
  ],
  [
    "postcode"=> 3872,
    "name"=> "Wharepuhunga",
    "lat"=> -38.0733,
    "lng"=> 175.4515
  ],
  [
    "postcode"=> 3873,
    "name"=> "Te Kawa",
    "lat"=> -38.138,
    "lng"=> 175.3534
  ],
  [
    "postcode"=> 3874,
    "name"=> "Matapara",
    "lat"=> -38.2716,
    "lng"=> 175.5108
  ],
  [
    "postcode"=> 3875,
    "name"=> "Te Mawhai",
    "lat"=> -38.0405,
    "lng"=> 175.29
  ],
  [
    "postcode"=> 3876,
    "name"=> "Pirongia",
    "lat"=> -38.0036,
    "lng"=> 175.1763
  ],
  [
    "postcode"=> 3877,
    "name"=> "Matapara",
    "lat"=> -38.1874,
    "lng"=> 175.538
  ],
  [
    "postcode"=> 3878,
    "name"=> "Hauturu",
    "lat"=> -38.0811,
    "lng"=> 174.9754
  ],
  [
    "postcode"=> 3879,
    "name"=> "Kihikihi",
    "lat"=> -38.0134,
    "lng"=> 175.4174
  ],
  [
    "postcode"=> 3880,
    "name"=> "Pukeatua",
    "lat"=> -38.1016,
    "lng"=> 175.5796
  ],
  [
    "postcode"=> 3881,
    "name"=> "Ohaupo",
    "lat"=> -37.9081,
    "lng"=> 175.3486
  ],
  [
    "postcode"=> 3882,
    "name"=> "Rukuhia",
    "lat"=> -37.8832,
    "lng"=> 175.2463
  ],
  [
    "postcode"=> 3883,
    "name"=> "Ngaroto",
    "lat"=> -37.9539,
    "lng"=> 175.2812
  ],
  [
    "postcode"=> 3885,
    "name"=> "Oparau",
    "lat"=> -38.0142,
    "lng"=> 174.9774
  ],
  [
    "postcode"=> 3886,
    "name"=> "Owhiro",
    "lat"=> -38.153,
    "lng"=> 174.9098
  ],
  [
    "postcode"=> 3889,
    "name"=> "Kawhia",
    "lat"=> -38.0416,
    "lng"=> 174.8314
  ],
  [
    "postcode"=> 3894,
    "name"=> "Te Papatapu",
    "lat"=> -37.9576,
    "lng"=> 174.8386
  ],
  [
    "postcode"=> 3895,
    "name"=> "Moerangi",
    "lat"=> -37.9508,
    "lng"=> 174.9124
  ],
  [
    "postcode"=> 3900,
    "name"=> "Otorohanga",
    "lat"=> -38.1807,
    "lng"=> 175.223
  ],
  [
    "postcode"=> 3910,
    "name"=> "Te Kuiti",
    "lat"=> -38.3353,
    "lng"=> 175.1654
  ],
  [
    "postcode"=> 3912,
    "name"=> "Piopio",
    "lat"=> -38.4671,
    "lng"=> 175.0126
  ],
  [
    "postcode"=> 3920,
    "name"=> "Taumarunui",
    "lat"=> -38.8765,
    "lng"=> 175.2729
  ],
  [
    "postcode"=> 3924,
    "name"=> "Manunui",
    "lat"=> -38.8965,
    "lng"=> 175.3202
  ],
  [
    "postcode"=> 3926,
    "name"=> "Ohura",
    "lat"=> -38.8467,
    "lng"=> 174.9669
  ],
  [
    "postcode"=> 3940,
    "name"=> "Otorohanga",
    "lat"=> -38.1864,
    "lng"=> 175.2115
  ],
  [
    "postcode"=> 3941,
    "name"=> "Te Kuiti",
    "lat"=> -38.3344,
    "lng"=> 175.1656
  ],
  [
    "postcode"=> 3942,
    "name"=> "Piopio",
    "lat"=> -38.4665,
    "lng"=> 175.016
  ],
  [
    "postcode"=> 3943,
    "name"=> "Hangatiki",
    "lat"=> -38.2521,
    "lng"=> 175.1787
  ],
  [
    "postcode"=> 3944,
    "name"=> "Benneydale",
    "lat"=> -38.5204,
    "lng"=> 175.3638
  ],
  [
    "postcode"=> 3945,
    "name"=> "Mahoenui",
    "lat"=> -38.5784,
    "lng"=> 174.8403
  ],
  [
    "postcode"=> 3946,
    "name"=> "Taumarunui",
    "lat"=> -38.8844,
    "lng"=> 175.2639
  ],
  [
    "postcode"=> 3947,
    "name"=> "Ohura",
    "lat"=> -38.8418,
    "lng"=> 174.9827
  ],
  [
    "postcode"=> 3948,
    "name"=> "National Park",
    "lat"=> -39.177,
    "lng"=> 175.4022
  ],
  [
    "postcode"=> 3949,
    "name"=> "Tongariro",
    "lat"=> -39.046,
    "lng"=> 175.6038
  ],
  [
    "postcode"=> 3970,
    "name"=> "Wairere Falls",
    "lat"=> -38.5064,
    "lng"=> 174.971
  ],
  [
    "postcode"=> 3971,
    "name"=> "Waitanguru",
    "lat"=> -38.402,
    "lng"=> 174.9066
  ],
  [
    "postcode"=> 3972,
    "name"=> "Maihiihi",
    "lat"=> -38.2227,
    "lng"=> 175.3551
  ],
  [
    "postcode"=> 3973,
    "name"=> "Honikiwi",
    "lat"=> -38.1305,
    "lng"=> 175.1012
  ],
  [
    "postcode"=> 3974,
    "name"=> "Te Kawa",
    "lat"=> -38.1444,
    "lng"=> 175.2717
  ],
  [
    "postcode"=> 3975,
    "name"=> "Maihiihi",
    "lat"=> -38.2977,
    "lng"=> 175.3482
  ],
  [
    "postcode"=> 3976,
    "name"=> "Hangatiki",
    "lat"=> -38.2536,
    "lng"=> 175.2194
  ],
  [
    "postcode"=> 3977,
    "name"=> "Hangatiki",
    "lat"=> -38.2342,
    "lng"=> 175.1494
  ],
  [
    "postcode"=> 3978,
    "name"=> "Mahoenui",
    "lat"=> -38.5532,
    "lng"=> 174.8526
  ],
  [
    "postcode"=> 3979,
    "name"=> "Waitewhenua",
    "lat"=> -38.6529,
    "lng"=> 174.9724
  ],
  [
    "postcode"=> 3980,
    "name"=> "Tatu",
    "lat"=> -38.8981,
    "lng"=> 174.9306
  ],
  [
    "postcode"=> 3981,
    "name"=> "Arapae",
    "lat"=> -38.4221,
    "lng"=> 175.1026
  ],
  [
    "postcode"=> 3982,
    "name"=> "Puketutu",
    "lat"=> -38.3574,
    "lng"=> 175.2779
  ],
  [
    "postcode"=> 3983,
    "name"=> "Kopaki",
    "lat"=> -38.4905,
    "lng"=> 175.2096
  ],
  [
    "postcode"=> 3985,
    "name"=> "Waitanguru",
    "lat"=> -38.4444,
    "lng"=> 174.901
  ],
  [
    "postcode"=> 3986,
    "name"=> "Te Kuiti",
    "lat"=> -38.2923,
    "lng"=> 175.1961
  ],
  [
    "postcode"=> 3987,
    "name"=> "Benneydale",
    "lat"=> -38.4993,
    "lng"=> 175.512
  ],
  [
    "postcode"=> 3988,
    "name"=> "Owhiro",
    "lat"=> -38.2017,
    "lng"=> 174.8979
  ],
  [
    "postcode"=> 3989,
    "name"=> "Oio",
    "lat"=> -39.0775,
    "lng"=> 175.4805
  ],
  [
    "postcode"=> 3990,
    "name"=> "Upper Ruatiti",
    "lat"=> -39.1517,
    "lng"=> 175.2283
  ],
  [
    "postcode"=> 3991,
    "name"=> "Aukopae",
    "lat"=> -38.9138,
    "lng"=> 175.1103
  ],
  [
    "postcode"=> 3992,
    "name"=> "Hikumutu",
    "lat"=> -38.9432,
    "lng"=> 175.2684
  ],
  [
    "postcode"=> 3993,
    "name"=> "Kirikau",
    "lat"=> -39.0204,
    "lng"=> 175.1751
  ],
  [
    "postcode"=> 3994,
    "name"=> "Ngapuke",
    "lat"=> -38.8637,
    "lng"=> 175.4107
  ],
  [
    "postcode"=> 3995,
    "name"=> "Matiere",
    "lat"=> -38.7469,
    "lng"=> 175.0987
  ],
  [
    "postcode"=> 3996,
    "name"=> "Okahukura",
    "lat"=> -38.8026,
    "lng"=> 175.2329
  ],
  [
    "postcode"=> 3997,
    "name"=> "Ongarue",
    "lat"=> -38.7362,
    "lng"=> 175.3731
  ],
  [
    "postcode"=> 3998,
    "name"=> "Waimiha",
    "lat"=> -38.6202,
    "lng"=> 175.3978
  ],
  [
    "postcode"=> 4010,
    "name"=> "Kaiti",
    "lat"=> -38.6673,
    "lng"=> 178.0286
  ],
  [
    "postcode"=> 4022,
    "name"=> "Te Karaka",
    "lat"=> -38.4678,
    "lng"=> 177.866
  ],
  [
    "postcode"=> 4032,
    "name"=> "Ruatoria",
    "lat"=> -37.8926,
    "lng"=> 178.32
  ],
  [
    "postcode"=> 4040,
    "name"=> "Kaiti",
    "lat"=> -38.6641,
    "lng"=> 178.025
  ],
  [
    "postcode"=> 4041,
    "name"=> "Tamarau",
    "lat"=> -38.6751,
    "lng"=> 178.0449
  ],
  [
    "postcode"=> 4042,
    "name"=> "Te Karaka",
    "lat"=> -38.4695,
    "lng"=> 177.8638
  ],
  [
    "postcode"=> 4043,
    "name"=> "Ruatoria",
    "lat"=> -37.8902,
    "lng"=> 178.3191
  ],
  [
    "postcode"=> 4044,
    "name"=> "Tiniroto",
    "lat"=> -38.7742,
    "lng"=> 177.5665
  ],
  [
    "postcode"=> 4045,
    "name"=> "Patutahi",
    "lat"=> -38.624,
    "lng"=> 177.8904
  ],
  [
    "postcode"=> 4046,
    "name"=> "Tolaga Bay",
    "lat"=> -38.3727,
    "lng"=> 178.2971
  ],
  [
    "postcode"=> 4047,
    "name"=> "Tokomaru",
    "lat"=> -38.1303,
    "lng"=> 178.315
  ],
  [
    "postcode"=> 4048,
    "name"=> "Te Puia",
    "lat"=> -38.0558,
    "lng"=> 178.3065
  ],
  [
    "postcode"=> 4049,
    "name"=> "Rangitukia",
    "lat"=> -37.7963,
    "lng"=> 178.4107
  ],
  [
    "postcode"=> 4050,
    "name"=> "Te Araroa",
    "lat"=> -37.6336,
    "lng"=> 178.3694
  ],
  [
    "postcode"=> 4051,
    "name"=> "Matawai",
    "lat"=> -38.3576,
    "lng"=> 177.5345
  ],
  [
    "postcode"=> 4052,
    "name"=> "Motu",
    "lat"=> -38.2569,
    "lng"=> 177.548
  ],
  [
    "postcode"=> 4071,
    "name"=> "Waerengaahika",
    "lat"=> -38.576,
    "lng"=> 177.9556
  ],
  [
    "postcode"=> 4072,
    "name"=> "Pehiri",
    "lat"=> -38.66,
    "lng"=> 177.623
  ],
  [
    "postcode"=> 4073,
    "name"=> "Whangara",
    "lat"=> -38.5389,
    "lng"=> 178.1478
  ],
  [
    "postcode"=> 4075,
    "name"=> "Rakauroa",
    "lat"=> -38.426,
    "lng"=> 177.4494
  ],
  [
    "postcode"=> 4077,
    "name"=> "Tolaga Bay",
    "lat"=> -38.2809,
    "lng"=> 178.1521
  ],
  [
    "postcode"=> 4078,
    "name"=> "Tiniroto",
    "lat"=> -38.8266,
    "lng"=> 177.6647
  ],
  [
    "postcode"=> 4079,
    "name"=> "Tokomaru",
    "lat"=> -38.1207,
    "lng"=> 178.1236
  ],
  [
    "postcode"=> 4081,
    "name"=> "Waitakaro",
    "lat"=> -37.9512,
    "lng"=> 178.2583
  ],
  [
    "postcode"=> 4082,
    "name"=> "Ruatoria",
    "lat"=> -37.863,
    "lng"=> 178.3902
  ],
  [
    "postcode"=> 4083,
    "name"=> "Pakihiroa",
    "lat"=> -37.8432,
    "lng"=> 178.165
  ],
  [
    "postcode"=> 4086,
    "name"=> "Whakaangiangi",
    "lat"=> -37.7749,
    "lng"=> 178.355
  ],
  [
    "postcode"=> 4087,
    "name"=> "Te Araroa",
    "lat"=> -37.6653,
    "lng"=> 178.3229
  ],
  [
    "postcode"=> 4091,
    "name"=> "Ahititi",
    "lat"=> -38.365,
    "lng"=> 177.9638
  ],
  [
    "postcode"=> 4092,
    "name"=> "Otoko",
    "lat"=> -38.3389,
    "lng"=> 177.6316
  ],
  [
    "postcode"=> 4093,
    "name"=> "Te Karaka",
    "lat"=> -38.4817,
    "lng"=> 177.8868
  ],
  [
    "postcode"=> 4094,
    "name"=> "Whatatutu",
    "lat"=> -38.3066,
    "lng"=> 177.8006
  ],
  [
    "postcode"=> 4102,
    "name"=> "Haumoana",
    "lat"=> -39.613,
    "lng"=> 176.9451
  ],
  [
    "postcode"=> 4104,
    "name"=> "Petane",
    "lat"=> -39.4185,
    "lng"=> 176.8765
  ],
  [
    "postcode"=> 4108,
    "name"=> "North Clyde",
    "lat"=> -39.0278,
    "lng"=> 177.4054
  ],
  [
    "postcode"=> 4110,
    "name"=> "Onekawa",
    "lat"=> -39.498,
    "lng"=> 176.8799
  ],
  [
    "postcode"=> 4112,
    "name"=> "Puk-kura",
    "lat"=> -39.5268,
    "lng"=> 176.8511
  ],
  [
    "postcode"=> 4120,
    "name"=> "HASTINGS",
    "lat"=> -39.6279,
    "lng"=> 176.8137
  ],
  [
    "postcode"=> 4122,
    "name"=> "Akina",
    "lat"=> -39.6462,
    "lng"=> 176.8502
  ],
  [
    "postcode"=> 4130,
    "name"=> "Havelock North",
    "lat"=> -39.6806,
    "lng"=> 176.8867
  ],
  [
    "postcode"=> 4140,
    "name"=> "Onekawa",
    "lat"=> -39.4909,
    "lng"=> 176.9182
  ],
  [
    "postcode"=> 4141,
    "name"=> "Taradale",
    "lat"=> -39.5357,
    "lng"=> 176.8487
  ],
  [
    "postcode"=> 4142,
    "name"=> "Pirimai",
    "lat"=> -39.5073,
    "lng"=> 176.8821
  ],
  [
    "postcode"=> 4143,
    "name"=> "Napier South",
    "lat"=> -39.5002,
    "lng"=> 176.9031
  ],
  [
    "postcode"=> 4144,
    "name"=> "Ahuriri",
    "lat"=> -39.4809,
    "lng"=> 176.8981
  ],
  [
    "postcode"=> 4145,
    "name"=> "Greenmeadows",
    "lat"=> -39.5264,
    "lng"=> 176.8614
  ],
  [
    "postcode"=> 4146,
    "name"=> "Pirimai",
    "lat"=> -39.5081,
    "lng"=> 176.8868
  ],
  [
    "postcode"=> 4147,
    "name"=> "Tamatea",
    "lat"=> -39.5106,
    "lng"=> 176.8696
  ],
  [
    "postcode"=> 4148,
    "name"=> "Clive",
    "lat"=> -39.5839,
    "lng"=> 176.9148
  ],
  [
    "postcode"=> 4149,
    "name"=> "Petane",
    "lat"=> -39.419,
    "lng"=> 176.8665
  ],
  [
    "postcode"=> 4153,
    "name"=> "Saint Leonards",
    "lat"=> -39.6312,
    "lng"=> 176.8314
  ],
  [
    "postcode"=> 4154,
    "name"=> "Flaxmere",
    "lat"=> -39.6254,
    "lng"=> 176.7866
  ],
  [
    "postcode"=> 4155,
    "name"=> "Parkvale",
    "lat"=> -39.6284,
    "lng"=> 176.8498
  ],
  [
    "postcode"=> 4156,
    "name"=> "Parkvale",
    "lat"=> -39.6408,
    "lng"=> 176.8425
  ],
  [
    "postcode"=> 4157,
    "name"=> "Havelock North",
    "lat"=> -39.6694,
    "lng"=> 176.8783
  ],
  [
    "postcode"=> 4158,
    "name"=> "Parkvale",
    "lat"=> -39.6396,
    "lng"=> 176.8466
  ],
  [
    "postcode"=> 4159,
    "name"=> "Mayfair",
    "lat"=> -39.6378,
    "lng"=> 176.8527
  ],
  [
    "postcode"=> 4160,
    "name"=> "Wairoa",
    "lat"=> -39.0333,
    "lng"=> 177.419
  ],
  [
    "postcode"=> 4161,
    "name"=> "Pahowai",
    "lat"=> -39.6038,
    "lng"=> 176.8928
  ],
  [
    "postcode"=> 4162,
    "name"=> "Tutira",
    "lat"=> -39.204,
    "lng"=> 176.8815
  ],
  [
    "postcode"=> 4163,
    "name"=> "Frasertown",
    "lat"=> -38.9666,
    "lng"=> 177.4102
  ],
  [
    "postcode"=> 4164,
    "name"=> "Tuai",
    "lat"=> -38.8194,
    "lng"=> 177.1439
  ],
  [
    "postcode"=> 4165,
    "name"=> "Nuhaka",
    "lat"=> -39.0425,
    "lng"=> 177.7403
  ],
  [
    "postcode"=> 4166,
    "name"=> "Mahia",
    "lat"=> -39.0869,
    "lng"=> 177.9185
  ],
  [
    "postcode"=> 4171,
    "name"=> "Blackburn",
    "lat"=> -39.8865,
    "lng"=> 176.292
  ],
  [
    "postcode"=> 4172,
    "name"=> "Te Awanga",
    "lat"=> -39.6588,
    "lng"=> 176.9405
  ],
  [
    "postcode"=> 4174,
    "name"=> "Te Hauke",
    "lat"=> -39.7357,
    "lng"=> 176.6463
  ],
  [
    "postcode"=> 4175,
    "name"=> "Flaxmere",
    "lat"=> -39.6232,
    "lng"=> 176.7618
  ],
  [
    "postcode"=> 4178,
    "name"=> "Poukawa",
    "lat"=> -39.772,
    "lng"=> 176.7544
  ],
  [
    "postcode"=> 4179,
    "name"=> "Puketitiri",
    "lat"=> -39.3382,
    "lng"=> 176.3562
  ],
  [
    "postcode"=> 4181,
    "name"=> "Tutira",
    "lat"=> -39.2293,
    "lng"=> 176.9144
  ],
  [
    "postcode"=> 4182,
    "name"=> "Te Pohue",
    "lat"=> -39.1869,
    "lng"=> 176.6716
  ],
  [
    "postcode"=> 4183,
    "name"=> "Taradale",
    "lat"=> -39.5439,
    "lng"=> 176.8176
  ],
  [
    "postcode"=> 4184,
    "name"=> "Puketitiri",
    "lat"=> -39.3052,
    "lng"=> 176.5916
  ],
  [
    "postcode"=> 4186,
    "name"=> "Patoka",
    "lat"=> -39.3916,
    "lng"=> 176.631
  ],
  [
    "postcode"=> 4188,
    "name"=> "Kotemaori",
    "lat"=> -39.0869,
    "lng"=> 177.0019
  ],
  [
    "postcode"=> 4189,
    "name"=> "Kotemaori",
    "lat"=> -38.9813,
    "lng"=> 176.9956
  ],
  [
    "postcode"=> 4191,
    "name"=> "Raupunga",
    "lat"=> -38.9822,
    "lng"=> 177.2238
  ],
  [
    "postcode"=> 4193,
    "name"=> "Wairoa",
    "lat"=> -39.0076,
    "lng"=> 177.343
  ],
  [
    "postcode"=> 4195,
    "name"=> "Tuai",
    "lat"=> -38.7742,
    "lng"=> 177.2002
  ],
  [
    "postcode"=> 4196,
    "name"=> "Opoiti",
    "lat"=> -38.9559,
    "lng"=> 177.5408
  ],
  [
    "postcode"=> 4197,
    "name"=> "Raupunga",
    "lat"=> -39.063,
    "lng"=> 177.2422
  ],
  [
    "postcode"=> 4198,
    "name"=> "Waitaniwha",
    "lat"=> -39.0721,
    "lng"=> 177.8021
  ],
  [
    "postcode"=> 4200,
    "name"=> "Waipukurau",
    "lat"=> -39.9973,
    "lng"=> 176.5591
  ],
  [
    "postcode"=> 4202,
    "name"=> "Otane",
    "lat"=> -39.898,
    "lng"=> 176.6275
  ],
  [
    "postcode"=> 4203,
    "name"=> "Takapau",
    "lat"=> -40.0222,
    "lng"=> 176.3499
  ],
  [
    "postcode"=> 4210,
    "name"=> "Waipawa",
    "lat"=> -39.9289,
    "lng"=> 176.5855
  ],
  [
    "postcode"=> 4240,
    "name"=> "Waipawa",
    "lat"=> -39.9442,
    "lng"=> 176.5868
  ],
  [
    "postcode"=> 4241,
    "name"=> "Otane",
    "lat"=> -39.8966,
    "lng"=> 176.6287
  ],
  [
    "postcode"=> 4242,
    "name"=> "Waipukurau",
    "lat"=> -39.9954,
    "lng"=> 176.5563
  ],
  [
    "postcode"=> 4243,
    "name"=> "Takapau",
    "lat"=> -40.0282,
    "lng"=> 176.3484
  ],
  [
    "postcode"=> 4244,
    "name"=> "Onga Onga",
    "lat"=> -39.913,
    "lng"=> 176.4185
  ],
  [
    "postcode"=> 4245,
    "name"=> "Porangahau",
    "lat"=> -40.3024,
    "lng"=> 176.6101
  ],
  [
    "postcode"=> 4271,
    "name"=> "Patangata",
    "lat"=> -40.0457,
    "lng"=> 176.7528
  ],
  [
    "postcode"=> 4272,
    "name"=> "Ruataniwha",
    "lat"=> -39.9184,
    "lng"=> 176.5078
  ],
  [
    "postcode"=> 4273,
    "name"=> "Argyll East",
    "lat"=> -39.8672,
    "lng"=> 176.5055
  ],
  [
    "postcode"=> 4274,
    "name"=> "Tikokino",
    "lat"=> -39.7684,
    "lng"=> 176.3925
  ],
  [
    "postcode"=> 4275,
    "name"=> "Waipawa",
    "lat"=> -39.9644,
    "lng"=> 176.5872
  ],
  [
    "postcode"=> 4276,
    "name"=> "Pukehou",
    "lat"=> -39.8517,
    "lng"=> 176.5974
  ],
  [
    "postcode"=> 4277,
    "name"=> "Patangata",
    "lat"=> -39.8817,
    "lng"=> 176.7075
  ],
  [
    "postcode"=> 4278,
    "name"=> "Blackburn",
    "lat"=> -39.8701,
    "lng"=> 176.3054
  ],
  [
    "postcode"=> 4279,
    "name"=> "Blackburn",
    "lat"=> -39.8275,
    "lng"=> 176.332
  ],
  [
    "postcode"=> 4281,
    "name"=> "Maharakeke",
    "lat"=> -40.0431,
    "lng"=> 176.4391
  ],
  [
    "postcode"=> 4282,
    "name"=> "Wanstead",
    "lat"=> -40.1206,
    "lng"=> 176.4449
  ],
  [
    "postcode"=> 4283,
    "name"=> "Ruataniwha",
    "lat"=> -39.9519,
    "lng"=> 176.4761
  ],
  [
    "postcode"=> 4284,
    "name"=> "Wallingford",
    "lat"=> -40.1509,
    "lng"=> 176.6552
  ],
  [
    "postcode"=> 4285,
    "name"=> "Waipukurau",
    "lat"=> -40.007,
    "lng"=> 176.623
  ],
  [
    "postcode"=> 4286,
    "name"=> "Ashley Clinton",
    "lat"=> -39.9488,
    "lng"=> 176.2699
  ],
  [
    "postcode"=> 4287,
    "name"=> "Norsewood",
    "lat"=> -40.0526,
    "lng"=> 176.2563
  ],
  [
    "postcode"=> 4288,
    "name"=> "Maharakeke",
    "lat"=> -39.9737,
    "lng"=> 176.402
  ],
  [
    "postcode"=> 4291,
    "name"=> "Porangahau",
    "lat"=> -40.3086,
    "lng"=> 176.5164
  ],
  [
    "postcode"=> 4292,
    "name"=> "Porangahau",
    "lat"=> -40.3623,
    "lng"=> 176.5533
  ],
  [
    "postcode"=> 4293,
    "name"=> "Porangahau",
    "lat"=> -40.3501,
    "lng"=> 176.6208
  ],
  [
    "postcode"=> 4294,
    "name"=> "Waimarama",
    "lat"=> -39.7714,
    "lng"=> 176.9602
  ],
  [
    "postcode"=> 4295,
    "name"=> "Elsthorpe",
    "lat"=> -39.8729,
    "lng"=> 176.8721
  ],
  [
    "postcode"=> 4310,
    "name"=> "Westown",
    "lat"=> -39.0724,
    "lng"=> 174.0554
  ],
  [
    "postcode"=> 4312,
    "name"=> "Fitzroy",
    "lat"=> -39.052,
    "lng"=> 174.1192
  ],
  [
    "postcode"=> 4314,
    "name"=> "Oakura",
    "lat"=> -39.115,
    "lng"=> 173.9504
  ],
  [
    "postcode"=> 4320,
    "name"=> "Waitara",
    "lat"=> -38.9976,
    "lng"=> 174.2367
  ],
  [
    "postcode"=> 4322,
    "name"=> "Eltham",
    "lat"=> -39.429,
    "lng"=> 174.2996
  ],
  [
    "postcode"=> 4330,
    "name"=> "Inglewood",
    "lat"=> -39.157,
    "lng"=> 174.2029
  ],
  [
    "postcode"=> 4332,
    "name"=> "Stratford",
    "lat"=> -39.3378,
    "lng"=> 174.2802
  ],
  [
    "postcode"=> 4335,
    "name"=> "Okato",
    "lat"=> -39.1936,
    "lng"=> 173.8777
  ],
  [
    "postcode"=> 4340,
    "name"=> "Brooklands",
    "lat"=> -39.0586,
    "lng"=> 174.0682
  ],
  [
    "postcode"=> 4341,
    "name"=> "Fitzroy",
    "lat"=> -39.05,
    "lng"=> 174.1017
  ],
  [
    "postcode"=> 4342,
    "name"=> "New Plymouth",
    "lat"=> -39.0572,
    "lng"=> 174.0741
  ],
  [
    "postcode"=> 4343,
    "name"=> "Frankleigh Park",
    "lat"=> -39.0708,
    "lng"=> 174.0628
  ],
  [
    "postcode"=> 4344,
    "name"=> "Moturoa",
    "lat"=> -39.0628,
    "lng"=> 174.0452
  ],
  [
    "postcode"=> 4345,
    "name"=> "Oakura",
    "lat"=> -39.1156,
    "lng"=> 173.9574
  ],
  [
    "postcode"=> 4346,
    "name"=> "Waitara",
    "lat"=> -39.0021,
    "lng"=> 174.2361
  ],
  [
    "postcode"=> 4347,
    "name"=> "Inglewood",
    "lat"=> -39.1568,
    "lng"=> 174.2072
  ],
  [
    "postcode"=> 4348,
    "name"=> "Okato",
    "lat"=> -39.191,
    "lng"=> 173.8794
  ],
  [
    "postcode"=> 4349,
    "name"=> "Urenui",
    "lat"=> -38.9979,
    "lng"=> 174.3907
  ],
  [
    "postcode"=> 4350,
    "name"=> "Mokau",
    "lat"=> -38.6991,
    "lng"=> 174.621
  ],
  [
    "postcode"=> 4351,
    "name"=> "Bell Block",
    "lat"=> -39.0355,
    "lng"=> 174.1432
  ],
  [
    "postcode"=> 4352,
    "name"=> "Stratford",
    "lat"=> -39.3393,
    "lng"=> 174.283
  ],
  [
    "postcode"=> 4353,
    "name"=> "Eltham",
    "lat"=> -39.4298,
    "lng"=> 174.2994
  ],
  [
    "postcode"=> 4360,
    "name"=> "Merrilands",
    "lat"=> -39.0666,
    "lng"=> 174.1028
  ],
  [
    "postcode"=> 4371,
    "name"=> "Mangorei",
    "lat"=> -39.1487,
    "lng"=> 174.0832
  ],
  [
    "postcode"=> 4372,
    "name"=> "Hillsborough",
    "lat"=> -39.0917,
    "lng"=> 174.1506
  ],
  [
    "postcode"=> 4373,
    "name"=> "Lepperton",
    "lat"=> -39.0487,
    "lng"=> 174.2197
  ],
  [
    "postcode"=> 4374,
    "name"=> "Oakura",
    "lat"=> -39.1464,
    "lng"=> 173.9507
  ],
  [
    "postcode"=> 4375,
    "name"=> "Okoki",
    "lat"=> -39.0529,
    "lng"=> 174.5077
  ],
  [
    "postcode"=> 4376,
    "name"=> "Awakino",
    "lat"=> -38.6827,
    "lng"=> 174.7513
  ],
  [
    "postcode"=> 4377,
    "name"=> "Uruti",
    "lat"=> -38.9421,
    "lng"=> 174.4819
  ],
  [
    "postcode"=> 4378,
    "name"=> "Okau",
    "lat"=> -38.9105,
    "lng"=> 174.6847
  ],
  [
    "postcode"=> 4379,
    "name"=> "Uruti",
    "lat"=> -38.9415,
    "lng"=> 174.5497
  ],
  [
    "postcode"=> 4381,
    "name"=> "Okato",
    "lat"=> -39.2227,
    "lng"=> 173.8774
  ],
  [
    "postcode"=> 4382,
    "name"=> "Sentry Hill",
    "lat"=> -39.0139,
    "lng"=> 174.2171
  ],
  [
    "postcode"=> 4383,
    "name"=> "Tikorangi",
    "lat"=> -39.0507,
    "lng"=> 174.3067
  ],
  [
    "postcode"=> 4386,
    "name"=> "Kaimiro",
    "lat"=> -39.2077,
    "lng"=> 174.1435
  ],
  [
    "postcode"=> 4387,
    "name"=> "Tarata",
    "lat"=> -39.155,
    "lng"=> 174.3796
  ],
  [
    "postcode"=> 4388,
    "name"=> "Te Tawa",
    "lat"=> -39.179,
    "lng"=> 174.2446
  ],
  [
    "postcode"=> 4389,
    "name"=> "Waiongona",
    "lat"=> -39.1194,
    "lng"=> 174.2335
  ],
  [
    "postcode"=> 4390,
    "name"=> "Ratapiko",
    "lat"=> -39.207,
    "lng"=> 174.3078
  ],
  [
    "postcode"=> 4391,
    "name"=> "Dawson Falls",
    "lat"=> -39.3216,
    "lng"=> 174.1399
  ],
  [
    "postcode"=> 4392,
    "name"=> "Puniwhakau",
    "lat"=> -39.3023,
    "lng"=> 174.7213
  ],
  [
    "postcode"=> 4393,
    "name"=> "Huinga",
    "lat"=> -39.3746,
    "lng"=> 174.3671
  ],
  [
    "postcode"=> 4394,
    "name"=> "Tuna",
    "lat"=> -39.2797,
    "lng"=> 174.2974
  ],
  [
    "postcode"=> 4395,
    "name"=> "Matau",
    "lat"=> -39.1747,
    "lng"=> 174.6005
  ],
  [
    "postcode"=> 4396,
    "name"=> "Tahora",
    "lat"=> -39.0278,
    "lng"=> 174.7836
  ],
  [
    "postcode"=> 4397,
    "name"=> "Tahora",
    "lat"=> -38.973,
    "lng"=> 174.7784
  ],
  [
    "postcode"=> 4398,
    "name"=> "Eltham",
    "lat"=> -39.4578,
    "lng"=> 174.3421
  ],
  [
    "postcode"=> 4399,
    "name"=> "Moeroa",
    "lat"=> -39.4309,
    "lng"=> 174.6351
  ],
  [
    "postcode"=> 4410,
    "name"=> "Summerhill",
    "lat"=> -40.3691,
    "lng"=> 175.6288
  ],
  [
    "postcode"=> 4412,
    "name"=> "Awapuni",
    "lat"=> -40.3658,
    "lng"=> 175.5885
  ],
  [
    "postcode"=> 4414,
    "name"=> "Kelvin Grove",
    "lat"=> -40.3326,
    "lng"=> 175.63
  ],
  [
    "postcode"=> 4440,
    "name"=> "Hokowhitu",
    "lat"=> -40.3581,
    "lng"=> 175.6107
  ],
  [
    "postcode"=> 4441,
    "name"=> "Terrace End",
    "lat"=> -40.3476,
    "lng"=> 175.6277
  ],
  [
    "postcode"=> 4442,
    "name"=> "Takaro",
    "lat"=> -40.3454,
    "lng"=> 175.5952
  ],
  [
    "postcode"=> 4443,
    "name"=> "West End",
    "lat"=> -40.3622,
    "lng"=> 175.5984
  ],
  [
    "postcode"=> 4444,
    "name"=> "Terrace End",
    "lat"=> -40.3555,
    "lng"=> 175.6123
  ],
  [
    "postcode"=> 4445,
    "name"=> "Awapuni",
    "lat"=> -40.3726,
    "lng"=> 175.593
  ],
  [
    "postcode"=> 4446,
    "name"=> "Hokowhitu",
    "lat"=> -40.3632,
    "lng"=> 175.6338
  ],
  [
    "postcode"=> 4448,
    "name"=> "Summerhill",
    "lat"=> -40.3874,
    "lng"=> 175.6393
  ],
  [
    "postcode"=> 4470,
    "name"=> "Ashhurst",
    "lat"=> -40.2905,
    "lng"=> 175.7028
  ],
  [
    "postcode"=> 4471,
    "name"=> "Aokautere",
    "lat"=> -40.3626,
    "lng"=> 175.7135
  ],
  [
    "postcode"=> 4473,
    "name"=> "Carnarvon",
    "lat"=> -40.3156,
    "lng"=> 175.3396
  ],
  [
    "postcode"=> 4474,
    "name"=> "Tokomaru",
    "lat"=> -40.5035,
    "lng"=> 175.5456
  ],
  [
    "postcode"=> 4475,
    "name"=> "Kairanga",
    "lat"=> -40.3307,
    "lng"=> 175.5202
  ],
  [
    "postcode"=> 4476,
    "name"=> "Rongotea",
    "lat"=> -40.2862,
    "lng"=> 175.4393
  ],
  [
    "postcode"=> 4477,
    "name"=> "Mangawhata",
    "lat"=> -40.3989,
    "lng"=> 175.4478
  ],
  [
    "postcode"=> 4478,
    "name"=> "Milson",
    "lat"=> -40.2979,
    "lng"=> 175.5922
  ],
  [
    "postcode"=> 4479,
    "name"=> "Sanson",
    "lat"=> -40.2317,
    "lng"=> 175.4358
  ],
  [
    "postcode"=> 4481,
    "name"=> "Bunnythorpe",
    "lat"=> -40.2755,
    "lng"=> 175.6657
  ],
  [
    "postcode"=> 4500,
    "name"=> "Aramoho",
    "lat"=> -39.9146,
    "lng"=> 175.0546
  ],
  [
    "postcode"=> 4501,
    "name"=> "Gonville",
    "lat"=> -39.9401,
    "lng"=> 175.0133
  ],
  [
    "postcode"=> 4510,
    "name"=> "Waverley",
    "lat"=> -39.7628,
    "lng"=> 174.6317
  ],
  [
    "postcode"=> 4520,
    "name"=> "White Tank",
    "lat"=> -39.7584,
    "lng"=> 174.4695
  ],
  [
    "postcode"=> 4540,
    "name"=> "Wanganui",
    "lat"=> -39.9344,
    "lng"=> 175.0532
  ],
  [
    "postcode"=> 4541,
    "name"=> "Wanganui",
    "lat"=> -39.9295,
    "lng"=> 175.0466
  ],
  [
    "postcode"=> 4542,
    "name"=> "Wanganui East",
    "lat"=> -39.9096,
    "lng"=> 175.0533
  ],
  [
    "postcode"=> 4543,
    "name"=> "Gonville",
    "lat"=> -39.9428,
    "lng"=> 175.0226
  ],
  [
    "postcode"=> 4544,
    "name"=> "Waverley",
    "lat"=> -39.7637,
    "lng"=> 174.6343
  ],
  [
    "postcode"=> 4545,
    "name"=> "White Tank",
    "lat"=> -39.756,
    "lng"=> 174.4737
  ],
  [
    "postcode"=> 4547,
    "name"=> "Turakina South",
    "lat"=> -40.0467,
    "lng"=> 175.2138
  ],
  [
    "postcode"=> 4548,
    "name"=> "Ratana",
    "lat"=> -40.0394,
    "lng"=> 175.1756
  ],
  [
    "postcode"=> 4549,
    "name"=> "Waitotara",
    "lat"=> -39.8069,
    "lng"=> 174.734
  ],
  [
    "postcode"=> 4571,
    "name"=> "Brunswick",
    "lat"=> -39.8193,
    "lng"=> 175.0206
  ],
  [
    "postcode"=> 4572,
    "name"=> "Wiritoa",
    "lat"=> -39.9823,
    "lng"=> 175.1162
  ],
  [
    "postcode"=> 4573,
    "name"=> "Kakatahi",
    "lat"=> -39.674,
    "lng"=> 175.2513
  ],
  [
    "postcode"=> 4574,
    "name"=> "Mowhanau",
    "lat"=> -39.8531,
    "lng"=> 174.8924
  ],
  [
    "postcode"=> 4575,
    "name"=> "Upokonui",
    "lat"=> -39.7918,
    "lng"=> 175.1865
  ],
  [
    "postcode"=> 4576,
    "name"=> "Matahiwi",
    "lat"=> -39.6251,
    "lng"=> 175.1175
  ],
  [
    "postcode"=> 4577,
    "name"=> "Kauangaroa",
    "lat"=> -39.8951,
    "lng"=> 175.3413
  ],
  [
    "postcode"=> 4578,
    "name"=> "Ahuiti",
    "lat"=> -39.6577,
    "lng"=> 175.0021
  ],
  [
    "postcode"=> 4581,
    "name"=> "Turakina",
    "lat"=> -40.0084,
    "lng"=> 175.2327
  ],
  [
    "postcode"=> 4582,
    "name"=> "Okoia",
    "lat"=> -39.9118,
    "lng"=> 175.1679
  ],
  [
    "postcode"=> 4584,
    "name"=> "Whakaihuwaka",
    "lat"=> -39.7719,
    "lng"=> 175.0626
  ],
  [
    "postcode"=> 4585,
    "name"=> "Kakatahi",
    "lat"=> -39.599,
    "lng"=> 175.4148
  ],
  [
    "postcode"=> 4586,
    "name"=> "Kakatahi",
    "lat"=> -39.7307,
    "lng"=> 175.4134
  ],
  [
    "postcode"=> 4587,
    "name"=> "Nukumaru",
    "lat"=> -39.8064,
    "lng"=> 174.8084
  ],
  [
    "postcode"=> 4588,
    "name"=> "Tawhiwhi",
    "lat"=> -39.6228,
    "lng"=> 174.8503
  ],
  [
    "postcode"=> 4591,
    "name"=> "Nukuhau",
    "lat"=> -39.6418,
    "lng"=> 174.6316
  ],
  [
    "postcode"=> 4592,
    "name"=> "Mangawhio",
    "lat"=> -39.7201,
    "lng"=> 174.7245
  ],
  [
    "postcode"=> 4597,
    "name"=> "Matukuroa",
    "lat"=> -39.7038,
    "lng"=> 174.5409
  ],
  [
    "postcode"=> 4598,
    "name"=> "Hurleyville",
    "lat"=> -39.6331,
    "lng"=> 174.4858
  ],
  [
    "postcode"=> 4610,
    "name"=> "Hawera",
    "lat"=> -39.5854,
    "lng"=> 174.2825
  ],
  [
    "postcode"=> 4612,
    "name"=> "Manaia",
    "lat"=> -39.5512,
    "lng"=> 174.1251
  ],
  [
    "postcode"=> 4614,
    "name"=> "Normanby",
    "lat"=> -39.5417,
    "lng"=> 174.2726
  ],
  [
    "postcode"=> 4616,
    "name"=> "Opunake",
    "lat"=> -39.4523,
    "lng"=> 173.8572
  ],
  [
    "postcode"=> 4625,
    "name"=> "Ohakune",
    "lat"=> -39.4103,
    "lng"=> 175.4031
  ],
  [
    "postcode"=> 4632,
    "name"=> "Raetihi",
    "lat"=> -39.4286,
    "lng"=> 175.2878
  ],
  [
    "postcode"=> 4640,
    "name"=> "Hawera",
    "lat"=> -39.59,
    "lng"=> 174.2837
  ],
  [
    "postcode"=> 4641,
    "name"=> "Manaia",
    "lat"=> -39.5515,
    "lng"=> 174.125
  ],
  [
    "postcode"=> 4642,
    "name"=> "Kaponga",
    "lat"=> -39.4293,
    "lng"=> 174.1506
  ],
  [
    "postcode"=> 4645,
    "name"=> "Opunake",
    "lat"=> -39.4539,
    "lng"=> 173.8585
  ],
  [
    "postcode"=> 4646,
    "name"=> "Raetihi",
    "lat"=> -39.4259,
    "lng"=> 175.2765
  ],
  [
    "postcode"=> 4660,
    "name"=> "Ohakune",
    "lat"=> -39.4176,
    "lng"=> 175.3987
  ],
  [
    "postcode"=> 4671,
    "name"=> "Okaiawa",
    "lat"=> -39.5534,
    "lng"=> 174.1949
  ],
  [
    "postcode"=> 4672,
    "name"=> "Meremere",
    "lat"=> -39.5673,
    "lng"=> 174.3894
  ],
  [
    "postcode"=> 4673,
    "name"=> "Te Roti",
    "lat"=> -39.4856,
    "lng"=> 174.2924
  ],
  [
    "postcode"=> 4674,
    "name"=> "Ararata",
    "lat"=> -39.528,
    "lng"=> 174.3533
  ],
  [
    "postcode"=> 4675,
    "name"=> "Matapu",
    "lat"=> -39.5066,
    "lng"=> 174.2201
  ],
  [
    "postcode"=> 4678,
    "name"=> "Kaupokonui",
    "lat"=> -39.5139,
    "lng"=> 174.079
  ],
  [
    "postcode"=> 4679,
    "name"=> "Riverlea",
    "lat"=> -39.4256,
    "lng"=> 174.0879
  ],
  [
    "postcode"=> 4681,
    "name"=> "Oaonui",
    "lat"=> -39.3861,
    "lng"=> 173.8804
  ],
  [
    "postcode"=> 4682,
    "name"=> "Waiteika",
    "lat"=> -39.4564,
    "lng"=> 173.9364
  ],
  [
    "postcode"=> 4684,
    "name"=> "Upper Kahui",
    "lat"=> -39.3292,
    "lng"=> 173.8783
  ],
  [
    "postcode"=> 4685,
    "name"=> "Parihaka Pa",
    "lat"=> -39.2896,
    "lng"=> 173.825
  ],
  [
    "postcode"=> 4691,
    "name"=> "Rangataua",
    "lat"=> -39.3296,
    "lng"=> 175.5524
  ],
  [
    "postcode"=> 4694,
    "name"=> "Raetihi",
    "lat"=> -39.477,
    "lng"=> 175.1922
  ],
  [
    "postcode"=> 4696,
    "name"=> "Te Ranga",
    "lat"=> -39.2854,
    "lng"=> 175.1762
  ],
  [
    "postcode"=> 4702,
    "name"=> "FEILDING",
    "lat"=> -40.2171,
    "lng"=> 175.5663
  ],
  [
    "postcode"=> 4710,
    "name"=> "Marton",
    "lat"=> -40.0724,
    "lng"=> 175.3798
  ],
  [
    "postcode"=> 4720,
    "name"=> "Taihape",
    "lat"=> -39.6828,
    "lng"=> 175.7899
  ],
  [
    "postcode"=> 4730,
    "name"=> "Hunterville",
    "lat"=> -39.9337,
    "lng"=> 175.5688
  ],
  [
    "postcode"=> 4740,
    "name"=> "FEILDING",
    "lat"=> -40.2267,
    "lng"=> 175.5654
  ],
  [
    "postcode"=> 4741,
    "name"=> "Marton",
    "lat"=> -40.0668,
    "lng"=> 175.3787
  ],
  [
    "postcode"=> 4742,
    "name"=> "Taihape",
    "lat"=> -39.6773,
    "lng"=> 175.7982
  ],
  [
    "postcode"=> 4744,
    "name"=> "Kimbolton",
    "lat"=> -40.0574,
    "lng"=> 175.7794
  ],
  [
    "postcode"=> 4745,
    "name"=> "Hunterville",
    "lat"=> -39.9371,
    "lng"=> 175.5685
  ],
  [
    "postcode"=> 4746,
    "name"=> "Mangaweka",
    "lat"=> -39.8096,
    "lng"=> 175.7887
  ],
  [
    "postcode"=> 4771,
    "name"=> "Umutoi",
    "lat"=> -39.9989,
    "lng"=> 175.9452
  ],
  [
    "postcode"=> 4774,
    "name"=> "Apiti",
    "lat"=> -39.9336,
    "lng"=> 175.8575
  ],
  [
    "postcode"=> 4775,
    "name"=> "Colyton",
    "lat"=> -40.213,
    "lng"=> 175.6339
  ],
  [
    "postcode"=> 4777,
    "name"=> "Kiwitea",
    "lat"=> -40.0927,
    "lng"=> 175.7361
  ],
  [
    "postcode"=> 4779,
    "name"=> "Waituna West",
    "lat"=> -40.0865,
    "lng"=> 175.5853
  ],
  [
    "postcode"=> 4780,
    "name"=> "Rewa",
    "lat"=> -39.9915,
    "lng"=> 175.6314
  ],
  [
    "postcode"=> 4781,
    "name"=> "Silverhope",
    "lat"=> -39.9088,
    "lng"=> 175.4387
  ],
  [
    "postcode"=> 4782,
    "name"=> "Pukeroa",
    "lat"=> -39.8123,
    "lng"=> 175.5464
  ],
  [
    "postcode"=> 4783,
    "name"=> "Pakihikura",
    "lat"=> -39.9228,
    "lng"=> 175.7
  ],
  [
    "postcode"=> 4784,
    "name"=> "Silverhope",
    "lat"=> -39.9478,
    "lng"=> 175.5212
  ],
  [
    "postcode"=> 4785,
    "name"=> "Poukiore",
    "lat"=> -39.8467,
    "lng"=> 175.6335
  ],
  [
    "postcode"=> 4786,
    "name"=> "Rewa",
    "lat"=> -39.956,
    "lng"=> 175.6052
  ],
  [
    "postcode"=> 4787,
    "name"=> "Cliff Road",
    "lat"=> -40.0513,
    "lng"=> 175.4852
  ],
  [
    "postcode"=> 4788,
    "name"=> "Marton",
    "lat"=> -40.0079,
    "lng"=> 175.3642
  ],
  [
    "postcode"=> 4789,
    "name"=> "Marton",
    "lat"=> -40.0996,
    "lng"=> 175.3098
  ],
  [
    "postcode"=> 4791,
    "name"=> "Raketapauma",
    "lat"=> -39.6042,
    "lng"=> 175.5897
  ],
  [
    "postcode"=> 4792,
    "name"=> "Erehwon",
    "lat"=> -39.5457,
    "lng"=> 175.9477
  ],
  [
    "postcode"=> 4793,
    "name"=> "Ohotu",
    "lat"=> -39.6785,
    "lng"=> 175.9334
  ],
  [
    "postcode"=> 4794,
    "name"=> "Utiku",
    "lat"=> -39.7556,
    "lng"=> 175.8942
  ],
  [
    "postcode"=> 4795,
    "name"=> "Waiouru Military Camp",
    "lat"=> -39.471,
    "lng"=> 175.8029
  ],
  [
    "postcode"=> 4796,
    "name"=> "Taihape",
    "lat"=> -39.7189,
    "lng"=> 175.7107
  ],
  [
    "postcode"=> 4797,
    "name"=> "Ruahine",
    "lat"=> -39.8068,
    "lng"=> 175.8736
  ],
  [
    "postcode"=> 4810,
    "name"=> "Ashhurst",
    "lat"=> -40.2927,
    "lng"=> 175.7565
  ],
  [
    "postcode"=> 4814,
    "name"=> "Foxton",
    "lat"=> -40.4708,
    "lng"=> 175.2874
  ],
  [
    "postcode"=> 4815,
    "name"=> "Foxton Beach",
    "lat"=> -40.464,
    "lng"=> 175.2318
  ],
  [
    "postcode"=> 4816,
    "name"=> "Ohakea",
    "lat"=> -40.2026,
    "lng"=> 175.3808
  ],
  [
    "postcode"=> 4817,
    "name"=> "Sanson",
    "lat"=> -40.2188,
    "lng"=> 175.4228
  ],
  [
    "postcode"=> 4818,
    "name"=> "Bulls",
    "lat"=> -40.1775,
    "lng"=> 175.3863
  ],
  [
    "postcode"=> 4820,
    "name"=> "lngburn",
    "lat"=> -40.4054,
    "lng"=> 175.5855
  ],
  [
    "postcode"=> 4821,
    "name"=> "Shannon",
    "lat"=> -40.5426,
    "lng"=> 175.4182
  ],
  [
    "postcode"=> 4822,
    "name"=> "Tangimoana",
    "lat"=> -40.2929,
    "lng"=> 175.2564
  ],
  [
    "postcode"=> 4825,
    "name"=> "Waiouru Military Camp",
    "lat"=> -39.4009,
    "lng"=> 175.6798
  ],
  [
    "postcode"=> 4826,
    "name"=> "Waiouru Military Camp",
    "lat"=> -39.4712,
    "lng"=> 175.6851
  ],
  [
    "postcode"=> 4847,
    "name"=> "Ashhurst",
    "lat"=> -40.2902,
    "lng"=> 175.7552
  ],
  [
    "postcode"=> 4848,
    "name"=> "Foxton",
    "lat"=> -40.4749,
    "lng"=> 175.2812
  ],
  [
    "postcode"=> 4849,
    "name"=> "Foxton Beach",
    "lat"=> -40.4683,
    "lng"=> 175.2382
  ],
  [
    "postcode"=> 4860,
    "name"=> "Shannon",
    "lat"=> -40.5496,
    "lng"=> 175.4138
  ],
  [
    "postcode"=> 4861,
    "name"=> "Waiouru",
    "lat"=> -39.4759,
    "lng"=> 175.6686
  ],
  [
    "postcode"=> 4862,
    "name"=> "Sanson",
    "lat"=> -40.2198,
    "lng"=> 175.4242
  ],
  [
    "postcode"=> 4863,
    "name"=> "Bulls",
    "lat"=> -40.174,
    "lng"=> 175.3864
  ],
  [
    "postcode"=> 4864,
    "name"=> "Tokomaru",
    "lat"=> -40.4745,
    "lng"=> 175.5097
  ],
  [
    "postcode"=> 4865,
    "name"=> "Rongotea",
    "lat"=> -40.293,
    "lng"=> 175.4249
  ],
  [
    "postcode"=> 4866,
    "name"=> "lngburn",
    "lat"=> -40.383,
    "lng"=> 175.5453
  ],
  [
    "postcode"=> 4867,
    "name"=> "Bunnythorpe",
    "lat"=> -40.2825,
    "lng"=> 175.6311
  ],
  [
    "postcode"=> 4884,
    "name"=> "Pohangina",
    "lat"=> -40.1382,
    "lng"=> 175.8664
  ],
  [
    "postcode"=> 4891,
    "name"=> "Himatangi",
    "lat"=> -40.3928,
    "lng"=> 175.2974
  ],
  [
    "postcode"=> 4893,
    "name"=> "Marotiri",
    "lat"=> -40.485,
    "lng"=> 175.3291
  ],
  [
    "postcode"=> 4894,
    "name"=> "Bulls",
    "lat"=> -40.1941,
    "lng"=> 175.293
  ],
  [
    "postcode"=> 4900,
    "name"=> "Eketahuna",
    "lat"=> -40.6497,
    "lng"=> 175.7023
  ],
  [
    "postcode"=> 4910,
    "name"=> "Pahiatua",
    "lat"=> -40.4548,
    "lng"=> 175.8395
  ],
  [
    "postcode"=> 4920,
    "name"=> "Woodville",
    "lat"=> -40.3337,
    "lng"=> 175.8663
  ],
  [
    "postcode"=> 4930,
    "name"=> "Dannevirke",
    "lat"=> -40.209,
    "lng"=> 176.1022
  ],
  [
    "postcode"=> 4940,
    "name"=> "Eketahuna",
    "lat"=> -40.6454,
    "lng"=> 175.7031
  ],
  [
    "postcode"=> 4941,
    "name"=> "Pahiatua",
    "lat"=> -40.4521,
    "lng"=> 175.8422
  ],
  [
    "postcode"=> 4942,
    "name"=> "Dannevirke",
    "lat"=> -40.2098,
    "lng"=> 176.0966
  ],
  [
    "postcode"=> 4943,
    "name"=> "Norsewood",
    "lat"=> -40.0694,
    "lng"=> 176.2163
  ],
  [
    "postcode"=> 4944,
    "name"=> "Pongaroa",
    "lat"=> -40.5426,
    "lng"=> 176.1906
  ],
  [
    "postcode"=> 4945,
    "name"=> "Woodville",
    "lat"=> -40.3378,
    "lng"=> 175.8689
  ],
  [
    "postcode"=> 4970,
    "name"=> "Ti-tree Point",
    "lat"=> -40.3934,
    "lng"=> 176.4123
  ],
  [
    "postcode"=> 4971,
    "name"=> "Waipatiki",
    "lat"=> -40.355,
    "lng"=> 176.1816
  ],
  [
    "postcode"=> 4972,
    "name"=> "Kiritaki",
    "lat"=> -40.2398,
    "lng"=> 176.006
  ],
  [
    "postcode"=> 4973,
    "name"=> "Waipatiki",
    "lat"=> -40.2554,
    "lng"=> 176.2901
  ],
  [
    "postcode"=> 4974,
    "name"=> "Ngamoko",
    "lat"=> -40.0491,
    "lng"=> 176.1951
  ],
  [
    "postcode"=> 4975,
    "name"=> "Tahoraiti",
    "lat"=> -40.3018,
    "lng"=> 176.0999
  ],
  [
    "postcode"=> 4976,
    "name"=> "Raumati",
    "lat"=> -40.1995,
    "lng"=> 176.1673
  ],
  [
    "postcode"=> 4977,
    "name"=> "Makotuku",
    "lat"=> -40.124,
    "lng"=> 176.2232
  ],
  [
    "postcode"=> 4978,
    "name"=> "Piripiri",
    "lat"=> -40.141,
    "lng"=> 176.0556
  ],
  [
    "postcode"=> 4979,
    "name"=> "Waione",
    "lat"=> -40.4364,
    "lng"=> 176.301
  ],
  [
    "postcode"=> 4981,
    "name"=> "Kaitawa",
    "lat"=> -40.5303,
    "lng"=> 175.8807
  ],
  [
    "postcode"=> 4982,
    "name"=> "Ngaturi",
    "lat"=> -40.4288,
    "lng"=> 175.9762
  ],
  [
    "postcode"=> 4983,
    "name"=> "Ballance",
    "lat"=> -40.3984,
    "lng"=> 175.767
  ],
  [
    "postcode"=> 4984,
    "name"=> "Konini",
    "lat"=> -40.5081,
    "lng"=> 175.7673
  ],
  [
    "postcode"=> 4985,
    "name"=> "Makuri",
    "lat"=> -40.4982,
    "lng"=> 176.0472
  ],
  [
    "postcode"=> 4986,
    "name"=> "Haunui",
    "lat"=> -40.5926,
    "lng"=> 175.958
  ],
  [
    "postcode"=> 4987,
    "name"=> "Konini",
    "lat"=> -40.522,
    "lng"=> 175.7835
  ],
  [
    "postcode"=> 4988,
    "name"=> "Mangamaire",
    "lat"=> -40.5086,
    "lng"=> 175.6967
  ],
  [
    "postcode"=> 4989,
    "name"=> "Makuri",
    "lat"=> -40.5302,
    "lng"=> 176.0492
  ],
  [
    "postcode"=> 4990,
    "name"=> "Waimiro",
    "lat"=> -40.4787,
    "lng"=> 176.1541
  ],
  [
    "postcode"=> 4991,
    "name"=> "Aohanga",
    "lat"=> -40.6126,
    "lng"=> 176.3283
  ],
  [
    "postcode"=> 4992,
    "name"=> "Waione",
    "lat"=> -40.5133,
    "lng"=> 176.2536
  ],
  [
    "postcode"=> 4993,
    "name"=> "Tane",
    "lat"=> -40.5785,
    "lng"=> 175.791
  ],
  [
    "postcode"=> 4994,
    "name"=> "Eketahuna",
    "lat"=> -40.6634,
    "lng"=> 175.6726
  ],
  [
    "postcode"=> 4995,
    "name"=> "Nireaha",
    "lat"=> -40.5863,
    "lng"=> 175.6436
  ],
  [
    "postcode"=> 4996,
    "name"=> "Tiraumea",
    "lat"=> -40.6679,
    "lng"=> 175.9936
  ],
  [
    "postcode"=> 4997,
    "name"=> "Hopelands",
    "lat"=> -40.3626,
    "lng"=> 175.9944
  ],
  [
    "postcode"=> 4998,
    "name"=> "Papatawa",
    "lat"=> -40.2855,
    "lng"=> 175.9108
  ],
  [
    "postcode"=> 4999,
    "name"=> "Woodville",
    "lat"=> -40.3205,
    "lng"=> 175.834
  ],
  [
    "postcode"=> 5010,
    "name"=> "Avalon",
    "lat"=> -41.1971,
    "lng"=> 174.9198
  ],
  [
    "postcode"=> 5011,
    "name"=> "Taita",
    "lat"=> -41.1943,
    "lng"=> 174.9522
  ],
  [
    "postcode"=> 5012,
    "name"=> "Petone",
    "lat"=> -41.2338,
    "lng"=> 174.8757
  ],
  [
    "postcode"=> 5013,
    "name"=> "Muritai",
    "lat"=> -41.3154,
    "lng"=> 174.8854
  ],
  [
    "postcode"=> 5014,
    "name"=> "Wainuiomata",
    "lat"=> -41.2634,
    "lng"=> 174.9483
  ],
  [
    "postcode"=> 5016,
    "name"=> "Korokoro",
    "lat"=> -41.2128,
    "lng"=> 174.8499
  ],
  [
    "postcode"=> 5018,
    "name"=> "Maoribank",
    "lat"=> -41.1195,
    "lng"=> 175.0935
  ],
  [
    "postcode"=> 5019,
    "name"=> "Pinehaven",
    "lat"=> -41.1703,
    "lng"=> 174.9949
  ],
  [
    "postcode"=> 5022,
    "name"=> "Elsdon",
    "lat"=> -41.1245,
    "lng"=> 174.8198
  ],
  [
    "postcode"=> 5024,
    "name"=> "Waitangirua",
    "lat"=> -41.1304,
    "lng"=> 174.8788
  ],
  [
    "postcode"=> 5026,
    "name"=> "Karehana Bay",
    "lat"=> -41.0629,
    "lng"=> 174.8705
  ],
  [
    "postcode"=> 5028,
    "name"=> "Grenada North",
    "lat"=> -41.175,
    "lng"=> 174.8503
  ],
  [
    "postcode"=> 5032,
    "name"=> "Paraparaumu Beach",
    "lat"=> -40.8948,
    "lng"=> 174.9695
  ],
  [
    "postcode"=> 5034,
    "name"=> "Paekakariki",
    "lat"=> -40.9769,
    "lng"=> 174.9682
  ],
  [
    "postcode"=> 5036,
    "name"=> "Waikanae",
    "lat"=> -40.8733,
    "lng"=> 175.0507
  ],
  [
    "postcode"=> 5040,
    "name"=> "Alicetown",
    "lat"=> -41.2095,
    "lng"=> 174.9015
  ],
  [
    "postcode"=> 5041,
    "name"=> "Taita",
    "lat"=> -41.1986,
    "lng"=> 174.9476
  ],
  [
    "postcode"=> 5042,
    "name"=> "Lower Hutt",
    "lat"=> -41.2148,
    "lng"=> 174.922
  ],
  [
    "postcode"=> 5043,
    "name"=> "Gracefield",
    "lat"=> -41.2271,
    "lng"=> 174.9053
  ],
  [
    "postcode"=> 5044,
    "name"=> "Naenae",
    "lat"=> -41.1988,
    "lng"=> 174.9358
  ],
  [
    "postcode"=> 5045,
    "name"=> "Petone",
    "lat"=> -41.2253,
    "lng"=> 174.8662
  ],
  [
    "postcode"=> 5046,
    "name"=> "Petone",
    "lat"=> -41.2255,
    "lng"=> 174.8795
  ],
  [
    "postcode"=> 5047,
    "name"=> "Eastbourne",
    "lat"=> -41.2906,
    "lng"=> 174.8985
  ],
  [
    "postcode"=> 5048,
    "name"=> "Wainuiomata",
    "lat"=> -41.2608,
    "lng"=> 174.9457
  ],
  [
    "postcode"=> 5049,
    "name"=> "Homedale",
    "lat"=> -41.2725,
    "lng"=> 174.9546
  ],
  [
    "postcode"=> 5050,
    "name"=> "Maungaraki",
    "lat"=> -41.209,
    "lng"=> 174.8783
  ],
  [
    "postcode"=> 5140,
    "name"=> "Kingsley Heights",
    "lat"=> -41.124,
    "lng"=> 175.0737
  ],
  [
    "postcode"=> 5141,
    "name"=> "Stokes Valley",
    "lat"=> -41.1745,
    "lng"=> 174.9816
  ],
  [
    "postcode"=> 5142,
    "name"=> "Silverstream",
    "lat"=> -41.148,
    "lng"=> 175.012
  ],
  [
    "postcode"=> 5143,
    "name"=> "Trentham",
    "lat"=> -41.138,
    "lng"=> 175.0342
  ],
  [
    "postcode"=> 5240,
    "name"=> "Ranui Heights",
    "lat"=> -41.1349,
    "lng"=> 174.8403
  ],
  [
    "postcode"=> 5242,
    "name"=> "Onepoto",
    "lat"=> -41.1086,
    "lng"=> 174.838
  ],
  [
    "postcode"=> 5243,
    "name"=> "Cannons Creek",
    "lat"=> -41.1416,
    "lng"=> 174.8654
  ],
  [
    "postcode"=> 5245,
    "name"=> "Whitby",
    "lat"=> -41.1178,
    "lng"=> 174.8933
  ],
  [
    "postcode"=> 5247,
    "name"=> "Paremata",
    "lat"=> -41.0933,
    "lng"=> 174.8686
  ],
  [
    "postcode"=> 5249,
    "name"=> "Linden",
    "lat"=> -41.1703,
    "lng"=> 174.8247
  ],
  [
    "postcode"=> 5250,
    "name"=> "Waikanae",
    "lat"=> -40.8767,
    "lng"=> 175.0642
  ],
  [
    "postcode"=> 5252,
    "name"=> "Paraparaumu Beach",
    "lat"=> -40.8939,
    "lng"=> 174.9811
  ],
  [
    "postcode"=> 5254,
    "name"=> "Paraparaumu",
    "lat"=> -40.9165,
    "lng"=> 175.0055
  ],
  [
    "postcode"=> 5255,
    "name"=> "Raumati Beach",
    "lat"=> -40.9189,
    "lng"=> 174.9822
  ],
  [
    "postcode"=> 5258,
    "name"=> "Paekakariki",
    "lat"=> -40.9884,
    "lng"=> 174.9515
  ],
  [
    "postcode"=> 5371,
    "name"=> "Upper Hutt",
    "lat"=> -41.1669,
    "lng"=> 175.088
  ],
  [
    "postcode"=> 5372,
    "name"=> "Kaitoke",
    "lat"=> -41.0519,
    "lng"=> 175.1665
  ],
  [
    "postcode"=> 5373,
    "name"=> "Homedale",
    "lat"=> -41.277,
    "lng"=> 175.0608
  ],
  [
    "postcode"=> 5381,
    "name"=> "Pukerua Bay",
    "lat"=> -41.0521,
    "lng"=> 174.9695
  ],
  [
    "postcode"=> 5391,
    "name"=> "Reikorangi",
    "lat"=> -40.9005,
    "lng"=> 175.1072
  ],
  [
    "postcode"=> 5510,
    "name"=> "Levin",
    "lat"=> -40.5963,
    "lng"=> 175.2511
  ],
  [
    "postcode"=> 5512,
    "name"=> "Otaki",
    "lat"=> -40.7402,
    "lng"=> 175.1404
  ],
  [
    "postcode"=> 5540,
    "name"=> "Levin",
    "lat"=> -40.6222,
    "lng"=> 175.2866
  ],
  [
    "postcode"=> 5541,
    "name"=> "Manakau",
    "lat"=> -40.7124,
    "lng"=> 175.2168
  ],
  [
    "postcode"=> 5542,
    "name"=> "Otaki",
    "lat"=> -40.7526,
    "lng"=> 175.1389
  ],
  [
    "postcode"=> 5543,
    "name"=> "Otaki",
    "lat"=> -40.7621,
    "lng"=> 175.156
  ],
  [
    "postcode"=> 5544,
    "name"=> "Te Horo",
    "lat"=> -40.804,
    "lng"=> 175.1192
  ],
  [
    "postcode"=> 5570,
    "name"=> "Ohau",
    "lat"=> -40.6766,
    "lng"=> 175.2619
  ],
  [
    "postcode"=> 5571,
    "name"=> "Levin",
    "lat"=> -40.621,
    "lng"=> 175.3004
  ],
  [
    "postcode"=> 5572,
    "name"=> "Waitarere",
    "lat"=> -40.5336,
    "lng"=> 175.2628
  ],
  [
    "postcode"=> 5573,
    "name"=> "Manakau",
    "lat"=> -40.7072,
    "lng"=> 175.2017
  ],
  [
    "postcode"=> 5574,
    "name"=> "Waitarere",
    "lat"=> -40.5584,
    "lng"=> 175.2388
  ],
  [
    "postcode"=> 5575,
    "name"=> "Koputaroa",
    "lat"=> -40.5666,
    "lng"=> 175.3219
  ],
  [
    "postcode"=> 5581,
    "name"=> "Manakau",
    "lat"=> -40.7911,
    "lng"=> 175.3732
  ],
  [
    "postcode"=> 5710,
    "name"=> "Featherston",
    "lat"=> -41.1169,
    "lng"=> 175.2937
  ],
  [
    "postcode"=> 5711,
    "name"=> "Martinborough",
    "lat"=> -41.2205,
    "lng"=> 175.4593
  ],
  [
    "postcode"=> 5712,
    "name"=> "Greytown",
    "lat"=> -41.0824,
    "lng"=> 175.4599
  ],
  [
    "postcode"=> 5713,
    "name"=> "Carterton",
    "lat"=> -41.0281,
    "lng"=> 175.5229
  ],
  [
    "postcode"=> 5740,
    "name"=> "Featherston",
    "lat"=> -41.117,
    "lng"=> 175.3248
  ],
  [
    "postcode"=> 5741,
    "name"=> "Martinborough",
    "lat"=> -41.2191,
    "lng"=> 175.4583
  ],
  [
    "postcode"=> 5742,
    "name"=> "Greytown",
    "lat"=> -41.0802,
    "lng"=> 175.4603
  ],
  [
    "postcode"=> 5743,
    "name"=> "Carterton",
    "lat"=> -41.0251,
    "lng"=> 175.5276
  ],
  [
    "postcode"=> 5771,
    "name"=> "Kahutara",
    "lat"=> -41.1711,
    "lng"=> 175.3549
  ],
  [
    "postcode"=> 5772,
    "name"=> "Pirinoa",
    "lat"=> -41.4073,
    "lng"=> 175.2716
  ],
  [
    "postcode"=> 5773,
    "name"=> "Cross Creek",
    "lat"=> -41.2346,
    "lng"=> 175.2043
  ],
  [
    "postcode"=> 5781,
    "name"=> "Pahautea",
    "lat"=> -41.3133,
    "lng"=> 175.3866
  ],
  [
    "postcode"=> 5782,
    "name"=> "Waipawa",
    "lat"=> -41.4322,
    "lng"=> 175.5092
  ],
  [
    "postcode"=> 5783,
    "name"=> "Martinborough",
    "lat"=> -41.1866,
    "lng"=> 175.432
  ],
  [
    "postcode"=> 5784,
    "name"=> "Hinakura",
    "lat"=> -41.3109,
    "lng"=> 175.6286
  ],
  [
    "postcode"=> 5791,
    "name"=> "Carterton",
    "lat"=> -40.9664,
    "lng"=> 175.5024
  ],
  [
    "postcode"=> 5792,
    "name"=> "Parkvale",
    "lat"=> -41.1168,
    "lng"=> 175.5423
  ],
  [
    "postcode"=> 5794,
    "name"=> "Greytown",
    "lat"=> -41.1065,
    "lng"=> 175.4291
  ],
  [
    "postcode"=> 5810,
    "name"=> "Masterton",
    "lat"=> -40.9516,
    "lng"=> 175.6535
  ],
  [
    "postcode"=> 5840,
    "name"=> "Masterton",
    "lat"=> -40.9497,
    "lng"=> 175.6614
  ],
  [
    "postcode"=> 5842,
    "name"=> "Masterton",
    "lat"=> -40.9581,
    "lng"=> 175.6487
  ],
  [
    "postcode"=> 5871,
    "name"=> "Bideford",
    "lat"=> -40.8459,
    "lng"=> 175.8391
  ],
  [
    "postcode"=> 5872,
    "name"=> "Riversdale Beach",
    "lat"=> -41.0602,
    "lng"=> 176.0402
  ],
  [
    "postcode"=> 5881,
    "name"=> "Kopuaranga",
    "lat"=> -40.8215,
    "lng"=> 175.5948
  ],
  [
    "postcode"=> 5882,
    "name"=> "Mauriceville",
    "lat"=> -40.7872,
    "lng"=> 175.7584
  ],
  [
    "postcode"=> 5883,
    "name"=> "Homewood",
    "lat"=> -41.215,
    "lng"=> 175.8129
  ],
  [
    "postcode"=> 5884,
    "name"=> "Gladstone",
    "lat"=> -41.116,
    "lng"=> 175.6531
  ],
  [
    "postcode"=> 5885,
    "name"=> "Masterton",
    "lat"=> -40.983,
    "lng"=> 175.6525
  ],
  [
    "postcode"=> 5886,
    "name"=> "Upper Tauheru",
    "lat"=> -40.8768,
    "lng"=> 175.7476
  ],
  [
    "postcode"=> 5887,
    "name"=> "Clareville",
    "lat"=> -41.0162,
    "lng"=> 175.6125
  ],
  [
    "postcode"=> 5888,
    "name"=> "Solway",
    "lat"=> -40.9051,
    "lng"=> 175.5684
  ],
  [
    "postcode"=> 5889,
    "name"=> "Tinui",
    "lat"=> -40.8671,
    "lng"=> 176.0156
  ],
  [
    "postcode"=> 5890,
    "name"=> "Tauweru",
    "lat"=> -41.0722,
    "lng"=> 175.8675
  ],
  [
    "postcode"=> 5894,
    "name"=> "Tinui",
    "lat"=> -40.7768,
    "lng"=> 176.0837
  ],
  [
    "postcode"=> 6011,
    "name"=> "Mount Victoria",
    "lat"=> -41.2829,
    "lng"=> 174.7842
  ],
  [
    "postcode"=> 6012,
    "name"=> "Karori",
    "lat"=> -41.2869,
    "lng"=> 174.7405
  ],
  [
    "postcode"=> 6021,
    "name"=> "Wellington",
    "lat"=> -41.3052,
    "lng"=> 174.779
  ],
  [
    "postcode"=> 6022,
    "name"=> "Miramar",
    "lat"=> -41.3155,
    "lng"=> 174.8166
  ],
  [
    "postcode"=> 6023,
    "name"=> "Owhiro Bay",
    "lat"=> -41.3375,
    "lng"=> 174.7506
  ],
  [
    "postcode"=> 6035,
    "name"=> "Khandallah",
    "lat"=> -41.2466,
    "lng"=> 174.7942
  ],
  [
    "postcode"=> 6037,
    "name"=> "Ohariu",
    "lat"=> -41.1951,
    "lng"=> 174.7805
  ],
  [
    "postcode"=> 6140,
    "name"=> "Mount Victoria",
    "lat"=> -41.2809,
    "lng"=> 174.7794
  ],
  [
    "postcode"=> 6141,
    "name"=> "Mount Victoria",
    "lat"=> -41.2942,
    "lng"=> 174.7762
  ],
  [
    "postcode"=> 6142,
    "name"=> "Mount Victoria",
    "lat"=> -41.2902,
    "lng"=> 174.7757
  ],
  [
    "postcode"=> 6143,
    "name"=> "Kelburn",
    "lat"=> -41.2825,
    "lng"=> 174.775
  ],
  [
    "postcode"=> 6144,
    "name"=> "Kaiwharawhara",
    "lat"=> -41.2759,
    "lng"=> 174.778
  ],
  [
    "postcode"=> 6145,
    "name"=> "Kelburn",
    "lat"=> -41.2798,
    "lng"=> 174.7761
  ],
  [
    "postcode"=> 6146,
    "name"=> "Mount Victoria",
    "lat"=> -41.2838,
    "lng"=> 174.7763
  ],
  [
    "postcode"=> 6147,
    "name"=> "Karori",
    "lat"=> -41.2851,
    "lng"=> 174.7368
  ],
  [
    "postcode"=> 6148,
    "name"=> "Mount Victoria",
    "lat"=> -41.2842,
    "lng"=> 174.7752
  ],
  [
    "postcode"=> 6149,
    "name"=> "Wellington",
    "lat"=> -41.2942,
    "lng"=> 174.7831
  ],
  [
    "postcode"=> 6150,
    "name"=> "Kelburn",
    "lat"=> -41.289,
    "lng"=> 174.7618
  ],
  [
    "postcode"=> 6160,
    "name"=> "Kelburn",
    "lat"=> -41.2776,
    "lng"=> 174.7775
  ],
  [
    "postcode"=> 6241,
    "name"=> "Rongotai",
    "lat"=> -41.3194,
    "lng"=> 174.7942
  ],
  [
    "postcode"=> 6242,
    "name"=> "Berhampore",
    "lat"=> -41.3133,
    "lng"=> 174.7795
  ],
  [
    "postcode"=> 6243,
    "name"=> "Miramar",
    "lat"=> -41.316,
    "lng"=> 174.8159
  ],
  [
    "postcode"=> 6244,
    "name"=> "Strathmore Park",
    "lat"=> -41.3331,
    "lng"=> 174.8103
  ],
  [
    "postcode"=> 6440,
    "name"=> "Newlands",
    "lat"=> -41.223,
    "lng"=> 174.807
  ],
  [
    "postcode"=> 6441,
    "name"=> "Ngaio",
    "lat"=> -41.2464,
    "lng"=> 174.7909
  ],
  [
    "postcode"=> 6442,
    "name"=> "Newlands",
    "lat"=> -41.2227,
    "lng"=> 174.8229
  ],
  [
    "postcode"=> 6443,
    "name"=> "Ngaio",
    "lat"=> -41.2512,
    "lng"=> 174.773
  ],
  [
    "postcode"=> 6972,
    "name"=> "Karori West",
    "lat"=> -41.2839,
    "lng"=> 174.6811
  ],
  [
    "postcode"=> 7005,
    "name"=> "Mapua",
    "lat"=> -41.2456,
    "lng"=> 173.0951
  ],
  [
    "postcode"=> 7007,
    "name"=> "Murchison",
    "lat"=> -41.8066,
    "lng"=> 172.3292
  ],
  [
    "postcode"=> 7010,
    "name"=> "Nelson East",
    "lat"=> -41.2881,
    "lng"=> 173.3357
  ],
  [
    "postcode"=> 7011,
    "name"=> "Stoke",
    "lat"=> -41.3097,
    "lng"=> 173.2432
  ],
  [
    "postcode"=> 7020,
    "name"=> "Richmond",
    "lat"=> -41.3437,
    "lng"=> 173.185
  ],
  [
    "postcode"=> 7022,
    "name"=> "Brightwater",
    "lat"=> -41.3743,
    "lng"=> 173.1066
  ],
  [
    "postcode"=> 7025,
    "name"=> "Wakefield",
    "lat"=> -41.406,
    "lng"=> 173.0398
  ],
  [
    "postcode"=> 7040,
    "name"=> "Nelson East",
    "lat"=> -41.271,
    "lng"=> 173.2837
  ],
  [
    "postcode"=> 7041,
    "name"=> "Stoke",
    "lat"=> -41.3146,
    "lng"=> 173.2328
  ],
  [
    "postcode"=> 7042,
    "name"=> "Nelson South",
    "lat"=> -41.2744,
    "lng"=> 173.2766
  ],
  [
    "postcode"=> 7043,
    "name"=> "Port Nelson",
    "lat"=> -41.265,
    "lng"=> 173.2774
  ],
  [
    "postcode"=> 7044,
    "name"=> "Wakatu",
    "lat"=> -41.301,
    "lng"=> 173.2426
  ],
  [
    "postcode"=> 7045,
    "name"=> "Nelson",
    "lat"=> -41.2832,
    "lng"=> 173.2772
  ],
  [
    "postcode"=> 7046,
    "name"=> "Nelson South",
    "lat"=> -41.281,
    "lng"=> 173.2699
  ],
  [
    "postcode"=> 7047,
    "name"=> "Nelson East",
    "lat"=> -41.269,
    "lng"=> 173.2944
  ],
  [
    "postcode"=> 7048,
    "name"=> "Mapua",
    "lat"=> -41.2532,
    "lng"=> 173.0958
  ],
  [
    "postcode"=> 7049,
    "name"=> "Murchison",
    "lat"=> -41.8003,
    "lng"=> 172.3257
  ],
  [
    "postcode"=> 7050,
    "name"=> "Richmond",
    "lat"=> -41.3385,
    "lng"=> 173.1848
  ],
  [
    "postcode"=> 7051,
    "name"=> "Brightwater",
    "lat"=> -41.3765,
    "lng"=> 173.11
  ],
  [
    "postcode"=> 7052,
    "name"=> "Wakefield",
    "lat"=> -41.4062,
    "lng"=> 173.0428
  ],
  [
    "postcode"=> 7053,
    "name"=> "Saint Arnaud",
    "lat"=> -41.8025,
    "lng"=> 172.8462
  ],
  [
    "postcode"=> 7054,
    "name"=> "Collingwood",
    "lat"=> -40.6764,
    "lng"=> 172.6823
  ],
  [
    "postcode"=> 7055,
    "name"=> "Tapawera",
    "lat"=> -41.389,
    "lng"=> 172.8237
  ],
  [
    "postcode"=> 7071,
    "name"=> "Hira",
    "lat"=> -41.1983,
    "lng"=> 173.4167
  ],
  [
    "postcode"=> 7072,
    "name"=> "Lake Rotoroa",
    "lat"=> -41.8618,
    "lng"=> 172.678
  ],
  [
    "postcode"=> 7073,
    "name"=> "Baton",
    "lat"=> -41.1442,
    "lng"=> 172.5673
  ],
  [
    "postcode"=> 7077,
    "name"=> "Matakitaki",
    "lat"=> -41.941,
    "lng"=> 172.433
  ],
  [
    "postcode"=> 7081,
    "name"=> "Richmond",
    "lat"=> -41.3498,
    "lng"=> 173.1862
  ],
  [
    "postcode"=> 7091,
    "name"=> "Brightwater",
    "lat"=> -41.4317,
    "lng"=> 173.1111
  ],
  [
    "postcode"=> 7095,
    "name"=> "Hiwipango",
    "lat"=> -41.5222,
    "lng"=> 172.9526
  ],
  [
    "postcode"=> 7096,
    "name"=> "Motupiko",
    "lat"=> -41.4449,
    "lng"=> 172.8166
  ],
  [
    "postcode"=> 7100,
    "name"=> "Havelock",
    "lat"=> -41.2818,
    "lng"=> 173.7574
  ],
  [
    "postcode"=> 7110,
    "name"=> "Takaka",
    "lat"=> -40.8562,
    "lng"=> 172.8102
  ],
  [
    "postcode"=> 7120,
    "name"=> "Port Motueka",
    "lat"=> -41.1188,
    "lng"=> 173.0119
  ],
  [
    "postcode"=> 7142,
    "name"=> "Takaka",
    "lat"=> -40.8595,
    "lng"=> 172.8059
  ],
  [
    "postcode"=> 7143,
    "name"=> "Port Motueka",
    "lat"=> -41.1127,
    "lng"=> 173.0112
  ],
  [
    "postcode"=> 7144,
    "name"=> "Upper Moutere",
    "lat"=> -41.2696,
    "lng"=> 173.0061
  ],
  [
    "postcode"=> 7145,
    "name"=> "Rai Valley",
    "lat"=> -41.228,
    "lng"=> 173.5831
  ],
  [
    "postcode"=> 7146,
    "name"=> "Riwaka",
    "lat"=> -41.085,
    "lng"=> 172.9971
  ],
  [
    "postcode"=> 7150,
    "name"=> "Havelock",
    "lat"=> -41.2786,
    "lng"=> 173.767
  ],
  [
    "postcode"=> 7173,
    "name"=> "Mahana",
    "lat"=> -41.2375,
    "lng"=> 173.0318
  ],
  [
    "postcode"=> 7175,
    "name"=> "Braeburn",
    "lat"=> -41.2153,
    "lng"=> 172.9868
  ],
  [
    "postcode"=> 7178,
    "name"=> "Havelock",
    "lat"=> -41.3084,
    "lng"=> 173.7316
  ],
  [
    "postcode"=> 7182,
    "name"=> "Onekaka",
    "lat"=> -40.7728,
    "lng"=> 172.737
  ],
  [
    "postcode"=> 7183,
    "name"=> "East Takaka",
    "lat"=> -40.9481,
    "lng"=> 172.8863
  ],
  [
    "postcode"=> 7192,
    "name"=> "Tinline",
    "lat"=> -41.2755,
    "lng"=> 173.5246
  ],
  [
    "postcode"=> 7193,
    "name"=> "Kapowai",
    "lat"=> -40.9131,
    "lng"=> 173.8039
  ],
  [
    "postcode"=> 7194,
    "name"=> "Rai Valley",
    "lat"=> -41.1742,
    "lng"=> 173.6329
  ],
  [
    "postcode"=> 7195,
    "name"=> "Tira-ora",
    "lat"=> -41.1272,
    "lng"=> 173.8206
  ],
  [
    "postcode"=> 7196,
    "name"=> "Ngatimoti",
    "lat"=> -41.2123,
    "lng"=> 172.8726
  ],
  [
    "postcode"=> 7197,
    "name"=> "Sandy Bay",
    "lat"=> -41.0172,
    "lng"=> 172.9848
  ],
  [
    "postcode"=> 7198,
    "name"=> "Kairuru",
    "lat"=> -41.0302,
    "lng"=> 172.9351
  ],
  [
    "postcode"=> 7201,
    "name"=> "Witherlea",
    "lat"=> -41.5456,
    "lng"=> 173.9572
  ],
  [
    "postcode"=> 7202,
    "name"=> "Grovetown",
    "lat"=> -41.4767,
    "lng"=> 173.9648
  ],
  [
    "postcode"=> 7204,
    "name"=> "Renwick",
    "lat"=> -41.5056,
    "lng"=> 173.8293
  ],
  [
    "postcode"=> 7206,
    "name"=> "Renwick",
    "lat"=> -41.5149,
    "lng"=> 173.8649
  ],
  [
    "postcode"=> 7210,
    "name"=> "Seddon",
    "lat"=> -41.6695,
    "lng"=> 174.071
  ],
  [
    "postcode"=> 7220,
    "name"=> "Picton",
    "lat"=> -41.2933,
    "lng"=> 174.0192
  ],
  [
    "postcode"=> 7240,
    "name"=> "Witherlea",
    "lat"=> -41.5137,
    "lng"=> 173.9573
  ],
  [
    "postcode"=> 7241,
    "name"=> "Blenheim",
    "lat"=> -41.5093,
    "lng"=> 173.9353
  ],
  [
    "postcode"=> 7242,
    "name"=> "Witherlea",
    "lat"=> -41.5301,
    "lng"=> 173.9552
  ],
  [
    "postcode"=> 7243,
    "name"=> "Renwick",
    "lat"=> -41.5077,
    "lng"=> 173.8276
  ],
  [
    "postcode"=> 7244,
    "name"=> "Spring Creek",
    "lat"=> -41.4611,
    "lng"=> 173.9618
  ],
  [
    "postcode"=> 7246,
    "name"=> "Tuamarina",
    "lat"=> -41.4301,
    "lng"=> 173.9632
  ],
  [
    "postcode"=> 7247,
    "name"=> "Seddon",
    "lat"=> -41.6736,
    "lng"=> 174.076
  ],
  [
    "postcode"=> 7248,
    "name"=> "Ward",
    "lat"=> -41.8292,
    "lng"=> 174.1308
  ],
  [
    "postcode"=> 7250,
    "name"=> "Picton",
    "lat"=> -41.2913,
    "lng"=> 174.007
  ],
  [
    "postcode"=> 7251,
    "name"=> "Waikawa",
    "lat"=> -41.2669,
    "lng"=> 174.0483
  ],
  [
    "postcode"=> 7260,
    "name"=> "Kekerengu",
    "lat"=> -42.0033,
    "lng"=> 174.0105
  ],
  [
    "postcode"=> 7271,
    "name"=> "Hillersden",
    "lat"=> -41.7462,
    "lng"=> 173.4386
  ],
  [
    "postcode"=> 7272,
    "name"=> "Renwick",
    "lat"=> -41.5682,
    "lng"=> 173.8533
  ],
  [
    "postcode"=> 7273,
    "name"=> "Tuamarina",
    "lat"=> -41.4053,
    "lng"=> 173.9266
  ],
  [
    "postcode"=> 7274,
    "name"=> "Upcot",
    "lat"=> -41.9176,
    "lng"=> 173.5384
  ],
  [
    "postcode"=> 7275,
    "name"=> "Te Rou",
    "lat"=> -41.467,
    "lng"=> 173.3532
  ],
  [
    "postcode"=> 7276,
    "name"=> "Avondale",
    "lat"=> -41.7268,
    "lng"=> 173.509
  ],
  [
    "postcode"=> 7281,
    "name"=> "Waikawa",
    "lat"=> -41.2286,
    "lng"=> 174.0824
  ],
  [
    "postcode"=> 7282,
    "name"=> "Manaroa",
    "lat"=> -41.1392,
    "lng"=> 174.0599
  ],
  [
    "postcode"=> 7284,
    "name"=> "Whakatahuri",
    "lat"=> -41.0251,
    "lng"=> 174.083
  ],
  [
    "postcode"=> 7285,
    "name"=> "Taimate",
    "lat"=> -41.7703,
    "lng"=> 174.0725
  ],
  [
    "postcode"=> 7300,
    "name"=> "Waioruarangi",
    "lat"=> -42.4066,
    "lng"=> 173.6889
  ],
  [
    "postcode"=> 7310,
    "name"=> "Cheviot",
    "lat"=> -42.8133,
    "lng"=> 173.2755
  ],
  [
    "postcode"=> 7332,
    "name"=> "Waiau",
    "lat"=> -42.6533,
    "lng"=> 173.0461
  ],
  [
    "postcode"=> 7334,
    "name"=> "Hanmer Springs",
    "lat"=> -42.5167,
    "lng"=> 172.8222
  ],
  [
    "postcode"=> 7340,
    "name"=> "Kaikoura Suburban",
    "lat"=> -42.3891,
    "lng"=> 173.6637
  ],
  [
    "postcode"=> 7341,
    "name"=> "Cheviot",
    "lat"=> -42.8123,
    "lng"=> 173.2738
  ],
  [
    "postcode"=> 7343,
    "name"=> "Waiau",
    "lat"=> -42.6555,
    "lng"=> 173.0396
  ],
  [
    "postcode"=> 7345,
    "name"=> "Culverden",
    "lat"=> -42.7747,
    "lng"=> 172.8473
  ],
  [
    "postcode"=> 7348,
    "name"=> "Hawarden",
    "lat"=> -42.9236,
    "lng"=> 172.6428
  ],
  [
    "postcode"=> 7360,
    "name"=> "Hanmer Springs",
    "lat"=> -42.5213,
    "lng"=> 172.8294
  ],
  [
    "postcode"=> 7371,
    "name"=> "Langridge",
    "lat"=> -42.1653,
    "lng"=> 173.5325
  ],
  [
    "postcode"=> 7373,
    "name"=> "Charwell Forks",
    "lat"=> -42.4375,
    "lng"=> 173.3735
  ],
  [
    "postcode"=> 7374,
    "name"=> "Goose Bay",
    "lat"=> -42.4831,
    "lng"=> 173.5086
  ],
  [
    "postcode"=> 7379,
    "name"=> "Waiau",
    "lat"=> -42.6926,
    "lng"=> 173.0303
  ],
  [
    "postcode"=> 7381,
    "name"=> "Spotswood",
    "lat"=> -42.7774,
    "lng"=> 173.2882
  ],
  [
    "postcode"=> 7382,
    "name"=> "Ethelton",
    "lat"=> -42.7904,
    "lng"=> 173.1121
  ],
  [
    "postcode"=> 7383,
    "name"=> "Domett",
    "lat"=> -42.885,
    "lng"=> 173.2276
  ],
  [
    "postcode"=> 7384,
    "name"=> "Hawkeswood",
    "lat"=> -42.6505,
    "lng"=> 173.3387
  ],
  [
    "postcode"=> 7385,
    "name"=> "Masons Flat",
    "lat"=> -42.7342,
    "lng"=> 172.4233
  ],
  [
    "postcode"=> 7387,
    "name"=> "Tormore",
    "lat"=> -42.9805,
    "lng"=> 173.0928
  ],
  [
    "postcode"=> 7391,
    "name"=> "Rotherham",
    "lat"=> -42.76,
    "lng"=> 172.9548
  ],
  [
    "postcode"=> 7392,
    "name"=> "Hanmer Springs",
    "lat"=> -42.5739,
    "lng"=> 172.5659
  ],
  [
    "postcode"=> 7395,
    "name"=> "Hanmer Springs",
    "lat"=> -42.4038,
    "lng"=> 172.8612
  ],
  [
    "postcode"=> 7400,
    "name"=> "Bells",
    "lat"=> -43.3058,
    "lng"=> 172.5992
  ],
  [
    "postcode"=> 7402,
    "name"=> "Waikuku Beach",
    "lat"=> -43.2838,
    "lng"=> 172.7162
  ],
  [
    "postcode"=> 7410,
    "name"=> "Amberley",
    "lat"=> -43.1451,
    "lng"=> 172.7382
  ],
  [
    "postcode"=> 7420,
    "name"=> "Waikari",
    "lat"=> -42.9673,
    "lng"=> 172.692
  ],
  [
    "postcode"=> 7430,
    "name"=> "Oxford",
    "lat"=> -43.2993,
    "lng"=> 172.1896
  ],
  [
    "postcode"=> 7440,
    "name"=> "Bells",
    "lat"=> -43.3035,
    "lng"=> 172.5954
  ],
  [
    "postcode"=> 7441,
    "name"=> "Amberley",
    "lat"=> -43.1546,
    "lng"=> 172.7302
  ],
  [
    "postcode"=> 7442,
    "name"=> "Waikari",
    "lat"=> -42.9672,
    "lng"=> 172.6851
  ],
  [
    "postcode"=> 7443,
    "name"=> "Oxford",
    "lat"=> -43.2977,
    "lng"=> 172.194
  ],
  [
    "postcode"=> 7444,
    "name"=> "Cust",
    "lat"=> -43.3114,
    "lng"=> 172.3793
  ],
  [
    "postcode"=> 7445,
    "name"=> "Sefton",
    "lat"=> -43.2486,
    "lng"=> 172.6683
  ],
  [
    "postcode"=> 7446,
    "name"=> "Leithfield",
    "lat"=> -43.2104,
    "lng"=> 172.7539
  ],
  [
    "postcode"=> 7447,
    "name"=> "Waipara",
    "lat"=> -43.0598,
    "lng"=> 172.7558
  ],
  [
    "postcode"=> 7448,
    "name"=> "Waikuku Beach",
    "lat"=> -43.2867,
    "lng"=> 172.7164
  ],
  [
    "postcode"=> 7471,
    "name"=> "West Eyreton",
    "lat"=> -43.356,
    "lng"=> 172.3985
  ],
  [
    "postcode"=> 7472,
    "name"=> "North Loburn",
    "lat"=> -43.1561,
    "lng"=> 172.5438
  ],
  [
    "postcode"=> 7473,
    "name"=> "Waikuku",
    "lat"=> -43.2928,
    "lng"=> 172.6558
  ],
  [
    "postcode"=> 7474,
    "name"=> "North Loburn",
    "lat"=> -43.2203,
    "lng"=> 172.3884
  ],
  [
    "postcode"=> 7481,
    "name"=> "Amberley",
    "lat"=> -43.1628,
    "lng"=> 172.7248
  ],
  [
    "postcode"=> 7482,
    "name"=> "Waipara",
    "lat"=> -43.101,
    "lng"=> 172.6438
  ],
  [
    "postcode"=> 7483,
    "name"=> "Omihi",
    "lat"=> -43.0041,
    "lng"=> 172.8703
  ],
  [
    "postcode"=> 7491,
    "name"=> "Waikari",
    "lat"=> -42.973,
    "lng"=> 172.6468
  ],
  [
    "postcode"=> 7495,
    "name"=> "Oxford",
    "lat"=> -43.2101,
    "lng"=> 172.1779
  ],
  [
    "postcode"=> 7500,
    "name"=> "Sheffield",
    "lat"=> -43.3895,
    "lng"=> 172.0202
  ],
  [
    "postcode"=> 7510,
    "name"=> "Darfield",
    "lat"=> -43.4859,
    "lng"=> 172.1154
  ],
  [
    "postcode"=> 7520,
    "name"=> "Akaroa",
    "lat"=> -43.8126,
    "lng"=> 172.9632
  ],
  [
    "postcode"=> 7540,
    "name"=> "Sheffield",
    "lat"=> -43.3872,
    "lng"=> 172.0163
  ],
  [
    "postcode"=> 7541,
    "name"=> "Darfield",
    "lat"=> -43.4898,
    "lng"=> 172.1106
  ],
  [
    "postcode"=> 7542,
    "name"=> "Akaroa",
    "lat"=> -43.8053,
    "lng"=> 172.9668
  ],
  [
    "postcode"=> 7543,
    "name"=> "Kirwee",
    "lat"=> -43.5009,
    "lng"=> 172.2168
  ],
  [
    "postcode"=> 7544,
    "name"=> "Hororata",
    "lat"=> -43.5436,
    "lng"=> 171.9775
  ],
  [
    "postcode"=> 7545,
    "name"=> "Duvauchelle",
    "lat"=> -43.7503,
    "lng"=> 172.9324
  ],
  [
    "postcode"=> 7546,
    "name"=> "Little River",
    "lat"=> -43.7703,
    "lng"=> 172.791
  ],
  [
    "postcode"=> 7548,
    "name"=> "Doyleston",
    "lat"=> -43.748,
    "lng"=> 172.3276
  ],
  [
    "postcode"=> 7571,
    "name"=> "Hawkins",
    "lat"=> -43.4717,
    "lng"=> 172.0706
  ],
  [
    "postcode"=> 7572,
    "name"=> "Lake Coleridge",
    "lat"=> -43.2868,
    "lng"=> 171.6068
  ],
  [
    "postcode"=> 7580,
    "name"=> "Craigieburn",
    "lat"=> -43.1316,
    "lng"=> 171.8273
  ],
  [
    "postcode"=> 7581,
    "name"=> "Akaroa",
    "lat"=> -43.8041,
    "lng"=> 172.9815
  ],
  [
    "postcode"=> 7582,
    "name"=> "Wainui",
    "lat"=> -43.8233,
    "lng"=> 172.9051
  ],
  [
    "postcode"=> 7583,
    "name"=> "Robinsons Bay",
    "lat"=> -43.7354,
    "lng"=> 172.9876
  ],
  [
    "postcode"=> 7591,
    "name"=> "Ataahua",
    "lat"=> -43.7998,
    "lng"=> 172.6341
  ],
  [
    "postcode"=> 7600,
    "name"=> "Burnham",
    "lat"=> -43.6162,
    "lng"=> 172.2987
  ],
  [
    "postcode"=> 7602,
    "name"=> "Southbridge",
    "lat"=> -43.804,
    "lng"=> 172.2416
  ],
  [
    "postcode"=> 7604,
    "name"=> "Prebbleton",
    "lat"=> -43.583,
    "lng"=> 172.5143
  ],
  [
    "postcode"=> 7608,
    "name"=> "Lincoln",
    "lat"=> -43.6416,
    "lng"=> 172.4835
  ],
  [
    "postcode"=> 7610,
    "name"=> "Woodend",
    "lat"=> -43.3206,
    "lng"=> 172.6648
  ],
  [
    "postcode"=> 7614,
    "name"=> "Rolleston",
    "lat"=> -43.6013,
    "lng"=> 172.377
  ],
  [
    "postcode"=> 7616,
    "name"=> "Springston",
    "lat"=> -43.6444,
    "lng"=> 172.4224
  ],
  [
    "postcode"=> 7630,
    "name"=> "Kaiapoi",
    "lat"=> -43.3848,
    "lng"=> 172.6766
  ],
  [
    "postcode"=> 7632,
    "name"=> "Leeston",
    "lat"=> -43.7612,
    "lng"=> 172.2977
  ],
  [
    "postcode"=> 7640,
    "name"=> "Lincoln",
    "lat"=> -43.6405,
    "lng"=> 172.4841
  ],
  [
    "postcode"=> 7641,
    "name"=> "Woodend",
    "lat"=> -43.3224,
    "lng"=> 172.6659
  ],
  [
    "postcode"=> 7642,
    "name"=> "Southbridge",
    "lat"=> -43.8115,
    "lng"=> 172.2516
  ],
  [
    "postcode"=> 7643,
    "name"=> "Rolleston",
    "lat"=> -43.5927,
    "lng"=> 172.376
  ],
  [
    "postcode"=> 7644,
    "name"=> "Kaiapoi",
    "lat"=> -43.3825,
    "lng"=> 172.6588
  ],
  [
    "postcode"=> 7645,
    "name"=> "Tai Tapu",
    "lat"=> -43.6618,
    "lng"=> 172.5484
  ],
  [
    "postcode"=> 7646,
    "name"=> "Coalgate",
    "lat"=> -43.4833,
    "lng"=> 171.9644
  ],
  [
    "postcode"=> 7647,
    "name"=> "Lincoln",
    "lat"=> -43.643,
    "lng"=> 172.4691
  ],
  [
    "postcode"=> 7649,
    "name"=> "Springfield",
    "lat"=> -43.3371,
    "lng"=> 171.9272
  ],
  [
    "postcode"=> 7656,
    "name"=> "Leeston",
    "lat"=> -43.7636,
    "lng"=> 172.296
  ],
  [
    "postcode"=> 7657,
    "name"=> "Dunsandel",
    "lat"=> -43.6591,
    "lng"=> 172.1964
  ],
  [
    "postcode"=> 7670,
    "name"=> "Coutts Island",
    "lat"=> -43.4325,
    "lng"=> 172.589
  ],
  [
    "postcode"=> 7671,
    "name"=> "Aylesbury",
    "lat"=> -43.5419,
    "lng"=> 172.213
  ],
  [
    "postcode"=> 7672,
    "name"=> "Motukarara",
    "lat"=> -43.7012,
    "lng"=> 172.6222
  ],
  [
    "postcode"=> 7673,
    "name"=> "Whitecliffs",
    "lat"=> -43.4603,
    "lng"=> 171.8825
  ],
  [
    "postcode"=> 7674,
    "name"=> "Springston South",
    "lat"=> -43.7046,
    "lng"=> 172.4643
  ],
  [
    "postcode"=> 7675,
    "name"=> "Rolleston",
    "lat"=> -43.5966,
    "lng"=> 172.3476
  ],
  [
    "postcode"=> 7676,
    "name"=> "Templeton",
    "lat"=> -43.5524,
    "lng"=> 172.4438
  ],
  [
    "postcode"=> 7681,
    "name"=> "Springfield",
    "lat"=> -43.3295,
    "lng"=> 171.9412
  ],
  [
    "postcode"=> 7682,
    "name"=> "Dunsandel",
    "lat"=> -43.6886,
    "lng"=> 172.1913
  ],
  [
    "postcode"=> 7683,
    "name"=> "Southbridge",
    "lat"=> -43.7963,
    "lng"=> 172.2491
  ],
  [
    "postcode"=> 7691,
    "name"=> "Flaxton",
    "lat"=> -43.3547,
    "lng"=> 172.6477
  ],
  [
    "postcode"=> 7692,
    "name"=> "Wetheral",
    "lat"=> -43.3948,
    "lng"=> 172.5762
  ],
  [
    "postcode"=> 7700,
    "name"=> "Allenton",
    "lat"=> -43.898,
    "lng"=> 171.745
  ],
  [
    "postcode"=> 7710,
    "name"=> "Rakaia",
    "lat"=> -43.7547,
    "lng"=> 172.0272
  ],
  [
    "postcode"=> 7730,
    "name"=> "Methven",
    "lat"=> -43.6323,
    "lng"=> 171.6437
  ],
  [
    "postcode"=> 7740,
    "name"=> "Hampstead",
    "lat"=> -43.903,
    "lng"=> 171.7503
  ],
  [
    "postcode"=> 7741,
    "name"=> "Tinwald",
    "lat"=> -43.92,
    "lng"=> 171.7206
  ],
  [
    "postcode"=> 7742,
    "name"=> "Allenton",
    "lat"=> -43.8874,
    "lng"=> 171.7427
  ],
  [
    "postcode"=> 7743,
    "name"=> "Rakaia",
    "lat"=> -43.7551,
    "lng"=> 172.0207
  ],
  [
    "postcode"=> 7745,
    "name"=> "Methven",
    "lat"=> -43.6332,
    "lng"=> 171.6468
  ],
  [
    "postcode"=> 7746,
    "name"=> "Mount Somers",
    "lat"=> -43.7058,
    "lng"=> 171.402
  ],
  [
    "postcode"=> 7747,
    "name"=> "Hinds",
    "lat"=> -44.0032,
    "lng"=> 171.5687
  ],
  [
    "postcode"=> 7748,
    "name"=> "Mayfield",
    "lat"=> -43.8208,
    "lng"=> 171.4225
  ],
  [
    "postcode"=> 7771,
    "name"=> "Staveley",
    "lat"=> -43.5078,
    "lng"=> 171.1763
  ],
  [
    "postcode"=> 7772,
    "name"=> "Mitcham",
    "lat"=> -43.7966,
    "lng"=> 171.8593
  ],
  [
    "postcode"=> 7773,
    "name"=> "Hinds",
    "lat"=> -44.0609,
    "lng"=> 171.5171
  ],
  [
    "postcode"=> 7774,
    "name"=> "Flemington",
    "lat"=> -44.0196,
    "lng"=> 171.7093
  ],
  [
    "postcode"=> 7775,
    "name"=> "Lismore",
    "lat"=> -43.9232,
    "lng"=> 171.471
  ],
  [
    "postcode"=> 7776,
    "name"=> "Lyndhurst",
    "lat"=> -43.7501,
    "lng"=> 171.6736
  ],
  [
    "postcode"=> 7777,
    "name"=> "Seafield",
    "lat"=> -43.9445,
    "lng"=> 171.9202
  ],
  [
    "postcode"=> 7778,
    "name"=> "Mayfield",
    "lat"=> -43.8003,
    "lng"=> 171.4252
  ],
  [
    "postcode"=> 7781,
    "name"=> "Dorie",
    "lat"=> -43.8524,
    "lng"=> 172.0759
  ],
  [
    "postcode"=> 7782,
    "name"=> "Highbank",
    "lat"=> -43.5682,
    "lng"=> 171.7097
  ],
  [
    "postcode"=> 7783,
    "name"=> "Te Pirita",
    "lat"=> -43.677,
    "lng"=> 171.9801
  ],
  [
    "postcode"=> 7784,
    "name"=> "Somerton",
    "lat"=> -43.7301,
    "lng"=> 171.9081
  ],
  [
    "postcode"=> 7791,
    "name"=> "Lake Coleridge",
    "lat"=> -43.3864,
    "lng"=> 171.332
  ],
  [
    "postcode"=> 7802,
    "name"=> "Runanga",
    "lat"=> -42.4169,
    "lng"=> 171.2586
  ],
  [
    "postcode"=> 7803,
    "name"=> "Rewanui",
    "lat"=> -42.3888,
    "lng"=> 171.2964
  ],
  [
    "postcode"=> 7804,
    "name"=> "Roa",
    "lat"=> -42.382,
    "lng"=> 171.372
  ],
  [
    "postcode"=> 7805,
    "name"=> "Marsden",
    "lat"=> -42.5254,
    "lng"=> 171.2455
  ],
  [
    "postcode"=> 7810,
    "name"=> "Hokitika",
    "lat"=> -42.7183,
    "lng"=> 170.9651
  ],
  [
    "postcode"=> 7811,
    "name"=> "Kaniere",
    "lat"=> -42.7416,
    "lng"=> 171.0127
  ],
  [
    "postcode"=> 7812,
    "name"=> "Ross",
    "lat"=> -42.9461,
    "lng"=> 170.8147
  ],
  [
    "postcode"=> 7822,
    "name"=> "Hector",
    "lat"=> -41.6077,
    "lng"=> 171.8763
  ],
  [
    "postcode"=> 7823,
    "name"=> "Granity",
    "lat"=> -41.6302,
    "lng"=> 171.8502
  ],
  [
    "postcode"=> 7825,
    "name"=> "Carters Beach",
    "lat"=> -41.7486,
    "lng"=> 171.5903
  ],
  [
    "postcode"=> 7830,
    "name"=> "Reefton",
    "lat"=> -42.1122,
    "lng"=> 171.8582
  ],
  [
    "postcode"=> 7832,
    "name"=> "Kumara",
    "lat"=> -42.6217,
    "lng"=> 171.1725
  ],
  [
    "postcode"=> 7840,
    "name"=> "Cobden",
    "lat"=> -42.4491,
    "lng"=> 171.2108
  ],
  [
    "postcode"=> 7841,
    "name"=> "Runanga",
    "lat"=> -42.402,
    "lng"=> 171.2522
  ],
  [
    "postcode"=> 7842,
    "name"=> "Hokitika",
    "lat"=> -42.7172,
    "lng"=> 170.9614
  ],
  [
    "postcode"=> 7843,
    "name"=> "Ahaura",
    "lat"=> -42.3486,
    "lng"=> 171.5407
  ],
  [
    "postcode"=> 7844,
    "name"=> "Haast",
    "lat"=> -43.862,
    "lng"=> 169.0035
  ],
  [
    "postcode"=> 7845,
    "name"=> "Moana",
    "lat"=> -42.5722,
    "lng"=> 171.483
  ],
  [
    "postcode"=> 7846,
    "name"=> "Ikamatua",
    "lat"=> -42.2712,
    "lng"=> 171.6849
  ],
  [
    "postcode"=> 7847,
    "name"=> "Springs Junction",
    "lat"=> -42.3351,
    "lng"=> 172.182
  ],
  [
    "postcode"=> 7848,
    "name"=> "Waimangaroa",
    "lat"=> -41.714,
    "lng"=> 171.763
  ],
  [
    "postcode"=> 7851,
    "name"=> "Reefton",
    "lat"=> -42.1194,
    "lng"=> 171.8648
  ],
  [
    "postcode"=> 7856,
    "name"=> "Franz Josef/Waiau",
    "lat"=> -43.3888,
    "lng"=> 170.182
  ],
  [
    "postcode"=> 7857,
    "name"=> "Whataroa",
    "lat"=> -43.262,
    "lng"=> 170.3604
  ],
  [
    "postcode"=> 7859,
    "name"=> "Weheka",
    "lat"=> -43.4655,
    "lng"=> 170.0172
  ],
  [
    "postcode"=> 7860,
    "name"=> "Ross",
    "lat"=> -42.8988,
    "lng"=> 170.8167
  ],
  [
    "postcode"=> 7861,
    "name"=> "Kumara",
    "lat"=> -42.63,
    "lng"=> 171.1859
  ],
  [
    "postcode"=> 7862,
    "name"=> "Maruia",
    "lat"=> -42.1885,
    "lng"=> 172.2204
  ],
  [
    "postcode"=> 7863,
    "name"=> "Harihari",
    "lat"=> -43.1491,
    "lng"=> 170.5572
  ],
  [
    "postcode"=> 7864,
    "name"=> "Market Cross",
    "lat"=> -41.2514,
    "lng"=> 172.1284
  ],
  [
    "postcode"=> 7865,
    "name"=> "Charleston",
    "lat"=> -41.9073,
    "lng"=> 171.4381
  ],
  [
    "postcode"=> 7866,
    "name"=> "Carters Beach",
    "lat"=> -41.7557,
    "lng"=> 171.599
  ],
  [
    "postcode"=> 7867,
    "name"=> "Hector",
    "lat"=> -41.6068,
    "lng"=> 171.8779
  ],
  [
    "postcode"=> 7871,
    "name"=> "Ikamatua",
    "lat"=> -42.2476,
    "lng"=> 171.6635
  ],
  [
    "postcode"=> 7872,
    "name"=> "Bell Hill",
    "lat"=> -42.5384,
    "lng"=> 171.717
  ],
  [
    "postcode"=> 7873,
    "name"=> "Barrytown",
    "lat"=> -42.2469,
    "lng"=> 171.3227
  ],
  [
    "postcode"=> 7875,
    "name"=> "Aickens",
    "lat"=> -42.8109,
    "lng"=> 171.6001
  ],
  [
    "postcode"=> 7881,
    "name"=> "Kowhitirangi",
    "lat"=> -42.907,
    "lng"=> 171.138
  ],
  [
    "postcode"=> 7882,
    "name"=> "Goldsborough",
    "lat"=> -42.6852,
    "lng"=> 171.1127
  ],
  [
    "postcode"=> 7883,
    "name"=> "Kowhitirangi",
    "lat"=> -42.886,
    "lng"=> 170.9152
  ],
  [
    "postcode"=> 7884,
    "name"=> "Harihari",
    "lat"=> -43.1353,
    "lng"=> 170.6262
  ],
  [
    "postcode"=> 7885,
    "name"=> "Fergusons",
    "lat"=> -43.0658,
    "lng"=> 170.8442
  ],
  [
    "postcode"=> 7886,
    "name"=> "Mahitahi",
    "lat"=> -43.7673,
    "lng"=> 169.3817
  ],
  [
    "postcode"=> 7891,
    "name"=> "Mokihinui Mine",
    "lat"=> -41.5514,
    "lng"=> 172.0406
  ],
  [
    "postcode"=> 7892,
    "name"=> "Te Kuha",
    "lat"=> -41.8946,
    "lng"=> 171.5678
  ],
  [
    "postcode"=> 7893,
    "name"=> "Oparara",
    "lat"=> -41.2059,
    "lng"=> 172.1609
  ],
  [
    "postcode"=> 7895,
    "name"=> "Crushington",
    "lat"=> -42.1053,
    "lng"=> 172.0272
  ],
  [
    "postcode"=> 7901,
    "name"=> "Twizel",
    "lat"=> -44.262,
    "lng"=> 170.0746
  ],
  [
    "postcode"=> 7903,
    "name"=> "Pleasant Point",
    "lat"=> -44.2585,
    "lng"=> 171.1305
  ],
  [
    "postcode"=> 7910,
    "name"=> "Seaview",
    "lat"=> -44.3864,
    "lng"=> 171.2285
  ],
  [
    "postcode"=> 7912,
    "name"=> "Pareora",
    "lat"=> -44.4875,
    "lng"=> 171.2097
  ],
  [
    "postcode"=> 7920,
    "name"=> "Temuka",
    "lat"=> -44.2374,
    "lng"=> 171.2852
  ],
  [
    "postcode"=> 7924,
    "name"=> "Waimate",
    "lat"=> -44.7318,
    "lng"=> 171.0476
  ],
  [
    "postcode"=> 7925,
    "name"=> "Fairlie",
    "lat"=> -44.0947,
    "lng"=> 170.8289
  ],
  [
    "postcode"=> 7930,
    "name"=> "Geraldine",
    "lat"=> -44.0934,
    "lng"=> 171.242
  ],
  [
    "postcode"=> 7940,
    "name"=> "Timaru",
    "lat"=> -44.395,
    "lng"=> 171.2532
  ],
  [
    "postcode"=> 7941,
    "name"=> "Puhuka",
    "lat"=> -44.3563,
    "lng"=> 171.238
  ],
  [
    "postcode"=> 7942,
    "name"=> "Parkside",
    "lat"=> -44.3906,
    "lng"=> 171.2445
  ],
  [
    "postcode"=> 7943,
    "name"=> "Kensington",
    "lat"=> -44.4053,
    "lng"=> 171.2344
  ],
  [
    "postcode"=> 7944,
    "name"=> "Twizel",
    "lat"=> -44.258,
    "lng"=> 170.1
  ],
  [
    "postcode"=> 7945,
    "name"=> "Lake Tekapo",
    "lat"=> -44.0043,
    "lng"=> 170.4772
  ],
  [
    "postcode"=> 7946,
    "name"=> "Glentanner",
    "lat"=> -43.7327,
    "lng"=> 170.0938
  ],
  [
    "postcode"=> 7947,
    "name"=> "Pleasant Point",
    "lat"=> -44.2601,
    "lng"=> 171.1275
  ],
  [
    "postcode"=> 7948,
    "name"=> "Temuka",
    "lat"=> -44.244,
    "lng"=> 171.2773
  ],
  [
    "postcode"=> 7949,
    "name"=> "Fairlie",
    "lat"=> -44.0984,
    "lng"=> 170.8295
  ],
  [
    "postcode"=> 7956,
    "name"=> "Geraldine",
    "lat"=> -44.0912,
    "lng"=> 171.2436
  ],
  [
    "postcode"=> 7957,
    "name"=> "Cave",
    "lat"=> -44.3103,
    "lng"=> 170.9524
  ],
  [
    "postcode"=> 7958,
    "name"=> "Winchester",
    "lat"=> -44.1851,
    "lng"=> 171.2782
  ],
  [
    "postcode"=> 7960,
    "name"=> "Waimate",
    "lat"=> -44.7334,
    "lng"=> 171.0476
  ],
  [
    "postcode"=> 7971,
    "name"=> "Esk Valley",
    "lat"=> -44.53,
    "lng"=> 171.086
  ],
  [
    "postcode"=> 7972,
    "name"=> "Southburn",
    "lat"=> -44.4575,
    "lng"=> 171.0532
  ],
  [
    "postcode"=> 7973,
    "name"=> "Seadown",
    "lat"=> -44.3175,
    "lng"=> 171.2918
  ],
  [
    "postcode"=> 7974,
    "name"=> "Claremont",
    "lat"=> -44.3314,
    "lng"=> 171.124
  ],
  [
    "postcode"=> 7977,
    "name"=> "Waihaorunga",
    "lat"=> -44.7324,
    "lng"=> 170.8388
  ],
  [
    "postcode"=> 7978,
    "name"=> "Waimate",
    "lat"=> -44.6945,
    "lng"=> 171.0101
  ],
  [
    "postcode"=> 7979,
    "name"=> "Waihao Forks",
    "lat"=> -44.8281,
    "lng"=> 170.9906
  ],
  [
    "postcode"=> 7980,
    "name"=> "Morven",
    "lat"=> -44.8219,
    "lng"=> 171.1192
  ],
  [
    "postcode"=> 7982,
    "name"=> "Hazelburn",
    "lat"=> -44.2151,
    "lng"=> 170.9986
  ],
  [
    "postcode"=> 7983,
    "name"=> "Sutherlands",
    "lat"=> -44.2887,
    "lng"=> 171.0431
  ],
  [
    "postcode"=> 7984,
    "name"=> "Mawaro",
    "lat"=> -44.3312,
    "lng"=> 170.8526
  ],
  [
    "postcode"=> 7985,
    "name"=> "Waitohi Flat",
    "lat"=> -44.204,
    "lng"=> 171.1929
  ],
  [
    "postcode"=> 7986,
    "name"=> "Orton",
    "lat"=> -44.1694,
    "lng"=> 171.3866
  ],
  [
    "postcode"=> 7987,
    "name"=> "Waikarua",
    "lat"=> -44.0577,
    "lng"=> 170.8816
  ],
  [
    "postcode"=> 7988,
    "name"=> "Esk Valley",
    "lat"=> -44.5216,
    "lng"=> 171.0694
  ],
  [
    "postcode"=> 7990,
    "name"=> "Sherwood Downs",
    "lat"=> -43.6686,
    "lng"=> 170.9233
  ],
  [
    "postcode"=> 7991,
    "name"=> "Te Moana",
    "lat"=> -44.0601,
    "lng"=> 171.108
  ],
  [
    "postcode"=> 7992,
    "name"=> "Arundel",
    "lat"=> -44.0184,
    "lng"=> 171.2776
  ],
  [
    "postcode"=> 7999,
    "name"=> "Glentanner",
    "lat"=> -43.9781,
    "lng"=> 170.1682
  ],
  [
    "postcode"=> 8011,
    "name"=> "Christchurch",
    "lat"=> -43.5341,
    "lng"=> 172.637
  ],
  [
    "postcode"=> 8013,
    "name"=> "Christchurch",
    "lat"=> -44.5772,
    "lng"=> 175.5503
  ],
  [
    "postcode"=> 8014,
    "name"=> "Saint Albans",
    "lat"=> -43.5136,
    "lng"=> 172.6204
  ],
  [
    "postcode"=> 8022,
    "name"=> "Cashmere Hills",
    "lat"=> -43.5831,
    "lng"=> 172.6644
  ],
  [
    "postcode"=> 8023,
    "name"=> "Hillsborough",
    "lat"=> -43.5559,
    "lng"=> 172.669
  ],
  [
    "postcode"=> 8024,
    "name"=> "Spreydon",
    "lat"=> -43.5565,
    "lng"=> 172.6024
  ],
  [
    "postcode"=> 8025,
    "name"=> "Halswell",
    "lat"=> -43.5884,
    "lng"=> 172.579
  ],
  [
    "postcode"=> 8041,
    "name"=> "Fendalton",
    "lat"=> -43.5225,
    "lng"=> 172.5813
  ],
  [
    "postcode"=> 8042,
    "name"=> "Hei Hei",
    "lat"=> -43.5294,
    "lng"=> 172.5174
  ],
  [
    "postcode"=> 8051,
    "name"=> "Harewood",
    "lat"=> -43.4611,
    "lng"=> 172.5247
  ],
  [
    "postcode"=> 8052,
    "name"=> "Papanui",
    "lat"=> -43.5008,
    "lng"=> 172.6257
  ],
  [
    "postcode"=> 8053,
    "name"=> "Burnside",
    "lat"=> -43.4957,
    "lng"=> 172.568
  ],
  [
    "postcode"=> 8061,
    "name"=> "Aranui",
    "lat"=> -43.5079,
    "lng"=> 172.7022
  ],
  [
    "postcode"=> 8062,
    "name"=> "Bromley",
    "lat"=> -43.541,
    "lng"=> 172.71
  ],
  [
    "postcode"=> 8081,
    "name"=> "Sumner",
    "lat"=> -43.5753,
    "lng"=> 172.7589
  ],
  [
    "postcode"=> 8082,
    "name"=> "Lyttelton",
    "lat"=> -43.6024,
    "lng"=> 172.7241
  ],
  [
    "postcode"=> 8083,
    "name"=> "Ouruhia",
    "lat"=> -43.4492,
    "lng"=> 172.6875
  ],
  [
    "postcode"=> 8140,
    "name"=> "Christchurch",
    "lat"=> -43.5319,
    "lng"=> 172.6316
  ],
  [
    "postcode"=> 8141,
    "name"=> "Christchurch",
    "lat"=> -43.5286,
    "lng"=> 172.6392
  ],
  [
    "postcode"=> 8142,
    "name"=> "Christchurch",
    "lat"=> -43.5352,
    "lng"=> 172.6403
  ],
  [
    "postcode"=> 8143,
    "name"=> "Saint Albans",
    "lat"=> -43.5136,
    "lng"=> 172.6377
  ],
  [
    "postcode"=> 8144,
    "name"=> "Christchurch",
    "lat"=> -43.5241,
    "lng"=> 172.6307
  ],
  [
    "postcode"=> 8145,
    "name"=> "Opawa",
    "lat"=> -43.5404,
    "lng"=> 172.6585
  ],
  [
    "postcode"=> 8146,
    "name"=> "Saint Albans",
    "lat"=> -43.5127,
    "lng"=> 172.622
  ],
  [
    "postcode"=> 8147,
    "name"=> "Linwood",
    "lat"=> -43.5302,
    "lng"=> 172.6573
  ],
  [
    "postcode"=> 8148,
    "name"=> "Avonside",
    "lat"=> -43.5177,
    "lng"=> 172.6559
  ],
  [
    "postcode"=> 8149,
    "name"=> "Spreydon",
    "lat"=> -43.5451,
    "lng"=> 172.6083
  ],
  [
    "postcode"=> 8240,
    "name"=> "Sydenham",
    "lat"=> -43.5452,
    "lng"=> 172.6365
  ],
  [
    "postcode"=> 8241,
    "name"=> "Woolston",
    "lat"=> -43.5498,
    "lng"=> 172.6842
  ],
  [
    "postcode"=> 8242,
    "name"=> "Beckenham",
    "lat"=> -43.5579,
    "lng"=> 172.6366
  ],
  [
    "postcode"=> 8243,
    "name"=> "Spreydon",
    "lat"=> -43.5457,
    "lng"=> 172.6085
  ],
  [
    "postcode"=> 8244,
    "name"=> "Somerfield",
    "lat"=> -43.5571,
    "lng"=> 172.6189
  ],
  [
    "postcode"=> 8245,
    "name"=> "Halswell",
    "lat"=> -43.5802,
    "lng"=> 172.567
  ],
  [
    "postcode"=> 8246,
    "name"=> "Hillsborough",
    "lat"=> -43.558,
    "lng"=> 172.6542
  ],
  [
    "postcode"=> 8247,
    "name"=> "Mount Pleasant",
    "lat"=> -43.5574,
    "lng"=> 172.7026
  ],
  [
    "postcode"=> 8440,
    "name"=> "Riccarton",
    "lat"=> -43.5298,
    "lng"=> 172.6012
  ],
  [
    "postcode"=> 8441,
    "name"=> "Hornby",
    "lat"=> -43.5432,
    "lng"=> 172.5252
  ],
  [
    "postcode"=> 8442,
    "name"=> "Upper Riccarton",
    "lat"=> -43.5318,
    "lng"=> 172.5723
  ],
  [
    "postcode"=> 8443,
    "name"=> "Wigram",
    "lat"=> -43.5384,
    "lng"=> 172.5559
  ],
  [
    "postcode"=> 8444,
    "name"=> "Ilam",
    "lat"=> -43.5167,
    "lng"=> 172.5718
  ],
  [
    "postcode"=> 8445,
    "name"=> "Templeton",
    "lat"=> -43.5533,
    "lng"=> 172.4732
  ],
  [
    "postcode"=> 8540,
    "name"=> "Fendalton",
    "lat"=> -43.5177,
    "lng"=> 172.5884
  ],
  [
    "postcode"=> 8542,
    "name"=> "Papanui",
    "lat"=> -43.4952,
    "lng"=> 172.6088
  ],
  [
    "postcode"=> 8543,
    "name"=> "Harewood",
    "lat"=> -43.489,
    "lng"=> 172.5872
  ],
  [
    "postcode"=> 8544,
    "name"=> "Harewood",
    "lat"=> -43.4884,
    "lng"=> 172.5388
  ],
  [
    "postcode"=> 8545,
    "name"=> "Burnside",
    "lat"=> -43.489,
    "lng"=> 172.567
  ],
  [
    "postcode"=> 8546,
    "name"=> "Belfast",
    "lat"=> -43.4454,
    "lng"=> 172.6324
  ],
  [
    "postcode"=> 8640,
    "name"=> "Dallington",
    "lat"=> -43.507,
    "lng"=> 172.6631
  ],
  [
    "postcode"=> 8641,
    "name"=> "New Brighton",
    "lat"=> -43.5073,
    "lng"=> 172.7287
  ],
  [
    "postcode"=> 8642,
    "name"=> "Bromley",
    "lat"=> -43.533,
    "lng"=> 172.6735
  ],
  [
    "postcode"=> 8643,
    "name"=> "Aranui",
    "lat"=> -43.5178,
    "lng"=> 172.7005
  ],
  [
    "postcode"=> 8840,
    "name"=> "Sumner",
    "lat"=> -43.569,
    "lng"=> 172.7593
  ],
  [
    "postcode"=> 8841,
    "name"=> "Lyttelton",
    "lat"=> -43.603,
    "lng"=> 172.7223
  ],
  [
    "postcode"=> 8842,
    "name"=> "North New Brighton",
    "lat"=> -43.4816,
    "lng"=> 172.706
  ],
  [
    "postcode"=> 8941,
    "name"=> "Diamond Harbour",
    "lat"=> -43.6252,
    "lng"=> 172.7402
  ],
  [
    "postcode"=> 8942,
    "name"=> "Diamond Harbour",
    "lat"=> -45.2397,
    "lng"=> 178.0457
  ],
  [
    "postcode"=> 8971,
    "name"=> "Allandale",
    "lat"=> -43.6438,
    "lng"=> 172.6814
  ],
  [
    "postcode"=> 8972,
    "name"=> "Pigeon Bay",
    "lat"=> -43.658,
    "lng"=> 172.8054
  ],
  [
    "postcode"=> 9010,
    "name"=> "Liberton",
    "lat"=> -45.8366,
    "lng"=> 170.5074
  ],
  [
    "postcode"=> 9011,
    "name"=> "Mornington",
    "lat"=> -45.8792,
    "lng"=> 170.472
  ],
  [
    "postcode"=> 9012,
    "name"=> "Saint Kilda",
    "lat"=> -45.9059,
    "lng"=> 170.4912
  ],
  [
    "postcode"=> 9013,
    "name"=> "Ocean Grove",
    "lat"=> -45.8962,
    "lng"=> 170.5368
  ],
  [
    "postcode"=> 9014,
    "name"=> "Broad Bay",
    "lat"=> -45.8447,
    "lng"=> 170.6214
  ],
  [
    "postcode"=> 9016,
    "name"=> "Waverley",
    "lat"=> -45.8714,
    "lng"=> 170.5119
  ],
  [
    "postcode"=> 9018,
    "name"=> "Sunnyvale",
    "lat"=> -45.9004,
    "lng"=> 170.4259
  ],
  [
    "postcode"=> 9019,
    "name"=> "Outram",
    "lat"=> -45.8603,
    "lng"=> 170.2312
  ],
  [
    "postcode"=> 9022,
    "name"=> "Maia",
    "lat"=> -45.8534,
    "lng"=> 170.5626
  ],
  [
    "postcode"=> 9023,
    "name"=> "Sawyers Bay",
    "lat"=> -45.8185,
    "lng"=> 170.6084
  ],
  [
    "postcode"=> 9024,
    "name"=> "Mosgiel",
    "lat"=> -45.8839,
    "lng"=> 170.3552
  ],
  [
    "postcode"=> 9035,
    "name"=> "Brighton",
    "lat"=> -45.9397,
    "lng"=> 170.337
  ],
  [
    "postcode"=> 9040,
    "name"=> "Maryhill",
    "lat"=> -45.881,
    "lng"=> 170.4796
  ],
  [
    "postcode"=> 9041,
    "name"=> "Opoho",
    "lat"=> -45.855,
    "lng"=> 170.5192
  ],
  [
    "postcode"=> 9042,
    "name"=> "Kaikorai",
    "lat"=> -45.8579,
    "lng"=> 170.4783
  ],
  [
    "postcode"=> 9043,
    "name"=> "North Dunedin",
    "lat"=> -45.8584,
    "lng"=> 170.501
  ],
  [
    "postcode"=> 9044,
    "name"=> "Saint Kilda",
    "lat"=> -45.8944,
    "lng"=> 170.4994
  ],
  [
    "postcode"=> 9045,
    "name"=> "Caversham",
    "lat"=> -45.8964,
    "lng"=> 170.4806
  ],
  [
    "postcode"=> 9046,
    "name"=> "Tainui",
    "lat"=> -45.9003,
    "lng"=> 170.5041
  ],
  [
    "postcode"=> 9047,
    "name"=> "Saint Clair",
    "lat"=> -45.9062,
    "lng"=> 170.4874
  ],
  [
    "postcode"=> 9048,
    "name"=> "Portobello",
    "lat"=> -45.8394,
    "lng"=> 170.6511
  ],
  [
    "postcode"=> 9049,
    "name"=> "Saint Kilda",
    "lat"=> -45.8976,
    "lng"=> 170.5154
  ],
  [
    "postcode"=> 9050,
    "name"=> "Port Chalmers",
    "lat"=> -45.8157,
    "lng"=> 170.6218
  ],
  [
    "postcode"=> 9052,
    "name"=> "Green Island",
    "lat"=> -45.9017,
    "lng"=> 170.4296
  ],
  [
    "postcode"=> 9053,
    "name"=> "Mosgiel",
    "lat"=> -45.8758,
    "lng"=> 170.3489
  ],
  [
    "postcode"=> 9054,
    "name"=> "Dunedin",
    "lat"=> -45.8777,
    "lng"=> 170.5018
  ],
  [
    "postcode"=> 9058,
    "name"=> "Dunedin",
    "lat"=> -45.8724,
    "lng"=> 170.5036
  ],
  [
    "postcode"=> 9059,
    "name"=> "Dunedin",
    "lat"=> -45.8661,
    "lng"=> 170.5096
  ],
  [
    "postcode"=> 9062,
    "name"=> "Outram",
    "lat"=> -45.8566,
    "lng"=> 170.2312
  ],
  [
    "postcode"=> 9067,
    "name"=> "Middlemarch",
    "lat"=> -45.507,
    "lng"=> 170.119
  ],
  [
    "postcode"=> 9069,
    "name"=> "Waitati",
    "lat"=> -45.7457,
    "lng"=> 170.5681
  ],
  [
    "postcode"=> 9073,
    "name"=> "Berwick",
    "lat"=> -45.9556,
    "lng"=> 170.0903
  ],
  [
    "postcode"=> 9074,
    "name"=> "Clarks",
    "lat"=> -45.7542,
    "lng"=> 170.0897
  ],
  [
    "postcode"=> 9076,
    "name"=> "Wingatui",
    "lat"=> -45.8904,
    "lng"=> 170.3894
  ],
  [
    "postcode"=> 9077,
    "name"=> "Portobello",
    "lat"=> -45.8414,
    "lng"=> 170.6419
  ],
  [
    "postcode"=> 9081,
    "name"=> "Mihiwaka",
    "lat"=> -45.7734,
    "lng"=> 170.6412
  ],
  [
    "postcode"=> 9082,
    "name"=> "Aramoana",
    "lat"=> -45.7887,
    "lng"=> 170.6719
  ],
  [
    "postcode"=> 9085,
    "name"=> "Mount Cargill",
    "lat"=> -45.7894,
    "lng"=> 170.5412
  ],
  [
    "postcode"=> 9091,
    "name"=> "Kuri Bush",
    "lat"=> -46.0502,
    "lng"=> 170.2184
  ],
  [
    "postcode"=> 9092,
    "name"=> "Mosgiel",
    "lat"=> -45.8778,
    "lng"=> 170.3416
  ],
  [
    "postcode"=> 9210,
    "name"=> "Kaitangata",
    "lat"=> -46.2799,
    "lng"=> 169.841
  ],
  [
    "postcode"=> 9220,
    "name"=> "Milton",
    "lat"=> -46.1224,
    "lng"=> 169.9704
  ],
  [
    "postcode"=> 9230,
    "name"=> "Balclutha",
    "lat"=> -46.2388,
    "lng"=> 169.7372
  ],
  [
    "postcode"=> 9231,
    "name"=> "Stirling",
    "lat"=> -46.2489,
    "lng"=> 169.7761
  ],
  [
    "postcode"=> 9240,
    "name"=> "Balclutha",
    "lat"=> -46.2388,
    "lng"=> 169.7388
  ],
  [
    "postcode"=> 9241,
    "name"=> "Milton",
    "lat"=> -46.1225,
    "lng"=> 169.9577
  ],
  [
    "postcode"=> 9242,
    "name"=> "Clydevale",
    "lat"=> -46.1006,
    "lng"=> 169.5278
  ],
  [
    "postcode"=> 9271,
    "name"=> "Glenomaru",
    "lat"=> -46.3681,
    "lng"=> 169.6834
  ],
  [
    "postcode"=> 9272,
    "name"=> "Hillend",
    "lat"=> -46.1549,
    "lng"=> 169.711
  ],
  [
    "postcode"=> 9273,
    "name"=> "Warepa",
    "lat"=> -46.2563,
    "lng"=> 169.5825
  ],
  [
    "postcode"=> 9274,
    "name"=> "Greenfield",
    "lat"=> -46.1034,
    "lng"=> 169.5665
  ],
  [
    "postcode"=> 9281,
    "name"=> "Kaitangata",
    "lat"=> -46.2711,
    "lng"=> 169.8161
  ],
  [
    "postcode"=> 9282,
    "name"=> "Wangaloa",
    "lat"=> -46.2844,
    "lng"=> 169.9177
  ],
  [
    "postcode"=> 9291,
    "name"=> "Milburn",
    "lat"=> -46.0511,
    "lng"=> 169.9467
  ],
  [
    "postcode"=> 9292,
    "name"=> "Tokoiti",
    "lat"=> -46.1673,
    "lng"=> 169.9725
  ],
  [
    "postcode"=> 9300,
    "name"=> "Frankton",
    "lat"=> -45.0379,
    "lng"=> 168.6879
  ],
  [
    "postcode"=> 9302,
    "name"=> "Arrowtown",
    "lat"=> -44.9446,
    "lng"=> 168.83
  ],
  [
    "postcode"=> 9304,
    "name"=> "Lower Shotover",
    "lat"=> -45.002,
    "lng"=> 168.7928
  ],
  [
    "postcode"=> 9305,
    "name"=> "Wanaka",
    "lat"=> -44.6862,
    "lng"=> 169.1274
  ],
  [
    "postcode"=> 9310,
    "name"=> "Cromwell",
    "lat"=> -45.042,
    "lng"=> 169.1991
  ],
  [
    "postcode"=> 9320,
    "name"=> "Bridge Hill",
    "lat"=> -45.2502,
    "lng"=> 169.3859
  ],
  [
    "postcode"=> 9330,
    "name"=> "Clyde",
    "lat"=> -45.1907,
    "lng"=> 169.3237
  ],
  [
    "postcode"=> 9332,
    "name"=> "Ranfurly",
    "lat"=> -45.1272,
    "lng"=> 170.1027
  ],
  [
    "postcode"=> 9340,
    "name"=> "Bridge Hill",
    "lat"=> -45.2562,
    "lng"=> 169.3928
  ],
  [
    "postcode"=> 9341,
    "name"=> "Clyde",
    "lat"=> -45.187,
    "lng"=> 169.3169
  ],
  [
    "postcode"=> 9342,
    "name"=> "Cromwell",
    "lat"=> -45.037,
    "lng"=> 169.1977
  ],
  [
    "postcode"=> 9343,
    "name"=> "Wanaka",
    "lat"=> -44.6943,
    "lng"=> 169.1392
  ],
  [
    "postcode"=> 9344,
    "name"=> "Albert Town",
    "lat"=> -44.6826,
    "lng"=> 169.1886
  ],
  [
    "postcode"=> 9345,
    "name"=> "Lake Hawea",
    "lat"=> -44.6108,
    "lng"=> 169.2564
  ],
  [
    "postcode"=> 9346,
    "name"=> "Makarora",
    "lat"=> -44.2315,
    "lng"=> 169.2322
  ],
  [
    "postcode"=> 9347,
    "name"=> "Tarras",
    "lat"=> -44.8376,
    "lng"=> 169.4117
  ],
  [
    "postcode"=> 9348,
    "name"=> "Queenstown",
    "lat"=> -45.0319,
    "lng"=> 168.6622
  ],
  [
    "postcode"=> 9349,
    "name"=> "Frankton",
    "lat"=> -45.0159,
    "lng"=> 168.731
  ],
  [
    "postcode"=> 9350,
    "name"=> "Glenorchy",
    "lat"=> -44.8498,
    "lng"=> 168.3841
  ],
  [
    "postcode"=> 9351,
    "name"=> "Arrowtown",
    "lat"=> -44.9388,
    "lng"=> 168.833
  ],
  [
    "postcode"=> 9352,
    "name"=> "Omakau",
    "lat"=> -45.0939,
    "lng"=> 169.6023
  ],
  [
    "postcode"=> 9353,
    "name"=> "Ranfurly",
    "lat"=> -45.1301,
    "lng"=> 170.0998
  ],
  [
    "postcode"=> 9354,
    "name"=> "Naseby",
    "lat"=> -45.0232,
    "lng"=> 170.1467
  ],
  [
    "postcode"=> 9355,
    "name"=> "Wedderburn",
    "lat"=> -45.0301,
    "lng"=> 170.01
  ],
  [
    "postcode"=> 9356,
    "name"=> "Mataura",
    "lat"=> -46.1943,
    "lng"=> 168.866
  ],
  [
    "postcode"=> 9371,
    "name"=> "Upper Shotover",
    "lat"=> -44.8414,
    "lng"=> 168.7167
  ],
  [
    "postcode"=> 9372,
    "name"=> "Glenorchy",
    "lat"=> -44.7968,
    "lng"=> 168.3693
  ],
  [
    "postcode"=> 9376,
    "name"=> "Matakanui",
    "lat"=> -45.0071,
    "lng"=> 169.5416
  ],
  [
    "postcode"=> 9377,
    "name"=> "Becks",
    "lat"=> -44.9861,
    "lng"=> 169.7227
  ],
  [
    "postcode"=> 9381,
    "name"=> "Glendhu",
    "lat"=> -44.767,
    "lng"=> 168.9844
  ],
  [
    "postcode"=> 9382,
    "name"=> "Makarora Wharf",
    "lat"=> -44.4032,
    "lng"=> 168.8713
  ],
  [
    "postcode"=> 9383,
    "name"=> "Lindis Crossing",
    "lat"=> -44.9205,
    "lng"=> 169.4594
  ],
  [
    "postcode"=> 9384,
    "name"=> "Gibbston",
    "lat"=> -45.0464,
    "lng"=> 169.0655
  ],
  [
    "postcode"=> 9386,
    "name"=> "Saint Bathans",
    "lat"=> -44.8322,
    "lng"=> 169.8647
  ],
  [
    "postcode"=> 9387,
    "name"=> "Moa Creek",
    "lat"=> -45.2408,
    "lng"=> 169.7576
  ],
  [
    "postcode"=> 9391,
    "name"=> "Clyde",
    "lat"=> -45.192,
    "lng"=> 169.294
  ],
  [
    "postcode"=> 9392,
    "name"=> "Fruitlands",
    "lat"=> -45.3788,
    "lng"=> 169.3764
  ],
  [
    "postcode"=> 9393,
    "name"=> "Chatto Creek",
    "lat"=> -45.1166,
    "lng"=> 169.4805
  ],
  [
    "postcode"=> 9395,
    "name"=> "Gimmerburn",
    "lat"=> -45.1302,
    "lng"=> 169.961
  ],
  [
    "postcode"=> 9396,
    "name"=> "Naseby",
    "lat"=> -44.9052,
    "lng"=> 170.1778
  ],
  [
    "postcode"=> 9397,
    "name"=> "Kokonga",
    "lat"=> -45.2161,
    "lng"=> 170.2615
  ],
  [
    "postcode"=> 9398,
    "name"=> "Paerau",
    "lat"=> -45.3578,
    "lng"=> 169.9176
  ],
  [
    "postcode"=> 9400,
    "name"=> "Oamaru",
    "lat"=> -45.0928,
    "lng"=> 170.9784
  ],
  [
    "postcode"=> 9401,
    "name"=> "Waiareka Junction",
    "lat"=> -45.0941,
    "lng"=> 170.931
  ],
  [
    "postcode"=> 9410,
    "name"=> "Hampden",
    "lat"=> -45.3318,
    "lng"=> 170.7353
  ],
  [
    "postcode"=> 9412,
    "name"=> "Berwen",
    "lat"=> -44.4715,
    "lng"=> 169.8719
  ],
  [
    "postcode"=> 9430,
    "name"=> "Palmerston",
    "lat"=> -45.4816,
    "lng"=> 170.7112
  ],
  [
    "postcode"=> 9435,
    "name"=> "Wharekuri",
    "lat"=> -44.7296,
    "lng"=> 170.334
  ],
  [
    "postcode"=> 9441,
    "name"=> "Roxburgh",
    "lat"=> -45.5409,
    "lng"=> 169.3153
  ],
  [
    "postcode"=> 9442,
    "name"=> "Hampden",
    "lat"=> -45.3238,
    "lng"=> 170.8172
  ],
  [
    "postcode"=> 9443,
    "name"=> "Palmerston",
    "lat"=> -45.4854,
    "lng"=> 170.7146
  ],
  [
    "postcode"=> 9444,
    "name"=> "Oamaru",
    "lat"=> -45.0976,
    "lng"=> 170.9701
  ],
  [
    "postcode"=> 9445,
    "name"=> "Duntroon",
    "lat"=> -44.8538,
    "lng"=> 170.6817
  ],
  [
    "postcode"=> 9446,
    "name"=> "Kurow",
    "lat"=> -44.733,
    "lng"=> 170.4698
  ],
  [
    "postcode"=> 9447,
    "name"=> "Otematata",
    "lat"=> -44.6063,
    "lng"=> 170.1929
  ],
  [
    "postcode"=> 9448,
    "name"=> "Omarama",
    "lat"=> -44.4883,
    "lng"=> 169.966
  ],
  [
    "postcode"=> 9471,
    "name"=> "Seacliff",
    "lat"=> -45.6302,
    "lng"=> 170.5342
  ],
  [
    "postcode"=> 9472,
    "name"=> "Waikouaiti",
    "lat"=> -45.552,
    "lng"=> 170.646
  ],
  [
    "postcode"=> 9481,
    "name"=> "Stoneburn",
    "lat"=> -45.4719,
    "lng"=> 170.605
  ],
  [
    "postcode"=> 9482,
    "name"=> "Katiki",
    "lat"=> -45.4259,
    "lng"=> 170.7892
  ],
  [
    "postcode"=> 9483,
    "name"=> "Macraes Flat",
    "lat"=> -45.3444,
    "lng"=> 170.4595
  ],
  [
    "postcode"=> 9491,
    "name"=> "Tokarahi",
    "lat"=> -45.017,
    "lng"=> 170.6487
  ],
  [
    "postcode"=> 9492,
    "name"=> "Kauru Hill",
    "lat"=> -45.1101,
    "lng"=> 170.7251
  ],
  [
    "postcode"=> 9493,
    "name"=> "Hilderthorpe",
    "lat"=> -44.9948,
    "lng"=> 171.0596
  ],
  [
    "postcode"=> 9494,
    "name"=> "Island Cliff",
    "lat"=> -44.9141,
    "lng"=> 170.7018
  ],
  [
    "postcode"=> 9495,
    "name"=> "Herbert",
    "lat"=> -45.2071,
    "lng"=> 170.795
  ],
  [
    "postcode"=> 9498,
    "name"=> "Kaingapai",
    "lat"=> -44.6013,
    "lng"=> 170.5476
  ],
  [
    "postcode"=> 9500,
    "name"=> "Roxburgh",
    "lat"=> -45.5421,
    "lng"=> 169.2875
  ],
  [
    "postcode"=> 9510,
    "name"=> "Waikouaiti",
    "lat"=> -45.5923,
    "lng"=> 170.6938
  ],
  [
    "postcode"=> 9522,
    "name"=> "Tapanui",
    "lat"=> -45.9434,
    "lng"=> 169.2597
  ],
  [
    "postcode"=> 9532,
    "name"=> "Lawrence",
    "lat"=> -45.9189,
    "lng"=> 169.6862
  ],
  [
    "postcode"=> 9534,
    "name"=> "Clinton",
    "lat"=> -46.202,
    "lng"=> 169.379
  ],
  [
    "postcode"=> 9535,
    "name"=> "Owaka",
    "lat"=> -46.4524,
    "lng"=> 169.6592
  ],
  [
    "postcode"=> 9541,
    "name"=> "Waikouaiti",
    "lat"=> -45.5958,
    "lng"=> 170.6701
  ],
  [
    "postcode"=> 9542,
    "name"=> "Tapanui",
    "lat"=> -45.9435,
    "lng"=> 169.2613
  ],
  [
    "postcode"=> 9543,
    "name"=> "Lawrence",
    "lat"=> -45.9129,
    "lng"=> 169.6866
  ],
  [
    "postcode"=> 9544,
    "name"=> "Millers Flat",
    "lat"=> -45.6629,
    "lng"=> 169.4118
  ],
  [
    "postcode"=> 9546,
    "name"=> "Owaka",
    "lat"=> -46.4511,
    "lng"=> 169.6583
  ],
  [
    "postcode"=> 9548,
    "name"=> "Clinton",
    "lat"=> -46.2023,
    "lng"=> 169.3741
  ],
  [
    "postcode"=> 9571,
    "name"=> "Roxburgh Hydro",
    "lat"=> -45.4504,
    "lng"=> 169.4062
  ],
  [
    "postcode"=> 9572,
    "name"=> "Millers Flat",
    "lat"=> -45.6087,
    "lng"=> 169.517
  ],
  [
    "postcode"=> 9583,
    "name"=> "Clinton",
    "lat"=> -46.312,
    "lng"=> 169.2557
  ],
  [
    "postcode"=> 9584,
    "name"=> "Clinton",
    "lat"=> -46.1678,
    "lng"=> 169.3947
  ],
  [
    "postcode"=> 9585,
    "name"=> "New Haven",
    "lat"=> -46.4584,
    "lng"=> 169.704
  ],
  [
    "postcode"=> 9586,
    "name"=> "Puketiro",
    "lat"=> -46.4864,
    "lng"=> 169.448
  ],
  [
    "postcode"=> 9587,
    "name"=> "Park Hill",
    "lat"=> -45.7378,
    "lng"=> 169.2347
  ],
  [
    "postcode"=> 9588,
    "name"=> "Raes Junction",
    "lat"=> -45.7799,
    "lng"=> 169.475
  ],
  [
    "postcode"=> 9591,
    "name"=> "Beaumont",
    "lat"=> -45.7898,
    "lng"=> 169.6184
  ],
  [
    "postcode"=> 9593,
    "name"=> "Waitahuna",
    "lat"=> -45.9805,
    "lng"=> 169.7257
  ],
  [
    "postcode"=> 9596,
    "name"=> "Ngapuna",
    "lat"=> -45.4417,
    "lng"=> 170.1501
  ],
  [
    "postcode"=> 9597,
    "name"=> "Rocklands",
    "lat"=> -45.5953,
    "lng"=> 170.0034
  ],
  [
    "postcode"=> 9598,
    "name"=> "Ngapuna",
    "lat"=> -45.5065,
    "lng"=> 170.2682
  ],
  [
    "postcode"=> 9600,
    "name"=> "Te Anau",
    "lat"=> -45.4033,
    "lng"=> 167.7195
  ],
  [
    "postcode"=> 9610,
    "name"=> "Otautau",
    "lat"=> -46.1439,
    "lng"=> 167.9955
  ],
  [
    "postcode"=> 9620,
    "name"=> "Tuatapere",
    "lat"=> -46.1327,
    "lng"=> 167.6766
  ],
  [
    "postcode"=> 9630,
    "name"=> "Nightcaps",
    "lat"=> -45.9708,
    "lng"=> 168.0264
  ],
  [
    "postcode"=> 9635,
    "name"=> "Ohai",
    "lat"=> -45.9322,
    "lng"=> 167.9562
  ],
  [
    "postcode"=> 9640,
    "name"=> "Te Anau",
    "lat"=> -45.4152,
    "lng"=> 167.7164
  ],
  [
    "postcode"=> 9641,
    "name"=> "Otautau",
    "lat"=> -46.1435,
    "lng"=> 167.9974
  ],
  [
    "postcode"=> 9642,
    "name"=> "Tuatapere",
    "lat"=> -46.1332,
    "lng"=> 167.6892
  ],
  [
    "postcode"=> 9643,
    "name"=> "Manapouri",
    "lat"=> -45.5661,
    "lng"=> 167.6092
  ],
  [
    "postcode"=> 9644,
    "name"=> "Nightcaps",
    "lat"=> -45.9702,
    "lng"=> 168.0321
  ],
  [
    "postcode"=> 9645,
    "name"=> "Ohai",
    "lat"=> -45.9326,
    "lng"=> 167.9568
  ],
  [
    "postcode"=> 9672,
    "name"=> "The Key",
    "lat"=> -45.3207,
    "lng"=> 168.0962
  ],
  [
    "postcode"=> 9679,
    "name"=> "Te Anau",
    "lat"=> -45.0353,
    "lng"=> 167.5133
  ],
  [
    "postcode"=> 9682,
    "name"=> "Eastern Bush",
    "lat"=> -45.9426,
    "lng"=> 167.7666
  ],
  [
    "postcode"=> 9683,
    "name"=> "Fairfax",
    "lat"=> -46.1885,
    "lng"=> 168.0818
  ],
  [
    "postcode"=> 9689,
    "name"=> "Ngaro",
    "lat"=> -45.9004,
    "lng"=> 167.9675
  ],
  [
    "postcode"=> 9691,
    "name"=> "Wairoto",
    "lat"=> -45.8003,
    "lng"=> 167.1138
  ],
  [
    "postcode"=> 9710,
    "name"=> "East Gore",
    "lat"=> -46.0997,
    "lng"=> 168.9357
  ],
  [
    "postcode"=> 9712,
    "name"=> "Mataura",
    "lat"=> -46.1902,
    "lng"=> 168.8673
  ],
  [
    "postcode"=> 9720,
    "name"=> "Winton",
    "lat"=> -46.1414,
    "lng"=> 168.3187
  ],
  [
    "postcode"=> 9730,
    "name"=> "Lumsden",
    "lat"=> -45.7391,
    "lng"=> 168.4462
  ],
  [
    "postcode"=> 9740,
    "name"=> "East Gore",
    "lat"=> -46.1,
    "lng"=> 168.9448
  ],
  [
    "postcode"=> 9741,
    "name"=> "Winton",
    "lat"=> -46.1428,
    "lng"=> 168.3247
  ],
  [
    "postcode"=> 9742,
    "name"=> "Lumsden",
    "lat"=> -45.7392,
    "lng"=> 168.4415
  ],
  [
    "postcode"=> 9743,
    "name"=> "Waikaka",
    "lat"=> -45.9215,
    "lng"=> 169.0145
  ],
  [
    "postcode"=> 9744,
    "name"=> "Riversdale",
    "lat"=> -45.9021,
    "lng"=> 168.7426
  ],
  [
    "postcode"=> 9745,
    "name"=> "Waikaia",
    "lat"=> -45.7249,
    "lng"=> 168.8484
  ],
  [
    "postcode"=> 9746,
    "name"=> "Balfour",
    "lat"=> -45.8377,
    "lng"=> 168.5889
  ],
  [
    "postcode"=> 9747,
    "name"=> "Mossburn",
    "lat"=> -45.6698,
    "lng"=> 168.2389
  ],
  [
    "postcode"=> 9748,
    "name"=> "Kingston",
    "lat"=> -45.3381,
    "lng"=> 168.7237
  ],
  [
    "postcode"=> 9749,
    "name"=> "Athol",
    "lat"=> -45.5095,
    "lng"=> 168.5739
  ],
  [
    "postcode"=> 9750,
    "name"=> "Garston",
    "lat"=> -45.467,
    "lng"=> 168.6838
  ],
  [
    "postcode"=> 9752,
    "name"=> "Dipton",
    "lat"=> -45.8989,
    "lng"=> 168.3698
  ],
  [
    "postcode"=> 9771,
    "name"=> "Conical Hills",
    "lat"=> -46.0435,
    "lng"=> 169.22
  ],
  [
    "postcode"=> 9772,
    "name"=> "Pukerau",
    "lat"=> -46.1897,
    "lng"=> 169.0657
  ],
  [
    "postcode"=> 9773,
    "name"=> "Wendon Valley",
    "lat"=> -45.9083,
    "lng"=> 168.9263
  ],
  [
    "postcode"=> 9774,
    "name"=> "Mataura",
    "lat"=> -46.1756,
    "lng"=> 168.8082
  ],
  [
    "postcode"=> 9775,
    "name"=> "Waikoikoi",
    "lat"=> -45.946,
    "lng"=> 169.1213
  ],
  [
    "postcode"=> 9776,
    "name"=> "Riversdale",
    "lat"=> -45.8847,
    "lng"=> 168.7116
  ],
  [
    "postcode"=> 9777,
    "name"=> "Waipounamu",
    "lat"=> -45.836,
    "lng"=> 168.7523
  ],
  [
    "postcode"=> 9778,
    "name"=> "Garston",
    "lat"=> -45.526,
    "lng"=> 168.9768
  ],
  [
    "postcode"=> 9779,
    "name"=> "Balfour",
    "lat"=> -45.8426,
    "lng"=> 168.5269
  ],
  [
    "postcode"=> 9781,
    "name"=> "Lochiel",
    "lat"=> -46.2018,
    "lng"=> 168.3909
  ],
  [
    "postcode"=> 9782,
    "name"=> "Otapiri Gorge",
    "lat"=> -46.0726,
    "lng"=> 168.4668
  ],
  [
    "postcode"=> 9783,
    "name"=> "South Hillend",
    "lat"=> -45.9932,
    "lng"=> 168.1991
  ],
  [
    "postcode"=> 9791,
    "name"=> "Dipton",
    "lat"=> -45.9097,
    "lng"=> 168.4293
  ],
  [
    "postcode"=> 9792,
    "name"=> "Mossburn",
    "lat"=> -45.6973,
    "lng"=> 168.2036
  ],
  [
    "postcode"=> 9793,
    "name"=> "Kingston",
    "lat"=> -45.3967,
    "lng"=> 168.6723
  ],
  [
    "postcode"=> 9794,
    "name"=> "Lumsden",
    "lat"=> -45.7516,
    "lng"=> 168.4602
  ],
  [
    "postcode"=> 9810,
    "name"=> "Waverley",
    "lat"=> -46.3894,
    "lng"=> 168.3722
  ],
  [
    "postcode"=> 9812,
    "name"=> "Tisbury",
    "lat"=> -46.4382,
    "lng"=> 168.3764
  ],
  [
    "postcode"=> 9814,
    "name"=> "Bluff",
    "lat"=> -46.6035,
    "lng"=> 168.3309
  ],
  [
    "postcode"=> 9816,
    "name"=> "Wallacetown",
    "lat"=> -46.339,
    "lng"=> 168.2904
  ],
  [
    "postcode"=> 9818,
    "name"=> "Halfmoon Bay (Oban)",
    "lat"=> -46.9877,
    "lng"=> 167.8382
  ],
  [
    "postcode"=> 9822,
    "name"=> "Riverton",
    "lat"=> -46.3631,
    "lng"=> 168.0165
  ],
  [
    "postcode"=> 9825,
    "name"=> "Edendale",
    "lat"=> -46.3084,
    "lng"=> 168.7938
  ],
  [
    "postcode"=> 9831,
    "name"=> "Wyndham",
    "lat"=> -46.3304,
    "lng"=> 168.8438
  ],
  [
    "postcode"=> 9840,
    "name"=> "Richmond",
    "lat"=> -46.4109,
    "lng"=> 168.3511
  ],
  [
    "postcode"=> 9841,
    "name"=> "Glengarry",
    "lat"=> -46.3982,
    "lng"=> 168.3658
  ],
  [
    "postcode"=> 9842,
    "name"=> "East End",
    "lat"=> -46.5991,
    "lng"=> 168.3436
  ],
  [
    "postcode"=> 9843,
    "name"=> "Waikiwi",
    "lat"=> -46.3772,
    "lng"=> 168.3471
  ],
  [
    "postcode"=> 9844,
    "name"=> "Strathern",
    "lat"=> -46.4276,
    "lng"=> 168.3612
  ],
  [
    "postcode"=> 9845,
    "name"=> "Hawthorndale",
    "lat"=> -46.4017,
    "lng"=> 168.3853
  ],
  [
    "postcode"=> 9846,
    "name"=> "Half-moon Bay",
    "lat"=> -46.8992,
    "lng"=> 168.1292
  ],
  [
    "postcode"=> 9847,
    "name"=> "Riverton",
    "lat"=> -46.3522,
    "lng"=> 168.0136
  ],
  [
    "postcode"=> 9848,
    "name"=> "Edendale",
    "lat"=> -46.3132,
    "lng"=> 168.7842
  ],
  [
    "postcode"=> 9849,
    "name"=> "Wyndham",
    "lat"=> -46.3322,
    "lng"=> 168.8477
  ],
  [
    "postcode"=> 9871,
    "name"=> "Woodlands",
    "lat"=> -46.3867,
    "lng"=> 168.5511
  ],
  [
    "postcode"=> 9872,
    "name"=> "Mabel Bush",
    "lat"=> -46.2616,
    "lng"=> 168.5395
  ],
  [
    "postcode"=> 9874,
    "name"=> "Waianiwa",
    "lat"=> -46.2773,
    "lng"=> 168.2387
  ],
  [
    "postcode"=> 9875,
    "name"=> "Fortrose",
    "lat"=> -46.553,
    "lng"=> 168.722
  ],
  [
    "postcode"=> 9876,
    "name"=> "Makarewa",
    "lat"=> -46.3136,
    "lng"=> 168.4063
  ],
  [
    "postcode"=> 9877,
    "name"=> "Awarua Plains",
    "lat"=> -46.5072,
    "lng"=> 168.4056
  ],
  [
    "postcode"=> 9879,
    "name"=> "Otatara",
    "lat"=> -46.4308,
    "lng"=> 168.2429
  ],
  [
    "postcode"=> 9881,
    "name"=> "Round Hill",
    "lat"=> -46.2657,
    "lng"=> 167.811
  ],
  [
    "postcode"=> 9883,
    "name"=> "Gummies Bush",
    "lat"=> -46.269,
    "lng"=> 168.0316
  ],
  [
    "postcode"=> 9884,
    "name"=> "Niagara",
    "lat"=> -46.585,
    "lng"=> 169.0999
  ],
  [
    "postcode"=> 9891,
    "name"=> "Glenham",
    "lat"=> -46.4308,
    "lng"=> 168.8784
  ],
  [
    "postcode"=> 9892,
    "name"=> "Mokoreta",
    "lat"=> -46.3907,
    "lng"=> 169.0861
  ],
  [
    "postcode"=> 9893,
    "name"=> "Kamahi",
    "lat"=> -46.3644,
    "lng"=> 168.7108
  ]
];
