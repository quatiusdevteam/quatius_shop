<?php 

return [
    'product-not-deliverable'=>'Some product(s) can not deliver to your address, please use pickup instead.',
    'product-no-price'=>'No price available',
    'product-not-available-destination'=>'Stocks are not available to be deliver to your area (%s).',
    'product-out-of-stock'=>'Some product(s) are out of stock',
    'product-field-require'=>'Some product information required.',
    'cart-is-empty'=>'Your cart is empty.',
    'address-required'=>'Address information required.',
    'address-country-not-allowed'=>'Please select a valid country',
    'payment-method-not-found'=>'Please select a payment method!',
    'session-expired'=>'Your Order has session expired!',
    'payment-completed'=>'Payment completed successfully',
    'payment-failed'=>'Unable to process your payment!',
    'cart-empty' => 'Your cart is empty.',
    'empty-confirm' => 'Remove all items in the cart?',
    'buttons'=>[
        'empty-cart' => 'Clear Cart',
        'yes' =>'Yes',
        'no' =>'No',
        'checkout' =>'CHECKOUT',
        'confirm' =>'CONFIRM'
    ],
    'status' => 
		[
			'Cancelled'=>'Cancelled',
			'pending' => 'Pending', 
      'confirmed'=>'Confirmed',  
			'loading'=>'Loading', 
			'processing' => 'Processing', 
			'refunded'=>'Refunded', 
			'refunded - partial' => 'Refunded Partially', 
			'picked'=>'Picked', 
			'delivered'=>'Delivered', 
			'completed'=>'Completed'
		],

    'order'                  => [
    	'options'     => [
            'status'  => ['male' => 'Male', 'female' => 'Female', 'other' => 'Other']
		],
		
		'label' => [
			'order_number' => 'Order Number:'
		]
    ],
    'courier'   => [
        'LJW'   => 'https://auspost.com.au/parcels-mail/track.html#/track?id=',
    ]
];
