<?php 

return [
    'BT' => 'Billing Address',
    'ST' => 'Shipment Address',
    'invalid'=>["type"=>"Invalid address type"],
    'country-codes'=>[
        'AU'=>'Australia',
        'NZ'=>'New Zealand'
    ]
];
