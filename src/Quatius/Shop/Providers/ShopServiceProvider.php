<?php

namespace Quatius\Shop\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Route;
use Prettus\Repository\Providers\RepositoryServiceProvider;
use Event;
use Quatius\Shop\Events\ShopListeners;
use Quatius\Shop\Models\OrderSession;
use Illuminate\Foundation\AliasLoader;
use Theme;

class ShopServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
	
    public function boot()
    {
        
        Route::bind('address', function($value){
            return \Quatius\Shop\Models\UserDetail::find(hashids_decode($value));
        });
        
        Route::bind('category_slug', function($value) {
            $suffix = config('quatius.shop.category.url_suffix','');
            $suffix_len = strlen($suffix);
            if ($suffix_len > 0)
            {
                abort_unless($suffix == substr($value,-$suffix_len), 404, "Category Not found");
                
                $value = substr($value,0,-$suffix_len);
            }
            return app("CategoryHandler")->getFromSlug($value);
    	});
    		
		Route::bind('product_slug', function($value) {
		    $suffix = config('quatius.shop.product.url_suffix','');
		    $suffix_len = strlen($suffix);
		    if ($suffix_len > 0)
		    {
		        abort_unless($suffix == substr($value,-$suffix_len), 404, "Product Not found");
		        
		        $value = substr($value,0,-$suffix_len);
		    }
		    return app("ProductHandler")->getFromSlug($value);
		});
		
	    Route::bind('product_id',function($value) {
	        return hashids_decode($value)?:abort(404,'Route Key Invalid');
	    });
	    
    	Route::bind('category_id',function($value) {
	        return hashids_decode($value)?:abort(404,'Route Key Invalid');
	    });
    	
    	$this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
    	$this->publishes([
    	    realpath(__DIR__.'/../').'/Publishes/Shop'=> app_path('Modules/Shop'),
    	]);
    	
    	$this->publishes([
    	    realpath(__DIR__.'/../').'/Publishes/public'=>public_path()
    	]);
    	
    	$this->publishes([
            realpath(__DIR__.'/../').'/Publishes/public-forced'=>public_path(),
            realpath(__DIR__.'/../').'/Publishes/Modules'=>app_path('Modules')
    	], 'public');
    }
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Barryvdh\DomPDF\ServiceProvider::class);
        $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        
        AliasLoader::getInstance()->alias('PDF', \Barryvdh\DomPDF\Facade::class);
        AliasLoader::getInstance()->alias('Debugbar', \Barryvdh\Debugbar\Facade::class);
        
        $this->app->singleton('order', function($app)
        {
            return OrderSession::getOrder();
        });

        $this->app->bind('OrderSession', function($app)
        {
            return $app->make('order');
        });
        
        Event::subscribe(ShopListeners::class);
        
        $this->app->register(RepositoryServiceProvider::class);
        
       // $this->app->bind('Quatius\Shop\Repositories\DescriptionRepository','Quatius\Shop\Repositories\Handlers\DescriptionHandler');
        //$this->app->bind('Quatius\Shop\Repositories\DescriptionRepository','Quatius\Shop\Repositories\Handlers\DescriptionHandler');
        
        $this->app->make(Factory::class)->load(__DIR__.'/../Databases/factories');
        
        $this->app->bind('Quatius\Shop\Repositories\ShipmentRepository','Quatius\Shop\Repositories\Handlers\ShipmentHandler');
        $this->app->bind('ShipmentHandler','Quatius\Shop\Repositories\ShipmentRepository');
        
        // $this->app->bind('Quatius\Shop\Repositories\PaymentRepository','Quatius\Shop\Repositories\Handlers\PaymentHandler');
        // $this->app->bind('PaymentHandler','Quatius\Shop\Repositories\PaymentRepository');
        
        $this->app->bind('Quatius\Shop\Repositories\ShopperRepository','Quatius\Shop\Repositories\Handlers\ShopperHandler');
        $this->app->bind('ShopperHandler','Quatius\Shop\Repositories\ShopperRepository');
        
        $this->app->bind('Quatius\Shop\Repositories\SpecificationRepository','Quatius\Shop\Repositories\Handlers\SpecificationHandler');
        $this->app->bind('SpecificationHandler','Quatius\Shop\Repositories\SpecificationRepository');
        
        $this->app->bind('Quatius\Shop\Repositories\ProductRepository','Quatius\Shop\Repositories\Handlers\ProductHandler');
        $this->app->bind('ProductHandler','Quatius\Shop\Repositories\ProductRepository');
        
        $this->app->bind('Quatius\Shop\Repositories\CategoryRepository','Quatius\Shop\Repositories\Handlers\CategoryHandler');
        $this->app->bind('CategoryHandler','Quatius\Shop\Repositories\CategoryRepository');
        
        $this->app->bind('Quatius\Shop\Repositories\WarehouseRepository','Quatius\Shop\Repositories\Handlers\WarehouseHandler');
        $this->app->bind('WarehouseHandler','Quatius\Shop\Repositories\WarehouseRepository');
        
        $this->app->bind('Quatius\Shop\Repositories\ProductPriceRepository','Quatius\Shop\Repositories\Handlers\ProductPriceHandler');
        $this->app->bind('ProductPriceHandler','Quatius\Shop\Repositories\ProductPriceRepository');
        
        $this->app->bind('Quatius\Shop\Repositories\WishListRepository','Quatius\Shop\Repositories\Handlers\WishListHandler');
        $this->app->singleton('WishListHandler','Quatius\Shop\Repositories\WishListRepository');

        $this->app->bind('ProductIndexTransformer','Quatius\Shop\Repositories\Presenters\Admin\ProductIndexTransformer');
        $this->app->bind('SpecListTransformer','Quatius\Shop\Repositories\Presenters\Admin\SpecListTransformer');
        $this->app->bind('CartTransformer','Quatius\Shop\Repositories\Presenters\CartTransformer');
        $this->app->bind('ProductSearchTransformer','Quatius\Shop\Repositories\Presenters\ProductSearchTransformer');
    }
}
