<?php
return[
    'API'=>[
        'google_map'=>env('GOOGLEMAP_API', ''),
    ],
    'domain'=>'AU',
    
    'cart'=>[
        'session' => "user-order",
        'name' => "My Cart",
        'url_prefix' => "order/",
    ],
    'order'=>[
        'prefix' => "OR",
        'admin-override'    => [
            'status'    => [
                'pending'   => 'Pending',
                'confirmed' => 'Confirmed',
                'pending_no_answer'   => 'No answer ',
                'cancelled_bought_else_where' => 'Bought from somewhere else',
                'cancelled_change_mind' => 'Do not want to proceed',
                'cancelled_other' => 'Other',
            ],
        ],
    ],
    'invoice'=>[
        'path' => storage_path('app/shop/invoices'),
        'logo_print_path' => public_path().'/themes/public/assets/img/logo.png'
    ],
    
    'email'=>[
        'from' => env('APP_EMAIL_ADDRESS', null),
    'to'=> [
            'order_team'    => env('SHOP_ORDER_TEAM_EMAIL', ''),
        ],        'address' => env('APP_EMAIL_ADMIN_ADDRESS', null),
        'on-confirm'=>[
            'attach-pdf'=>true,
            'notify_customer'=>true,
            'notify_admin'=>true,
        ]
    ],
    'url' => [
        'no-image' => asset('images/shop/no-image.png'),
        'logo' => asset('themes/public/assets/img/logo.png'),
        'logo_print' => asset('themes/public/assets/img/logo.png')
    ],
    
    'product' =>[
        'model' => 'Quatius\\Shop\\Models\\Product',
        'url_prefix' => 'product/',
        'url_suffix' => '',
        'tile_img_size' => ['width'=>340, 'height'=>240],
    ],
    
    'price' =>[
        'model' => 'Quatius\\Shop\\Models\\ProductPrice',
        'default' => 'GUEST',
        'compared-to' => 'RRP',
        'symbol' => '$',
        'currency' => 'AUD',
        'use-cheapest' => true
    ],
    
    'tax'=>[
        'label'=>'GST',
        'percentage'=>0.1,
        'percentage-invert'=>11,
    ],
    
    'category' =>[
        'model' => 'Quatius\\Shop\\Models\\Category',
        'url_prefix' => 'category/',
        'url_suffix' => '',
        'root' => 0,
        'shoppers'=>[]
    ],
    
    'specification' =>[
        'model' => 'Quatius\\Shop\\Models\\Specification',
    ],
    
    'default-groups'=>[
        'guest' => 1,
        'registered' => 2,
        'wholesale' => 3
    ],
    
    'admin'=>[
        'product' => ['mode'=>env('SHOP_ADMIN_PRODUCT_MODE','advance')], // 'basic'
    ]
];