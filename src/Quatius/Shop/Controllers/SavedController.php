<?php

namespace App\Modules\Product\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Quatius\\Common\\Models\Save;
use App\Modules\Product\Models\Product;

class SavedController extends Controller
{
	public function __construct()
	{
		$this->middleware('web');
		$this->setupTheme(config('theme.map.public.theme'), config('theme.map.public.layout'));
	}
	
	public function save(Request $request, Product $product){
		if (!Auth::check())
			return "0";
		$product->saveForLater();
		
		$prodIds = Save::getSaveModelId('App\Modules\Product\Models\Product');
		return Product::whereIn('id',$prodIds)->status(2)->count();
	}
	
	public function unsave(Request $request, Product $product){
		if (!Auth::check())
			return "0";
		
		$product->removeSaved();
		
		$prodIds = Save::getSaveModelId('App\Modules\Product\Models\Product');
		return Product::whereIn('id',$prodIds)->status(2)->count();
	}
	
	public function index(Request $request){
		if (!Auth::check())
			return "";
		$name= $request->get("name", "My Favourite");
		$prodIds = Save::getSaveModelId('App\Modules\Product\Models\Product');
		return $this->theme->of('Product::list',["name"=>$name,"products"=>Product::whereIn('id',$prodIds)->status(2)->get()])->render();
	}
}
