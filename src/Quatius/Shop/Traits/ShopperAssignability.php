<?php

namespace Quatius\Shop\Traits;

use Quatius\Shop\Models\ShopperGroup;
use DB;

trait ShopperAssignability
{	
	public function shoppers()
	{
	    return $this->morphToMany(ShopperGroup::class, 'shopper_assignable');
	}
	
	public function getShoppers()
	{
		if (!$this->id && !isset($this->getRelations()['shoppers'])){
			$this->setRelation('shoppers', ShopperGroup::defaultShoppers()->get());
		}
		
		return $this->shoppers;
	}
	
	public function ScopeDistinctItem($query)
	{
		return $query->selectRaw('DISTINCT(id), '.$this->getTable().'.*');
	}
	
	public function isBelongToShopper($shopperGroupIds=[])
	{
	    return !! $this->getShoppers()->whereIn('id', $shopperGroupIds)->count();
	}
	
	public function getShopper()
	{
		if (!$this->id)
			return ShopperGroup::getDefault();
		
		return $this->shoppers()->orderBy('ordering','desc')->first();
	}
	
	public function ScopeOfShoppers($query, $shopperGroups = [])
	{
		$query->join('shopper_assignables', function ($join) use ($shopperGroups){
			if (!$shopperGroups) // fixed error for no shopper.
				$shopperGroups = [-1];
			
			// override shopper scope to support muliple class.
			if(method_exists(get_called_class(), "getSingleTableTypeMap"))
			{
			    $join->on('id', '=', 'shopper_assignable_id')
    			    ->whereIn('shopper_assignable_type',  self::getSingleTableTypeMap())
    			    ->whereIn('shopper_group_id', $shopperGroups);
			}
			else{
			    $join->on('id', '=', 'shopper_assignable_id')
			    ->where('shopper_assignable_type', '=', get_class($this))
			    ->whereIn('shopper_group_id', $shopperGroups);
			}
			
		});
		return $query;
	}
	
	public function ScopeWithShopper($query)
	{
	    if(method_exists(get_called_class(), "getSingleTableTypeMap")) {
	       $query->leftJoin(
	           DB::raw("(select shopper_assignable_id, shopper_group_id FROM shopper_assignables WHERE shopper_assignable_type in ('".implode("','",self::getSingleTableTypeMap())."') AS assignables"),
	           'shopper_assignable_id','=','id'
	        );
	    }
	    else{
	        $query->leftJoin(
	            DB::raw('(select shopper_assignable_id, shopper_group_id FROM shopper_assignables WHERE shopper_assignable_type = ?) AS assignables'),
	            'shopper_assignable_id','=','id'
	            );
	        
	        $query->addBinding(get_class($this), 'join');
	    }
	    
	    return $query;
	}
	
	public function scopeWithShopperIds($query){
	    if(method_exists(get_called_class(), "getSingleTableTypeMap")) {
	        $query->leftJoin(
                DB::raw("(SELECT shopper_assignable_id, GROUP_CONCAT(shopper_group_id) as shopper_group_ids FROM shopper_assignables WHERE shopper_assignable_type in ('".str_replace('\\', '\\\\',implode("','",self::getSingleTableTypeMap()))."') group by shopper_assignable_id) AS assignids"),
	            'id','assignids.shopper_assignable_id'
	            );
	    }
	    else{
	        $query->leftJoin(
	            DB::raw('(SELECT shopper_assignable_id, GROUP_CONCAT(shopper_group_id) as shopper_group_ids FROM shopper_assignables WHERE shopper_assignable_type = ? group by shopper_assignable_id) AS assignids'),
	            'id','assignids.shopper_assignable_id'
	            );
	        $query->addBinding(get_class($this), 'join');
	    }
	    
	    return $query;
	}
	
	public function scopeWithShopperNames($query){
	    if(method_exists(get_called_class(), "getSingleTableTypeMap")) {
	        $query->leftJoin(
	            DB::raw("(SELECT shopper_assignable_id, GROUP_CONCAT(name) as shopper_names FROM shopper_assignables LEFT JOIN shopper_groups on id=shopper_group_id WHERE shopper_assignable_type in ('".str_replace('\\', '\\\\',implode("','",self::getSingleTableTypeMap()))."') group by shopper_assignable_id) AS assignables"),
	            'id','assignables.shopper_assignable_id'
	            );
	    }
	    else{
	        $query->leftJoin(
	            DB::raw('(SELECT shopper_assignable_id, GROUP_CONCAT(name) as shopper_names FROM shopper_assignables LEFT JOIN shopper_groups on id=shopper_group_id WHERE shopper_assignable_type = ? group by shopper_assignable_id) AS assignables'),
	            'id','assignables.shopper_assignable_id'
	            );
	        $query->addBinding(get_class($this), 'join');
	    }
	    
	    return $query;
	}
	
	public function addShopper($shopper)
	{
		if (is_object($shopper))
			$shopper = $shopper->id;
		
		$this->shoppers()->syncWithoutDetaching([$shopper]);
	}
	
	public function removeShoppers()
	{
		return $this->shoppers()->detach();
	}
	
	public function removeShopper($shopper)
	{
		if (is_object($shopper))
			$shopper = $shopper->id;
		
		return $this->shoppers()->detach($shopper);
	}
}
