<?php
namespace Quatius\Shop\Traits\Controllers;

use Illuminate\Http\Request;
use Quatius\Shop\Models\UserDetail;
use Auth;
use Form;

trait AddressUpdate{
    
    /* public function addOrEditAddress(Request $request){
        $address_type = $request->get('address_type');
        $return_url = $request->get('return_url');
        $user = Auth::user();
        $address_id = hashids_decode($request->get('hash_id'));
        $address = UserDetail::find($address_id);
        
        if($address){
            $address->update($request->all());
        }
        else if($address_type == "BT"){
            $address = new UserDetail($request->all());
        }
        
        flash('Address has been updated');
        
        if ($address){
            $address->user_id = $user->id;
            $address->address_type = $request->get("address_type","");
            $address->save();
        }
        return redirect(url($return_url));
    } */
    
    public function removeAddress(Request $request){
        $address_id = $request->get('address_id');
        $user = Auth::user();
        $user->details()->whereId($address_id)->delete();
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $address = new UserDetail();
        Form::populate($address);
        return response()->json(
            [
                'view'  => view("Shop::address.partials.create", ["address"=>$address])->render(),
            ],
            200);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $address = new UserDetail($request->all());
        
        $user = Auth::user();
        
        $address->user_id = $user->id;
        $address->address_type = $request->get("address_type","");
        $address->save();

        $order = app('OrderSession');
        $order->setBillingDetail($address->toArray());
        $order->save();

        return response()->json(
            [
                'message'  => trans('messages.success.created', ['Module' => trans('Account::address.'.$request->get("address_type","BT"))]),
                'view'      => view("Shop::address.partials.view", ["address"=>$address])->render()
            ],
            201);
    }
    
    /**
     * Display the specified resource.
     *
     * @param UserDetail $address
     * @return \Illuminate\Http\Response
     */
    public function show($address=null)
    {
        abort_unless($address, 400);
        return response()->json(
            [
                'view'  => view("Shop::address.partials.view", ["address"=>$address])->render(),
            ],
            200);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param UserDetail $address
     * @return \Illuminate\Http\Response
     */
    public function edit($address=null)
    {
        abort_unless($address, 400);
        Form::populate($address);
        return response()->json(
            [
                'view'  => view("Shop::address.partials.edit", ["address"=>$address])->render(),
            ],
            200);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param UserDetail $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $address=null)
    {
        abort_unless($address, 400);
        $address->update($request->all());
        
        $order = app('OrderSession');
        $order->setBillingDetail($address->toArray());
        $order->save();

        return response()->json(
            [
                'view'  => view("Shop::address.partials.view", ["address"=>$address])->render(),
                'message'  => trans('messages.success.updated', ['Module' => trans('Account::address.'.$address->address_type)]),
            ],
            201);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param UserDetail $address
     * @return \Illuminate\Http\Response
     */
    public function destroy($address=null)
    {
        abort_unless($address, 400);
        $address->delete();
        
        return response()->json(
            [
                'message'  => trans('messages.success.deleted', ['Module' => trans('Account::address.'.$address->address_type)]),
            ],
            201);
    }
}