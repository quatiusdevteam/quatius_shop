<?php
namespace Quatius\Shop\Traits\Controllers\Admin;

use Illuminate\Http\Request;
use Form;
use Lavalite\Page\Interfaces\PageRepositoryInterface;
use Theme;
use Quatius\Shop\Repositories\CategoryRepository;

trait ManageCategory{
    
    private $cateRepo;
    private $pageRepo;
    public function __construct(CategoryRepository $cateRepo ,PageRepositoryInterface $pageRepo)
    {
        parent::__construct();
        $this->cateRepo = $cateRepo->skipCache();
        $this->pageRepo = $pageRepo->skipCache();
    }

    public function index(Request $request){
        $categories = $this->cateRepo->getFromRoot(0,['name'], null, null)->toTree();
        $this->theme->asset()->container('footer')->add('nestable', 'packages/nestable/jquery.nestable.js');
        return $this->theme->of('Shop::admin.category.index',['categories'=>$categories])->render();
    }

    public function create(Request $request){
        
        $category = $this->cateRepo->makeModel();
        
        $model = $category;
        
        // if (config('app.debug',false))
        //     $page = factory($this->pageRepo->model())->make();
        // else
        //     
        
        $page = $this->pageRepo->makeModel()->newInstance([]);
        
        $cateList = array_merge([hashids_encode(0)=>htmlentities('<< Select a parent >>')], $this->cateRepo->getSelectionOptions());
        
        Form::populate($page);
        $this->theme->layout(config('theme.map.admin.ajax'));
        return $this->theme->of('Shop::admin.category.partials.dialog-create',compact('cateList','page','model'))->render();
    }
    
    public function edit(Request $request, $category_id){
        $category = $this->cateRepo->scopeQuery(function($query){
            return $query->query()->with('page');
        })->find($category_id);
        
        $model = $category;
        
        $page = $category->page;
        
        $cateList = array_merge([hashids_encode(0)=>htmlentities('<< Select a parent >>')], $this->cateRepo->getSelectionOptions($category));
        
        Form::populate($page);
        $this->theme->layout(config('theme.map.admin.ajax'));
        return $this->theme->of('Shop::admin.category.partials.dialog-edit',compact('category','cateList','page','model'))->render();
    }
    
    public function createCategory(Request $request){
        
        $category = $this->cateRepo->makeModel();
        
        $model = $category;
        
        // if (config('app.debug',false))
        //     $page = factory($this->pageRepo->model())->make();
        // else
        //     
        $page = $this->pageRepo->makeModel()->newInstance([]);
        
        $cateList = array_merge([hashids_encode(0)=>htmlentities('<< Select a parent >>')], $this->cateRepo->getSelectionOptions());
        
        Form::populate($page);
        $this->theme->layout(config('theme.map.admin.ajax'));
        return $this->theme->of('Shop::admin.category.partials.create',compact('category','cateList','page','model'))->render();
        //return $this->theme->of('Shop::admin.category.partials.create',compact('cateList','page','model'))->render();
    }


    public function editCategory(Request $request, $category_id){
        $category = $this->cateRepo->scopeQuery(function($query){
            return $query->query()->with('page');
        })->find($category_id);
        
        $model = $category;
        
        $page = $category->page;
        
        $cateList = array_merge([hashids_encode(0)=>htmlentities('<< Select a parent >>')], 
            $this->cateRepo->getSelectionOptions(null, null, "- ", " &nbsp;", true, 0, false, null));
        
        Form::populate($page);
        $this->theme->layout(config('theme.map.admin.ajax'));
        return $this->theme->of('Shop::admin.category.partials.edit',compact('category','cateList','page','model'))->render();
    }

    public function showPage(Request $request, $product_id){
        $category = $this->cateRepo->scopeQuery(function($query){
            return $query->query()->with('page');
        })->find($product_id);
        
        $this->theme->layout(config('theme.map.admin.ajax'));
        $page = $category->page;
        $model = $category;
        
        if ($page){
            Form::populate($page);
            return $this->theme->of('Shop::admin.category.partials.show',compact('page','model'))->render();
        }
        else {
            $page = $this->pageRepo->makeModel()->newInstance([]);
            
            Form::populate($page);
            return $this->theme->of('Shop::admin.page.partials.create',compact('page','model'))->render();
        }
    }
    
    public function store(Request $request){
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            
            unset($attributes['parent_id']);
            unset($attributes['selected']);
            unset($attributes['cate_status']);
            $page = $this->pageRepo->create($attributes);
            
            //fixed untranslated name/
            $page = $this->pageRepo->findWhere(['id'=>$page->id])->first();
            
            $cate = $this->cateRepo->create([
                'parent_id'=>hashids_decode($request->get('parent_id','')),
                'page_id'=>$page->id,
                'status'=>$request->get('cate_status','2'),
                'slug' => str_slug($page->name)
            ]);
            
            if(config('quatius.shop.category.shoppers', [])){
                $cate->shoppers()->sync(config('quatius.shop.category.shoppers', []), false);
            }

            $selectedTxt = $request->get('selected','[]');
            $assignedIds = collect(json_decode($selectedTxt,true));
            
            $categories = $this->cateRepo->getFromRoot(0,['name'], null)->toTree();
            $this->theme->layout(config('theme.map.admin.ajax'));
            $view = view('Shop::admin.category.partials.tree',compact('categories','assignedIds'))->render();
            
            return response()->json(
                [
                    'message' => "New Category added ".json_encode(config('quatius.shop.category.shoppers', [])),
                    'code'    => 201,
                    'view'    => $view
                ],
                201);
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    public function delete(Request $request, $category_id){
        try {
            $category = $this->cateRepo->find($category_id);
            $category->shoppers()->sync([]);

            $page_id = $category->page_id;
            $this->pageRepo->delete($page_id);
            $this->cateRepo->delete($category_id);
            return response()->json(
                [
                    'message' => "Category deleted",
                    'code'    => 201
                ],
                201);
        }
        catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    public function update(Request $request, $category_id){
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            
            unset($attributes['parent_id']);
            unset($attributes['selected']); 
            unset($attributes['cate_status']);
            
            $category = $this->cateRepo->find($category_id);
            $this->pageRepo->update($attributes, $category->page_id);
            $page = $category->page;
            
            $shoppergroups = $request->get('shopper_groups',[]);
            $category->shoppers()->sync($shoppergroups);

            $this->cateRepo->update([
                'parent_id'=>hashids_decode($request->get('parent_id','')),
                'page_id'=>$page->id,
                'status'=>$request->get('cate_status','2'),
                'slug' => str_slug($page->name)
            ], $category_id);

            $selectedTxt = $request->get('selected','[]');
            $assignedIds = collect(json_decode($selectedTxt,true));
            $categories = $this->cateRepo->getFromRoot(0,['name'], null, user('admin.web')->isSuperuser()?null:[])->toTree();
            
            $this->theme->layout(config('theme.map.admin.ajax'));
            $view = view('Shop::admin.category.partials.tree',compact('categories','assignedIds'))->render();
            
            return response()->json(
                [
                    'message' => "Category Updated",
                    'code'    => 201,
                    'view'    => $view
                ],
                201);
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
}