<?php
namespace Quatius\Shop\Traits\Controllers\Admin;

use Illuminate\Http\Request;
use Theme;
use Form;
use Quatius\Shop\Repositories\ShipmentRepository;
use Quatius\Shop\Models\ShipmentMethod;

trait ManageShipment{
    
    private $repo;
    public function __construct(ShipmentRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }
    
    public function index(Request $request){
            
        if ($request->wantsJson()) {
            $shipment = $this->repo->makeModel()->withShopperNames()->get();
            return response()->json(['data'=> $shipment], 200);
        }

        return $this->theme->of('Shop::admin.shipment.index')->render();
    }
    
    
    public function reference(Request $request, $type, $link=""){
        $shipment = $this->repo->getType($type);
        
        return $shipment->reference($request,$link);
    }
    
    public function update(Request $request, $shipment_id){
        
       try {
            $shipment = $this->repo->findWhere(['id'=>$shipment_id])->first();
            
            abort_unless($shipment, 404, 'Shipment not found');
            
            $attributes = $request->only(['name',
                'label',
                'description',
                'customer_note',
                'order_note',
                'type',
                'selectable_by',
                'status',
                'ordering']);
            
            $shoppergroups = $request->get('shopper_groups',[]);
            $params = $request->get('params',[]);
            
            if (is_string($params)){
                $attributes['params'] = $params;
            }else if (is_array($params)){
                $attributes['params'] = json_encode($params);
            }
            
            $shipment->fill($attributes);
            $shipment->fillRequest($request);
            
            if ($request->has('override_params'))
            {
                $params = $request->get('params',[]);
                
                if (is_string($params)){
                    $shipment->params = $params;
                }else if (is_array($params)){
                    $shipment->params = json_encode($params);
                }
            }
            
            $shipment->shoppers()->sync($shoppergroups);
            $shipment->save();
            $this->repo->clearCache();
            
             return response()->json([
                'message'  => 'Shipment had been updated',
                'code'     => 204,
                 'redirect' => trans_url('/admin/shop/shipment/'.$shipment->id.'/edit'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/shop/shipment/'.$shipment_id.'/edit'),
            ], 400);

        } 
    }

    public function create(Request $request, $type){
        $shipment = $this->repo->getType($type);
        abort_unless($shipment, 404, 'Shipment type not found');
        Form::populate($shipment);
        return view('Shop::admin.shipment.create',['shipment'=>$shipment]);
    }

    public function initial(Request $request){

        return view('Shop::admin.shipment.initial');
    }

    public function edit(Request $request, $shipment_id){

        $shipment = $this->repo->findWhere(['id'=>$shipment_id])->first();
        abort_unless($shipment, 404, 'Shipment type not found');
        
        Form::populate($shipment);
        return view('Shop::admin.shipment.edit',['shipment'=>$shipment]);
    }
    
    public function store(Request $request){
        try {
            $shipment = $this->repo->getType($request->get('type',''));
            
            abort_unless($shipment, 404, 'Shipment not found');
            
            $attributes = $request->only(['name',
                'label',
                'description',
                'customer_note',
                'order_note',
                'selectable_by',
                'type',
                'status',
                'ordering']);
            
            $shoppergroups = $request->get('shopper_groups',[]);
            $params = $request->get('params',[]);
            
            if (is_string($params)){
                $attributes['params'] = $params;
            }else if (is_array($params)){
                $attributes['params'] = json_encode($params);
            }
            
            $shipment->fill($attributes);
            $shipment->fillRequest($request);
            if ($request->has('override_params'))
            {
                $params = $request->get('params',[]);
                
                if (is_string($params)){
                    $shipment->params = $params;
                }else if (is_array($params)){
                    $shipment->params = json_encode($params);
                }
            }
            $shipment->save();
            
            $shipment->shoppers()->sync($shoppergroups);
            $this->repo->clearCache();
            
            
            return response()->json(
                [
                    'message'  => trans('messages.success.updated', ['Module' => "Shipment: ".$shipment->name]),
                    'code'     => 204,
                    'redirect' => trans_url('/admin/shop/shipment/'.$shipment->id.'/edit'),
                ],
                201);
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
}