<?php
namespace Quatius\Shop\Traits\Controllers\Admin;

use Illuminate\Http\Request;
use Quatius\Payment\Repositories\PaymentRepository;

trait ManagePayment{
    
    private $repo;
    public function __construct(PaymentRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }
    
    public function index(Request $request){
            
        if ($request->wantsJson()) {
            $payments = $this->repo->getPublished(false);
            $payments = app('ShopperHandler')->attachShoppers($payments);

            return response()->json(['data'=> $payments], 200);
        }
    }

    public function update(Request $request){
        try {
            abort_unless($request->get('payment_id', ""),400,'No Payment ID.');
            $payment = $this->repo->find($request->get('payment_id', ""));

            abort_unless($payment,400,'No Payment Found.');
            
            $assignGrpIds = array_unique($request->get('assignGrpIds', []));
            
            app('ShopperHandler')->updateShoppers($assignGrpIds, $payment, null, true);
            $this->repo->clearCache();
            return response()->json(
                [
                    'message'  => "Payment group updated",
                ],
                201);
            
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
}