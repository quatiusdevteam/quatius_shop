<?php
namespace Quatius\Shop\Traits\Controllers\Admin;

use Illuminate\Http\Request;
use Quatius\Shop\Repositories\SpecificationRepository;
use Quatius\Shop\Repositories\Presenters\Admin\SpecListPresenter;
use Quatius\Framework\Models\CsvImporter;

trait ManageSpecification{
    
    private $repo;
    public function __construct(SpecificationRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }
    
    public function remove(Request $request){
        try {
            $class_name = $request->get("class_name",'');
            $class_id = hashids_decode($request->get("class_id",''));
            $result = 0;
            
            $value = $request->get("value",null);
            
            if ($value !== null){
                if ($value ==""){
                    $spec_id = $request->get("spec_id",null);
                    if ($spec_id){
                        $result = $this->repo->removeByValue(['specification_id'=>$spec_id], $class_name, $class_id);
                    }
                }
                else{
                    $result = $this->repo->removeByValue(['value'=>explode(' ', $value)], $class_name, $class_id);
                }
            }
            
            abort_if($result==0,400,'Fail to remove specification. '. json_encode($request->all()));
            
            return response()->json(
                [
                    'result' => $result,
                    'message'  => "Specification Values Removed",
                    'data' => $request->all(),
                    
                ],
                201);
            
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }

    public function delete(Request $request, $spec_id){
        try {
            
            $spec = $this->repo->find($spec_id);
            
            abort_unless($spec, 400,"Specification not found ($spec_id)");
            
            $this->repo->removeByValue(['specification_id'=> $spec_id], null, null);
            
            $result = $spec->delete();
            
            abort_if($result==0,400,'Fail to delete specification'. $spec_id);
            
            return response()->json(
                [
                    'result' => $result,
                    'message'  => "Specification Removed",
                    'data' => $request->all(),
                    'deleted' => $spec_id
                ],
                201);
            
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function updateField(Request $request, $spec_id){
        try {
            $spec = $this->repo->find($spec_id);
            $datas = [];
            $post = $request->all();
            if (isset($post["spec_name"])){
                $spec->name = $request->get("spec_name", "");
                $datas['name'] = $spec->spec_name;
            }
            
            if (isset($post["unit_type"])){
                $spec->unit_type = $request->get("unit_type", "");
                $spec->save();
                $datas['unit_type'] = $spec->unit_type;
            }
            
            if (isset($post["unit_display"])){
                $spec->unit_display = $request->get("unit_display", "");
                $spec->save();
                $datas['unit_display'] = $spec->unit_display;
            }
            
            if (isset($post["search_by"])){
                $spec->search_by = $request->get("search_by", null)?:null;
                $spec->save();
                $datas['search_by'] = $spec->search_by;
            }

            if (isset($post["params"])){
                $datas['params'] = $post["params"];
                foreach($post["params"] as $key=>$val){
                    $spec->setParams($key, $val);
                }
                $spec->save();
            }

            if (isset($post["ordering"])){
                $spec->ordering = $request->get("ordering", "");
                $spec->save();
                $datas['ordering'] = $spec->ordering;
            }
            
            if ($datas){
                $this->repo->clearCache();
                $datas['id'] = $spec_id;
            }
            else{
                throw new \Exception("Specification not change");
            }
                
            return response()->json(
                [
                    'data' => $this->repo->setPresenter(new SpecListPresenter())->parserResult($spec),
                    'message' => "Specification updated",
                    'code'    => 400,
                ],
                201);
        }
        catch (\Exception $e) {
            return response()->json(
                [
                    "posted"  => $request->all(),
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function update(Request $request){
        return $this->store($request);
    }
    
    public function store(Request $request){
        try {
            $specId = $request->get("spec_id",'0');
            if ($specId == '0'||$specId == '') {
                $specId = $this->repo->getSpecIdByName($request->get("spec_name",''));
            }
            
            $spec = $this->repo->findWhere(['id'=>$specId])->first();
            
            if (!$spec){
                $spec = $this->repo->create([
                    'name'  =>  trim($request->get("spec_name",'')),
                    'unit_display' => $request->get("unit_display", "")
                ]);
            }
            
            if ($spec){
                $class_name = $request->get("class_name",'');
                $class_id = hashids_decode($request->get("class_id",''));
                
                $datas = [];
                
                $labels = $request->get('label',[]);
                $icons = $request->get('media_id',[]);
                $params = $request->get('params',[]);

                for($v = 0; $v < count($labels); $v++){
                    if ($labels[$v] == "") continue;

                    $datas[$labels[$v]] = [
                        'specification_id'=>$spec->id,
                        'label'=>$labels[$v],
                        'media_id'=>$icons[$v],
                        'params'=>$params[$v]
                    ];
                }

                if (!$datas){ // empty label
                    $datas[] =  [
                        'specification_id'=>$spec->id,
                        'label'=>""
                    ];
                }
                
                $this->repo->removeByValue(['specification_id'=>$spec->id], $class_name, $class_id);
                $this->repo->updateValues($datas, $class_name, $class_id, false, false);
                $this->repo->clearCache();
                
                $specs = $this->repo->groupValues($this->repo->getLinksFrom($class_name,$class_id));
                $spec = $specs->where('id',$spec->id)->first();
            }
            else {
                abort(404, "Specfication not found");
            }
            

            $specMediaIcons = app('MediaHandler')->getLinkFrom(\Quatius\Shop\Models\Specification::class);
            $specIcons = [];
            foreach($specMediaIcons as $icon){
                $specIcons[$icon->pivot->grouping] = ['media_id'=> $icon->id, "params"=> $icon->pivot->params, "view"=>view('Media::admin.partials.tile', ['media'=>$icon,"extras"=>"","onSelect"=>""])->render()];
            }

            return response()->json(
                [
                    'post' => $request->all(),
                    'request' => $request->get('label',[]),
                    'message'  => trans('messages.success.updated', ['Module' => "Spec: ".$spec->name]),
                    'code'     => 204,
                    'view' => view('Shop::admin.specification.partials.item', ['spec'=>$spec])->render(),
                    'data' => $this->repo->setPresenter(new SpecListPresenter())->parserResult($spec),
                    'icons' => $specIcons
                ],
                201);
            
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
        
    }

    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            
            return response()->json([], 200);
        }
        return $this->theme->of('Shop::admin.specification.index')->render();
    }

    public function importSpecCsv(Request $request, $type="tv"){
        $save_path = 'specs/'.$type.'-import-specs.csv';
        $files = array_values($request->allFiles());
        $file = isset($files[0])?$files[0]:null;
        try{
            if ($file && $file->isValid())
            {
                $save_path = $file->storeAs('specs',$type.'-import-specs.csv');
            }
            $skipFields = json_decode(setting('product','spec-import-exclude','["SKU","Product","Category"]'), false);

            $path = storage_path('app/'.$save_path);
            $numOfupdates = 0;
            if (file_exists($path)){

                $importer = new CsvImporter($path,true);
                $prodRepo = app('ProductHandler');
                $isfirstRun = true;
                $usedFields = [];
                while($dataRows = $importer->get(500, true))
                {
                    foreach ($dataRows as $row)
                    {
                        if (!$row['SKU']) break;

                        $product = $prodRepo->makeModel()->whereSku($row['SKU'])->first();
                        if($product){
                            foreach($row as $field=>$values){
                                $field = trim($field);
                                
                                if (array_search($field,$skipFields) !== false) continue;

                                if ($isfirstRun) $usedFields[] = $field;

                                $values = trim($values);
                                $this->repo->skipCache()->updateProductSpec($product, $field, $values?explode('|',$values):null, false, true);
                            }
                            
                            $prodRepo->generateProductMeta($product);
                            
                            $numOfupdates++;

                            if ($isfirstRun){
                                setting_save('product','spec-import-fields-'.$type, json_encode($usedFields));
                                $isfirstRun = false;
                            }
                        }
                    }
                }
                
                if(!$isfirstRun)
                    $prodRepo->clearCache();
            }

            return response()->json(
                [
                    'updates' => $numOfupdates,
                    'message'  => "Specification update for $numOfupdates products",
                ],
                201);
            
        } catch (\Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }

    public function exportSpecCsv(Request $request, $type='tv'){

    }
}