<?php
namespace Quatius\Shop\Traits\Controllers\Admin;

use Illuminate\Http\Request;

use Theme;
use Quatius\Shop\Models\Order;
use Form;
use DB;

trait ManageOrder{
    
    
    public function index(Request $request){
            
        if ($request->wantsJson()) {
            $query = DB::table('orders')->leftJoin(DB::raw("(select `user_id`, `order_id`, CONCAT(`first_name`,' ',`last_name`) as name, email from `order_details`
            where `address_type` = 'BT') `detail`"), 
            'id', '=', 'order_id');

            $orders = dataTableQuery($request->all(), $query);
            return response()->json($orders, 200);
        }

        return $this->theme->of('Shop::admin.order.index')->render();
    }

    public function show(Request $request, $order_id){
        $order = Order::find($order_id);
        return view('Shop::admin.order.show',['order'=>$order]);
    }
}