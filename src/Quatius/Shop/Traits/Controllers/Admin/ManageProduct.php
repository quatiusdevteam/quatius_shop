<?php
namespace Quatius\Shop\Traits\Controllers\Admin;

use Illuminate\Http\Request;
use Quatius\Shop\Repositories\Presenters\Admin\ProductIndexPresenter;
use Form;
use Theme;
use Quatius\Shop\Repositories\Presenters\Admin\SpecListPresenter;
use Event;
use DB;

trait ManageProduct{
    private $prodRepo;
    private $pageRepo;

    public function prodRepo()
    {
        return $this->prodRepo?:$this->prodRepo = app('Quatius\Shop\Repositories\ProductRepository');
    }
    
    public function pageRepo()
    {
        return $this->pageRepo?:$this->pageRepo = app('Lavalite\Page\Interfaces\PageRepositoryInterface')->skipCache();
    }
    /**
     * list all products
     * @return Ambigous <\Illuminate\View\View, \Illuminate\Contracts\View\Factory>
     */
    public function index(Request $request, $filter = 'none'){
        
        if ($request->wantsJson()) {
            $this->prodRepo()->setPresenter(new ProductIndexPresenter());
            
            $productQuery = $this->prodRepo()->scopeQuery(function($query) use ($request){
                $query = $query->query();

                if ($request->get('deleted', 0) == 1) $query = $query->onlyTrashed();
                
                return $query->categoryIds()
                    ->leftJoin(DB::raw('(SELECT (SUM(stock) - SUM(ordered)) as available, SUM(ordered) as ordered, product_id FROM product_warehouse group by product_id) AS stocks'),'id','stocks.product_id');
            });
                
            $products = $productQuery->all(['*'], $request->get('deleted', 0));
            return response()->json($products, 200);
        }
        return $this->theme->of('Shop::admin.product.index')->render();
    }
    
    /**
     * list all products
     * @return Ambigous <\Illuminate\View\View, \Illuminate\Contracts\View\Factory>
     */
    public function groupProducts(Request $request, $filter = 'none'){
        $products = $this->prodRepo()->makeModel()->status([1,2])->page(['name'])->get();
        $groups = $products->groupBy('page_id');
        
        $formatGroups = [];
        
        foreach ($groups as $page_id=>$group){
            $formatProd = [];
            
            foreach ($group as $prod)
            {
                $formatProd['name'] = $prod->page->name;
                $formatProd['id'] = $prod->getRouteKey();
                $formatProd['page_id'] = $prod->page_id;
                if (isset($formatProd['skus']))
                    $formatProd['skus'] .= ", ".$prod->sku;
                else 
                    $formatProd['skus'] = $prod->sku;
            }
            
            $formatGroups[] = $formatProd;
        }
        
        return response()->json(["data"=>$formatGroups], 200);
    }
    /**
     * list all products
     * @return Ambigous <\Illuminate\View\View, \Illuminate\Contracts\View\Factory>
     */
    public function indexSpecs(Request $request, $product_id){
        
        if ($request->wantsJson()) {
            $this->prodRepo()->setPresenter(new SpecListPresenter());
            /*  $products = $this->prodRepo()->scopeQuery(function($query){
             return $query->query()->page();
             })->get(); 
            
            return response()->json([$specs], 200);//json_encode($products);
            */
            return response()->json([], 200);
        }
        
        return $this->theme->of('Shop::admin.product.partials.spec')->render();
    }

    public function createBasicSave(Request $request, $product_id=0){

        if ($product_id == 0){
            $product = $this->newProduct($request, true);
            $this->storePage($request, $product->id);
        }else{
            $product = $this->prodRepo()->find($product_id);
            $this->updatePage($request, $product);

            $discount_price = floatval($request->get('discount-price', 0));
            $price = floatval($request->get('price', 0));
            $taxInvert = config('quatius.shop.tax.percentage-invert', 11);
            $price = $price - ($price / $taxInvert);

            if ($discount_price == 0)
                $discount_price = $price;
            else{
                $discount_price = $discount_price - ($discount_price / $taxInvert);
            }

            $product->prices->where('price_group',config('quatius.shop.price.compared-to'))->first()->update(['price'=>$price]);
            $shopperGroup = app('ShopperHandler')->getDefault([config('quatius.shop.default-groups.guest'),config('quatius.shop.default-groups.registered')]);

            foreach($shopperGroup as $shopper){
                $priceGrp = $product->prices->where('price_group',$shopper->price_group)->first();
                if ($priceGrp)
                    $priceGrp->update(['price'=>$discount_price]);
            }

            app('ProductPriceHandler')->clearCache();
        }
        
        $product->update(['status'=>intval($request->get('product-publish' ,1))]);
        
        $warehouseRepo = app('WarehouseHandler');
        $warehouse = null;
        $warehouses = $warehouseRepo->all();
        if ($warehouses->count() == 1){
            $warehouse = $warehouses->first();
        }
        elseif ($warehouses->count() == 0){
            $warehouse = $warehouseRepo->create([
                'name' => 'Default',
                'code' => 'Default'
            ]);
            
            DB::table('warehouse_destinations')->insert(['warehouse_id'=>$warehouse->id,'country'=>'AU','state'=>'ALL']);
        }

        if ($warehouse){
            $syncDatas = [];
            $syncDatas[$warehouse->id] = [
                'stock'=>intval($request->get("product-stock", 0)),
                'low_stock'=>0,
                'reserve'=>0,
                'ordered'=>0
            ];
        
            $product->warehouses()->sync($syncDatas);
            app('WarehouseHandler')->clearcache();
        }

        $medias = [];

        foreach($request->get("medias", []) as $dataImg){
            unset($dataImg["ordering"]);
            $medias[] = $dataImg;
        }

        app("MediaHandler")->updateLink($medias,$product);

        $cateIds = [];

        foreach($request->get("category", []) as $cateHasdId){
            $cateIds[] = hashids_decode($cateHasdId);
        }
        $product->categories()->sync($cateIds);

        $resonse = response()->json(
            [
                'message'  => $product_id?trans('messages.success.updated', ['Module' => 'Product']):trans('messages.success.created', ['Module' => 'Product']),
                'code'     => 201,
                'redirect' => $product->exists?trans_url('/admin/shop/product/' . $product->getRouteKey()):trans_url('/admin/shop/product/create')
            ],
            201);
        
        return $resonse;
    }
    public function createProduct(Request $request){
        return $this->show($request, 0);
    }

    public function show(Request $request, $product_id){

        if (config('quatius.shop.admin.product.mode') == 'basic'){
            if ($product_id == 0){
                $product = $this->prodRepo()->makeModel()->newInstance([]);
                $page = $this->pageRepo()->makeModel()->newInstance([]);;
            }else{
                $product = $this->prodRepo()->scopeQuery(function($query){
                    return $query->query()->with('page');
                })->withTrashed()->find($product_id);
                $page = $product->page;
                $page->sku = $product->sku;
            }
            
            $model = $product;
            if ($page){
                Form::populate($page);
                $leftContent = view('Shop::admin.page.edit-basic',compact('page','model'));
                Theme::set("entry-left", $leftContent);
                $rightContent = view('Shop::admin.product.partials.details-basic',compact('product'));
                
                Theme::set("entry-right", $rightContent);
            }
            $this->theme->layout(config('theme.map.admin.ajax'));
            return $this->theme->of('Shop::admin.product.show-basic',compact('product','page','model'))->render();
        }
        else{
            $product = $this->prodRepo()->scopeQuery(function($query){
                return $query->query()->with('page');
            })->withTrashed()->find($product_id);
            
            $this->theme->layout(config('theme.map.admin.ajax'));
            $page = $product->page;
            $model = $product;
            
            if ($page){
                Form::populate($page);
                $leftContent = view('Shop::admin.page.show',compact('page','model'));
                $leftContent.= view('Shop::admin.product.partials.show-childs',compact('product','page','model'));
                
                Theme::set("entry-left", $leftContent);
                
                $rightContent = view('Shop::admin.product.partials.details',compact('product'));
                
                Theme::set("entry-right", $rightContent);
                
                //return $this->theme->of('Shop::admin.product.partials.show',compact('product','page','model'))->render();
            }
            else {
                Event::fire('shop.admin-adding-product',[$product]);
                
                return $this->theme->of('Shop::admin.product.create',compact('product','model'))->render();
            }
            return $this->theme->of('Shop::admin.product.show',compact('product','page','model'))->render();
        }
    }
    
    public function showDetail(Request $request, $product_id){
        $product = $this->prodRepo()->scopeQuery(function($query){
            return $query->query()->with('page');
        })->withTrashed()->find($product_id);
        
        $this->theme->layout(config('theme.map.admin.ajax'));
        
        return $this->theme->of('Shop::admin.product.partials.details',compact('product'))->render();
    }
    
    public function addImage(Request $request, $product_id){
        
        try {
            $product = $this->prodRepo()->find($product_id);
            $medias = $request->get('medias',[]);
            
            app("MediaHandler")->updateLink($medias,$product, null, false);
            
            return response()->json(
                [
                    'message'  => "Product's image added",
                    'code'     => 204,
                ],
                201);
            
            return $this->show($request, $product_id);
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function orderingImage(Request $request, $product_id){
        try {
            
            $product = $this->prodRepo()->find($product_id);
            $medias = $request->get('medias',[]);
            
            $medias = app("MediaHandler")->updateLinkOrdering($medias, $product);
            
            return response()->json(
                [
                    'medias'    => $medias,
                    'message'  => "Product's image sorted",
                    'code'     => 204,
                ],
                201);
            
            return $this->show($request, $product_id);
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function removeImage(Request $request, $product_id){
        try {
            $product = $this->prodRepo()->find($product_id);
            $medias = $request->get('medias',[]);
            
            $mediaIds = collect($medias)->pluck('media_id');
            $result = app("MediaHandler")->deleteLinkFrom($product, null, $mediaIds);
            abort_if($result==0,400,'Unable to remove image from product.');
            return response()->json(
                [
                    'message'  => "Product's image removed",
                    'code'     => 204,
                ],
                201);
            
            return $this->show($request, $product_id);
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function create(Request $request, $product_id){
        
        $product = $this->prodRepo()->scopeQuery(function($query){
            return $query->query()->with('page');
        })->find($product_id);
        
        $this->theme->layout(config('theme.map.admin.ajax'));
        $model = $product;
        
        $page = $this->pageRepo()->makeModel()->newInstance([]);
        $page->name = $product->description;
        Event::fire('shop.admin-creating-product-page',[$product, $page], true);
        Form::populate($page);
        
        return $this->theme->of('Shop::admin.product.partials.create',compact('product','page','model'))->render();
    }
    
    public function updateStatus(Request $request, $product_id){
        try {
            $product = $this->prodRepo()->find($product_id);
            $product->timestamps = false;
            Event::fire('shop.admin-status-product',[$request->get('status',1), $product->status]);

            $product->status = $request->get('status',1);
            $product->save();
            $this->prodRepo()->clearCache();

            return response()->json(
                [
                    'message'  => trans('messages.success.updated', ['Module' => "Status"]),
                    'code'     => 204,
                ],
                201);
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }

    public function delete(Request $request, $product_id){
        try {
            $product = $this->prodRepo()->find($product_id);
            $product->timestamps = false;
            Event::fire('shop.admin-delete-product',[$product]);
            $product->delete();
            $this->prodRepo()->clearCache();
            return response()->json(
            [
                'message'  => trans('messages.success.deleted', ['Module' => "Product: ". $product_id]),
                'code'     => 204,
            ],
            201);
        
        } catch (Exception $e) {
            return response()->json(
                [
                    'id' => $product_id,
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }

    public function restore(Request $request, $product_id){
        try {
            
            $product = $this->prodRepo()->withTrashed()->find($product_id);
            $product->timestamps = false;
            Event::fire('shop.admin-restore-product',[$product]);
            $product->restore();
            $this->prodRepo()->clearCache();

            return response()->json(
            [
                'message'  => trans('messages.success.added', ['Module' => "Product: ". $product_id]),
                'code'     => 204,
            ],
            201);
        
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }

    public function preview(Request $request, $product_id, $related_id){
        $product = $this->prodRepo()->find($product_id);
        
        $this->theme->layout(config('theme.map.admin.ajax'));
        
        $productRelated = $this->prodRepo()->scopeQuery(function($query){
            return $query->query()->with('page');
        })->find(hashids_decode($related_id));
        $page = $productRelated->page;
        
        $model = $product;
        
        Form::populate($page);
        
        return $this->theme->of('Shop::admin.product.partials.add-child',compact('product','page','model'))->render();
    }
    
    public function assignCategories(Request $request, $product_id){
        try {
            $product = $this->prodRepo()->find($product_id);
            
            $categories = [];
            foreach ($request->get('category', []) as $rawId) {
                $categories[] = hashids_decode($rawId);
            }
            
            $product->categories()->sync($categories);
            $this->prodRepo()->update(['keyword'=>$this->prodRepo()->generateKeyword($product)], $product_id);
            $this->prodRepo()->clearCache();
            
            Event::fire('shop.product-category-change', [$product, $categories]);
            return response()->json(
                [
                    'message'  => "Successfully update product's categories",
                    'code'     => 204,
                    'category_ids' => implode(',',$product->categories->pluck('id')->toArray())
                ],
                201);
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function setPage(Request $request, $product_id, $page_id){
        try {
            $page_id = hashids_decode($page_id);
            $product = $this->prodRepo()->scopeQuery(function($query){
                return $query->query()->with('page');
            })->find($product_id);
            
            $product->page_id = $page_id;
            
            $this->prodRepo()->update([
                'keyword'=>$this->prodRepo()->generateKeyword($product),
                'page_id'=>$page_id,
                'status'=>1
            ], $product_id);
            
            Event::fire('shop.admin-assign-product-page',[$product, $page_id], true);
            
            $this->prodRepo()->clearCache();
            $view = $this->show($request, $product_id);
            
            return response()->json(
                [
                    'status'=>1,
                    'view'     => $view->original,
                    'message'  => "Successfully add product to group",
                    'code'     => 204,
                ],
                201);
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function detachPage(Request $request, $product_id){
        try {
            $product = $this->prodRepo()->scopeQuery(function($query){
                return $query->query()->with('page');
            })->find($product_id);
            
            $product->page_id = 0;
            
            $this->prodRepo()->update([
                'keyword'=>$this->prodRepo()->generateKeyword($product),
                'page_id'=>0,
                'status'=>3
            ], $product_id);
            
            $this->prodRepo()->clearCache();
            
            return response()->json(
                [
                    'product_id'   => $product->getRouteKey(),
                    'status'   => 3,
                    'message'  => "Successfully remove product from group.",
                    'code'     => 204,
                ],
                201);
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function createPage(Request $request, $product_id){
        $product = $this->prodRepo()->scopeQuery(function($query){
            return $query->query()->with('page');
        })->find($product_id);
        
        $this->theme->layout(config('theme.map.admin.ajax'));
        $page = $product->page;
        $model = $product;
        
        $page = $this->pageRepo()->makeModel()->newInstance([]);
        
        Form::populate($page);
        return $this->theme->of('Shop::admin.page.partials.create',compact('page','model'))->render();
    }
    
    public function editPage(Request $request, $product_id){
        $product = $this->prodRepo()->scopeQuery(function($query){
            return $query->query()->with('page');
        })->find($product_id);
        
        $this->theme->layout(config('theme.map.admin.ajax'));
        $page = $product->page;
        $model = $product;
        
        Form::populate($page);
        return $this->theme->of('Shop::admin.page.partials.edit',compact('page','model'))->render();
        
    }
    
    public function indexPage(Request $request){
        $pages = $this->pageRepo()->setPresenter('\\Lavalite\\Page\\Repositories\\Presenter\\PageListPresenter')
        ->scopeQuery(function ($query) {
            return $query->where('abstract', 'product')->orderBy('id', 'DESC');
        })->all();
        return response()->json($pages, 200);
    }
    
    public function showPage(Request $request, $product_id){
        $product = $this->prodRepo()->scopeQuery(function($query){
            return $query->query()->with('page');
        })->find($product_id);
        
        $this->theme->layout(config('theme.map.admin.ajax'));
        $page = $product->page;
        $model = $product;
        
        if ($page){
            Form::populate($page);
            return $this->theme->of('Shop::admin.page.partials.show',compact('page','model'))->render();
        }
        else {
            $page = $this->pageRepo()->makeModel()->newInstance([]);
            
            Form::populate($page);
            return $this->theme->of('Shop::admin.page.partials.create',compact('page','model'))->render();
        }
    }
    
    public function update(Request $request, $product_id){
        try {
            $attributes = $request->all();
            
            $product = $this->prodRepo()->scopeQuery(function($query){
                return $query->query()->with('page');
            })->find($product_id);
            
            if (isset($attributes['setparam'])){
                $param = $attributes['setparam'];
                $paramJS = json_decode($param, true);
                if($paramJS){
                    foreach($paramJS as $key => $dataObj){
                        if (!$dataObj){
                            $product->unsetParams($key);
                            continue;
                        }
                        $product->setParams($key, $dataObj);
                    }
                    unset($attributes['setparam']);
                    $attributes['params'] = $product->params;
                }
            }

            $attributes['keyword']=$this->prodRepo()->generateKeyword($product);
            
            $this->prodRepo()->update($attributes, $product_id);
            $this->prodRepo()->clearCache();
            
            return response()->json(
                [
                    'message'  => trans('messages.success.updated', ['Module' => 'Product']),
                    'code'     => 204,
                    'attributes' =>$attributes
                ],
                201);
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function updatePage(Request $request, $product_id){
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            if (is_a($product_id , $this->prodRepo()->model())){
                $product = $product_id;
                $product_id = $product->id;
            }
            else
                $product = $this->prodRepo()->find($product_id);
            
            $page = $this->pageRepo()->findWhere(['id'=>$product->page_id])->first();
            
            unset($attributes['page_medias']);
            $medias = $request->get('page_medias',[]);
            app("MediaHandler")->updateLink($medias, $page);
            
            $this->pageRepo()->update($attributes, $product->page_id);
            $this->prodRepo()->update(['keyword'=>$this->prodRepo()->generateKeyword($product)], $product_id);

            $this->prodRepo()->clearCache();
            return response()->json(
                [
                    'message'  => trans('messages.success.updated', ['Module' => trans('page::page.name')]),
                    'code'     => 204,
                    'redirect' => trans_url('/admin/shop/product/' . $product->getRouteKey().'/page'),
                ],
                201);
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function storePage(Request $request, $product_id){
        try {
            
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            
            $product = $this->prodRepo()->find($product_id);
            
            unset($attributes['page_medias']);
            
            $page = $this->pageRepo()->create($attributes);
            $medias = $request->get('page_medias',[]);
            app("MediaHandler")->updateLink($medias, $page);
            
            $product->page_id = $page->id;
            
            Event::fire('shop.admin-assign-product-page',[$product, $product->page_id], true);
            
            $this->prodRepo()->update([
                'keyword'=>$this->prodRepo()->generateKeyword($product),
                'page_id'=>$page->id,
                'status'=>1
            ], $product_id);
            
            $response = response()->json(
                [
                    'message'  => trans('messages.success.created', ['Module' => 'Product']),
                    'code'     => 201,
                    'redirect' => trans_url('/admin/shop/product/' . $product->getRouteKey()),
                ],
                201);
            
            return $response;
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function setProduceType(Request $request, $product_id, $type){
        try {
            
            $prodClasses = $this->prodRepo()->makeModel()->getSingleTableTypeMap();
            
            abort_unless(isset($prodClasses[$type]), 300, "Product Type not found");
            
            
            $product = $this->prodRepo()->find($product_id);
            
            $product = $product->changeType($type);
            
            if (isset($prodClasses[$type]) == get_class($product)){
                $attributes = $request->all();
                
                $product->syncProductType($attributes, false);
                $product->status = 1;
                
            }
            
            if ($type == 'generic'){
                $product->status = 3;
            }
            
            $product->save();
            $this->prodRepo()->clearCache();
            
            $resonse = response()->json(
                [
                    'product_id' => $product->getRouteKey(),
                    'view'     => $product->status==3?"":$this->show($request, $product->id)->original,
                    'status'   => $product->status,
                    'message'  => "Successfully change product to $type",
                    'code'     => 204
                ],
                201);
            
            return $resonse;
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
    
    public function newProduct(Request $request, $returnProd = false){
        try {

            $product = $this->prodRepo()->create([
                'description' => $request->get('name', ''),
                'sku' => strtoupper($request->get('sku', '')),
                'slug' => strtolower($request->get('sku', '')),
                'barcode' => '',
                'status' => 3,
                'params' => '{}'
            ]);
            
            $discount_price = floatval($request->get('discount-price', 0));
            $price = floatval($request->get('price', 0));

            $taxInvert = config('quatius.shop.tax.percentage-invert', 11);
            $price = $price - ($price / $taxInvert);

            if ($discount_price == 0)
                $discount_price = $price;
            else{
                $discount_price = $discount_price - ($discount_price / $taxInvert);
            }

            $shopperRepo = app('ShopperHandler');
            
            $initialGroups = $shopperRepo->getDefault([
                config('quatius.shop.default-groups.guest'),
                config('quatius.shop.default-groups.registered')
            ]);
            
            if (!$initialGroups){
                $shopperRepo->create(['name'=>'Guest', 'price_group'=> config('quatius.shop.price.default'), 'default'=>config('quatius.shop.default-groups.guest')]);
                if (config('quatius.shop.default-groups.registered') !=  config('quatius.shop.default-groups.guest')){
                    $shopperRepo->create(['name'=>'Registered', 'price_group'=> 'REGISTER', 'default'=>config('quatius.shop.default-groups.registered')]);
                }
                $initialGroups = $shopperRepo->getDefault([
                    config('quatius.shop.default-groups.guest'),
                    config('quatius.shop.default-groups.registered')
                ]);
            }
            
            if (!$initialGroups->where('price_group', config('quatius.shop.price.compared-to'))->first()){
                $product->prices()->save(new \Quatius\Shop\Models\ProductPrice(['price'=>$price,'price_group'=>config('quatius.shop.price.compared-to'), 'currency'=>config('quatius.shop.price.currency')]));
            }
            
            foreach ($initialGroups->keyBy('price_group')->keys() as $priceGrp){
                $product->prices()->save(new \Quatius\Shop\Models\ProductPrice(['price'=>$discount_price,'price_group'=>$priceGrp, 'currency'=>config('quatius.shop.price.currency')]));
            }
            
            $shopperRepo->updateShoppers($initialGroups->pluck('id')->toArray(), $product);
            
            $this->prodRepo()->clearCache();

            if ($returnProd){
                return $product;
            }
            
            
            return response()->json(
                [
                    'message'  => "Successfully created new product - ".$product->description,
                    'code'     => 204
                ],
                201);
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
}