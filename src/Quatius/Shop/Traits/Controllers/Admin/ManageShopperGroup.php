<?php
namespace Quatius\Shop\Traits\Controllers\Admin;

use Illuminate\Http\Request;
use Form;
use Theme;
use Quatius\Shop\Models\ShopperGroup;
use App\User;
use DB;

trait ManageShopperGroup
{

    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $query = ShopperGroup::leftJoin(DB::RAW('(SELECT count(shopper_group_id) as assigns, shopper_group_id FROM `shopper_assignables` GROUP BY `shopper_group_id`) grp'), 'shopper_group_id', 'id')->get();
            return response()->json(['data' => $query], 200);
        }

        return $this->theme->of('Shop::admin.shopper.index')->render();
    }

    public function show(Request $request, $id)
    {
        $shoppergroup = ShopperGroup::findOrNew($id);
        Form::populate($shoppergroup);
        $types = DB::table('shopper_assignables')->where('shopper_group_id', $id)->select(DB::RAW('DISTINCT shopper_assignable_type '))->get()->pluck('shopper_assignable_type')->toArray();

        return view('Shop::admin.shopper.show', compact('shoppergroup', 'types'));
    }

    public function edit(Request $request, $id)
    {
        $shoppergroup = ShopperGroup::findOrNew($id);
        Form::populate($shoppergroup);
        $types = DB::table('shopper_assignables')->where('shopper_group_id', $id)->select(DB::RAW('DISTINCT shopper_assignable_type '))->get()->pluck('shopper_assignable_type')->toArray();

        return view('Shop::admin.shopper.edit', compact('shoppergroup', 'types'));
    }

    public function update(Request $request, $id)
    {
        try {
            App('ShopperHandler')->update($request->all(), $id);
            return response()->json(
                [
                    'message' => trans('messages.success.updated', ['Module' =>"Shopper Group"]),
                    'code' => 204,
                    'redirect' => trans_url('/admin/shop/shoppers/' . $id),
                ],
                201
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code' => 400,
                    //'redirect' => trans_url('/admin/shop/shoppers/' . $id),
                ],
                400
            );
        }
    }

    public function showUsers(Request $request, $id)
    {
        $shoppergroup = ShopperGroup::findOrNew($id);
        if ($request->wantsJson()) {
            $users = $shoppergroup->assignedClass(User::class)->get();
            return response()->json(['data' => $users], 200);
        }

        return view('Shop::admin.shopper.partials.show-users', compact('shoppergroup', 'types'));
    }

    public function allUsers(Request $request)
    {
        if ($request->wantsJson()) {
            $users = User::all();
            return response()->json(['data' => $users], 200);
        }

        return '';
    }

    public function removeUser(Request $request, $shopper_id, $link_id)
    {
        $user = User::find($link_id);
        if ($user) {
            if ($user->removeShopper($shopper_id) > 0){
                return response()->json(
                    [
                        'message' => "Successfully remove: \"".$user->name."\" from Shopper group.",
                        'code' => 204,
                    ],
                    201
                );
            }
        }

        return response()->json(
            [
                'message' => "Failed to remove User" ,
                'code' => 400
            ],
            400
        );
    }

    public function addUsers(Request $request, $shopper_id, $user_ids)
    {
        $shoppergroup = ShopperGroup::find($shopper_id);
        if ($shoppergroup) {
            $shoppergroup->assignedClass(User::class)->sync(explode(',', $user_ids), false);
            return 'true';
        }

        return 'false';
    }
}