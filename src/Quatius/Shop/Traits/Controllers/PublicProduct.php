<?php
namespace Quatius\Shop\Traits\Controllers;

use Quatius\Shop\Repositories\ProductRepository;
use Theme;
use Quatius\Shop\Models\Category;
use Illuminate\Http\Request;
use Quatius\Shop\Models\Product;
use Event;

trait PublicProduct{
    private $repo;
    public function __construct(ProductRepository $prodRepo)
    {
        parent::__construct();
        $this->repo = $prodRepo;
    }
    
    public function searchData(Request $request)
    {
        return response()->json($this->repo->generateSearchContent(false));
    }
    
    public function search(Request $request)
    {
        $order = app('OrderSession');
        $allProducts = $this->repo->getFromCategory(0, $order->getShopperGroupIds());

        $products = [];


        $search =  trim($request->get("search",""));
        if ($search){

            $words = explode(" ", trim($search));

            foreach ($allProducts as $prod){
                foreach ($words as $word){
                    if (!trim($word)) continue;

                    $keyword = strtolower($prod->keyword);
                    if (strpos($keyword, strtolower($word)) !== false){
                        $products[] = $prod;
                        break;
                    }
                }
            }

        }
        
        $products = collect($products);
        Event::fire('shop.product-listing', [$products]);

        return $this->theme->of('Shop::product.list',["search"=>$search, 'showIds'=>[], "order"=>$order,"specFilter"=>app('SpecificationHandler')->getFilterView($products, true),"name"=> 'Search',"description"=> '',"category"=>null, "products"=>$products])->render();
    }
    
    public function index()
    {
        $order = app('OrderSession');
        $products = $this->repo->getFromCategory(0, $order->getShopperGroupIds());

        Event::fire('shop.product-listing', [$products]);

        return $this->theme->of('Shop::product.list',["search"=>"", "order"=>$order,"specFilter"=>app('SpecificationHandler')->getFilterView($products, true),"name"=> 'All Products',"description"=> '',"category"=>null, "products"=>$products])->render();
    }
    
    public function showCategory(Request $request, Category $cate){

        if (!$cate->exists){
            app('log')->warning("showCategory:72",[$request->fullUrl()]);
            return redirect(url("/"));
        }

        $cate->page->title = $cate->page->name;

        render_page_meta($this->theme, $cate->page);
        $order = app('OrderSession');

        $products = $this->repo->getFromCategory($cate->id, $order->getShopperGroupIds());

        category_breadcrumb(Theme::breadcrumb(), $cate->id);
        $showIds = [];
        $search =  trim($request->get("search",""));
        if ($search){
            $words = explode(" ", trim($search));

            foreach ($products as $prod){
                foreach ($words as $word){
                    if (strlen($word) < 2) continue;

                    $keyword = strtolower($prod->keyword);
                    if (strpos($keyword, strtolower($word)) !== false){
                        $showIds[] = $prod->id;
                        break;
                    }
                }

            }

        }
        $products = collect($products);

        Event::fire('shop.product-listing', [$products]);

        return $this->theme->of('Shop::product.list',["search"=>$search, 'showIds'=>$showIds, "order"=>$order,"specFilter"=>app('SpecificationHandler')->getFilterView($products, true),"name"=> $cate->page->name, "description"=> $cate->page->description,"content"=> $cate->page->content,"category"=>$cate, "products"=>$products])->render();
    }
    
    public function show(Request $request, Product $product){
        abort_unless($product->exists, 404, "Product Not found");
        abort_unless($product->page, 404, "No Page Assigned");
        
        $cateRepo = app('CategoryHandler');
        
        $fullCatePath = url(config('quatius.shop.category.url_prefix','category/'));
        
        $order = app('OrderSession');
        $product = $this->repo->preloadViewingProducts(collect([$product]), $order->getShopperGroupIds()[0])[0];

        $category = null;
        // Check if access from category url.
        if(strpos(url()->previous(), $fullCatePath) === 0) {
            $findCateSlug = substr(url()->previous(), strlen($fullCatePath)+1);
            $category = $cateRepo->getFromSlug($findCateSlug);
        }else {
            $category = $product->categories->first();
        }
        
        if ($category){
            //category menu active flags
            $cateRepo->setActiveList($category);
            category_breadcrumb(Theme::breadcrumb(), $category->id);
        }
        
        Theme::breadcrumb()->add($product->page->name, trans_url($product->url));
        
        $image = $product->getMedia();
        if ($image){
            $product->page->image_path = media_url($image->getSize('facebook_xs'));
        }
        
        $product->page->title = $product->description;
        $product->page->keyword = $product->keyword;
        render_page_meta($this->theme, $product->page);
        
        Event::fire('shop.product-detail', [$product]);
        
        return $this->theme->of('Shop::product.detail',["order"=>app('OrderSession'),'product'=>$product,'category'=>$category])->render();
    }
}