<?php
namespace Quatius\Shop\Traits\Controllers;

use Illuminate\Http\Request;
use Quatius\Shop\Models\OrderSession;
use Quatius\Shop\Repositories\Presenters\CartPresenter;
use Theme;
use Auth;
use Validator;
use Form;
use Quatius\Shop\Models\OrderDetail;
use Quatius\Shop\Models\UserDetail;
use App\Http\Controllers\Auth\RegisterController;
use Quatius\Shop\Models\Order;
use Event;
use Analytics;

trait UpdateCart{
    
    public function cart(Request $request, $session_name=null){
        $order= OrderSession::getOrder(null,$session_name);
        if ($request->wantsJson()){
            return response()->json([
                'order' => $order->setPresenter(new CartPresenter())->parserResult()["data"]
            ]);
        }

        Theme::breadcrumb()->add('Home', url('/'))->add(config('quatius.shop.cart.name','My Cart'), url(config('quatius.shop.cart.url_prefix','order/')));
        
        $order->save();
        Event::fire('shop.cart-view-showing',[$order, $request]);
        $order->hasStock(true);
        Event::fire('shop.cart-check-valid', [$order, true, true]);
        
        return $this->theme->of('Shop::order.cart', compact(['order','session_name']))->render();
    }

    public function checkout(Request $request, $session_name=null){
        
        $order= OrderSession::getOrder(null,$session_name);
        
        if ($request->isMethod('post')) {
            self::updateOrder($request, $order);
        
            if ($request->has('pay_checkout')){
                $order->setPaymentById(intval(hashids_decode($request->get('pay_checkout',''))));
                $order->save();
                return redirect('payment/checkout');
            }
            
            if ($request->input('method','') == 'update'){
                /** Analytics - start */
                    Event::fire('analytics.order',[orderAnalyticsDetails($order),'trigger', 'checkout']);
                /** Analytics - end */
                
                if ($request->wantsJson()){
                    				
                    return response()->json([
                        'success' => true,
                        'analytic_triggers'=>Analytics::triggerPayloads(),
                        //'view'=>view('Shop::order.partials.list',['order'=>$order])->render()
                        'action' => 'update',
                        'flash' => $request->session()->get('flash_notification'),
                        'flash_view' => view('flash::message')->render(),
                        'order' => $order->setPresenter(new CartPresenter())->parserResult()["data"],
                        'message' =>"Update Cart",
                        'view'  => view("Shop::order.cart-preview", ['order' => $order])->render(),
                    ]);
                }
                else{
                    return redirect(config('quatius.shop.cart.url_prefix','order/').($session_name?"$session_name":""));
                }
            }
            
            $order->resetPaymentMethod();
            $order->save();
        }
        
        if (!$order->hasBillingDetail()){
            return redirect(config('quatius.shop.cart.url_prefix','order/').'address/'.($session_name?"$session_name":""));
        }
        
        if (!$order->isValid(true)){
            return redirect(config('quatius.shop.cart.url_prefix','order/').($session_name?"$session_name":""));
        }   
        
        Event::fire('shop.cart-view-confirming',[$order, $request]);
        
        return $this->theme->of('Shop::order.order', compact(['order','session_name']))->render();
    }
    
    public function pdfOrder(Request $request, $orderNumber)
    {
        $order = Order::whereOrderNumber($orderNumber)->first();
        $pdf = $order->getPdfFile();
        return response()->make(file_get_contents($pdf) , 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$order->order_number.'.pdf"'
        ]);
    }
    
    public function printOrder(Request $request, $orderNumber)
    {
        $order = Order::whereOrderNumber($orderNumber)->first();
        return view('Shop::order.exports.print', compact(['order']));
    }
    
    public function success(Request $request, $orderNumber)
    {
        $request->session()->put('completed_order_number', $orderNumber);
        return redirect(config('quatius.shop.cart.url_prefix','order/').'completed');
    }

    public function completed(Request $request, $orderNumber="")
    {
        $order = Order::whereOrderNumber($orderNumber)->first();
        if (!$orderNumber){
            $sessionNumber=session('completed_order_number','');
            $order = Order::whereOrderNumber($sessionNumber)->first();
        }else
            $order = Order::whereOrderNumber($orderNumber)->first();

        if(!$order)
            return redirect(config('quatius.shop.cart.url_prefix','order/'));
        
        Theme::breadcrumb()->add('Home', url('/'))->add('Completed Order', '#');

        Event::fire('shop.cart-view-completed',[$order, $orderNumber]);

        return $this->theme->of('Shop::order.completed', compact(['order']))->render();
    }
    
    public function cancel(Request $request, $session_name=null){
        return redirect(config('quatius.shop.cart.url_prefix','order/').'checkout'.($session_name?"/$session_name":""));
    }
    
    public function confirm(Request $request, $session_name=null){
        
        
        $orderSession = OrderSession::getOrder(null,$session_name);
        
        if ($request->has('payment_id')){
            $orderSession->setPaymentById(intval(hashids_decode($request->get('payment_id',''))));
        }

        if ($orderSession->isValid(true)){
            
            $orderSession->applyCustomer();
            
            $orderSession->status = 'pending';
            
            $order = $orderSession->saveToDatabase();
            
            $order->updateStatus('pending');
            
            if($order->total == 0){
                $order->update(['payment_method'=>'coupon']);
                $order->updateStatus('confirmed', ['comments'=>'Payment Completed By Coupon']);
                $orderSession->resetOrder(false);
                flash('Payment completed successfully');
                return redirect(config('quatius.shop.cart.url_prefix','order/').'completed/'.$order->order_number);
            }
            elseif ($orderSession->getPaymentMethod()){
                $order->update(['payment_method'=>$orderSession->getPaymentMethod()->name]);
                
                if ($request->input('checkout','0') == '1'){
                    return redirect('payment/checkout/confirm');
                }

                if ($orderSession->getPaymentMethod()->requiredForm())
                {
                    $validator = Validator::make($request->all(), $orderSession->getPaymentMethod()->getValidation());
                    
                    if ($validator->fails()) {
                        $this->throwValidationException($request, $validator);
                    }
                   
                    $request->session()->flash('credit-card', $request->all());
                }
                
                return redirect('payment/confirm');
            }
        }
        return $this->cancel($request, $session_name);
    }
    
    public function editAddress(Request $request, $session_name=null){
        $order = OrderSession::getOrder(null,$session_name);
        
        $request->getSession()->put('url.intended', config('quatius.shop.cart.url_prefix','order/').'checkout');
        return $this->theme->of('Shop::order.address', compact(['order','session_name']))->render();
    }
    
    public function saveAddress(Request $request, $session_name=null){
        
        if($request->get('signup') == 1){
            $validator = Validator::make($request->all(), [
                'first_name'           => 'required|max:64',
                'last_name'            => 'required|max:64',
                
                'email'                => 'required|email|max:255|unique:users',
                'phone_1' 			   => 'required|min:8|max:15',
                'address_1'			   => 'required|max:64',
                'city'				   => 'required|max:64',
                'postcode' 			   => 'required|regex:/\b\d{4}\b/',
                'country'			   => 'required|not_in:0',
                'password'             => 'required|confirmed|min:6',
                'g-recaptcha-response' => 'required|recaptcha'
            ]);
            
            if ($validator->fails()) {
                if ($request->wantsJson()){
                    return response()->json([
                        'success' => false,
                    ])->withErrors($validator)->withInput();
                }
                
                return redirect(config('quatius.shop.cart.url_prefix','order/').'address#guest')
                ->withErrors($validator)
                ->withInput();
            }
        }elseif (Auth::guest()){
            $validator = Validator::make($request->all(), [
                'first_name'           => 'required|max:64',
                'last_name'            => 'required|max:64',
                'email'                => 'required|email|max:255',
                'phone_1' 			   => 'required|min:8|max:15',
                'address_1'			   => 'required|max:64',
                'city'				   => 'required|max:64',
                'postcode' 			   => 'required|regex:/\b\d{4}\b/',
                'country'			   => 'required|not_in:0',
            ]);
            
            if ($validator->fails()) {
                if ($request->wantsJson()){
                    return response()->json([
                        'success' => false,
                    ])->withErrors($validator)->withInput();
                }
                
                return redirect(config('quatius.shop.cart.url_prefix','order/').'address#guest')
                ->withErrors($validator)
                ->withInput();
            }
        }
        else{
            $validator = Validator::make($request->all(), [
                'first_name'           => 'required|max:64',
                'last_name'            => 'required|max:64',
                'phone_1' 			   => 'required|min:8|max:15',
                'address_1'			   => 'required|max:64',
                'city'				   => 'required|max:64',
                'postcode' 			   => 'required|regex:/\b\d{4}\b/',
                'country'			   => 'required|not_in:0',
            ]);
            
            if ($validator->fails()) {
                if ($request->wantsJson()){
                    return response()->json([
                        'success' => false,
                    ])->withErrors($validator)->withInput();
                }
                
                return redirect(config('quatius.shop.cart.url_prefix','order/').'address#guest')
                ->withErrors($validator)
                ->withInput();
            }
            
            $request->request->add(["email"=>user()->email]);
        }

        $order= OrderSession::getOrder(null,$session_name);
        $addressData = array_map('trim', $request->all());

        $address_detail = new OrderDetail($addressData);
        
        if($request->get('signup') == 1){
            $register = new RegisterController($request);
            
            $request->request->add(['name' => $request->get('first_name')]);

            if (config('user.verify_email')) {
                $user = $register->create($addressData);
                $register->sendVerificationMail($user);
            }
            else
                $user = $register->create($addressData, 'Active');
                
            $address_detail->user_id = $user->id;
            
            unset($addressData['email']);
            
            $address = new UserDetail($addressData);
            $address->setAsBillingAddress();
            $address->user_id = $address_detail->user_id;
            $address->save();
            Auth::guard()->login($user);
        }
        else if ($order->getCustomerId()){ // not guest
           
            unset($addressData['email']);
            
            $address_detail->user_id = $order->getCustomerId();
            
            $address = UserDetail::whereUserId($address_detail->user_id)->whereAddressType(UserDetail::$BILLING_TYPE)->first();
            
            if ($address){
                $address->fill($addressData);
            }
            else {
                $address = new UserDetail($addressData);
                $address->setAsBillingAddress();
                $address->user_id = $address_detail->user_id;
            }

            if($order->allowAddressSaving())
                $address->save();

            $address_detail->email = $order->getCustomer()->email;
        }
        
        if($order->getCustomerId()){ //Make sure we assign the user to the order.
            $address_detail->email = $order->getCustomer()->email;
        }
        
        $order->setBillingDetail($address_detail);
        $order->save();
        
        if ($request->wantsJson()){
            return response()->json([
                'success' => true,
                'isValid' => $order->isValid(false),
            ])->withErrors($validator)->withInput();
        }
        
        if ($order->isValid(true)){
            return redirect(config('quatius.shop.cart.url_prefix','order/').'checkout'.($session_name?"/$session_name":""));
        }
        else{
            return redirect(config('quatius.shop.cart.url_prefix','order/').($session_name?"/$session_name":""));
        }
    }
    
    public function authenticate(Request $request){
        $validator = Validator::make($request->all(), [
            'email'		=> 'required|email|max:255',
            'password'		=> 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'),"status"=>"Active"], $request->has('remember'))) {
            // Authentication passed...
            return redirect(config('quatius.shop.cart.url_prefix','order/').'checkout'.($session_name?"/$session_name":""));
        }else{
            //flash()->error('<strong>Login failed</strong><br />Email and password do not match!');
            return redirect(config('quatius.shop.cart.url_prefix','order/').'address#logintab')
            ->withErrors('Authentication failed!')
            ->withInput();
        }
    }
    
    static function updateOrder(Request $request, $order){
        if ($request->has('prod_ids'))
        {
            $method = $request->input('method','');
            $ids = $request->input('prod_ids',array());
            $qtys = $request->input('prod_qty',array());
            
            if ($request->has('shipment_method')){
                $order->setShipmentMethod(hashids_decode($request->get('shipment_method','')));
            }
            
            self::addMultipleProducts($order, $ids, $qtys, false, $request->input('cart_extras',[]));
            $order->removeZeroQty();
            
            
            if ($request->has('comments')){
                $order->comments = $request->get('comments','');
            }
            
            if ($request->has('delivery_date')){
                $order->delivery_date = $request->get('delivery_date','');
            }
            Event::fire('shop.cart-update',[$order, $request]);
            $order->save();
        }
    }
    
    public function clear(Request $request, $session_name=null){
        
        $order= OrderSession::getOrder(null,$session_name);
        
        $order->resetOrder(!config('app.debug'));
        
        return redirect(config('quatius.shop.cart.url_prefix','order/'));
    }
    
    public function removeProduct(Request $request, $session_name=null) {
        $order= OrderSession::getOrder(null,$session_name);
        
        $id = $request->input('prod_id','');
        
        $product_view = "";
        $success = false;
        
        $addedProds = count($order->getProducts());
        
        $removedProd = $order->removeProduct(hashids_decode($id));
        
        if ($addedProds != count($order->getProducts())){
            $success = true;
            if ($removedProd){
                /** Analytics - start */
                Event::fire('analytics.order',[['items'=>productAnalyticsDetails([$removedProd],[
                    'quantity'=>$removedProd->qty,
                    ])],'cart', 'remove']);
                
                /** Analytics - end */
            }
        }

        return response()->json(
            [
                'success' => $success,
                'action' => 'remove',
                'flash' => $request->session()->get('flash_notification'),
                'flash_view' => view('flash::message')->render(),
                'order' => $order->setPresenter(new CartPresenter())->parserResult()["data"],
                'message' =>"Product has been removed from Cart",
                'view'  => view("Shop::order.cart-preview", ['order' => $order])->render(),
                'analytic_push'=>Analytics::renderPayloads(),
                'analytic_triggers'=>Analytics::triggerPayloads(),
            ],
            201);
    }
    
    static function addMultipleProducts($order ,$ids = array(), $qtys = array(), $appendQty=false, $row_extras=[]){
        if (count($ids) > 0)
        {
            for($q=0; $q < count($qtys); $q++)
            {
                if(!isset($ids[$q]))
                    continue;
                    
                $intQty = intval(trim($qtys[$q]));
                $order->addProductById(hashids_decode($ids[$q]), $intQty, true, $appendQty, isset($row_extras[$ids[$q]])?$row_extras[$ids[$q]]:[]);
            }
            $order->calculatePrice();
        }

        return $order;
    }
    
    public function overrideProduct(Request $request, $session_name=null){
        if($request->has('prod_ids')){ 
            $order= OrderSession::getOrder(null, $session_name);
            $prevQty = $order->total_qty;

            self::updateOrder($request, $order);

            return response()->json(
                [
                    'success' => $prevQty != $order->total_qty,
                    'action' => 'add',
                    'flash' => $request->session()->get('flash_notification'),
                    'flash_view' => view('flash::message')->render(),
                    'order' => $order->setPresenter(new CartPresenter())->parserResult()["data"],
                    'message' =>"Update Cart",
                    'view'  => view("Shop::order.cart-preview", ['order' => $order])->render(),
                ],
                201);
        }
        else
            return $this->addProduct($request, $session_name, false);
    }
    
    public function addProduct(Request $request, $session_name=null, $appendQty = true){
        
        $order= OrderSession::getOrder(null, $session_name);
        $id = $request->input('prod_id','');
        $qty = $request->input('prod_qty',0);
        $cart_extras = $request->input('cart_extras',[]);

        $order->applyCustomer();

        $prevQty = $order->total_qty;


        if($id){
            
            $addLine = $order->addProductById(hashids_decode($id), $qty, true, $appendQty, isset($cart_extras[$id])?$cart_extras[$id]:[]);
            Event::fire('shop.cart-update',[$order, $request]);
        }
        else {
            return response()->json(
                [
                    'success' => false,
                    'action' => 'add',
                    'flash' => $request->session()->get('flash_notification'),
                    'flash_view' => view('flash::message')->render(),
                    'message' =>"Update Cart",
                ],
                301);
        }

        /** Analytics - start */
            Event::fire('analytics.order',[orderAnalyticsDetails($order),'trigger', 'checkout-trigger']);
        /** Analytics - end */

        return response()->json(
            [
                'success' => $prevQty != $order->total_qty,
                'action' => 'add',
                'flash' => $request->session()->get('flash_notification'),
                'flash_view' => view('flash::message')->render(),
                'order' => $order->setPresenter(new CartPresenter())->parserResult()["data"],
                'message' =>"Update Cart",
                'view'  => view("Shop::order.cart-preview", ['order' => $order])->render(),
                'analytic_push'=>Analytics::renderPayloads(),
                'analytic_triggers'=>Analytics::triggerPayloads(),
            ],
            201);
    }
    
}