<?php
namespace Quatius\Shop\Traits\Controllers;

use Illuminate\Http\Request;

trait UpdateWishList{
    
/**
     * Toggle a Product sku to wishlist.
     *
     * @return \Illuminate\Http\Response
     */
    public function toggle(Request $request)
    {
        $sku = $request->get('sku','');
        return response()->json(
            [
                'success' => true,
                'action' => app('WishListHandler')->check($sku)?'remove':'add',
                'wishlist' => app('WishListHandler')->toggle($sku),
                'size'=> app('WishListHandler')->countList(),
                'view'  => view("Shop::wishlist.preview")->render(),
            ],
            200);
    }
}