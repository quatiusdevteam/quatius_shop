<?php

namespace Quatius\Shop\Traits;


use App\User;


trait AddressTypes{
	
	public static $BILLING_TYPE = "BT";
	public static $SHIPMENT_TYPE = "ST";
	
	public function isFromGuest()
	{
		return $this->user_id == 0;
	}
	
	public function getAddressFields()
	{
		return $this->fillable;
	}

	public function isValid(){
		return (!empty($this->address_1));
	}
	
	public function setAsShippmentAddress()
	{
		$this->address_type = self::$SHIPMENT_TYPE;
	}
	
	public function isBillingAddress()
	{
		return $this->address_type == self::$BILLING_TYPE;
	}
	
	public function scopeShipments($query)
	{
		return $query->whereAddressType(self::$SHIPMENT_TYPE);
	}
	
	public function scopeBillings($query)
	{
		return $query->whereAddressType(self::$BILLING_TYPE);
	}
	
	public function setAsBillingAddress()
	{
		$this->address_type = self::$BILLING_TYPE;
	}
	
	public function getAddressTypeAttribute($type = "")
	{
		return $type == "" ? self::$BILLING_TYPE : $type;
	}
	
	public function user(){
		return $this->belongsTo(User::class);
	}
	
	public function getUser(){
		return $this->user;
	}
}