<?php

namespace Quatius\Shop\Traits;

trait HasOrderItems{
	
	abstract public function getProducts();
	
	abstract public function addProduct($product, $qty = 1);
	
	abstract public function addProductById(int $product_id, $qty = 1);
}