<?php
namespace Quatius\Shop\Traits;

use Quatius\Shop\Models\Dictionary;

trait UseDictionary{
    
    public static function getGroupName(){
        return strtolower(substr(get_called_class(), strrpos(get_called_class(),'\\')+1));
    }
    
    public static function saveDictionary($value, $label, $group=null, $comment=null){
        if (!$group){
            $group = self::getGroupName();
        }
        
        return Dictionary::set($group, $value, $label, $comment);
    }
    
    public static function removeDictionary($value, $group=null){
        if (!$group){
            $group = self::getGroupName();
        }
        
        return Dictionary::remove($group, $value);
    }
    
    public static function lookupLabel($value, $group=null){
        if (!$group){
            $group = self::getGroupName();
        }
        return lookup($group, $value);
    }
}