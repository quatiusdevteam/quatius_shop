<?php

namespace Quatius\Shop\Traits;

use Quatius\Shop\Models\History;
use Event;

trait HasHistories
{	
	public function histories()
	{
	    return $this->morphMany(History::class,'shop_histories');
	}
	
	public function getLastHistoryStatus($status='pending'){
	    return $this->histories->where('status',$status)->sortByDesc('updated_at')->first();
	}
	
	public function updateHistoryLinkStatus($linkModel, $attributes=[])
	{
	    $attributes['status'] == isset($attributes['status'])?$attributes['status']:'pending';
	    
	    $history = $this->getLastHistoryStatus($attributes['status']);
	    
	    if (!$history || $history && ($history->link_type!="")) {
	        $history = $this->addHistory($attributes, $linkModel);
	    }
	    else if(method_exists($linkModel, 'linkToHistory')) { //when status without link then update it.
	        unset($attributes['status']);
	        $linkModel->linkToHistory($history, $attributes);
		}
		else if ($history){
			$this->updateHistoryByStatus($attributes['status'], $attributes, $linkModel, $history);
		}
	}
	
	public function addHistory($attributes=[], $linkModel = null)
	{
	    $newHistory = new History($attributes);
	    
	    if ($linkModel){
    	    $newHistory->link_type = get_class($linkModel);
    	    $newHistory->link_id = $linkModel->id;
	    }
	    
	    $history = $this->histories()->save($newHistory);
	    $this->load('histories'); // update relations.
	    Event::fire('shop.added-history',[$history]);
	    return $history;
	}
	
	public function updateHistoryByStatus($status='pending',$attributes, $linkModel = null, $history=null)
	{
	    $history = $history?:$this->getLastHistoryStatus($status);
	    
	    if ($history){
	        if (count($attributes)>0){
				if ($linkModel){
					$attributes['link_type'] = get_class($linkModel);
					$attributes['link_id'] = $linkModel->id;
				}
				$history->update($attributes);
				$this->load('histories'); // update relations.
				
				Event::fire('shop.updated-linked-history',[$history, $attributes]);
	        }
	        return $history;
	    }
	    
	    return $this->addHistory($attributes);
	}
}
