<?php
namespace Quatius\Shop\Traits;
use Lavalite\Page\Models\Page;

trait LinkPage{
    
    public function setPageName($name){
        
        if ($this->page_id){
            $page = App('Lavalite\Page\Interfaces\PageRepositoryInterface')->findWhere(['id'=> $this->page_id])->first();
           
            $page->update([
                'name' => $name,
                'title' => $name
            ]);
        }
        else {
            $type = strtolower(class_basename(get_class($this)));
            $page = App('Lavalite\Page\Interfaces\PageRepositoryInterface')->create([
                'name' => $name,
                'title' => $name,
                'description' => "",
                'content' => "",
                'slug' => $type,
                'view' => $type,
                'abstract' => $type,
                'status' => 1,
                'keyword' => "",
            ]);
            $this->update(['page_id' => $page->id]);
            
        }
        $this->Load('page');
    }
    
    public function scopePage($query, $list=['name','content']){
 
        $createFields = [];
        foreach ($list as $field){
            $createFields[] = $field .' AS page_'.$field;
        }
        if (count($createFields) > 0)
        {
            $query->leftJoin(
                \DB::raw('(select id as link_page_id, '.implode(', ', $createFields).' FROM pages) AS page'), 
                'link_page_id','=','page_id'
            );
        }
        return $query;
    }
    
    public function getAttribute($key) {
        if (!$this->relationLoaded('page') && ($key == "page")){
            
            $temp = new Page();
            $added = false;
            foreach ($this->attributes as $attrKey=>$attrValue)
            {
                if (substr($attrKey, 0, 5) == "page_")
                {
                    $pageField = substr($attrKey, 5);
                    
                    $temp->attributes[$pageField] = $attrValue;
                    
                    if ($pageField != "id"){
                        $added = true;
                    }
                }
            }
            if($added){
                return $temp;
            }
            else {
                $this->load('page');
            }
        }
        return parent::getAttribute($key);
    }
    
    public function page(){
        return $this->belongsTo(App('Lavalite\Page\Interfaces\PageRepositoryInterface')->model());
    }
}