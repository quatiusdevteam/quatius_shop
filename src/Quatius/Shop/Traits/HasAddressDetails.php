<?php

namespace Quatius\Shop\Traits;

use Illuminate\Database\Eloquent\Collection;
use Quatius\Shop\Models\UserDetail;

trait HasAddressDetails{
    
    public function details()
    {
        return $this->hasMany(UserDetail::class);
    }
    
	public function hasBillingDetail()
    {
    	return !! $this->details()->billings()->count();
    }
    
    public function hasShipmentDetail()
    {
    	return !! $this->details()->shipments()->count();
    }
    
    public function getBillingDetail()
    {
    	return $this->details()->billings()->first();
    }

    public function getShipmentDetails()
    {
    	return $this->details()->shipments()->get();
    }
    
    public function isAssignedAddressId($address_id){

    	return !!$this->details()->shipments()->whereId($address_id)->count();
    }
    
    public function getShipmentDetail($address_id = 0)
    {
    	if ($address_id == 0)
    		return $this->getDefaultShipmentDetail();
    	else 
    		return $this->details()->shipments()->whereId($address_id)->first();
    }
    
    public function getDefaultShipmentDetail()
    {
    	if (property_exists($this, 'default_shipment_id') && $this->default_shipment_id != 0)
	    	$shipping = $this->details()->shipments()->whereId($this->default_shipment_id)->first();
    	else
    		$shipping = $this->details()->shipments()->first();
    	
    	if ($shipping)
    		return $shipping;
    	else
    		return $this->getBillingDetail();	
    }
    
    public function setShipmentDetail($detail)
    {
    	if (is_array($detail) || $detail instanceof Collection)
    	{
    		$detail->each(function ($address){
    			$address->setAsShippmentAddress();
    		});
    		return $this->details()->saveMany($detail);
    	}
    	else 
    	{
    		$detail->setAsShippmentAddress();
    		return $this->details()->save($detail);
    	}
    }
    
    public function setBillingDetail($detail)
    {
    	$detail->setAsBillingAddress();

    	$getNumberOfBillingDetails = $this->details()->billings()->count();
	    	
    	if ($getNumberOfBillingDetails == 1)
    	{
    		$oldBillingDetail = $this->getBillingDetail();
    		if ($detail->id != $oldBillingDetail->id)
    		{
    			if ($detail->id)
    			{
    				throw new \Exception('A Billing Address already assigned');
    				return;
    			}
    			else 
    			{
    				//Update the detail instead creating a new one
    				$oldBillingDetail->update($detail->toArray());
    			}
    		}
    		return $this;
    	}
    	elseif ($getNumberOfBillingDetails == 0)
    	{
    		//Add the billing detail to the user
    		return $this->details()->save($detail);
    	}
    	
    }
    	
    public function getStateName()
    {
    	return trans('user::user.user.options.states')[$this->state];
    }
    
    public function getStateCode()
    {
    	$states = array_flip(trans('user::user.user.options.states'));
    	return $states[$this->state];
    }
    
    public function getCountryCode()
    {
    	return 'AU';
    }
}