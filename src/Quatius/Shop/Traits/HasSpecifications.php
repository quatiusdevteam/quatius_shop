<?php
namespace Quatius\Shop\Traits;

trait HasSpecifications{
    
    public function specifications($groupByValues = false, $model=null)
    {
        $repo = app('SpecificationHandler');
        $specs = $repo->getLinksFrom($model?:$this);
        if (!$groupByValues){
            return $specs;
        }
        //$groupById = $specs->key
        
        return $repo->groupValues($repo->getLinksFrom($model?:$this));
        
    }
     
    
    public function syncSpecs($attributes, $detaching = true){
        app('SpecificationHandler')->updateValues($attributes, $this, null, $detaching);
        //->getLinkFrom($this)//
    }
    /*
    public function medias(){
        return 
        //return app('MediaHandler')->getLinkFrom($this);
    }
    
    public function hasMedia(){
        return !!$this->medias->count();
    }
    
    public function filterTypes($types = ['image/png','image/jpeg','image/gif']){
        return $this->medias->whereIn('mime_type',$types);
    }
    
    public function getImages(){
        return $this->filterTypes(['image/png','image/jpeg','image/gif']);
    }
    
    public function getThumbnailUrl(){
        $mediaThumb = $this->getImages()->first();
        if ($mediaThumb)
            return $mediaThumb->getThumbnailUrl();
            return "";
    }
    
    public function getMedia(){
        return $this->medias->first();
    }
    
    public function getMedias(){
        return $this->medias;
    }
    
    
    public function scopePage($query, $list=['name','content']){
 
        $createFields = [];
        foreach ($list as $field){
            $createFields[] = $field .' AS page_'.$field;
        }
        if (count($createFields) > 0)
        {
            $query->leftJoin(
                DB::raw('(select id as link_page_id, '.implode(', ', $createFields).' FROM pages) AS page'), 
                'link_page_id','=','page_id'
            );
        }
        return $query;
    }
    
    public function getAttribute($key) {
        if (!$this->relationLoaded('page') && ($key == "page")){
            $transfields = config('package.page.page.translate', []);
            
            $temp = new \stdClass();
            $added = false;
            foreach ($transfields as $pageField)
            {
                $LoadedFiled = "page_".$pageField;
                if (array_key_exists($LoadedFiled, $this->attributes)){
                    $added = true;
                    $temp->$pageField = ($this->attributes[$LoadedFiled])?$this->getPageTranslation($this->attributes[$LoadedFiled]):"";
                }
            }
            if($added)
                return $temp;
        } 
        else if(substr($key, 0, 5) == "page_")
        {
            $transfields = config('package.page.page.translate', []);
            foreach ($transfields as $pageField)
            {
                $LoadedFiled = "page_".$pageField;
                
                if (array_key_exists($LoadedFiled, $this->attributes)){
                    return ($this->attributes[$LoadedFiled])?$this->getPageTranslation($this->attributes[$LoadedFiled]):"";
                }
            }
        }
        return parent::getAttribute($key);
    }
    
    public function decodePageLang($value)
    {
        $langs = json_decode($value, true);
        
        if (json_last_error() == JSON_ERROR_NONE && is_array($langs)) {
            return $langs;
        }
        
        $trans[self::pageLocale()] = $value;
        
        return $trans;
    }
    
    static function pageLocale()
    {
        return App::getLocale()
        ?: Lang::getLocale();
    }
    
    public function getPageTranslation($value)
    {
        $langs = $this->decodePageLang($value);
        
        $locale = self::pageLocale();
        
        if (isset($langs[$locale])) {
            return $langs[$locale];
        }
    }
    
    public function page(){
        return $this->belongsTo(App('Lavalite\Page\Interfaces\PageRepositoryInterface')->model());
    } */
}