<?php

namespace Quatius\Shop\Traits;

use Quatius\Shop\Models\History;
use Event;

trait LinkedHistories
{	
	public function histories()
	{
	    return $this->morphMany(History::class,'shop_histories','link_type', 'link_id');
	}
	
	public function linkToHistory($history, $attributes=[])
	{
	    if ($history){
	        if (count($attributes)>0){
	           $history->update($attributes);
	        }
	        $this->histories()->save($history);
	        $this->load('histories'); // update relations.
	        Event::fire('shop.updated-history',[$history, $attributes]);
	        return $history;
	    }
	}
}
