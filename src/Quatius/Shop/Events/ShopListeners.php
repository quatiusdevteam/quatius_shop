<?php

namespace Quatius\Shop\Events;

use Event;
use Quatius\Shop\Models\Order;
use Quatius\Shop\Models\OrderDetail;
use Quatius\Shop\Models\UserDetail;

class ShopListeners
{

    //lookup reference only
    public static $event_list = [
        'shop.product-category-change' => ['product', 'categoryIds'],
        'shop.product-type-change' => ['product', 'from', 'to'],
        'shop.product-listing' => ['products'],
        'shop.product-detail' => ['product'],
        'shop.order-created' => ['order'],
        'shop.order-status-changed' => ['order', 'oldStatus'],
        'shop.cart-saving' => ['orderSession', 'order'],
        'shop.cart-saved' => ['orderSession', 'order'],
        'shop.cart-calculating-price' => ['orderSession'],
        'shop.cart-calculated-subtotal' => ['orderSession'],
        'shop.cart-calculated-total' => ['orderSession'],
        'shop.cart-saved' => ['orderSession'],
        'shop.cart-created' => ['orderSession'],
        'shop.cart-clearing' => ['sessionKey', 'orderSession'],
        'shop.cart-add-billing-addr' => ['orderSession', 'prevAddr'],
        'shop.cart-add-shipping-addr' => ['orderSession', 'prevAddr'],
        'shop.cart-remove-shipping-addr' => ['orderSession'],
        'shop.cart-view-showing' => ['orderSession', 'request'],
        'shop.cart-view-confirming' => ['orderSession', 'request'],
        'shop.cart-view-completed' => ['orderSession', 'ordernumber'],
        'shop.cart-update' => ['orderSession', 'request'],
        'shop.cart-add-product' => ['orderSession', 'orderItem'],
        'shop.cart-remove-product' => ['orderSession', 'orderItem'],
        'shop.cart-calculating-discount' => ['orderSession'],

        'shop.added-history' => ['history'],
        'shop.updated-history' => ['history', 'attributes'],
        'shop.updated-linked-history' => ['history', 'attributes'],
        'shop.admin-adding-product' => ['product'],
        'shop.admin-creating-product-page' => ['product', 'page'],
        'shop.admin-assign-product-page', ['product', 'page_id'],
        'shop.admin-delete-product',['product_id'],
        'shop.admin-restore-product',['product_id'],
        'shop.admin-status-product',['newStatus', 'oldStatus'],
        'quatius.update-link' => ['morph', 'action', 'classPath', 'classId', 'datas', 'result'],
        'quatius.cache-clear' => ['group', 'callByAction', 'keys'],
        'shop.cart-setting-customer' => ['orderSession', 'user'],
        'shop.cart-apply-customer' => ['orderSession'],
        'shop.cart-check-valid' => ['orderSession', 'response', 'cartView'],
    ];

    public function onOrderStatusChange($order, $oldStatus)
    {
        if ($oldStatus == 'pending' && $order->status == 'confirmed') {
            $order->updateProductStock();

            if (config('quatius.shop.email.on-confirm.notify_customer', true)) {
                email_order_confirmaion_to_user($order);
            }
            if (config('quatius.shop.email.on-confirm.notify_admin', true)) {
                email_order_confirmaion_to_admin($order);
            }
            Event::fire('analytics.order', [orderAnalyticsDetails($order), 'checkout', 'purchase']);
        } else if ($order->status == "refunded" && $oldStatus != 'refunded') {
            Event::fire('analytics.order', [orderAnalyticsDetails($order), 'Ecommerce', 'refund']);
        }
    }

    public function onUserLogin($user, $remember)
    {
        if (\Request::get('role', '') == 'admin.web') {
            return;
        }

        app("WishListHandler")->loadCustomer($user);

        $order = app('OrderSession');
        $order->setCustomer($user);
        $order->save();
    }

    public function onUserLogout($user)
    {

        app("WishListHandler")->loadCustomer(null);

        app('OrderSession')->removeFromSession();
    }

    public function subscribe($events)
    {
        $events->listen('shop.cart-view-completed', function ($order, $request) {
            /** Analytics - start */
            Event::fire('analytics.order', [orderAnalyticsDetails($order), 'checkout', 'purchase']);
            /** Analytics - end */
        });
        $events->listen('shop.cart-view-showing', function ($orderSession, $request) {
            /** Analytics - start */
            Event::fire('analytics.order', [orderAnalyticsDetails($orderSession), 'trigger', 'checkout']);
            /** Analytics - end */
        });

        $events->listen('shop.cart-add-product', function ($orderSession, $orderItem) {
            /** Analytics - start */
            Event::fire('analytics.order', [['items' => productAnalyticsDetails([$orderItem->getProduct()], [
                'quantity' => $orderItem->qty,
            ])], 'cart', 'add']);
            /** Analytics - end */
        });

        $events->listen('shop.product-detail', function ($product) {
            /** Analytics - start */
            Event::fire('analytics.order', [['items' => productAnalyticsDetails([$product])], 'product', 'detail']);
            /** Analytics - end */
        });


        $events->listen('shop.product-category-change', function ($product, $categories) {
            $prodRepo = app('ProductHandler');
            $product->specs = $prodRepo->getSpecList($product);
            $product->save();

            \Quatius\Shop\Models\Product::baseUpdate($product);
        });

        $events->listen('eloquent.updated: Quatius\Shop\Models\Specification', function ($spec) {
            if ($spec->getOriginal()["search_by"] != $spec->search_by) {
                $prodRepo = app('ProductHandler');

                $prodRepo->makeModel()->where('specs', 'like', "% {$spec->id}_%")->get()->each(function ($product) use ($prodRepo) {
                    $prodRepo->update(["keyword" => $prodRepo->generateKeyword($product)], $product->id);
                });
            }
        });

        $events->listen('shop.product-type-change', function ($product, $from, $to) {
            //update morph relation
            updateMorphClass('shopper_assignables', $product->id, $from, $to);
            updateMorphClass('specification_values', $product->id, $from, $to);
            updateMorphClass('media_links', $product->id, $from, $to);


            /* if ($to==\Quatius\Shop\Models\ProductReference::class){
                $specRepo = app('SpecificationHandler');
                $conditionSpecId = $specRepo->getSpecIdByName('Condition');
                
                $specRepo->updateValues([[
                    'specification_id'=>$conditionSpecId,
                    'label'=>"Refurbished"]],
                    $to,$product->id, false, true);
            } */
        });

        $events->listen('quatius.cache-clear', function ($group, $callByAction, $keys, $repo) {
            if (
                $group == "Quatius\\Shop\\Repositories\\Handlers\\ProductHandler" ||
                $group == "Quatius\\Shop\\Repositories\\Handlers\\CateogoryHandler" ||
                $group == "Quatius\\Shop\\Repositories\\Handlers\\SpecificationHandler"
            ) {
                //app('log')->info('shop event: quatius.cache-clear',[$repo]);
                if ($repo->isSkippedCache()) return;

                try {
                    if (!!setting('product', 'search-cache-ver', '')) {
                        app('ProductHandler')->removeSearchFile(setting('product', 'search-cache-ver', ''));
                        setting_save('product', 'search-cache-ver', '');
                    }
                } catch (\Exception $e) {
                    //app('log')->error('Generate search.js : '.$e->getMessage(),[$group, $keys]);
                }
            }
        });

        // update the product'specs field in database.
        $events->listen('quatius.update-link', function ($morph, $action, $classPath, $classId, $datas = [], $result, $repo) {

            if ($morph != "specification_value") {
                return;
            }

            //if ($repo->isSkippedCache()) return;

            app('ProductHandler')->generateProductMeta($classId, $classPath);
        });

        $events->listen('shop.cart-calculating-price', function ($order) {
            // Anychanges to order's total should reset payment methods.
            $order->resetPaymentMethod();
        });

        $events->listen('shop.payment-pending', function ($order, $response) {
            $order->updateStatus('pending');

            $payment = $order->getPaymentMethod();
            $order->updateHistoryLinkStatus($payment, [
                'status' => 'pending',
                'comments' => 'Pending payment from ' . $payment->name,
                'params' => ($response) ? json_encode($response->getData()) : '[]'
            ]);
        });

        $events->listen('payment.notify', function ($invoice_no, $status, $amt, $tranId, $data, $msg = null) {
            $order = Order::whereOrderNumber($invoice_no)->first();
            if ($order == null) return;

            if ($status == "pending") {
                $payment = $order->getPaymentMethod();
                $order->updateHistoryLinkStatus($payment, [
                    'status' => $status,
                    'comments' => "Payment $status from " . $payment->name,
                    'params' => json_encode($data)
                ]);
            } else if ($status == "completed" || $status == "confirmed") {
                Event::fire('analytics.order', [orderAnalyticsDetails($order), 'checkout', 'purchase']);
                $order->setParams('payment.reciept_no', $tranId);

                if ($order->status == "pending") {
                    $order->updateStatus('confirmed');
                }
                $order->save();

                $status = ($status == "completed" ? "accepted" : "confirmed");

                $payment = $order->getPaymentMethod();
                $order->updateHistoryLinkStatus($payment, [
                    'status' => $status,
                    'value' => $tranId,
                    'comments' => "Payment $status from " . $payment->name,
                    'params' => json_encode($data)
                ]);
            } elseif ($status == "refunded" || $status == "partially refunded") {
                $order->updateStatus('refunded');
                $order->save();

                $payment = $order->getPaymentMethod();
                $order->updateHistoryLinkStatus($payment, [
                    'status' => 'refunded',
                    'value' => $tranId,
                    'comments' => "Payment refunded of " . format_currency($amt),
                    'params' => json_encode($data)
                ]);

                // May not work due to server to server request
                Event::fire('analytics.order', [orderAnalyticsDetails($order), 'Ecommerce', 'refund']);
            } elseif ($status == 'checkout') {
                $payment = $order->getPaymentMethod();
                $order->updateHistoryLinkStatus($payment, [
                    'status' => 'checkout',
                    'value' => "",
                    'comments' => 'Request checkout info from ' . $payment->name,
                    'params' => json_encode($data)
                ]);
            } else if ($status == 'decline') {
                $payment = $order->getPaymentMethod();
                $order->updateHistoryLinkStatus($payment, [
                    'status' => 'decline',
                    'value' => $tranId,
                    'comments' => "Declined from " . $payment->name,
                    'params' => json_encode($data)
                ]);
            } else {
                $payment = $order->getPaymentMethod();
                $order->updateHistoryLinkStatus($payment, [
                    'status' => 'unknown',
                    'value' => $tranId,
                    'comments' => "Unknown status('$status') from " . $payment->name,
                    'params' => json_encode($data)
                ]);
            }
        });

        $events->listen('payment.confirmed', function ($order, $response) {
            if (method_exists($order, 'removeFromSession')) $order->removeFromSession(); //clear session
        });

        $events->listen('payment.checkout.confirmed', function ($order, $response) {
            if (method_exists($order, 'removeFromSession')) $order->removeFromSession(); //clear session
        });

        $events->listen('payment.address.update', function ($order_session, $data, $response) {
            if ($order_session->hasShipmentDetail()) {
                $order_session->setShipmentDetail($data);
            } else {
                if($order_session->getCustomerId() && $order_session->allowAddressSaving()){
                    $userAddress = UserDetail::whereUserId($order_session->getCustomerId())->whereAddressType(UserDetail::$BILLING_TYPE)->first();
                    if ($userAddress){
                        $userAddress->fill($data);
                    }
                    else {
                        $userAddress = new UserDetail($data);
                        $userAddress->setAsBillingAddress();
                        $userAddress->user_id = $order_session->getCustomerId();
                    }
                    
                    $userAddress->save();
                }

                $order_session->setBillingDetail($data);
            }
        });

        $events->listen('shop.order-status-changed', 'Quatius\Shop\Events\ShopListeners@onOrderStatusChange');
        $events->listen('auth.login', 'Quatius\Shop\Events\ShopListeners@onUserLogin');
        $events->listen('auth.logout', 'Quatius\Shop\Events\ShopListeners@onUserLogout');
        $events->listen('shop.send_order_pending_notification', function ($request, $pending_orders) {
            email_pending_order_to_manager($pending_orders->toArray());
        });
    }
}
