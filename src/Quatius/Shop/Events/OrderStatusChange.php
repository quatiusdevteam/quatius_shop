<?php

namespace Quatius\Shop\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Quatius\Shop\Models\Order;

class OrderStatusChange extends Event
{
    use SerializesModels;

    public $order = null;
    public $oldStatus = null;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $order, $oldStatus='pending')
    {
    	$this->order = $order;
    	$this->oldStatus = $oldStatus;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
