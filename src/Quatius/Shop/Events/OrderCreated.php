<?php

namespace Quatius\Shop\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class OrderCreated extends Event
{
    use SerializesModels;

    public $order = null;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(OrderCreated $order)
    {
    	$this->order = $order;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
