<?php
namespace Quatius\Shop\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

interface ProductRepository extends RepositoryInterface{
    public function getFromSlug($slug);
}