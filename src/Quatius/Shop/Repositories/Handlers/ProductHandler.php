<?php

namespace Quatius\Shop\Repositories\Handlers;

use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Shop\Repositories\ProductRepository;
use File;
use Quatius\Shop\Repositories\Presenters\ProductSearchPresenter;

class ProductHandler extends QuatiusRepository implements ProductRepository{
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.shop.product.model',\Quatius\Shop\Models\Product::class);
    }
    
    public function searchVersion($search_ver=null, $groupIds=null){
        $search_ver = $search_ver?:setting('product', 'search-cache-ver','');
        if (!$search_ver){
            $search_ver =  strtotime('now');
            setting_save('product', 'search-cache-ver', $search_ver);
        }
        
        if ($groupIds === null){
            $groupIds = app('OrderSession')->getShopperGroupIds();
        }
        $search_ver .= implode("", $groupIds);
        
        return $search_ver;
    }
    
    public function searchFilename($search_ver=null, $groupIds=null){
        $searchVersion = $this->searchVersion($search_ver, $groupIds);
        return "js/shop/search/ver-$search_ver.js";
    }
    
    public function searchFileUrl($search_ver=null){
        $file = $this->searchFilename($search_ver);
        $path = dirname(public_path($file));
        if (file_exists(public_path($file))){
            return url($file);
        }
        if (!file_exists($path)){
            mkdir($path, 777, true);
        }
        file_put_contents(public_path($file), $this->generateSearchContent());
        
        return url($file);
    }
    
    public function generateSearchContent($toJavacript=true){
        
        $this->setPresenter(new ProductSearchPresenter());
        $order = app('OrderSession');
        
        $producDatas = $this->parserResult($this->makeModel()->query()->published()->categoryIds()->ofShoppers($order->getShopperGroupIds())->get());
        //$categories = app('CategoryHandler')->getFromRoot()->toTree();
        
        $structCats = app('CategoryHandler')->formatStructure();
        //$options = app('CategoryHandler')->getSelectionOptions(null, $categories, $prefix="", "", false, 0, true);
        
        $searchData['products'] = $producDatas['data'];
        $searchData['categories'] = [];
        
        
        foreach ($structCats as $cat){
            $searchData['categories'][] = [
                "id" => $cat->id,
                "url" => $cat->url,
                "name" => $cat->page->name,
                "subs" => $cat->subIds,
                "level" => $cat->level,
            ];
        }
        
        //$searchData['categories'] = $cateList = array_flip($options);
        
        if (!$toJavacript){
            return ($searchData);
        }

        return view('Shop::search.partials.list-js', compact(['searchData']))->render();
    }
    
    public function removeSearchFile($search_ver){
        $file = $this->searchFilename($search_ver);
        
        $files = glob(dirname(public_path($file)).'/*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
        
        setting_save('product', 'search-cache-file', "");
    }
    
    public function generateKeyword($product, $addon=""){
        $keyword = "";
        
        if ($product->page){
            $keyword .= " ".$product->page->name;
            $keyword .= " ".$product->page->keyword;
        }
        
        $keyword .= " ".$product->description;
        
        $keyword .= " ".$product->sku;
        
        foreach ($product->categories as $cate){
            $keyword .= " ".$cate->page->name;
        }
        $specs = $product->specifications(true)
            ->filter(function($item){
                return $item->search_by!=null;
            });
        
        foreach ($specs as $spec){
            if ($spec->search_by == 'field' || $spec->search_by == 'both'){
                $keyword .= ' '.$spec->name;
            }
            
            if ($spec->search_by == 'value' || $spec->search_by == 'both'){
                $spec->labelSeparator= ' ';
                if ($spec->label){
                    $keyword .= ' '.$spec->label;
                }
            }
        }
        
        if(!empty($addon)){
            $keyword .= " ".$addon;
        }
        
        return $this->reduceKeyword($keyword);
    }
    
    /*
        Generate Metadata for product

        @param id or product object
        @param classPath 
    */
    public function generateProductMeta($classId = 0, $classPath=null){
        try{
            $prodClasses = $this->makeModel()->getSingleTableTypeMap();
            if (array_search($classPath, $prodClasses) !==false || $classPath==null){
                $product = is_int($classId)?$this->skipCache()->findWhere(['id'=>$classId])->first():$classId;
                
                $product->timestamps = false;
                $product->update([
                    "specs" => $this->getSpecList($product),
                    "keyword"=>$this->generateKeyword($product)
                    
                ]);
                $this->clearCache();
            }
            
            $cateRepo = app('CategoryHandler');
            if ($classPath == $cateRepo->model()){
                $products = $cateRepo->skipCache()->findWhere(['id'=>$classId])->first()->getAllProducts(null);
                foreach ($products as $product){
                    $this->update([
                        "specs" => $this->getSpecList($product),
                        "keyword"=>$this->generateKeyword($product)
                        
                    ], $product->id);
                }
            }
            
            if ($classPath == "Lavalite\\Page\\Models\\Page"){
                $products = $this->skipCache()->findWhere(['page_id'=>$classId]);
                foreach ($products as $product){
                    $this->update([
                        "specs" => $this->getSpecList($product),
                        "keyword"=>$this->generateKeyword($product)
                        
                    ], $product->id);
                }
            }

            $this->removeSearchFile(setting('product', 'search-cache-ver',''));
            
            setting_save('product', 'search-cache-ver', strtotime('now'));
        }
        catch(\Exception $e)
        {
            //app('log')->error('Generate Keywords : '.$e->getMessage(),[$classPath]);
        }
    }

    private function reduceKeyword($keyword){
        
        $keyword = str_replace(['.',','], " ", trim($keyword));
        
        return implode(' ',array_unique(explode(' ', $keyword)));
    }
    
    public function getSpecList($product){
        return ' '.implode(' ', $product->specifications()->pluck('value')->toArray()).' ';
    }
    
    public function getFromSlug($slug){
        
        if (!$this->allowedCache('getFromSlug') || $this->isSkippedCache()) {
            return $this->makeModel()->whereSlug($slug)->published()->with('page','prices','categories')->first();
        }
        
        $key = $this->getCacheKey('getFromSlug', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($slug) {
            return $this->makeModel()->whereSlug($slug)->published()->with('page','prices','categories')->first();
        });
        
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    
    private function _getFromCategory($cateId = 0, $shopperGroupIds=[], $status=2){
        
        if (!$shopperGroupIds){
            $shopperGroupIds = app('ShopperHandler')->getDefault()->pluck('id')->toArray();
        }
        
        $query = $this->makeModel()->categoryIds()->page()->withCategory()->ofShoppers($shopperGroupIds);
        
        if ($cateId != 0){
            $cate = app('CategoryHandler')->makeModel()->find($cateId);
            if (!$cate){
                return collect([]);
            }
            
            $cateIds = $cate->getSubIds($status);
            $query->whereIn('category_id',$cateIds);
        }
        
        $query->status($status)->groupby('id');
        return $query->get();
    }

    public function preloadViewingProducts($prods, $shopperGroupId){
        $price_group = app('ShopperHandler')->find($shopperGroupId)->price_group;
        //$prices = app('ProductPriceHandler')->findWhere(['price_group'=>$price_group])->keyBy('product_id');
        //$prices_rrp = app('ProductPriceHandler')->findWhere(['price_group'=>config('quatius.shop.price.compared-to', 'RRP')])->keyBy('product_id')
        
        $prodIds = $prods->pluck('id')->toArray();
        foreach ($prods->where('type','reference') as $refProd){
                $prodIds[] = $refProd->getProductReferenceId();
        }

        $stocks = app('WarehouseHandler')->getAvailable(config('quatius.shop.domain','AU'));
        $thumbs = app('MediaHandler')->getThumbLinkFrom(array_values(\Quatius\Shop\Models\Product::getSingleTableTypeMap()), $prodIds, 320, 240);
        $price_group_rrp = config('quatius.shop.price.compared-to', 'RRP');
        $groupList = explode(',',$price_group.','. $price_group_rrp.','. config('quatius.shop.price.groups', 'GUEST-AU,REGISTER-AU,GUEST-AU,VIP-AU'));

        $priceList = app('ProductPriceHandler')->getByGroups($groupList)->groupBy('product_id');

        foreach($prods as $product){
            $product->stock = isset($stocks[$product->id])?$stocks[$product->id]: 0;
            $product->price_group = $price_group;
            
            $price_list = [];
            $prodPrices = $priceList->get($product->id);
            if ($prodPrices){
                foreach($prodPrices as $priceObj)
                {
                    $price_list[$priceObj->price_group] =  round($priceObj->getPrice(), 2);
                }
            }
            $product->price_list = $price_list;
            $product->shopper_group_id = $shopperGroupId;

            if ($product->type == 'reference'){
                $product->thumb_url = isset($thumbs[$product->getProductReferenceId()])?$thumbs[$product->getProductReferenceId()]: "";
            }else{
                $product->thumb_url = isset($thumbs[$product->id])?$thumbs[$product->id]: "";
            }
        }
        return $prods->unique('id');
    }

    public function getFromCategory($cateId = 0, $shopperGroupIds=[],  $status=2){
        $key = $this->getCacheKey('getFromCategory', func_get_args());
        if (!$shopperGroupIds){
            $shopperGroupIds = app('ShopperHandler')->getDefault()->pluck('id')->toArray();
        }
        
        if (!$this->allowedCache('getFromCategory') || $this->isSkippedCache()) {
            $value = $this->_getFromCategory($cateId, $shopperGroupIds, $status);
        }
        else{
            $minutes = $this->getCacheMinutes();
            $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($cateId, $shopperGroupIds, $status) {
                return $this->_getFromCategory($cateId, $shopperGroupIds,  $status);
            });
            
            $this->resetModel();
            $this->resetScope();
        
        }
        
        return $this->preloadViewingProducts($value, $shopperGroupIds[0]);
        
    }

    public function getParentCategory($prod){
        $key = $this->getCacheKey('getParentCategory', func_get_args());
        
        if (!$this->allowedCache('getParentCategory') || $this->isSkippedCache()) {
            $value = $this->_getParentCategory($prod);
        }
        else{
            $minutes = $this->getCacheMinutes();
            $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($prod) {
                return $this->_getParentCategory($prod);
            });
            
            $this->resetModel();
            $this->resetScope();
        
        }
        
        return $value;
        
    }

    private function _getParentCategory($product){
        $category = $product->categories->first();

        if ($category)
            return app('CategoryHandler')->getParentCategories($category->getParentIds());
        
        return collect([]);
    }

    private function _getBy($field, $list, $shopperGroupIds=[]){
        if ($shopperGroupIds===null){
            return $this->makeModel()->query()->whereIn($field,$list)->published()->get();
        }
        
        return $this->makeModel()->query()->whereIn($field,$list)->published()->ofShoppers($shopperGroupIds)->get();
    }

    public function getBy($field, $list, $shopperGroupIds=[]){
        $key = $this->getCacheKey('getBy', func_get_args());
        
        if (!$this->allowedCache('getBy') || $this->isSkippedCache()) {
            $value = $this->_getBy($field, $list, $shopperGroupIds);
        }
        else{
            $minutes = $this->getCacheMinutes();
            $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($field, $list, $shopperGroupIds) {
                return $this->_getBy($field, $list, $shopperGroupIds);
            });
            
            $this->resetModel();
            $this->resetScope();
        }
        
        return $value;
    }

    public function getCategoryIdMap($shopperGroupIds=[]){
        $key = $this->getCacheKey('getAllWithCategories', func_get_args());
        
        if (!$this->allowedCache('getAllWithCategories') || $this->isSkippedCache()) {
            $query = $this->makeModel();
            if ($shopperGroupIds !== null)
                $query->ofShoppers($shopperGroupIds);

            $value = $query->categoryId()->published()->select(['id','category_id'])->distinct()->get();
        }
        else{
            $minutes = $this->getCacheMinutes();
            $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($shopperGroupIds) {
                $query = $this->makeModel();
                if ($shopperGroupIds !== null)
                    $query->ofShoppers($shopperGroupIds);
                    
                return $query->categoryId()->published()->select(['id','category_id'])->distinct()->get();
            });
            
            $this->resetModel();
            $this->resetScope();
        }
        
        return $value;
    }
}