<?php

namespace Quatius\Shop\Repositories\Handlers;

use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Shop\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\Collection;

class CategoryHandler extends QuatiusRepository implements CategoryRepository{
    
    private static $activeList = null;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.shop.category.model',\Quatius\Shop\Models\Category::class);
    }
    
    public function getMenuTree($categoryRoute= null, $rootId = 0){
        $categories = $this->getFromRoot($rootId);
        
        if ($categoryRoute){
            $this->setActiveList($categoryRoute);
        }
        
        if ($this->getActiveList()->count() > 0){
            foreach ($categories as $cate){
                $cate->active = self::$activeList->contains($cate);
            }
        } 
        return $categories->toTree();
    }
    
    public function getActiveList(){
        return self::$activeList?:collect([]);
    }
    
    public function setActiveList($baseCate){
        
        if (is_array($baseCate) || $baseCate instanceof Collection)
        {
            self::$activeList = collect();
            foreach ($baseCate as $cate)
            {
                self::$activeList = self::$activeList->merge($this->getParentPaths($cate->id));
            }
        }
        else
            self::$activeList = ($baseCate)?$this->getParentPaths($baseCate->id): collect([]);
    }
    
    public function getFromSlug($slug){
        
        if (!$this->allowedCache('getFromSlug') || $this->isSkippedCache()) {
            return $this->makeModel()->whereSlug($slug)->published()->with('page')->first();
        }
        
        $key = $this->getCacheKey('getFromSlug', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($slug) {
            return $this->makeModel()->whereSlug($slug)->published()->with('page')->first();
        });
        
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    
    private function _getFromRoot($rootId = 0, $withInfo=['name'], $publish=2, $shopperIds=[])
    {
        if ($rootId) { // may not fully supported
            $cateQuery = $this->makeModel()->find($rootId)->descendants();
        }
        else {
            $cateQuery = $this->makeModel()->query();
        }
        
        $cateQuery->status($publish); // null for all
        $cateQuery->page($withInfo);
        if($shopperIds){
            $cateQuery->ofShoppers($shopperIds);
        }
        return $cateQuery->get();
    }
    
    public function getFromRoot($rootId = 0, $withInfo=['name'], $publish=2, $shopperIds=[])
    {
        if (!$rootId) {
            $rootId = config('quatius.shop.category.root');
        }
        if ($shopperIds !== null) $shopperIds = $shopperIds? $shopperIds: config('quatius.shop.category.shoppers', []);

        if (!$this->allowedCache('getFromRoot') || $this->isSkippedCache()) {
            $value = $this->_getFromRoot($rootId, $withInfo, $publish, $shopperIds);
        }else{
            $args = [$rootId, $withInfo, $publish, $shopperIds];
            $key = $this->getCacheKey('getFromRoot', $args);
            $minutes = $this->getCacheMinutes();
            $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($rootId, $withInfo, $publish, $shopperIds) {
                return $this->_getFromRoot($rootId, $withInfo, $publish, $shopperIds);
            });
            
            $this->resetModel();
            $this->resetScope();
        }
        
        $prodMapId = app('ProductHandler')->getCategoryIdMap($shopperIds)->groupBy('category_id');

        foreach($value as $category){
            if ($prodMapId->get($category->id)){
                $category->product_count = $prodMapId->get($category->id)->count();
            }else{
                $category->product_count = 0;
            }
        }

        return $value;
    }
    
    public function getByName($name, $CateTree=null, $includeSub = false){
        if (!$CateTree){
            $CateTree = $this->getFromRoot()->toTree();
        }
        
        foreach ($CateTree as $cate){
            
            if ($cate->page_id && $cate->page->name == $name){
                return $cate;
            }
        }
        
        return null;
    }
    
    public function formatStructure($CateTree=null, $prefix="- ", $indent=" &nbsp;", $level = 0){
        $indents = "";
        if (!$CateTree){
            $CateTree = $this->getFromRoot()->toTree();
        }
        
        for ($i=0; $i < $level; $i++) {
            $indents.=$indent;
        }
        $options = [];
        foreach ($CateTree as $cate){
            $subIds = $cate->getSubIds();
            $hashedIds = [];
            foreach ($subIds as $id){
                $hashedIds[] = hashids_encode($id);
            }
            $cate->subHashIds =implode(",", $hashedIds);
            $cate->subIds =implode(",", $subIds);
            $cate->indent_name = $indents.($level==0?"":$prefix).$cate->page->name;
            $cate->level = $level;
            $options[$cate->id] = $cate;
            $sub_options = $this->formatStructure($cate->children, $prefix, $indent, $level + 1);
            $options = $options + $sub_options;
        }
        
        return $options;
    }
        
    public function getSelectionOptions($currentCate=null, $CateTree=null, $prefix="- ", $indent=" &nbsp;", $hashed = true, $level = 0, $collapseIds= false, $shopperIds=[]){
        $indents = "";
        if (!$CateTree)
            $CateTree = $this->getFromRoot(0,[],null,$shopperIds)->toTree();
        
        for ($i=0; $i < $level; $i++) {
            $indents.=$indent;
        }
        $options = [];
        foreach ($CateTree as $cate){
            if ($currentCate && $currentCate->id == $cate->id) {
                continue;
            }
            $refIds = ($hashed?$cate->getRouteKey():$cate->id);
            if ($collapseIds){
                if ($hashed) {
                    $hashedIds = [];
                    foreach ($cate->getSubIds() as $id){
                        $hashedIds[] = hashids_encode($id);
                    }
                    $refIds =implode(",", $hashedIds);
                }
                else {
                    $refIds = implode(",", $cate->getSubIds());
                }
            }
            $options[$refIds] = $indents.($level==0?"":$prefix).$cate->page->name;
            $sub_options = $this->getSelectionOptions($currentCate, $cate->children, $prefix, $indent, $hashed, $level + 1, $collapseIds);
            $options = $options + $sub_options;
        }
        
        return $options;
    }
    
    private function _getParentPaths($child_id, $includeSelf=true){
        $category = $this->makeModel()->whereId($child_id)->page(['name'])->first();
        $paths = $category?$category->ancestors()->page(['name'])->get():collect([]);
        if ($category && $includeSelf) {
            $paths->push($category);
        }
        
        return $paths;
    }
    
    private function _getSubSpecs($categories, $list=[], $specRepo=null){
        $list = collect($list);
        
        $specRepo = $specRepo?:app('SpecificationHandler');
        
        foreach ($categories as $cate){
            if ($cate->status == 1) continue; // disable
            
            $specs = $specRepo->getLinksFrom($cate);
            if ($cate->children->count() > 0){
                $specs = $this->_getSubSpecs($cate->children, $specs, $specRepo);
            }
            
            $list = merge_collect($list, $specs);
        }
        
        return $list;
    }
    
    public function _getParentCategories($cateIds=[]){
        $categories = $this->makeModel()->query()->whereIn('id', $cateIds)->get();
        
        $catLists = [];
        foreach ($categories as $cate){
            $catLists[$cate->id] = $cate->id;
            foreach ($cate->ancestors as $ancestors){
                $catLists[$ancestors->id] = $ancestors->id;
            }
        }
        
        return $this->makeModel()->query()->page(['name'])->whereIn('id', $catLists)->get();
    }
    
    public function getParentCategories($cateIds=[]){
        
        if (!$this->allowedCache('getParentCategories') || $this->isSkippedCache()) {
            return $this->_getParentCategories($cateIds);
        }
        
        $key = $this->getCacheKey('getParentCategories', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($cateIds) {
            return $this->_getParentCategories($cateIds);
        });
            
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    
    public function getSpecs($categories){
        $cateTree = $categories->toTree();
        
        $speclist = $this->_getSubSpecs($cateTree);
        
        return $speclist;
    }
    
    public function getParentPaths($child_id, $includeSelf=true){
        if (!$this->allowedCache('getParentPaths') || $this->isSkippedCache()) {
            return $this->_getParentPaths($child_id, $includeSelf);
        }
        
        $key = $this->getCacheKey('getParentPaths', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($child_id, $includeSelf) {
            return $this->_getParentPaths($child_id, $includeSelf);
        });
        
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    
    public function getAll(){
        if (!$this->allowedCache('getAll') || $this->isSkippedCache()) {
            return $this->makeModel()->query()->page(['name'])->get();
        }
        
        $key = $this->getCacheKey('getAll', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () {
            return $this->makeModel()->query()->page(['name'])->get();
        });
            
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
}