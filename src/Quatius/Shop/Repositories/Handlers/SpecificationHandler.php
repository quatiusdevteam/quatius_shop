<?php

namespace Quatius\Shop\Repositories\Handlers;

use Quatius\Shop\Models\Specification;
use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Shop\Repositories\SpecificationRepository;
use Quatius\Framework\Traits\MorphLinkage;
use function GuzzleHttp\json_encode;

class SpecificationHandler extends QuatiusRepository implements SpecificationRepository{
    
    use MorphLinkage {
        updateValues as updateSpecValues;
    }

    private $specIconCacheKey = "spec_icon_positions.css";
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.shop.specification.model',Specification::class);
    }
    
    public function updateProductSpec($product, $name, $value, $clearOldData = true, $clearBeforeAdd = false){
        
        $specId = $this->getSpecIdByName($name);
        
        if (!$specId){
            $spec = $this->create(['name'=>$name]);
            $specId = $spec->id;
        }
        
        if (!$specId) return;
        
        if($clearBeforeAdd) $this->removeByValue(['specification_id'=>$specId], $product);

        if ($value){
            $specValues = [];
            if (is_array($value)){
                foreach($value as $eachValue){
                    $specValues[] = [
                        'specification_id'=>$specId,
                        'label'=>trim($eachValue)
                    ];
                }
            }else{
                $specValues[] = [
                    'specification_id'=>$specId,
                    'label'=>$value
                ];
            }
            $this->updateValues($specValues, $product ,null, $clearOldData, false);
        }
        else if (!$clearBeforeAdd) {
            $this->removeByValue(['specification_id'=>$specId], $product);
        }
    }

    public function getFilterView($prodList, $returnHtml=true){
        $args = [$prodList->pluck('id')];
        $key = $this->getCacheKey('getFilterView', $args);
        if ($returnHtml){
            if ($this->getCacheRepository()->has($key)){
                return $this->getCacheRepository()->get($key,'');
            }
        }
        
        $numOfProds = $prodList->count();
        
        if ($numOfProds == 0){
            $specValues = collect([]);
        }else{
            $ids = array_unique(
                explode(
                    ',',
                    implode(",", $prodList->pluck('category_ids')->toArray())
                    )
                );
            sort($ids);
            
            $cateRepo = app('CategoryHandler');
            
            $categories = $cateRepo->getParentCategories($cateRepo->getParentCategories($ids)->pluck('id')->toArray());
            
            $combineSpecs = $cateRepo->getSpecs($categories);
            
            $prodSpecs = $prodList->pluck('specs')->toArray();
            
            $SpecValues = explode(" ",implode(" ", $prodSpecs));
            
            $valUsed = array_count_values($SpecValues);
            
            $specList = [];
            foreach ($combineSpecs as $spec)
            {
                foreach($spec->getValues() as $value=>$label){
                    
                    if (isset($valUsed[$value])){
                        if (!isset($specList[$spec->id])){
                            
                            $spec->values = [];
                            $specList[$spec->id] = $spec;
                        }
                        $values = $specList[$spec->id]->values;
                        if (!isset($values[$value])){
                            $values[$value] = $valUsed[$value];
                            $specList[$spec->id]->values = $values;
                        }
                    }
                }
            }
            
            usort($specList , function($a, $b){
                if ($a == $b) {
                    return 0;
                }
                return (count($a->values) > count($b->values)) ? -1 : 1;
            });
            $specValues = collect($specList);
        }
        
        if ($returnHtml)
        {
            $view = view('Shop::product.partials.spec-filter', ['specList'=>$specValues,'numOfProds'=>$numOfProds])->render();
            $minutes = $this->getCacheMinutes();
            $this->getCacheRepository()->put($key, $view, $minutes);
            
            return $view;
        }
        return $specValues;
    }
    
    public function getSpecIdByName($name){
        $names = array_change_key_case(array_flip(lookup('specification_name')));
        $name = trim(strtolower($name));
        return isset($names[$name])?$names[$name]:null;
    }
    
    public function groupValues($specList){
       
        $groupSpecs = $specList->groupBy('id');
        $collapeValue = [];
        foreach ($groupSpecs as $specGrp){
            $values = $specGrp->pluck('value')->toArray();
            $spec = $specGrp->first();
            $spec->value = implode(' ', $values);
            $collapeValue[] = $spec;
        }
        
        return collect($collapeValue)->sortBy('ordering'); 
    }
    
    function getLinkFields(){
        
        return [
            'morph'=>'specification_value',
            'link_id'=>'specification_id',
            'values'=>[
                'value'=>"",
                'status'=>"2"
            ]
        ];
    }

    public function generateIconCss(){
        \Debugbar::startMeasure('Loading generateIconCss');
        
        $version = setting('Specification','css-version',1);
        $cssUrl = "css/shop/".$version."_".$this->specIconCacheKey;
        $path = public_path($cssUrl);

        if (!file_exists($path) || !$this->getCacheRepository()->has($this->specIconCacheKey)){ // incase it is deleted
            $contents = $this->getCacheRepository()->remember($this->specIconCacheKey, $this->getCacheMinutes(), function(){
                $icons = app('MediaHandler')->getLinkFrom(\Quatius\Shop\Models\Specification::class);
                $contentCss = view("Shop::specification.css.spec_icon_positions", compact('icons'))->render();
                
                $version = setting('Specification','css-version',1);
                $path = public_path("css/shop/".$version."_".$this->specIconCacheKey);
                
                if (file_exists($path)){
                    unlink($path);
                }
    
                $version = intval(setting('Specification','css-version',1)) + 1;
                setting_save('Specification','css-version',$version);
                $path = public_path("css/shop/".$version."_".$this->specIconCacheKey);
            
                file_put_contents($path, $contentCss);
                return $contentCss;
            });

            $version = setting('Specification','css-version',1);
            $cssUrl = "css/shop/".$version."_".$this->specIconCacheKey;
            $path = public_path($cssUrl);

            if (!file_exists($path))
                file_put_contents($path, $contents);
        }

        \Debugbar::stopMeasure('Loading generateIconCss');
        return $cssUrl;
    }
    
    public function updateValues($linkDatas, $relationClass, $id = null, $detach = false, $removeDuplicate = true){
        $filteredDatas = [];
        
        foreach ($linkDatas as $data){
             $data['value'] = "";
             if (isset($data['label']) && isset($data['specification_id'])){
                $attr=['label'=>$data['label']];
                
                if (isset($data['comment'])){
                $attr['comment']=$data['comment'];
                unset($data['comment']);
                }
                unset($data['label']);
                
                if ($attr['label']!=""){
                    $data['value'] = Specification::updateValue($attr, $data['specification_id']);

                    
                    if (isset($data["media_id"]) && $data["media_id"]){
                        app('MediaHandler')->updateLink([['media_id'=>$data['media_id'],'params'=>$data['params']]], Specification::class, $data['specification_id'], true, $data['value']);
                    }
                    else{
                        app('MediaHandler')->deleteLinkFrom(Specification::class, $data['specification_id'], null, $data['value']);
                    }

                    $this->getCacheRepository()->forget($this->specIconCacheKey);
                }else{
                    $data['value'] = "";
                }

                unset($data['media_id']);
                unset($data['params']);
            }
            $filteredDatas[] = $data;
        }
        
        $this->updateSpecValues($filteredDatas, $relationClass, $id, $detach, $removeDuplicate);
    }
}