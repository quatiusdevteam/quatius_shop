<?php

namespace Quatius\Shop\Repositories\Handlers;

use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Shop\Repositories\ShopperRepository;
use Illuminate\Database\Eloquent\Model;
use DB;
use Quatius\Framework\Traits\MorphLinkage;

class ShopperHandler extends QuatiusRepository implements ShopperRepository{
    
    use MorphLinkage;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.shop.shoppergroup.model',\Quatius\Shop\Models\ShopperGroup::class);
    }
    
    public function getShopperGroups($model)
    {
        if (method_exists($model,'getShoppers')){
            return $model->getShoppers()->sortByDesc('ordering');
        }
        return $this->getLinksFrom($model)->sortByDesc('ordering');
    }
    
    public function getUserShopperGroups($user)
    {
        if ($user->id){
            $shoppers = $this->getShopperGroups($user);
            $envShoppers = $this->getDefault(config('quatius.shop.default-groups.registered'));
            $ids = merge_collect($shoppers, $envShoppers);
            return $ids->sortByDesc('ordering');
        }
        
        return $this->getDefault();
    }
    
    public function getDefault($default=null)
    {
        if ($default===null){
            $default = config('quatius.shop.default-groups.guest');
        }
            
        if(is_array($default)){
            return $this->all()->whereIn('default', $default);
        }
        return $this->all()->where('default', $default);
    }
    
    public function getLinkFields()
    {
        return [
            'morph'=>'shopper_assignable', 
            'link_id'=>'shopper_group_id'
        ];
    }
    
    public function updateShoppers($ids, $relationClass, $id = null, $detach = false){
        $datas = [];
        foreach ($ids as $id){
            $datas[] = ['shopper_group_id'=>$id];
        }
        
        $this->updateValues($datas, $relationClass, $id, $detach, true);
    }


    public function getAssigned($classPath){
        if (!$this->allowedCache('getAssigned') || $this->isSkippedCache()) {
            return DB::table('shopper_assignables')->where('shopper_assignable_type', $classPath)->select(['shopper_assignable_id','shopper_group_id'])->get()->groupBy('shopper_assignable_id');
        }
        
        $key = $this->getCacheKey('getAssigned', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($classPath) {
            return DB::table('shopper_assignables')->where('shopper_assignable_type', $classPath)->select(['shopper_assignable_id','shopper_group_id'])->get()->groupBy('shopper_assignable_id');
        });
        
        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    public function attachShoppers($models){
        if (count($models) == 0) return $models;
        $models = collect($models);
        $hasLoaded = true;
        foreach ($models as $model){
            if(isset($model->shopper_group_ids)) continue;

            $hasLoaded = false;
            break;
        }

        if (!$hasLoaded){
            
            $shoppers = $this->getAssigned(get_class($models->first()));

            foreach ($models as $model){
                $groups = $shoppers->get($model->id);
                if ($groups) 
                    $model->shopper_group_ids = $groups->pluck('shopper_group_id')->toArray();
                else
                    $model->shopper_group_ids = [];
            }
        }

        return $models;
    }

    public function filterShoppers($models, $shopperIds=[]){
        $models = $this->attachShoppers($models);
        $newCollection = [];
        foreach ($models as $model){
            if (!array_intersect($shopperIds, $model->shopper_group_ids)) continue;

            $newCollection[] = $model;
        }
        return collect($newCollection);
    }
}