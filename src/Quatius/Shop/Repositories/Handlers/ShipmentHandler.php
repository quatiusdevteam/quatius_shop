<?php

namespace Quatius\Shop\Repositories\Handlers;

use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Shop\Repositories\ShipmentRepository;

class ShipmentHandler extends QuatiusRepository implements ShipmentRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.shop.shipment.model', \Quatius\Shop\Models\ShipmentMethod::class);
    }

    public function getByShoppers($shopperGroupIds = [])
    {
        if (!$this->allowedCache('getByShoppers') || $this->isSkippedCache()) {
            return $this->makeModel()->ofShoppers($shopperGroupIds)->published()->get()->sortBy('ordering')->keyBy('id')->values();
        }

        $key = $this->getCacheKey('getByShoppers', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($shopperGroupIds) {
            return $this->makeModel()->ofShoppers($shopperGroupIds)->published()->get()->sortBy('ordering')->keyBy('id')->values();
        });

        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    public function resetItemCost($order)
    {
        foreach ($order->getProducts() as $item) {
            unset($item->shipCost);
        }
    }

    public function getType($type, $attributes = [])
    {
        $typeList = \Quatius\Shop\Models\ShipmentMethod::getSingleTableTypeMap();

        if (isset($typeList[$type])) {
            $classObj = new $typeList[$type]($attributes);
            return $classObj;
        }
        return null;
    }

    public function getTypes($excludes = [])
    {
        $typeList = \Quatius\Shop\Models\ShipmentMethod::getSingleTableTypeMap();

        $retTypes = [];
        foreach ($typeList as $type => $class) {
            if (array_search($type, $excludes) !== false) continue;
            $classObj = new $class();
            $retTypes[$type] = $classObj->name;
        }

        return $retTypes;
    }

    public function create(array $attributes)
    {
        if (config('database.cache.clean.enabled', true) && config('database.cache.clean.on.create', true)) {
            $this->clearCache('create');
        }

        $type = isset($attributes['type']) ? $attributes['type'] : '';

        $typeList = \Quatius\Shop\Models\ShipmentMethod::getSingleTableTypeMap();

        if (isset($typeList[$type])) {
            $classObj = new $typeList[$type]($attributes);
            $classObj->save();
            return $classObj;
        }

        return null;
    }

    public function getCost($order, $shipmentList = null)
    {

        if ($shipmentList === null) {
            $shipmentList = $this->getByShoppers($order->getShopperGroupIds());
        }

        $selShips = $shipmentList->where('selectable_by', 'system');
        $shipmentMethod = $order->getShipmentMethod();
        $selShips->push($shipmentMethod);

        if (!$shipmentMethod) {
            return null;
        }

        $selShips = $selShips->sortBy('ordering');

        foreach ($selShips as $ship) {
            $ship->initalise($order);
        }

        $totalCost = 0;
        foreach ($order->getProducts(false) as $item) {
            // will use old shipCost if already set.
            if (!isset($item->shipCost)) {
                $cost = null;

                foreach ($selShips as $eachShip) {

                    $curCost = $eachShip->getItemCost($item->getProduct(), $order);

                    if ($curCost === null) continue; // skip if return null

                    // override prev cost
                    $cost = $curCost;

                    if ($eachShip->isHalted()) {
                        break;
                    }
                }

                $item->shipCost = $cost;
            }
            if ($item->shipCost !== null)
                $totalCost += $item->shipCost * $item->qty;
        }

        foreach ($selShips as $gship) {
            $curCost =  $gship->getTotalCost($order);

            if ($curCost === null) continue; // skip if return null

            $totalCost = $curCost;

            if ($gship->isHalted()) {
                break;
            }
        }

        return $totalCost;
    }
}
