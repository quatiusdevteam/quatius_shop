<?php

namespace Quatius\Shop\Repositories\Handlers;

use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Shop\Repositories\WishListRepository;
use DB;

class WishListHandler extends QuatiusRepository implements WishListRepository{
    
    /**
     * Specify Model class name
     *
     * @return string
     */

    private $wishlist = null;
    private $user = null;

    public function model()
    {
        return \Quatius\Shop\Models\Product::class;
    }

    public function loadCustomer($user){
        $this->user = $user;
        if ($user){
            $this->wishlist = array_merge(array_filter($user->getParams('wishlist', []), function($var, $key ){return !!$var && !!$key;}, ARRAY_FILTER_USE_BOTH), $this->getList());
            $this->saveCustomerList();
        }else{
            $this->wishlist = [];
        }
        session(['wishlist-skus'=>$this->wishlist]);
    }

    public function getCustomer(){
        if ($this->user) return $this->user;
            
        $user = app('OrderSession')->getCustomer();
        if ($user->exists){
            $this->user = $user;
        }

        return $this->user;
    }

    public function saveCustomerList(){
        if ($this->getCustomer()){
            $this->user->setParams('wishlist', $this->getList());
            $this->user->save();
        }
    }

    public function getList(){
        if ($this->wishlist === null)
            $this->wishlist = session('wishlist-skus',[]);

        return array_filter($this->wishlist, function($var){return !!$var;});
    }

    public function countList(){
        return count($this->getList());
    }

    public function check($sku){
        return isset($this->getList()[$sku]);
    }
    
    public function toggle($sku){
        $list = $this->getList();
        if (isset($list[$sku])){
            unset($list[$sku]);
        }
        else{
            $list[$sku] = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        }

        $this->wishlist = $list;

        session(['wishlist-skus'=>$this->wishlist]);
        $this->saveCustomerList();
        return $this->wishlist;
    }
}