<?php

namespace Quatius\Shop\Repositories\Handlers;

use Quatius\Framework\Repositories\QuatiusRepository;
use DB;
use Quatius\Shop\Repositories\ProductPriceRepository;

class ProductPriceHandler extends QuatiusRepository implements ProductPriceRepository{
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.shop.price.model',\Quatius\Shop\Models\ProductPrice::class);
    }
    
    public function getByGroups($groups=[]){
        
        if (!$this->allowedCache('getByGroups') || $this->isSkippedCache()) {
            return $this->findWhereIn('price_group',$groups);
        }
        
        $key = $this->getCacheKey('getByGroups', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($groups) {
            return $this->findWhereIn('price_group',$groups);
        });
        
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
}