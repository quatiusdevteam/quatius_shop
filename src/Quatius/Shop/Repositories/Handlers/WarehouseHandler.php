<?php

namespace Quatius\Shop\Repositories\Handlers;

use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Shop\Repositories\WarehouseRepository;
use DB;

class WarehouseHandler extends QuatiusRepository implements WarehouseRepository{
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.shop.warehouse.model',\Quatius\Shop\Models\Warehouse::class);
    }
    
    public function getAvailableQuery($country='AU', $state=null){
        $stateSelect = $state===null?"":", `state`";
        $stateFilter = $state===null?"":" AND (state = '$state' OR state = 'ALL')";
        
        return DB::table('product_warehouse')
        ->leftJoin(DB::raw("(SELECT DISTINCT(warehouse_id) as wh_id, `country` $stateSelect FROM `warehouse_destinations` WHERE (country='$country' OR country='all') $stateFilter) as dest"), 'warehouse_id','wh_id')
        ->whereNotNull('country');
    }
    
    public function getAvailable($country='AU', $state=null){
        
        if (!$this->allowedCache('getProductQty') || $this->isSkippedCache()) {
            return $this->getAvailableQuery($country, $state)->selectRaw("product_id, (SUM(stock) - SUM(reserve) - SUM(ordered)) as available")->groupBy('product_id')->get()->pluck('available', 'product_id');
        }
        
        $key = $this->getCacheKey('getProductQty', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($country, $state) {
            return $this->getAvailableQuery($country, $state)->selectRaw("product_id, (SUM(stock) - SUM(reserve) - SUM(ordered)) as available")->groupBy('product_id')->get()->pluck('available', 'product_id');
        });
            
        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    public function getProductQty($product_id, $country='AU', $state=null){
        if (!$this->allowedCache('getProductQty') || $this->isSkippedCache()) {
            return $this->getAvailableQuery($country, $state)->whereProductId($product_id)
                ->selectRaw("*, (stock - reserve - ordered) as available")
                ->get();
        }
        
        $key = $this->getCacheKey('getProductQty', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($product_id, $country, $state) {
            return $this->getAvailableQuery($country, $state)->whereProductId($product_id)
                ->selectRaw("*, (stock - reserve - ordered) as available")
                ->get();
        });
        
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    
    public function updateOrdered($product_id, $qty, $warehouse_id){
        $qty = intval($qty);
        if ($qty > 0){
            $updated = DB::table('product_warehouse')
                ->where('product_id', $product_id)
                ->where('warehouse_id', $warehouse_id)
                ->update(['ordered' => DB::raw("ordered+$qty")]);
            
            if ($updated){
                $this->clearCache();
            }
            return $updated;
        }
        return false;
    }
    
    public function updateProductOrdered($product_id, $qty, $country, $state){
        $qty = intval($qty);
        if ($qty > 0){
            $warehouse_stock= $this->getProductQty($product_id, $country, $state)->sortByDesc('available')->first();
            
            if ($warehouse_stock){
                return $this->updateOrdered($product_id, $qty, $warehouse_stock->warehouse_id);
            }
        }
        return false;
    }
}