<?php

namespace Quatius\Shop\Repositories\Presenters;

use League\Fractal\TransformerAbstract;
use Quatius\Shop\Models\Product;

/**
 * Class PostTransformer
 * @package namespace App\Modules\GenerateTransformers;
 */
class ProductSearchTransformer extends TransformerAbstract
{

    /**
     * Transform the Post entity
     * @param Quatius\Shop\Models\Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'description'=> $model->description,
            'keyword'=> $model->keyword,
            'specs'=> $model->specs,
            'category_ids'=> $model->category_ids,
            'url'=> $model->url
        ];
    }
}
