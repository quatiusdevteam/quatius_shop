<?php

namespace Quatius\Shop\Repositories\Presenters;

use League\Fractal\TransformerAbstract;

/**
 * Class PostTransformer
 * @package namespace App\Modules\GenerateTransformers;
 */
class CartTransformer extends TransformerAbstract
{

    /**
     * Transform the Post entity
     *
     * @return array
     */
    public function transform($cart)
    {
        $cartData = [
            'shipping_cost' => number_format($cart->shipping_cost, 2),
            'subtotal' => number_format($cart->subtotal, 2),
            'tax_total' => $cart->tax_total,
            'total' =>  number_format($cart->total, 2),
            'params' =>  $cart->getParams(),
            'total_qty' => $cart->total_qty,
            'products' => []
        ];

        foreach ($cart->getProducts(false) as $prodId => $prod) {
            $prodArray = $prod->toArray();
            unset($prodArray['id']); // precaution to not show raw id.
            $prodArray['product_id'] = hashids_encode($prodId);
            $prodArray['total'] = number_format($prod->total, 2);
            $prodArray['tax'] = number_format($prod->tax, 2);
            $cartData['products'][] = $prodArray;
        }

        return $cartData;
    }
}
