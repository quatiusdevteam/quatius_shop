<?php

namespace Quatius\Shop\Repositories\Presenters\Admin;

use League\Fractal\TransformerAbstract;
use Quatius\Shop\Models\Specification;

/**
 * Class PostTransformer
 * @package namespace App\Modules\GenerateTransformers;
 */
class SpecListTransformer extends TransformerAbstract
{

    /**
     * Transform the Post entity
     * @param Quatius\Shop\Models\Specification $model
     *
     * @return array
     */
    public function transform(Specification $model)
    {
        return [
            'id' => $model->id,
            'name'=> $model->name,
            'unit_type'=> $model->unit_type,
            'unit_display'=> $model->unit_display,
            'search_by'=> $model->search_by,
            
            'ordering'=> $model->ordering,
            'params'=> $model->getParams(),
            'values'=> $model->getValues()
        ];
    }
}
