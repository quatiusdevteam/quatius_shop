<?php
namespace Quatius\Shop\Repositories\Presenters\Admin;

use Prettus\Repository\Presenter\FractalPresenter;

class SpecListPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return app('SpecListTransformer');
    }
}
