<?php

namespace Quatius\Shop\Repositories\Presenters\Admin;

use League\Fractal\TransformerAbstract;
use Quatius\Shop\Models\Product;

/**
 * Class PostTransformer
 * @package namespace App\Modules\GenerateTransformers;
 */
class ProductIndexTransformer extends TransformerAbstract
{

    /**
     * Transform the Post entity
     * @param Quatius\Shop\Models\Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id' => $model->getRouteKey(),
            'product_id' => $model->id,
            'page_id'=> ($model->page_id)?:0,
            'description'=> $model->description,
            'sku'=> $model->sku,
            'slug'=> $model->slug,
            'status'=> $model->status,
            'category_ids'=> $model->category_ids,
            'available'=> intval($model->available),
            'ordered'=> intval($model->ordered)
        ];
    }
}
