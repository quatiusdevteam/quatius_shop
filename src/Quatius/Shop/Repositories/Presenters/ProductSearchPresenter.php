<?php
namespace Quatius\Shop\Repositories\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;

class ProductSearchPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return app('ProductSearchTransformer');
    }
}
