<?php
namespace Quatius\Shop\Repositories\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;

class CartPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return app('CartTransformer');
    }
}
