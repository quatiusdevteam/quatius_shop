<?php

// include cart on all pages
Theme::asset()->container('last')->add('shop-cart.css', 'css/shop/shop-cart-preview.css');
Theme::asset()->container('last')->add('shop-search.css', 'css/shop/shop-search.css');

// Route::group(['prefix'=>'payment/','middleware' => ['web'], 'namespace' => 'App\Modules\Shop\Controllers'], function() {
    
//     Route::get('checkout', 'PaymentController@gatewayCheckout');
//     Route::get('checkout/cancel', 'PaymentController@gatewayCheckoutCancel');
//     Route::get('checkout/complete', 'PaymentController@gatewayCheckoutComplete');
//     Route::get('checkout/confirm', 'PaymentController@gatewayCheckoutConfirm');
    
//     Route::get('confirm', 'PaymentController@gatewayConfirm');
//     Route::get('confirm/complete', 'PaymentController@gatewayConfirmComplete');
    
//     Route::get('cancel', 'PaymentController@gatewayCancel');
//     Route::get('cart', 'PaymentController@returnToCart');
    
//     // app/Http/Middleware/VerifyCsrfToken = > payment/route/{gateway}/{name}/{order_number?}

//     Route::post('route/{gateway}/{name}/{order_number?}', 'PaymentController@paymentRoute');
//     Route::get('route/{gateway}/{name}/{order_number?}', 'PaymentController@paymentRoute');
    
//     if (config('app.debug')){
//         Route::get('/payment/test/notification/{order_number?}', 'PaymentController@gatewayNotificationTest');
//     }
// });

Route::group(['prefix'=>config('quatius.shop.cart.url_prefix','order/'),'middleware' => ['web'], 'namespace' => 'App\Modules\Shop\Controllers'], function() {
    if (config('app.debug')){
        Route::get('email/{order_number}', function($order_number){
            $order = Quatius\Shop\Models\Order::whereOrderNumber($order_number)->first();
            return view('Shop::order.emails.confirm_to_user', compact('order'));
        });
        Route::get('email/{order_number}/admin', function($order_number){
            $order = Quatius\Shop\Models\Order::whereOrderNumber($order_number)->first();
            return view('Shop::order.emails.confirm_to_admin', compact('order'));
        });
    }

    Route::get('pdf/{order_number}', 'CartController@pdfOrder');
    Route::get('print/{order_number}', 'CartController@printOrder');
    Route::get('completed/{order_number?}', 'CartController@completed');
    Route::get('success/{order_number}', 'CartController@success');
    Route::post('confirm/{session_name?}', 'CartController@confirm');
    Route::get('confirm/{session_name?}', 'CartController@confirm');
    Route::get('cancel', 'CartController@cancel');
    
    Route::get('get/{session_name?}', 'CartController@getOrder');
    Route::get('clear/{session_name?}', 'CartController@clear');
    Route::get('checkout/{session_name?}', 'CartController@checkout');
    Route::post('checkout/{session_name?}', 'CartController@checkout');
    Route::get('clear/{session_name?}', 'CartController@clear');
    Route::get('address/{session_name?}', 'CartController@editAddress');
    Route::post('address/{session_name?}', 'CartController@saveAddress');
    
    //Route::post('/login', 'CartController@address');
    //Last only
    Route::get('/{session_name?}', 'CartController@cart');
});

Route::group(['prefix'=>'shop','middleware' => ['web'], 'namespace' => 'App\Modules\Shop\Controllers'], function() {
    Route::post('cart/add/product','CartController@addProduct');
    Route::post('cart/override/product','CartController@overrideProduct');
    Route::post('cart/remove/{session_name?}', 'CartController@removeProduct');

    Route::post('wishlist/toggle', 'WishListController@toggle');
});

Route::group(['prefix'=>'shop','middleware' => ['web', 'auth']], function() {
    Route::resource('address', 'App\Modules\Shop\Controllers\AddressController',[
        'except' => ['index']
    ]);
});	

Route::group(['middleware' => ['web'], 'namespace' => 'App\Modules\Shop\Controllers'], function() {
    /* Route::get('/user-save/product', 'SavedController@index');
     Route::get('/user-save/product/{product_id}', 'SavedController@save');
     Route::get('/user-unsave/product/{product_id}', 'SavedController@unsave'); */
    Route::get('shop/search/data.json', 'ProductController@searchData');
    Route::get('shop/search', 'ProductController@search');
    
    //Route::get('shop/{product_slug}','ProductController@show');
    
    Route::get(config('quatius.shop.category.url_prefix','category/'), 'ProductController@index');
    Route::get(config('quatius.shop.product.url_prefix','product/').'{product_slug}', 'ProductController@show');
    Route::get(config('quatius.shop.category.url_prefix','category/').'{category_slug}', 'ProductController@showCategory');
});	

Route::group(['prefix'=>'admin/shop',  'namespace' => 'App\Modules\Shop\Controllers\Admin'],function(){
    Route::resource('shoppers', 'ShopperGroupController',[
        'except' => ['create', 'delete', 'store']
    ]);

    Route::get('/shoppers/{id}/user', 'ShopperGroupController@showUsers');
    Route::get('/shoppers/{id}/remove-user/{link_id}', 'ShopperGroupController@removeUser');
    Route::resource('shipment', 'ShipmentController',[
        'except' => ['create', 'show', 'delete']
    ]);
    
    Route::get('/shipment/initial', 'ShipmentController@initial');
    Route::get('/shipment/create/{type}', 'ShipmentController@create');
    Route::get('/shipment/reference/{type}/{link?}', 'ShipmentController@reference');

    Route::get('/admin/payment', 'PaymentController@index');
    Route::post('/admin/payment', 'PaymentController@update');

    Route::group(['prefix'=>'order'],function(){
        Route::get('/', 'OrderController@index');
        Route::post('/', 'OrderController@store');
      
     //   Route::put('/{payment_id}', 'PaymentController@update' );

        Route::get('/{order_id}', 'OrderController@show');

    }); 

/*
    Route::group(['prefix'=>'shopper'],function(){
        Route::get('shopper/users', 'ShopperAdminController@allUsers');
        Route::get('/', 'ShopperAdminController@index');
        Route::get('{id}', 'ShopperAdminController@show');
        Route::get('{id}/user', 'ShopperAdminController@showUsers');
        Route::get('{id}/remove-user/{user_id}/', 'ShopperAdminController@removeUser');
        Route::get('{id}/add-users/{user_ids}', 'ShopperAdminController@addUsers');
    });	
    */
    Route::group(['prefix'=>'category'],function(){
        Route::get('/', 'CategoryController@index');
        Route::get('create', 'CategoryController@create');
        Route::get('/{category_id}/edit', 'CategoryController@edit');
        Route::get('category/create', 'CategoryController@createCategory');
        Route::get('category/{category_id}/edit', 'CategoryController@editCategory');
        Route::post('/', 'CategoryController@store');
        Route::put('/{category_id}', 'CategoryController@update');
        Route::delete('/{category_id}', 'CategoryController@delete');
    });
    
    Route::group(['prefix'=>'product'],function(){ 
        
        Route::get('/create', 'ProductController@createProduct');
        Route::post('/create/{product_id?}', 'ProductController@createBasicSave');
        Route::post('/new', 'ProductController@newProduct');
            
        Route::delete('/{product_id}', 'ProductController@delete');
        Route::put('/{product_id}/restore', 'ProductController@restore');
        
        Route::get('/{product_id}/preview/{related_id}', 'ProductController@preview');
        Route::get('/{product_id}/create', 'ProductController@create');
        Route::put('/{product_id}/status', 'ProductController@updateStatus');
        Route::get('/filter/{filter?}', 'ProductController@index');
        Route::get('/groups/{filter?}', 'ProductController@groupProducts');
        Route::get('/', 'ProductController@index');
        Route::get('/page', 'ProductController@indexPage');
        Route::get('/{product_id}', 'ProductController@show');
        Route::patch('/{product_id}', 'ProductController@update');
        Route::get('/{product_id}/detail', 'ProductController@showDetail');
        
        Route::put('/{product_id}/image', 'ProductController@addImage');
        Route::put('/{product_id}/image/ordering', 'ProductController@orderingImage');
        Route::delete('/{product_id}/image', 'ProductController@removeImage');
        
        Route::post('/{product_id}/page/assign/{page_id}', 'ProductController@setPage');
        Route::get('/{product_id}/page', 'ProductController@showPage');
        Route::get('/{product_id}/page/create', 'ProductController@createPage');
        Route::get('/{product_id}/page/edit', 'ProductController@editPage');
        Route::post('/{product_id}/page', 'ProductController@storePage');
        Route::put('/{product_id}/page', 'ProductController@updatePage');
        
        Route::post('/{product_id}/page/remove', 'ProductController@detachPage');
        Route::post('/{product_id}/assign/categories', 'ProductController@assignCategories');
        Route::post('/{product_id}/type/{type}', 'ProductController@setProduceType');
    }); 
    
    Route::group(['prefix'=>'specification'],function(){
        Route::get('/', 'SpecificationController@index');
        Route::post('/', 'SpecificationController@store');
        Route::post('/{spec_id}/update', 'SpecificationController@updateField');
        Route::put('/', 'SpecificationController@update');
        Route::delete('/', 'SpecificationController@remove');
        Route::delete('/{spec_id}', 'SpecificationController@delete');

        Route::get('/export/{type}', 'SpecificationController@exportSpecCsv');
        Route::post('/import/{type}', 'SpecificationController@importSpecCsv');
    });
    
});