@php
    $icons = app('MediaHandler')->getLinkFrom(\Quatius\Shop\Models\Specification::class);
@endphp

<div class="detail-tags">

    @foreach($icons as $icon)
        @php
            $params = json_decode($icon->pivot->params, true);
        @endphp
        @if(data_get($params, 'class', ".cnr-DT") == '.cnr-DT')
        <div class="cnr-DT" data-spec-value = "{{$icon->pivot->grouping}}">
            <img src="{!!media_url($icon)!!}" alt="">
        </div>
        @endif
    @endforeach
</div>