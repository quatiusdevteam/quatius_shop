@php
$params = json_decode($icon->pivot->params, true);
@endphp

@if(data_get($params, 'class', ".cnr-DT") == '.cnr-DT')
.product-details[data-spec-values~="{{$icon->pivot->grouping}}"] .detail-tags {!!data_get($params, 'class', ".cnr-TL").'[data-spec-value="'.$icon->pivot->grouping.'"]'!!}{
    display:inline-block;
}

@else
.product-tile[data-spec-values~="{{$icon->pivot->grouping}}"] {!!data_get($params, 'class', ".cnr-TL")!!}{
	width: 30%;
    height: 40%;
    background-size: contain;
    background-repeat: no-repeat;
    background-image: url('{!!media_url($icon)!!}');
}

.product-details[data-spec-values~="{{$icon->pivot->grouping}}"] {!!data_get($params, 'class', ".cnr-TL")!!}{
	width: 30%;
    height: 40%;
    background-size: contain;
    background-repeat: no-repeat;
    background-image: url('{!!media_url($icon)!!}');
}
@endif