@extends('admin::curd.index')
@section('heading')
<h1>
Product Category
<small> Manage Categories</small>
</h1>
@stop
@section('title')
Manage Categories
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! trans_url('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Manage Categories</li>
</ol>
@stop
@section('entry')
@stop
@section('tools')
    <button type="button" class="btn btn-primary btn-sm" data-action='NEW' data-load-to='#menu-entry' data-href='{{Trans::to('admin/shop/category/category/create')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.new') }}</button>
@stop
@section('content')
<div style="min-height:700px;">
<div class="col-md-5" style="max-height:700px; overflow:auto">
    
    @include("Shop::admin.category.partials.nestable",['categories'=>$categories])

</div>
<div class="col-md-7" id='menu-entry'>
</div>
</div>
@stop
@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){

    var updateMenu = function(e)
    {
        var out = $(e.target).nestable('serialize');
        out     = JSON.stringify(out);
        console.log(out);
        // var formData = new FormData();
        // formData.append('tree', out);

        // var url  = '{!! trans_url('admin/menu/menu/tree') !!}';

        // $.ajax( {
        //     url: url,
        //     type: 'POST',
        //     data: formData,
        //     processData: false,
        //     contentType: false,
        //     success:function (data, textStatus, jqXHR)
        //     {
        //         console.log(data);
        //     },
        //     error: function(jqXHR, textStatus, errorThrown)
        //     {
        //     }
        // });
    };

    //$('.dd').nestable().on('change', updateMenu);


    //$('#menu-entry').load('{{trans_url('admin/menu/menu')}}/$parent->getRouteKey()');


});
</script>
@stop
@section('style')
<style type="text/css">
.box-body{
    min-height: 420px;
}

.dd > .dd-list > ul{
    padding-inline-start: 0px;
}

</style>
@stop
