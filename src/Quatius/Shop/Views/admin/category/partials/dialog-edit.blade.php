<?php $classname = 'category';?>


<div class="modal-dialog category" role="document" data-id="{{$category->getRouteKey()}}">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Category</h4>
      </div>
      <div class="modal-body">
        {!!Form::vertical_open()
        ->id('edit-cate-page')
        ->method('PUT')
        ->class('form-compact')
        ->enctype('multipart/form-data')
        ->action(Trans::to('admin/shop/category/'.$category->getRouteKey()))!!}
        {!!Form::token()!!}
        
        <div class="row">
            <div class="col-xs-8">
              {!!Form::select('parent_id','Parent')->options($cateList)->select(hashids_encode($category->parent_id))!!}
            </div>
            <div class="col-xs-4">
              {!!Form::select('cate_status')->options(['2'=>'Show','3'=>'Hide','1'=>'Disable'])->select($category->status)!!}
            </div>
        </div>
        {!!Form::hidden('selected')!!}
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
            <li><a href="#metatags{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
            <li class="hide"><a href="#settings{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
            <li class="{{user('admin.web')->isSuperuser()?'':'hide'}}"><a href="#shoppergroup{{ $page?$page->id:''}}" data-toggle="tab">Shopper Group</a></li>
        </ul>
        
        @include('Shop::admin.page.partials.entry',['extra'=>'<div class="tab-pane" id="shoppergroup'.($page?$page->id:"").'">'.view('Shop::admin.shopper.assign',['model'=>$category]).'</div>'])
        
        {!!Form::close()!!}
      </div>
      <div class="modal-footer">
   		@if($category->children->count() > 0)
        	<a tabindex="0" class="btn btn-warning pull-left" role="button" data-toggle="popover" data-trigger="focus" data-placement='top' title="Unable to delete" data-content="Please remove sub categories first."><i class="fa fa-trash"></i> Delete</a>
        @else
        	<a tabindex="0" class="btn btn-danger pull-left" role="button" data-toggle="popover" data-trigger="focus" data-placement='top' title="Delete this category?" data-html="true" data-content="<?php 
echo <<<EOF
<div class='text-center'>
<button class='btn btn-warning' type='button' onclick='deleteCategory(this)'> Yes </button>
<button class='btn btn-default' type='button'> No </button>
</div>
EOF;
?>"><i class="fa fa-trash"></i> Delete</a>
        @endif
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveForm('#edit-cate-page','#category-tree', function(res,itm){$(itm).parents('.modal').modal('hide');})" ><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
      </div>
    </div>
  </div>
  
@script
<script type="text/javascript">

$(document).ready(function(){
	$('.modal-dialog.category [data-toggle="popover"]').popover();
});

</script>
@endscript
