<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Category Specifications</h3>
        <div class="box-tools pull-right">
        	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" >
    	@php
    		$cateTree = $categories->toTree();
    	@endphp
    	
        @foreach($cateTree as $category)
        	@include("Shop::admin.category.partials.sub.cate-spec",['category'=>$category])
        @endforeach
	</div>
</div>