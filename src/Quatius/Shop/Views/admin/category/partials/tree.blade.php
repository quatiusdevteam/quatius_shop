<?php
	$isRootView = false;
	if (!isset($allCategories)){
		$allCategories = app('CategoryHandler')->getAll()->keyBy('id');
		$isRootView = true;
	}
	$onSelect = isset($onSelect)?$onSelect:'updateCategory(this)';
?>
@if($isRootView)
<div class="category-list-area">
@endif

@if ($categories->count() > 0)
<ul class="category-list">
	@foreach($categories as $category)
		@php
			unset($allCategories[$category->id]);
		@endphp
	<li class="category-item category" data-id="{{$category->getRouteKey()}}" data-id-raw="{{$category->id}}">
	 	<label class="shopper-current" style="{!!$category->status==1?'opacity:0.5; text-decoration: line-through;':($category->status==3?'opacity:0.5':'')!!}"><input name="category[]" {{$assignedIds->contains($category->id)?"checked":""}} onchange="{{ $onSelect}}" type="checkbox" value="{{$category->getRouteKey()}}"/> 
	 		<span class="category-name">{{$category->page->name}}</span>
	 	</label>
	 	<i class="fa fa-edit" onclick="editCategory(this)"></i>
		@include("Shop::admin.category.partials.tree",['categories'=>$category->children, 'allCategories'=>$allCategories, 'onSelect'=>$onSelect])
	</li> 
	@endforeach
</ul>
@endif

@if($isRootView)
<ul class="category-list" style="display:none">
	@foreach($allCategories as $category)
	<li class="category-item category" data-id="{{$category->getRouteKey()}}" data-id-raw="{{$category->id}}">
	 	<label class="shopper-other" style="{!!$category->status==1?'opacity:0.5; text-decoration: line-through;':($category->status==3?'opacity:0.5':'')!!}"><input name="category[]" {{$assignedIds->contains($category->id)?"checked":""}} type="checkbox" value="{{$category->getRouteKey()}}"/> 
	 		<span class="category-name">{{$category->page->name}}</span>
	 	</label>
	</li> 
	@endforeach
</ul>
</div>
@endif
