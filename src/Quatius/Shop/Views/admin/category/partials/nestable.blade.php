<div class="dd" id="nestable">
    <ol class="dd-list">
        @if ($categories->count() > 0)
        <ul class="category-list">
            @include( 'Shop::admin.category.partials.sub.nestable', ['categories'=>$categories])
        </ul>
        @endif
    </ol>
</div>
