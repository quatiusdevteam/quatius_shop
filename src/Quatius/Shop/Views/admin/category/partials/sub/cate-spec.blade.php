<?php
$model = $category;
$specRepo = isset($specRepo)?$specRepo:app('SpecificationHandler');
$specs = $specRepo->groupValues($specRepo->getLinksFrom($model)->sortBy('id')); 
$level = isset($level)?$level+1:0;
?>


<table class="table">
<tr>
@if($level > 0)
<td width="10px">- </td>
@endif
<td>
	<div class="spec-group">
	<div class="col-xs-6"><strong>{{$category->page->name}}</strong></div><div class="col-xs-6">@include('Shop::admin.specification.partials.adding')</div>
    @include('Shop::admin.specification.partials.list', ['specs'=>$specs,'header'=>false])
    
    </div>
    
    @foreach($category->children as $category_child)
    	@include("Shop::admin.category.partials.sub.cate-spec",['level'=>$level,'category'=>$category_child,'specRepo'=>$specRepo])
    @endforeach
</td>
</tr>
</table>