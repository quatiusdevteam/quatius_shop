@foreach($categories as $category)
    @if ($category->children)
    <li class="dd-item dd3-item category-item category" data-id="{{$category->getRouteKey()}}">
        <div class="dd-handle dd3-handle">Drag</div>
        <div class="dd3-content">
            <a href="#" data-action="LOAD" data-load-to='#menu-entry' data-href='{{trans_url('admin/shop/category/category/')}}/{!!$category->getRouteKey()!!}/edit'>
                {{$category->page->name}}
                <span class="pull-right"><i class="fa fa-angle-double-right"></i></span>
            </a>
        </div>
        <ol class="dd-list">
            @include( 'Shop::admin.category.partials.sub.nestable', ['categories'=>$category->children])
        </ol>
    </li>
    @else
    <li class="dd-item dd3-item category-item category" data-id="{{$category->getRouteKey()}}">
        <div class="dd-handle dd3-handle">Drag</div>
        <div class="dd3-content">
            <a href="#" data-action="LOAD" data-load-to='#menu-entry' data-href='{{trans_url('admin/shop/category/category/')}}/{!!$category->getRouteKey()!!}/edit'>
                {{$category->page->name}}
                <span class="pull-right"><i class="fa fa-angle-double-right"></i></span>
            </a>
        </div>
    </li>
    @endif
@endforeach
