<div class="box box-warning">
  <div class="box-header with-border">
    <h3 class="box-title"> Edit Category [{!!$category->page->name or 'New Category'!!}]</h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-cate-page'  data-load-to='#menu-entry'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>
  <div class="box-body" >
    <?php $classname = 'category';?>

    {!!Form::vertical_open()
    ->id('edit-cate-page')
    ->method('PUT')
    ->class('form-compact')
    ->enctype('multipart/form-data')
    ->action(Trans::to('admin/shop/category/'.$category->getRouteKey()))!!}
    {!!Form::token()!!}

    <div class="row">
        <div class="col-xs-8">
          {!!Form::select('parent_id','Parent')->options($cateList)->select(hashids_encode($category->parent_id))!!}
        </div>
        <div class="col-xs-4">
          {!!Form::select('cate_status')->options(['2'=>'Show','3'=>'Hide','1'=>'Disable'])->select($category->status)!!}
        </div>
    </div>
    {!!Form::hidden('selected')!!}
    <ul class="nav nav-tabs primary">
        <li class="active"><a href="#details{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
        <li><a href="#metatags{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
        <li><a href="#shopper{{ $page?$page->id:''}}" data-toggle="tab">Shopper Groups</a></li>
    </ul>
    @include('Shop::admin.page.partials.entry',['extra'=>
      '<div class="tab-pane active" id="shopper'.($page?$page->id:'').'">'.view('Shop::admin.shopper.assign',['model'=>$category,'filterIds'=>json_decode(setting('shop.category','shopper.assignable.ids',null),true)])->render().'</div>'
    ])

    {!!Form::close()!!}
      
  </div>
  <div class="box-footer hide">
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-page' data-href=''><i class="fa fa-floppy-o"></i> Apply </button>
       </div>
  </div>
</div>
