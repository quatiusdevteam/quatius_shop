<?php $classname = 'category';?>


<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Category</h4>
      </div>
      <div class="modal-body">
        {!!Form::vertical_open()
        ->id('edit-cate-page')
        ->method('POST')
        ->class('form-compact')
        ->enctype('multipart/form-data')
        ->action(Trans::to('admin/shop/category'))!!}
        {!!Form::token()!!}
        
        <div class="row">
            <div class="col-xs-8">
              {!!Form::select('parent_id','Parent')->options($cateList)!!}
            </div>
            <div class="col-xs-4">
                {!!Form::select('cate_status')->options(['2'=>'Show','3'=>'Hide','1'=>'Disable'])->select('2')!!}
            </div>
        </div>
        
        {!!Form::hidden('selected')!!}
        
        <ul class="nav nav-tabs primary">
          <li class="active"><a href="#details{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
          <li><a href="#metatags{{$page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
          <li class="hide"><a href="#settings{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
        </ul>
        @include('Shop::admin.page.partials.entry')
        {!!Form::close()!!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm" onclick="saveForm('#edit-cate-page','#category-tree', function(res,itm){$(itm).parents('.modal').modal('hide');})" ><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
      </div>
    </div>
  </div>