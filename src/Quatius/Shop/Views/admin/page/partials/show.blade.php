@php 

$classname = strtolower(substr(get_class($model), strrpos(get_class($model),'\\')+1));
$class_id = $model->getRouteKey();

@endphp
<div class="box-header with-border">
    <h3 class="box-title">Description{{$page->id?" - ".$page->name:""}}</h3>
    <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
    </div>
</div>
<div class="box-body"  style="display:none">
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
            <li><a href="#metatags{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
            <li class="hide"><a href="#settings{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('show-page')!!}
        
        @include('Shop::admin.page.partials.entry', ['disable'=>true])
    {!!Form::close()!!}
    </div>
</div>
<div class="box-footer"  style="display:none">
	<div class="box-tools pull-right">
        @if($page->id)
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-page' data-href="{{trans_url('admin/shop/'.$classname.'/'.$class_id.'/page/edit')}}"><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        @endif
    </div>
</div>