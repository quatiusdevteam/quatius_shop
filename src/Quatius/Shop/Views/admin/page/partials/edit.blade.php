@php 

$classname = strtolower(substr(get_class($model), strrpos(get_class($model),'\\')+1));
$class_id = $model->getRouteKey();

@endphp
<div class="box-header with-border">
    <h3 class="box-title">Edit {{$page->id?" - ".$page->name:""}}</h3>
    <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body">
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
            <li><a href="#metatags{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
            <li class="hide"><a href="#settings{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('create-page')
        ->method('PUT')
        ->files('true')
        ->class('form-compact')
        ->enctype('multipart/form-data')
        ->action(Trans::to('admin/shop/'.$classname.'/'.$class_id.'/page'))!!}
        {!!Form::token()!!}
        
        @include('Shop::admin.page.partials.entry')
        
        @php
        	$medias = app("MediaHandler")->getLinkFrom($page);
        @endphp
        
        @foreach($medias as $media)
        	<input type="hidden" class="media_id" data-media_url="{{media_url($media)}}" name="page_medias[][media_id]" value="{{$media->id}}">
        @endforeach
    
        {!!Form::close()!!}
    </div>
</div>
<div class="box-footer">
	<div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#create-page'  data-load-to='#entry-page'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-page' data-href='{{trans_url("admin/shop/".$classname."/".$class_id)}}/page'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
    </div>
</div>
