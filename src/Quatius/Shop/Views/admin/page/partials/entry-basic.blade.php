@php
    $disable = isset($disable)?$disable:false;
    $extra = isset($extra)?$extra:'';
@endphp
<div class="tab-content">
            <div class="tab-pane active" id="details{{ $page?$page->id:''}}">
                {!! Form::text('name')
                -> required()
                -> label(trans(trans('page::page.label.name')))
                -> placeholder(trans(trans('page::page.placeholder.name')))
                -> disabled($disable)!!}

                {!! Form::text('sku')
                -> required()
                -> attributes(['onchange'=>"$('.product .media-selection-area').attr('data-path','products/'+this.value)"])
                -> addClass('form-control')
                -> disabled($disable)!!}

                {!! Form::textarea('description')
                -> label('Description')
                -> rows(4)
                -> disabled($disable)
                -> placeholder('Description')!!}
                                

            </div>
            <div class="tab-pane" id="metatags{{ $page?$page->id:''}}">
                {!! Form::textarea('content','')
                //-> label(trans('page::page.label.content'))
                -> value(e($page['content']))
                -> addClass('html-editor')
                -> dataPath('pages/'.$classname)
                -> disabled($disable)
                -> placeholder(trans('page::page.placeholder.content'))!!}
                
            </div>
            <div class="tab-pane" id="keywords{{ $page?$page->id:''}}">
                {!! Form::textarea('keyword')
                -> label('Keywords')
                -> rows(4)
                -> disabled($disable)
                -> placeholder('Keywords')!!}
            </div>
            <div class="tab-pane" id="settings{{ $page?$page->id:''}}">
                <div class="row">
                    <div class="col-md-6 ">
                        {!! Form::range('order')
                        -> label(trans('page::page.label.order'))
                        -> placeholder(trans('page::page.placeholder.order'))!!}
                        {!! Form::text('slug')
                        -> label(trans('page::page.label.slug'))
                        -> forceValue($classname)
                        -> append(config('package.page.suffix','.html'))
                        -> placeholder(trans('page::page.placeholder.slug'))!!}

                        {!! Form::select('view')
                        -> options(trans('page::page.options.view'))
                        -> label(trans('page::page.label.view'))
                        -> forceValue($classname)
                        -> placeholder(trans('page::page.placeholder.view'))!!}
                    </div>
                    <div class='col-md-6'>
                        {!! Form::select('compiler')
                        -> options(trans('page::page.options.compiler'))
                        -> label(trans('page::page.label.compiler'))
                        -> placeholder(trans('page::page.placeholder.compiler'))!!}
                        {!! Form::select('category_id')
                        -> options([])
                        -> label(trans('page::page.label.category'))
                        -> placeholder(trans('page::page.placeholder.category'))!!}
                        {!! Form::hidden('status')
                        -> forceValue('0')!!}
                        {!! Form::checkbox('status')->check()
                        -> label(trans('page::page.label.status'))
                        ->inline()!!}
                        
                        {!! Form::hidden('abstract')->forceValue($classname)!!}
                    </div>

                </div>
            </div>
            {!!$extra!!}
    </div>