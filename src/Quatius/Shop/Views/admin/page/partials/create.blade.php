<?php 

$classname = strtolower(substr(get_class($model), strrpos(get_class($model),'\\')+1));
$class_id = $model->getRouteKey();



?>

<div class="box-header with-border">
    <h3 class="box-title">New Description</h3>
    <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
{!!Form::vertical_open()
    ->id('create-page')
    ->method('POST')
    ->files('true')
    ->class('form-compact')
    ->enctype('multipart/form-data')
    ->action(Trans::to('admin/shop/'.$classname.'/'.$class_id.'/page'))!!}
<div class="box-body" >
	{!!Form::token()!!}
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
            <li><a href="#metatags{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
            <li class="hide"><a href="#settings{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
        </ul>
        @include('Shop::admin.page.partials.entry')
    </div>
    
    @if (isset($extras))
		{!!$extras!!}	
	@endif
</div>

<div class="box-footer" >
	<div class="box-tools pull-right">
	@if (isset($buttons))
		{!!$buttons!!}	
	@else
    	<button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#create-page'  data-load-to='#entry-page' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#box-area' data-href='{{trans_url("admin/shop/".$classname."/".$class_id)}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
    @endif

    </div>
</div>
{!!Form::close()!!}
