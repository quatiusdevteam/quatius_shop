@php 

$classname = strtolower(substr(get_class($model), strrpos(get_class($model),'\\')+1));
$class_id = $model->getRouteKey();

@endphp
<div class="box-header with-border">
    <h3 class="box-title">Edit {{$page->id?" - ".$page->name:""}}</h3>
    <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body">
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
            <li><a href="#metatags{{ $page?$page->id:''}}" data-toggle="tab">Details</a></li>
            <li><a href="#keywords{{ $page?$page->id:''}}" data-toggle="tab">Keywords</a></li>
            <li class="hide"><a href="#settings{{ $page?$page->id:''}}" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
        </ul>

        @include('Shop::admin.page.partials.entry-basic')
        @php
            $medias = app("MediaHandler")->getLinkFrom($page);
        @endphp
        
        @foreach($medias as $media)
            <input type="hidden" class="media_id" data-media_url="{{media_url($media)}}" name="page_medias[][media_id]" value="{{$media->id}}">
        @endforeach
    </div>
</div>
