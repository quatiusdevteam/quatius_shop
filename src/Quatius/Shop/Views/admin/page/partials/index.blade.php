@php 

$classname = strtolower(substr(get_class($model), strrpos(get_class($model),'\\')+1));
$class_id = $model->getRouteKey();

@endphp
<div class="box-header with-border">
    <h3 class="box-title">Save list</h3>
    <div class="box-tools pull-right">
    	<button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-page' data-href='{{trans_url("admin/shop/".$classname."/".$class_id)}}/page/create'><i class="fa fa-times-circle"></i> {{ trans('cms.new') }}</button>
        
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <table id="page-list" class="table table-responsive table-striped table-bordered">
			
        </table>
    </div>
</div>
<div class="box-footer" >
&nbsp;
</div>
<script type="text/javascript">
var oPage;
	//$('#entry-user').load('{{URL::to('admin/user/user/0')}}');
$(document).ready(function(){
	oPage = $('#page-list').DataTable( {
        "ajax": '{{ URL::to("/admin/".$classname."/page") }}',
        "columns": [
        { title: "Name", "data": "name" }
        ]
    });

	$('#page-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#page-list').DataTable().row( this ).data();
        $('#box-area').load('{{URL::to("admin/".$classname."/".$class_id)}}' + '/page/assign/' + d.id);
    });
});
</script>

