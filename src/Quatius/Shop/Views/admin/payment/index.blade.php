@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> {!! trans('Shop::payment.payment-name') !!} <small> {!! trans('Shop::payment.payment-manage') !!} </small>
@stop
@section('title')
{!! trans('Shop::payment.payment-name') !!}
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">{!! trans('Shop::payment.payment-name') !!}</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-user'>
	@include("Shop::admin.payment.initial")
</div>
@stop
@section('tools')
<h4>

</h4>
@stop
@section('content')
<table id="main-list" class="table table-striped table-bordered">
    <thead>
        <th>{!! trans('Shop::payment.payment-name')!!}</th>
        <th>{!! trans('Shop::payment.payment-type')!!}</th>
        <th>{!! trans('Shop::payment.payment-mode')!!}</th>
        <th>{!! trans('Shop::payment.payment-notes')!!}</th>
        <th>{!! trans('Shop::payment.payment-status')!!}</th>
        <th>Shopper Groups</th>
    </thead>
</table>
@stop
@section('script')

@includeStyle('css/shop/shop-product-admin.css')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
    
    oTable = $('#main-list').DataTable( {
        "ajax": '{{ URL::to('/admin/shop/payment') }}',
        "columns": [
        { "data": "name" },
        { "data": "type" },
        { "data": "mode" },
        { "data": "notes" },
        { "data": "status" },
        { "data": "shopper_names" }],
        "columnDefs": [
        	{
            	"render": function ( data, type, row ) {
						switch(parseInt(data)){
						case 1:
							return '<span class="hide">1</span><i class="icon-unpublish" aria-hidden="true"></i>';
						case 2:
							return '<span class="hide">2</span><i class="icon-published" aria-hidden="true"></i>';
						case 3:
							return '<span class="hide">3</span><i class="icon-configure" aria-hidden="true"></i>';
							
						}
                },"targets": 4
            },
        	{
        		"createdCell": function (td, cellData, rowData, row, col) {
        			$(td).addClass('text-center');
        		},"targets": [2,4]
        	},
		]
    }); 
    $('#main-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('admin/shop/payment')}}' + '/' + d.id + '/edit');
    });

    
});
</script>
@stop
@section('style')
@stop


