<div class="box-header with-border">
    <h3 class="box-title">Payment</h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='CANCEL' data-load-to='#entry-user' data-href='{{Trans::to('admin/shop/payment/create')}}'><i class="fa fa-floppy-o"></i> {{ trans('cms.new') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">Profile</a></li>
            <!--<li><a href="#roles" data-toggle="tab">Details</a></li>-->
        </ul>
        
    </div>
</div>
<div class="box-footer" >
    &nbsp;
</div>
