
                        {!!Form::text('name')->value($gateway->getName())!!}
                        {!!Form::hidden('type')->value($gateway->getShortName())!!}
                        {!!Form::select('mode')
                            ->options(['sandbox'=> 'sandbox', 'live'=> 'live', 'env'=> 'env'])
                        !!}
                        {!!Form::select('status')->options(['1'=> 'non active' , '2' => 'active']) -> select('1')!!}

                        @foreach ($gateway->getDefaultParameters() as $key=>$value)
                           
                            @if (is_array($value))
                                {!!Form::select('credentials['.$key.']')->options($value)!!}
                            
                            @else 
                                {!!Form::text('credentials['.$key.']')->value($value)!!}
                            
                            @endif
                        @endforeach
