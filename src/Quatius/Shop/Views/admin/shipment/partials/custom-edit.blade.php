@if($shipment->getParams('admin_edit'))
	<?php echo renderBlade($shipment->getParams('admin_edit'), ['shipment'=>$shipment])?>

@endif

@if(user('admin.web')->isSuperuser())

{!!Form::textArea('execute_item')->label('Each Item Code($item, $order): $return {cost or null to skip}')->value($shipment->getParams('execute_item', ''))!!}

{!!Form::textArea('execute_total')->label('Total Code($order): $return {cost or null to skip}')->value($shipment->getParams('execute_total', ''))!!}


<div class="row">
<div class="col-md-6">
{!!Form::textArea('admin_edit')->label('Admin Edit Form ($shipment): return {blade}')->value($shipment->getParams('admin_edit', ''))!!}
</div>

<div class="col-md-6">
{!!Form::textArea('admin_save')->label('Admin Save ($request):')->value($shipment->getParams('admin_save', ''))!!}
</div>
</div>
@endif