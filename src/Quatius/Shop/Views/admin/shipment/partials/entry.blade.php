<div class="well">{{$shipment->description}}</div>
<div class="nav-tabs-custom">
        <!-- Nav tabs -->
    <ul class="nav nav-tabs primary">
        <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
        <li><a href="#configurations" data-toggle="tab">Configurations</a></li>
        <li><a href="#settings" data-toggle="tab">Settings</a></li>
    </ul>
    
</div>
<div class="tab-content">
    <div class="tab-pane active" id="details">
        <div class="row">
            <div class="col-sm-6">
            {!!Form::text('label')->value($shipment->label)!!}
            </div>
            
            <div class="col-sm-6">
            <div class="row">
            <div class="col-xs-4">
            @if(user('admin.web')->isSuperuser())
            	{!!Form::select('selectable_by','')->placeholder('Selection Type')->options([
            		'user'=>'User Selectable',
            		'default'=>'Default',
            		'manual'=>'Manual',
            		'system'=>'System']
            		)->value($shipment->selectable_by)->require()!!}
            @else
                @if ($shipment->selectable_by != 'system' && $shipment->selectable_by != 'manual')
                    {!!Form::hidden('selectable_by')->forceValue('user')!!}
                	@if ($shipment->selectable_by == 'default')
                		{!!Form::checkbox('selectable_by')->label('default ')->forceValue('default')->check()->inline()!!}
                	@else
                		{!!Form::checkbox('selectable_by')->label('default ')->forceValue('default')->inline()!!}
                	@endif
                @else
                	{!!Form::hidden('selectable_by')->value($shipment->selectable_by)!!}
                	{!!Form::checkbox('selectable_by')->label('System Select Only ')->forceValue('system')->check(true)->disabled()->inline()!!}
                	
                @endif
            @endif
            </div>
             <div class="col-xs-8">
            	{!!Form::radios('status')->radios([
                        ' <i class="icon-published" aria-hidden="true"></i> Active' => ['value' => 2],
                        ' <i class="icon-unpublish" aria-hidden="true"></i> Disabled' => ['value' => 1]
                    ])->check($shipment->status)->inline()!!}
            </div>
            </div>
            
           </div>
            <div class="clearfix"></div>
            <div class="col-sm-6">
            {!!Form::textarea('customer_note')->value($shipment->customer_note)!!}
            </div>
            <div class="col-sm-6">
            {!!Form::textarea('order_note')->value($shipment->order_note)!!}
            </div>
        </div>
    </div>
    
    <div class="tab-pane" id="configurations">
    {!!Form::hidden('type')->value($shipment->type)!!}
    
    @include($shipment->editView())
        
    </div>

	<div class="tab-pane" id="settings">
		{!!Form::text('name')->value($shipment->name)!!}
    	@include('Shop::admin.shopper.assign',['model'=>$shipment])
    	
    	{!!Form::textarea('description')->label('Description Admin notes')->value($shipment->description)!!}
        @if(user('admin.web')->isSuperuser())
        {!!Form::checkbox('override_params')->forcevalue(1)->inline()!!}
        {!!Form::textarea('params')->value($shipment->params)!!}
        {!!Form::text('ordering')->value($shipment->ordering)!!}
        @else
        {!!Form::hidden('params')->value($shipment->params)!!}
        {!!Form::hidden('ordering')->value($shipment->ordering)!!}
        @endif
    </div>
</div>

