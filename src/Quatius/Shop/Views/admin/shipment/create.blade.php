<div class="box-header with-border">
    <h3 class="box-title">New Shipment </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-shipment'  data-load-to='#entry-box' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-box' data-href='{{Trans::to('admin/shop/shipment/initial')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >


        {!!Form::vertical_open()
        ->id('edit-shipment')
        ->method('POST')
        ->enctype('multipart/form-data')
        ->action(URL::to('admin/shop/shipment'))!!}
    	
            @include('Shop::admin.shipment.partials.entry')
        
        {!!Form::close()!!}
    
</div>
<div class="box-footer" >
    &nbsp;
</div>