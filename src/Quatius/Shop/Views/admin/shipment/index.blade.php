@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> Shipment Manager
@stop
@section('title')
Shipment Methods
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Shipment Methods</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-box'>
	@include("Shop::admin.shipment.initial")
</div>
@stop
@section('tools')
<h4>
    @if(user('admin.web')->isSuperuser())
    <button type="button" class="btn btn-primary label label-primary" onclick="createShipment()"><i class="fa fa-floppy-o"></i> {{ trans('cms.new') }}</button>
    @endif
</h4>
@stop
@section('content')
<table id="main-list" class="table table-striped table-bordered">
    <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Type</th>
        <th>Status</th>
        <th>Ordering</th>
        <th>Shopper Groups</th>
    </thead>
</table>
@stop
@section('script')

@includeStyle('css/shop/shop-product-admin.css')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
    
    oTable = $('#main-list').DataTable( {
        "ajax": '{{ URL::to('/admin/shop/shipment') }}',
        "order": [[ 4, "asc" ]], // "desc" 
        "columns": [
        { "data": "name" },
        { "data": "description" },
        { "data": "type" },
        { "data": "status" },
        { "data": "ordering" },
        { "data": "shopper_names" },
        ],
        "columnDefs": [
        	{
            	"render": function ( data, type, row ) {
						switch(parseInt(data)){
						case 1:
							return '<span class="hide">1</span><i class="icon-unpublish" aria-hidden="true"></i>';
						case 2:
							return '<span class="hide">2</span><i class="icon-published" aria-hidden="true"></i>';
						case 3:
							return '<span class="hide">3</span><i class="icon-configure" aria-hidden="true"></i>';
							
						}
                },"targets": 3
            },
        	{
        		"createdCell": function (td, cellData, rowData, row, col) {
        			$(td).addClass('text-center');
        		},"targets": [3,4]
        	},
		]
    }); 
    $('#main-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-box').load('{{URL::to('admin/shop/shipment')}}' + '/' + d.id + '/edit');
    });
});
        
function createShipment(){
	var sel = '{!!Form::select("ship-type")->label('')->options(app("ShipmentHandler")->getTypes([""]))!!}';
	
	swal({ html:true, title:'<i>Select Shipment</i>', text:sel},function(isConfirm){
		if (isConfirm)
		{
			$('#entry-box').empty();
			$('#entry-box').load('{{URL::to('admin/shop/shipment')}}' + '/create/' + encodeURI($('#ship-type').val()));
		}
	});
}

</script>
@stop
@section('style')
@stop


