<?php 
	$filterIds = isset($filterIds)?$filterIds:null;
	$queryShopper = !$filterIds?app('ShopperHandler')->all():app('ShopperHandler')->whereIn('id',$filterIds);
?>
{!!Form::multiselect('shopper_groups')->
	options($queryShopper->pluck('name','id')->toArray())->select(
	$model->exists?$model->shoppers->pluck('id')->toArray():[])
!!}