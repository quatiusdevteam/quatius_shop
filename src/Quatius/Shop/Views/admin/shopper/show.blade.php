<div class="box-header with-border" data-id="{{$shoppergroup->id}}">
    <h3 class="box-title"> {{ $shoppergroup->name }}</h3>
    <div class="box-tools pull-right">

        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#shopper-entry' 
        data-href='{{ trans_url('/admin/shop/shoppers/') }}/{{$shoppergroup->id}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#shopper-entry' 
        data-href='{{ trans_url('/admin/shop/shoppers/') }}/{{$shoppergroup->id}}/user'><i class="fa fa-users"></i> Users</button>

        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body">
    {!! Form::vertical_open() !!}
    <div class="col-xs-6">{!! Form::text('name')->disabled(true) !!}</div>
    <div class="col-xs-6">{!! Form::text('account')->disabled(true) !!}</div>
    <div class="col-xs-6">{!! Form::text('price_group')->disabled(true) !!}</div>
    <div class="col-xs-6">{!! Form::text('reference')->disabled(true) !!}</div>
    <div class="col-xs-6">{!! Form::text('ordering')->disabled(true) !!}</div>
    <div class="col-xs-6">{!! Form::text('default')->disabled(true) !!}</div>
    <div class="clearfix"></div>
    <br/>
    <hr />
    <h4>Assigned Classes</h4>
    <ul>
        @foreach ($types as $type)
        <li>
            {{$type}}
        </li>
        @endforeach
    </ul>
    {!! Form::close()!!}
</div>
<div class="box-footer">
    &nbsp;
</div>

<script type="text/javascript">
    var oTable;
$(document).ready(function(){
	
    // oTable = $('#user-list').DataTable( {
    // 	"ajax":{
	//             "url": '{{ URL::to("/admin/shop/shoppers/".$shoppergroup->id."/user") }}',
	//             "dataSrc": function ( jsonData ) {
	// 	            if ($('#shopper-entry > [data-id]').attr('data-id') != 1)
	//             		$('#add-user').prop('disabled',false);
	// 	            return jsonData.data;
	//             }
	//           },
    //     "columns": [
	//         { "data": "name" },
	//         { "data": "email" },
	//         { "data": "id" , },
    //     ],
    //     "columnDefs": [
    //     	{
    //     		"render": function ( data, type, row ) {
    //              	return '<button type="button" onclick="removeUser(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>';
    //            	},
    //            	"targets": 2
	// 		},
    //     ],
    //     "fnDrawCallback": function( oSettings ) {
           
    //     }
    // });
    
});

</script>