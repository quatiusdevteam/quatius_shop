<div class="box-header with-border" data-id="{{$shoppergroup->id}}">
    <h3 class="box-title"> {{ $shoppergroup->name }}'s Users  </h3>
       <div class="box-tools pull-right">
       {{-- <button id="add-user" disabled="disabled" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#user-dialog" ><i class="fa fa-times-circle"></i> Add User</button> --}}

        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >

    <table id="user-list" class="table table-striped table-bordered">
    <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Action</th>
    </thead>
	</table>
</div>
<div class="box-footer" >
    &nbsp;
</div>

<script type="text/javascript">
var oTableUser;
$(document).ready(function(){
	
    oTableUser = $('#user-list').DataTable( {
    	"ajax":{
	            "url": '{{ URL::to("/admin/shop/shoppers/".$shoppergroup->id."/user") }}',
	            "dataSrc": function ( jsonData ) {
		            if ($('#shopper-entry > [data-id]').attr('data-id') != 1)
	            		$('#add-user').prop('disabled',false);
		            return jsonData.data;
	            }
	          },
        "columns": [
	        { "data": "name" },
	        { "data": "email" },
	        { "data": "id" , },
        ],
        "columnDefs": [
        	{
        		"render": function ( data, type, row ) {
                 	return '<button type="button" onclick="removeUser(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>';
               	},
               	"targets": 2
			},
        ],
        "fnDrawCallback": function( oSettings ) {
           
        }
    });
    
});
</script>
