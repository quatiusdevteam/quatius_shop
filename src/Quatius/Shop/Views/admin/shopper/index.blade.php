@extends('admin::curd.index') 
@section('heading')
<h1>
	Shopper Groups
	<small> Manage user shopper</small>
</h1>

@stop 
@section('title') Shopper Groups 
@stop 
@section('breadcrumb')
<ol class="breadcrumb">
	<li><a href="{!! trans_url('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
	<li class="active">Shopper Groups</li>
</ol>

@stop 
@section('entry') 
@stop 
@section('tools') 
@stop 
@section('content')
<div style="min-height:700px;">
	<div class="col-md-4">
		<table id="main-list" class="table table-striped table-bordered">
			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>assigns</th>
			</thead>
		</table>
	</div>
	<div class="col-md-8">
		<div class="box box-warning" id='shopper-entry'>
		</div>
	</div>
</div>

<div id="user-dialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Select User</h4>
			</div>
			<div class="modal-body">
				<table id="sel-user-list" class="table table-striped table-bordered" style="width:100%">
					<thead>
						<th>Name</th>
						<th>Email</th>
						<th>Select</th>
					</thead>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary"
				 onclick="submitAddUsers()" data-dismiss="modal">Apply</button>
			</div>
		</div>
	</div>
</div>

@stop 
@section('script')
<script type="text/javascript">
	var env;
var selected_users = [];
var allUsers = null;
var oUserTable = null;
var oTable;
$(document).ready(function(){

	$('#user-dialog').on('show.bs.modal', function (e) {
		selected_users = [];
		var options = {
		        "columns": [
		        { "data": "name" },
		        { "data": "email" },
		        { "data": "id" },],
				"columnDefs": [
		           	{
		           		"render": function ( data, type, row ) {
		           				if ($.inArray(data, selected_users) == -1)
		                    		return '<input type="checkbox" value="'+data+'" />';
		                    	else
		                    		return '<input type="checkbox" checked value="'+data+'" />';	
		                  	},
		                  	"targets": 2
		   			},
		   		],
		   		"fnDrawCallback": function( oSettings ) {
		   			$('#sel-user-list input').iCheck({
					    checkboxClass: 'icheckbox_square-blue',
					    radioClass: 'iradio_square-red',
					    //increaseArea: '20%' // optional
					});
					
					setTimeout(function(){
						$('#sel-user-list input').on('ifChanged', function(event){
							selectUser(event.target);
			   			});
			   		}, 500);
		        }
		    };
	    
		if(oUserTable != null)
			oUserTable.destroy();
	    
		if (allUsers == null)
		{
			options.ajax = {
		            "url": "{{ URL::to('/admin/shop/shopper/users') }}",
		            "dataSrc": function ( jsonData ) {
		            	allUsers = jsonData.data;
		            	
			            return jsonData.data;
		            }
		          };
			oUserTable = $('#sel-user-list').DataTable(options);
			
		}
		else
		{
			options.data = allUsers;
			oUserTable = $('#sel-user-list').DataTable(options);
			
		}
	});
	
    oTable = $('#main-list').DataTable( {
        "ajax": {
		            "url": '{{ URL::to('/admin/shop/shoppers') }}',
		            "dataSrc": function ( jsonData ) {
		            	//console.log(jsonData);
			            return jsonData.data;
		            }
		},
        "columns": [
        { "data": "id" },
        { "data": "name" },
        { "data": "assigns" },],
        "userLength": 50
    });
	
   $('#main-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#shopper-entry').load('{{URL::to('/admin/shop/shoppers')}}' + '/' + d.id);
    });
   
});

function selectUser(itm)
{
	var searchIndex = $.inArray(itm.value, selected_users);
	if ($(itm).prop("checked"))
	{
		if (searchIndex == -1)
			selected_users.push(itm.value);
	}
	else
	{
		if (searchIndex > -1)
			selected_users.splice(searchIndex, 1);
	}
}

var remReloading = null; 
function removeUser(itm, user_id)
{
	if (remReloading != null)
	{
		clearTimeout(remReloading);
		remReloading= null;
	}
	
	var d = $('#user-list').DataTable().row( $(itm).parents('tr') ).data();

	$(itm).parents('tr').remove();
	$.get('{{ URL::to("/admin/shop/shoppers") }}/'+$('#shopper-entry > [data-id]').attr('data-id')+'/remove-user/'+d.pivot.shopper_assignable_id, function (textResponse){
		remReloading = setTimeout(function(){$('#user-list').DataTable().ajax.reload();}, 1000);
	});
}

function submitAddUsers()
{
	$('#user-dialog').modal('hide');
	
	$.get('{{URL::to('/admin/shop/shoppers')}}' + '/' + $('#shopper-entry > [data-id]').attr('data-id') +'/add-users/'+selected_users.join(), function(response){
		if (response == 'true')
			$('#user-list').DataTable().ajax.reload();
	});
}

function getIDs(data)
{
	var ids = [];
	for( var i = 0; i < data.length; i++)
	{
		ids[i] = data[i].id;
	}	
	return ids;
}

</script>

@stop 
@section('style')
<style type="text/css">
	.box-body {
		min-height: 420px;
	}
</style>

@stop