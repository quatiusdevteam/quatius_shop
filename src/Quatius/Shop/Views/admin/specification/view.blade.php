<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">{{isset($heading)?$heading:"Specifications"}}</h3>
        <div class="box-tools pull-right">
        	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body spec-group" >
    	<?php
    	$specRepo = app('SpecificationHandler');
    	$specs = isset($specs)?$specs:$specRepo->groupValues($specRepo->getLinksFrom($model)->sortBy('id'));
    	
    	?>

		@include('Shop::admin.specification.partials.list', ['specs'=>$specs, 'readonly'=>true])
	</div>
</div>

<script type="text/javascript">
	


</script>