<?php 

$classname = isset($classname)?$classname:'spec-item';

$spec = isset($spec)?$spec:(object)[
    'id'=>"0",
    'name'=>"",
    'value'=>"",
];

$readonly = isset($readonly)?$readonly: false;
$allowDelete = isset($spec->deletable)?$spec->deletable: true;
?>
<tr class="{{$classname}}" data-id="{{$spec->id}}" data-value="{{$spec->value}}">
	<td>{!! $spec->search_by=='field' || $spec->search_by=='both'?'<i class="fa fa-search"></i>':''!!} {{$spec->name}}</td>
	<td>{!! $spec->search_by=='value' || $spec->search_by=='both'?'<i class="fa fa-search"></i>':''!!} {{$spec->label}}</td>
  <td style="width: 60px">
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
          <div class="btn-group btn-group-xs" role="group">
            <button type="button" class="btn btn-default" {{$readonly?'disabled':''}} {{$allowDelete?'':'disabled'}} onclick="removeSpecValue(this)"><i class="fa fa-trash"></i></button>
          </div>
          <div class="btn-group btn-group-xs" role="group">
            <button type="button" class="btn btn-default" {{$readonly?'disabled':''}} onclick="editSpec(this)"><i class="fa fa-edit"></i></button>
          </div>
        </div>
</tr>