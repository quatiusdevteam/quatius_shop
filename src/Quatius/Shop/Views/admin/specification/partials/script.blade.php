<?php 
$specRepo = app('SpecificationHandler');
$specList = $specRepo
->setPresenter(new Quatius\Shop\Repositories\Presenters\Admin\SpecListPresenter())
->getPublished();

$specList = (collect($specList['data'])->keyBy('id')->sortBy('id'));

$specMediaIcons = app('MediaHandler')->getLinkFrom(\Quatius\Shop\Models\Specification::class);
$specIcons = [];
foreach($specMediaIcons as $icon){
 	$specIcons[$icon->pivot->grouping] = ['media_id'=> $icon->id, "params"=> $icon->pivot->params, "view"=>view('Media::admin.partials.tile', ['media'=>$icon,"extras"=>"","onSelect"=>""])->render()];
}
?>
@script
<script type="text/javascript">
var specList = JSON.parse("{!!addslashes(json_encode($specList))!!}");

var iconList = JSON.parse("{!!addslashes(json_encode($specIcons))!!}");

$(document).ready(function(){
	$('.spec-select.unload').each(function(){
		createSelection(this);
	});
});

</script>
@endscript