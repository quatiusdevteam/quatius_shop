<?php
$readonly = isset($readonly)?$readonly:false;
$header = isset($header)?$header:true;
?>

<table class="spec-table table table-responsive table-striped table-bordered">
	<thead {!! $header?'':'style="display:none"'!!}>
		<tr><th>Name</th><th>Value</th><th style="width: 60px">Action</th></tr>	
	</thead>
	<tbody>
	
	@foreach($specs as $spec)
		@include('Shop::admin.specification.partials.item', ['spec'=>$spec])
	@endforeach
		
	</tbody>
</table>