<?php 
    $classname = strtolower(substr(get_class($model), strrpos(get_class($model),'\\')+1));
    $class_id = $model->getRouteKey();
?>
<div style="display: none">

    <div class="spec-setting-tmpl">
		<div class="input-group input-group-sm spec-detail">
          	<span class="input-group-addon">Search By</span>
          	<select name="search_by" onchange="updateSpecData(this)" class="search_by form-control">
          		<option value=''>none</option>
          		<option value='field'>Field</option>
          		<option value='value'>Values</option>
          		<option value='both'>Field and Values</option>
          	</select>
          </div>
        <div class="input-group input-group-sm spec-detail">
          	<span class="input-group-addon">Unit Type</span>
          	<select name="unit_type" onchange="updateSpecData(this)" class="unit_type form-control">
          		<option value=''>Text</option>
          		<option value='integer'>number</option>
          		<option value='boolean'>Yes/No</option>
          		<option value='sku'>Model</option>
          	</select>
        </div>
        <div class="input-group input-group-sm spec-detail">
          <span class="input-group-addon">Visible</span>
          <select name="params[visible]" onchange="updateSpecData(this)" class="unit_type form-control">
            <option value='1'>show</option>
            <option value='0'>hide</option>
          </select>
        </div>
        <div class="input-group input-group-sm spec-detail">
          <span class="input-group-addon">Order</span>
          <input type="number" name="ordering" onchange="updateSpecData(this)" class="ordering form-control"/>
        </div>
		<br>
        <div style="text-align:center; width:250px">
        <button data-title='Delete this specification?' data-placement="top" class='btn pull-left btn-warning btn-xs btn-spec-delete' type='button'> <i class='fa fa-trash'></i> </button>
        <button class='btn btn-default btn-xs ' type='button' onclick="$('.btn-spec-setting').popover('hide')"> Close </button>
        </div>
    </div>

    <table class="spec-template" style="display: none">
    	<tr class="spec-template-new"><td>
        	<input type="hidden" name="class_name" value="{{get_class($model)}}">
        	<input type="hidden" name="class_id" value="{{$class_id}}">
        	<input type="hidden" name="spec_id" value="0">
        	<input type="hidden" name="_method" value="POST">
        	<div class="input-group input-group-sm spec-detail">
              <input name="spec_name" style="width: 80%" class="spec-name form-control">
              <input name="unit_display" style="width: 20%" placeholder="Unit" class="spec-name form-control">
            </div>
        	</td><td>
        	<div class="input-group spec-item-value input-group-sm">
              <input name="label[]" style="width:100%" type="text" class="form-control">
              <div class="input-group-btn">
                <button type="button" class="btn btn-default" onclick="cloneSpecValue(this)"><span class="fa fa-plus"></span></button>
              </div>
            </div>
        	</td><td style="width: 60px"><button class="btn btn-sm btn-success" onclick="saveSpec(this)"><i class="fa fa-floppy-o"></i> Save</button></td>
        </tr>
        <tr class="spec-template-delete">
        <td>
        	<input type="hidden" name="class_name" value="{{get_class($model)}}">
        	<input type="hidden" name="class_id" value="{{$class_id}}">
        	<input type="hidden" name="_method" value="DELETE">
        	<input type="hidden" name="value" value="">
        </td>
        </tr>
    	<tr class="spec-template-value"><td>
        	<input type="hidden" name="class_name" value="{{get_class($model)}}">
        	<input type="hidden" name="class_id" value="{{$class_id}}">
        	<input type="hidden" name="_method" value="PUT">
        	<input type="hidden" name="spec_id" value="0">
        	
        	<div class="input-group input-group-sm spec-detail">
              <input name="spec_name" style="width: 80%" readonly onchange="updateSpecData(this)" class="spec-name form-control">
              <input name="unit_display" style="width: 20%" readonly placeholder="Unit" onchange="updateSpecData(this)" class="spec-name form-control">
              <div class="input-group-btn">
                <button type="button" class="btn btn-default btn-spec-view" onclick="$(this).parents('.input-group').addClass('spec-edit'); $(this).parents('.input-group').find('.spec-name').removeAttr('readonly')"><span class="fa fa-edit"></span></button>
                
                <button type="button" title="Specification Settings" class="btn btn-default btn-spec-edit btn-spec-setting" data-placement='top'><i class="fa fa-gear"></i></button>
    
                <button type="button" class="btn btn-default btn-spec-edit" onclick="$(this).parents('.input-group').removeClass('spec-edit');$(this).parents('.input-group').find('.spec-name').attr('readonly','readonly')"><span class="fa fa-remove"></span></button>
              </div>
            </div>
        	</td><td>
        	<div class="input-group spec-item-value input-group-sm">
              <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select <span class="caret"></span></button>
                <ul class="dropdown-menu opt-values">
                  
                </ul>
              </div>
              <input name="label[]" style="width:100%" type="text" class="form-control">
              <input name="media_id[]" type="hidden" value="0">
              <input name="params[]" type="hidden" value="{}">
              <div class="input-group-btn">
                <button type="button" class="btn btn-default btn-spec-value-media"><span class="fa fa-image"></span></button>
                <button type="button" class="btn btn-default" onclick="cloneSpecValue(this)"><span class="fa fa-plus"></span></button>
              </div>
            </div>
        	</td><td style="width: 60px">
        	<div class="btn-group btn-group-justified" role="group" aria-label="...">
              <div class="btn-group btn-group-xs" role="group">
                <button type="button" class="btn btn-default btn-spec-cancel"><i class="fa fa-close"></i></button>
              </div>
              <div class="btn-group btn-group-xs" role="group">
                <button type="button" class="btn btn-default btn-success" onclick="saveSpec(this)"><i class="fa fa-floppy-o"></i></button>
              </div>
            </div>
            	
        	</td>
        </tr>
    </table>
</div>

<select class="form-control spec-select unload" onclick="checkUnloadSpec(this)" onchange="selectSpec(this)">
	<option value=''>Add Specification</option>
</select>