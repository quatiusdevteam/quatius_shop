@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> Specification Management
@stop
@section('title')
<div class="btn-group" role="group" aria-label="...">
	<button type="button" class="btn btn-warning" onclick="$('#box-area').empty();"><i class="fa fa-angle-double-up" aria-hidden="true"></i></button>
	
	<button type="button" class="btn btn-default" onclick="reset(this);"><i class="fa fa-refresh" aria-hidden="true"></i></button>
</div>
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Import Specification CSV</li>
</ol>
@stop
@section('entry')
<div id="box-area">
</div>
@stop

@section('tools')

@stop

@section('content')
<h1>Upload Specification of TVs</h1>

{!!Form::vertical_open_for_files()->action(url('admin/shop/specification/import/tv'))->id('spec-import-form')!!}
{!!Form::file('spec-file')->attributes(['id' => 'spec-file', 'accept'=>'.csv', 'onchange'=>"$(this).parents('form').find('[type=submit]').removeAttr('disabled');"])!!}

{!!Form::submit('Upload Specs')->attributes(['disabled'=>'disabled','data-loading-text'=>"Sending..."])->class('btn btn-primary')!!}
{!!Form::close()!!}


<div id='output-form'>


</div>
@stop

@section('script')

<script src="{{url('js/jquery.form.js')}}"></script>
<script src="{{url('js/shop/shop-product-admin.js')}}"></script>

<script type="text/javascript">

var shopBaseUrl = '{{URL::to("admin/shop")}}/';

$(document).ready(function(){
    $('#spec-import-form').on('submit', function(e) {
            e.preventDefault(); // <-- important
            $(this).ajaxSubmit({
                target: '#output-form',
                beforeSubmit:function(a, itm, options){
                    $('#spec-import-form [type="submit"]').button('loading');
                    return true;
                },
                complete:function(xhr, status){
                    //console.log(xhr.responseJSON);
                    $('#spec-import-form [type="submit"]').button('reset');
                    $('#spec-import-form [type="submit"]').attr('disabled','disabled');
                },
                error:function(xhr, status, error){
                    console.log(error);
                }

            });
        });
});

</script>
@stop

@section('style')
@stop
