<div class="row product" data-id="{{$product->getRouteKey()}}">
	<div class="col-lg-6" id='entry-left'>
		{!!Theme::place('entry-left')!!}
	</div>
	<div class="col-lg-6" id='entry-right'>
		{!!Theme::place('entry-right')!!}
	</div>
</div>