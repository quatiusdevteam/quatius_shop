<div class="box-header with-border">
	    <h3 class="box-title"> {{ trans('cms.edit') }} [{!!$product->description!!}] </h3>
	    <div class="box-tools pull-right">
	        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-product'  data-load-to='#entry-product' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
	        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-product' data-href='{{url("admin/product/".$product->getRouteKey())}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
	        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	    </div>
	</div>
	<div class="box-body" >
	    <div class="nav-tabs-custom">
	        <!-- Nav tabs -->
	        <ul class="nav nav-tabs primary" id="product-tabs">
			  <li role="presentation"><a href="#product-basic" data-toggle="tab">Basic</a></li>
			  <li role="presentation"><a href="#product-price" data-toggle="tab">Price</a></li>
			  <li role="presentation"><a href="#product-size" data-toggle="tab">Size</a></li>
			  <!--<li role="presentation"><a href="#product-status" data-toggle="tab">Status</a></li>
			  <li role="presentation"><a href="#product-specification" data-toggle="tab">Specification</a></li>
			  <li role="presentation"><a href="#product-feature" data-toggle="tab">Feature</a></li>
			   <li role="presentation"><a href="#product-tag" data-toggle="tab">Tag</a></li>
			  <li role="presentation"><a href="#product-media" data-toggle="tab">Media</a></li> -->
			</ul>
			
	         {!!Form::vertical_open()
	        ->id('edit-product')
	        ->method('PUT')
	        ->enctype('multipart/form-data')
	        ->action(url('admin/shop/product/'. $product->id))!!}
	        	
	        {!!Form::close()!!}
	        
	        
	    </div>
	</div>
	<div class="box-footer" >
	    &nbsp;
	</div>
	
	