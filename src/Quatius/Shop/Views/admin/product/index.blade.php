@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> Products Management
@stop
@section('title')
<div class="btn-group" role="group" aria-label="...">
	<button type="button" class="btn btn-warning" onclick="$('#box-area').empty();"><i class="fa fa-angle-double-up" aria-hidden="true"></i></button>
	
	<button type="button" class="btn btn-default" onclick="reset(this);"><i class="fa fa-refresh" aria-hidden="true"></i></button>
</div>
<span id="product_filter">
    
    @if(config('quatius.shop.admin.product.mode') != 'basic')
    <div class="btn-group" role="group" aria-label="...">
      <button type="button" class="btn btn-default" onclick="setStatusFilter(this)" data-filter_status="2"><i class="icon-published" aria-hidden="true"></i></button>
      <button type="button" class="btn btn-default" onclick="setStatusFilter(this)" data-filter_status="1"><i class="icon-unpublish" aria-hidden="true"></i></button>
      <button type="button" class="btn btn-default" onclick="setStatusFilter(this)" data-filter_status="3"><i class="icon-configure" aria-hidden="true"></i></button>
    </div>

    <div class="btn-group" role="group" aria-label="...">
      <button type="button" class="btn btn-warning" onclick="getTrashProds(this)"><i class="fa fa-trash" aria-hidden="true"></i></button>
    </div>

    <div class="btn-group" role="group" aria-label="...">
      <button type="button" class="btn btn-default" onclick="setGroupFilter(this)" data-filter_group="unique"><i class="fa fa-cubes" aria-hidden="true"></i> Grouped Only</button>
    </div>
    
    @else

    <div class="btn-group" role="group" aria-label="...">
      <button type="button" class="btn btn-default" onclick="setStatusFilter(this)" data-filter_status="2"><i class="icon-published" aria-hidden="true"></i></button>
      <button type="button" class="btn btn-default" onclick="setStatusFilter(this)" data-filter_status="1"><i class="icon-unpublish" aria-hidden="true"></i></button>
    </div>

    @endif
    <div class="btn-group" role="group" aria-label="...">
    @php 
      $categories = app('CategoryHandler')->getFromRoot(0,['name'], null, user('admin.web')->isSuperuser()?null:[])->toTree();
            
		$options = app('CategoryHandler')->getSelectionOptions(null, $categories, $prefix="- ", " &nbsp;", false, 0, true);
		$newOptions = ['0'=> 'All'];
		foreach($options as $key => $value){
			$newOptions["$key,"] = $value;
		}
    @endphp
    {!!Form::select('filter-category', '')->attributes(["class"=>"form-control","onchange"=>"oTable.draw()"])->options($newOptions)
    	
     !!}
    </div>
</span>
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Edit Product</li>
</ol>
@stop
@section('entry')
<div id="box-area">
</div>
@stop

@section('tools')
<h4>
  @if(config('quatius.shop.admin.product.mode') == 'basic')
  <button tabindex="0" class="label label-warning filter-role" role="button" onclick="$('#box-area').load(getProdUrl('create'))" data-toggle="manual"><i class="fa fa-star"></i> Create Product</button>
  @else
	@if(user('admin.web')->isSuperuser())
	<a tabindex="0" class="label label-warning filter-role" role="button" data-toggle="manual" data-trigger="click" onclick="$(this).popover('show')" data-placement='left' title="New Product Detail" data-html="true" data-content="<?php 
echo <<<EOF
<form onsubmit='return newProduct(this);'>

    <input name='name' required='required' type='text' class='form-control' placeholder='Name'>
    <input name='sku' type='text' required='required' class='form-control' placeholder='SKU'>
    <input name='price' type='text' required='required' class='form-control' placeholder='Price'>
    <div class='text-center'>
    <button class='btn btn-success btn-xs' type='submit' onclick=''> Submit </button>
    
</form>
EOF;
?>"><i class="fa fa-star"></i> Create Product</a>
    @endif	
    
	@endif	
	<a href='#' class="label label-primary filter-role" onclick="oTable.ajax.reload()" data-role=''>Reload</a>
</h4>
@stop

@section('content')

<table id="main-list" class="table table-responsive table-striped table-bordered">

</table>

<div id="changeCategory" class="modal fade product" tabindex="-1" data-id="" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Change Catgory</h4>
      </div>
      <div class="modal-body">
          <div id="category-tree">
        	@include("Shop::admin.category.partials.tree",['categories'=>$categories, 'assignedIds'=>collect([])])
          </div>  
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-success btn-sm" onclick="createCategory(this)"> <i class="fa fa-star"></i> New Category</button>
		
        <button type="button" class="btn btn-default  btn-sm" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>

    	

@stop

@section('script')

@include('Media::admin.partials.selection-script')
@includeStyle('css/shop/shop-product-admin.css')

<script src="{{url('js/shop/shop-product-admin.js')}}"></script>

<script type="text/javascript">
summernoteMediaPlugin('.html-editor');

var shopBaseUrl = '{{URL::to("admin/shop")}}/';
var dataSets = []; 

var oTable;
//$('#entry-user').load('{{URL::to('admin/user/user/0')}}');
$(document).ready(function(){
    oTable = $('#main-list').DataTable(configTable());
    oTable.on( 'xhr', function () {
    	dataSets = oTable.ajax.json();
    });

    addFilteringStatus('main-list');
    addFilteringGroup('main-list');
    addFilteringCategroy('main-list');
    
    $('#main-list tbody').on( 'click', 'td', function () {
    	onSelectProductRow(this);
    });
});

@if(config('quatius.shop.admin.product.mode') == 'basic')
  function removeImage(itm){
    $(itm).parents('.media-tile').css('opacity','0.5');
    $(itm).prop('disabled',true);
    $(itm).parents('.media-tile').remove();
  }

  function completedSelect(itm, addedbutton, mediaSelect){
    $(itm).find('input.ordering').val($(itm).parents('.media-selection-area').attr('data-size'));
    replaceRemoveButton(itm.find('.remove-indicator'));
    mediaSelect.css("opacity", "0.5");
    return true;
  }

  function completedDrag(itm){

    
  }

  function updateCategory(itm){
    if (disableCategoryPost || $(itm).parents('.modal').length == 0)
		return;
		
    var inputs =  $(itm).parents('.category-list').find('input:checked');

    $.post(getProdUrl("/assign/categories", itm), 
      inputs.serialize(),
      function(result){
        var prodId = $(itm).parents('.product[data-id]').attr('data-id');
        updateTableData(prodId, 'category_ids', result.category_ids);
      
    },'json');
  }
@endif
</script>
@stop

@section('style')
@stop
