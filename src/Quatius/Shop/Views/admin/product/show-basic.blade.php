<div class="row product" data-id="{{$product->getRouteKey()}}">
	{!!Form::vertical_open()
		->id('create-product')
		->method('POST')
		->files('true')
		->class('form-compact')
		->enctype('multipart/form-data')
		->action(url('/').'/admin/shop/product/create'.($product->exists?"/".$product->getRouteKey():""))
		!!}
		
		{!!Form::token()!!}
	<div class="col-lg-6" id='entry-left'>
		{!!Theme::place('entry-left')!!}
	</div>
	<div class="col-lg-6" id='entry-right'>
		{!!Theme::place('entry-right')!!}
	</div>
	{!!Form::close()!!}
</div>