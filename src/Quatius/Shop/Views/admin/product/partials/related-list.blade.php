<?php 

$classname = strtolower(substr(get_class($product), strrpos(get_class($product),'\\')+1));
$class_id = $product->getRouteKey();

?>

<div class="box {{$product->deleted_at?'box-default':'box-warning'}}">
<div class="box-header with-border">
    <h3 class="box-title">Select a Releated Product</h3>
    <div class="box-tools pull-right">
    	<button type="button" class="btn btn-info btn-sm btn-restore" {!!(!$product->deleted_at)?'disabled="disabled" style="display:none"':''!!} onclick="restoreProd('{{$product->getRouteKey()}}')" ><i class="fa fa fa-hospital-o"></i> Restore</button>
        <button type="button" class="btn btn-danger btn-sm btn-delete" {!!$product->deleted_at?'disabled="disabled" style="display:none"':''!!} onclick="deleteProd('{{$product->getRouteKey()}}')" ><i class="fa fa-trash"></i> Delete</button>
        
        <button type="button" class="btn btn-warning btn-sm" {!!$product->deleted_at?'disabled="disabled"':''!!} onclick="showReferenceLink('{{$product->getRouteKey()}}')" ><i class="fa fa-times-circle"></i> Reference Of Another</button>
        <button type="button" class="btn btn-success btn-sm" {!!$product->deleted_at?'disabled="disabled"':''!!} onclick="$('#product-list tbody tr').removeClass('info');" data-action='NEW' data-load-to='#entry-right' data-href='{{trans_url("admin/shop/".$classname."/".$class_id)}}/create'><i class="fa fa-times-circle"></i> Create New Product</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>

<div class="box-body" >
	<table id="product-list" class="table table-responsive table-striped table-bordered">
    </table>
</div>
</div>
<script type="text/javascript">
var oProd;
var releateOptions = {
        "ajax": '{{ URL::to('/admin/shop/product/groups/publishable') }}',
        "columns": [
        { title: "Name", "data": "name" },
        /* { title: "Description", "data": "description" }, */
        { title: "SKU", "data": "skus" },
        //{ title: "Status", "data": "status" },
        ],
        "columnDefs": [
        	{
        		"targets": [0],
        		"createdCell": function (td, cellData, rowData, row, col) {
            		
        			$(td).addClass('text-center');
        		}
        	}/* ,
        	{
            	"render": function ( data, type, row ) {
						switch(data){
						case 1:
							return '<i style="color:red; font-size: 18px"  class="fa fa-times-circle-o" aria-hidden="true"></i>';
						case 2:
							return '<i style="color:green; font-size: 18px" class="fa fa-check-circle-o" aria-hidden="true"></i>';
						case 3:
							return '<i style="color:orange; font-size: 18px"  class="fa fa-exclamation-triangle" aria-hidden="true"></i>';
							
						}
                },"targets": 2
            }, */
            //{ "visible": false,  "targets": [0] }
        ]
    };


$(document).ready(function(){
    setTimeout(function(){
        oProd = $('#product-list').DataTable(releateOptions);

        $('#product-list tbody').on( 'click', 'tr', function () {
            if ($('#entry-left .box.box-default').length) return;
            
            $(this).toggleClass("info").siblings(".info").removeClass("info");
            var d = $('#product-list').DataTable().row( this ).data();
            $('#entry-right').load('{{URL::to("admin/shop/".$classname."/".$class_id)}}' + '/preview/' + d.id);
        });
    },500);
	
});
</script>