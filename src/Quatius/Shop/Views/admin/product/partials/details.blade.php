<div class="box box-primary product {{$product->status == 2?'status-published': 'status-unpublish'}}" data-id="{{$product->getRouteKey()}}">
<div class="box-header with-border">
    <h3 class="box-title">Product({{$product->sku}}) - {{$product->description}} </h3>
    <div class="box-tools pull-right">
		<a href="{{url($product->url)}}" class="show-on-published prod-preview btn btn-primary btn-sm" target="prod-preview">View</a>
    	<button class="show-on-published btn btn-default btn-sm" onclick="updateStatus(this,'1')"><i class="icon-published" aria-hidden="true"></i> Published</button>
		<button class="show-on-unpublish btn btn-default btn-sm" onclick="updateStatus(this,'2')"><i class="icon-unpublish" aria-hidden="true"></i> Unpublish</button>
    	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >

@if ($product->type == 'reference')
	<h4 class="text-danger"><i>This is reference product of (<button class="btn btn-link text-danger" onclick="$('#box-area').load(getProdUrl('{{$product->getProductReference()->getRouteKey()}}'));">{{$product->getProductReference()->sku}}</button>)</i></h4>
@endif

	<div class="nav nav-tabs-custom">
      <ul class="nav nav-tabs primary" role="tablist">
        <li role="presentation" class="active"><a href="#prod-image" aria-controls="prod-image" role="tab" data-toggle="tab">Images</a></li>
        <li role="presentation"><a href="#prod-specifications" aria-controls="#prod-specifications" role="tab" data-toggle="tab">Specifications</a></li>
        <li role="presentation"><a href="#category" aria-controls="category" role="tab" data-toggle="tab">Category</a></li>
      	<li role="presentation"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail</a></li>
      </ul>
    
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="prod-image">
        	@if ($product->type == 'reference')
        		@include('Media::admin.partials.selection-list',['medias'=>$product->getMedias(),'extras'=>"",'onSelect'=>"",'input_prefix'=>"",'input_prefix'=>"", 'list_only'=>true])
        		<h4>Supports/Manuals</h4>
        		@include('Media::admin.partials.selection-list',['medias'=>$product->getMedias('support'),'grouping'=>'support', 'extras'=>"",'onSelect'=>"",'input_prefix'=>"",'input_prefix'=>"", 'list_only'=>true])
            	
        	@else
            	@include('Media::admin.selection-dialog',['medias'=>$product->getMedias(),'mime_types' => 'image/png,image/jpeg,image/gif' ,'path'=>$product->getParams('files.base_path','products/').$product->sku, 'attributes'=>['data-multi-select'=>'true']])
            	
            	<h4>Supports/Manuals</h4>
            	@include('Media::admin.selection-dialog',['medias'=>$product->getMedias('support'),'mime_types' => '' ,'grouping'=>'support','path'=>$product->getParams('files.base_path','products/').$product->sku, 'attributes'=>['data-multi-select'=>'true']])
            	
            @endif	
		</div>
        <div role="tabpanel" class="tab-pane" id="prod-specifications">
    		<?php 
    		$cateRepo = app('CategoryHandler');
            $cateParents = $cateRepo->getParentCategories($product->categories->pluck('id')->toArray());
           
            $extraSpecs = $cateRepo->getSpecs($cateParents);
            $specRepo = app('SpecificationHandler');
            
            $specCate = [];
            foreach ($specRepo->groupValues($extraSpecs->sortBy('id')) as $checkSpec){
                if ($checkSpec->label == ''){
                    $checkSpec->deletable = false;
                    $specCate[] = $checkSpec;
                }
            }
            
            $specCatePage = merge_collect(collect($specCate), $specRepo->groupValues($specRepo->getLinksFrom($product->page)->sortBy('id')));
            
            ?>
        	
        	@if ($product->type == 'reference')
        		@include('Shop::admin.specification.view',['heading'=>$product->getProductReference()->sku.' - Referece Includes','model'=>$product->getProductReference(), 'specs'=>$product->getProductReference()->specifications()])
        	@else
        		@include('Shop::admin.category.partials.edit-spec',['categories'=>$cateParents])
        		@include('Shop::admin.specification.edit',['heading'=>'Common Specification','model'=>$product->page, 'specs'=>$specCatePage])
        	@endif	
        	@include('Shop::admin.specification.edit',['heading'=>'Product Specification','model'=>$product])
        	@include('Shop::admin.specification.partials.script')
		</div>
        <div role="tabpanel" class="tab-pane" id="category" data-selected='{!!json_encode(isset($assignedIds)?$assignedIds:[])!!}'>
			@if ($product->type == 'reference')
				<h4 class="text-info">Category is link to referance a product</h4>	
        	@else
                <button type="button" class="btn btn-success btn-sm" onclick="createCategory(this)" style="position: absolute;right: 25px;"> <i class="fa fa-star"></i> New Category</button>
        		@php
            		$categories = app('CategoryHandler')->getFromRoot(0,['name'], null)->toTree();
            	@endphp
            	
            	<div id="category-tree">
                	@include("Shop::admin.category.partials.tree",['categories'=>$categories, 'assignedIds'=>$product->categories->pluck('id')])
                </div>
            @endif	
		</div>
		<div role="tabpanel" class="tab-pane" id="detail">
			
			<div class="input-group">
              <span class="input-group-addon">Product Name</span>
              <input onchange="updateProduct(this,'description')" type="text" class="form-control" value="{{$product->description}}" id="product-name">
            </div>

			<br>
			<h3>Varient Params <button class="btn btn-xs btn-primary" type="button" onclick="loadExtra()">Add</button></h3>
            @foreach($product->getParams('cart.extras',[]) as $field => $valObj)

			<div class="input-group">
              <span class="input-group-addon">{{$product->getParams('cart.extras.'.$field.'.name', $field)}}</span>
              <input type="text" class="form-control" readonly onclick="loadExtra($(this))" value="">
			  
				{!!Form::hidden('setparams')->value(json_encode(['cart.extras.'.$field=>$valObj]))!!}
            </div>


			@endforeach

            @if($product->type != 'generic')
                <div class="col-xs-12 text-center">
                	<br>
                	<button class="btn-warning btn" onclick="removeType(this, '{{$product->type}}')">Remove {{$product->type}}</button>
                	<br><br>
                </div>
            @endif
		</div>
      </div>
	</div>
	@script
	<script type="text/javascript">
		function removeType(itm, type){
			swal({
	            title: "Are you sure?",
	            text: "You want to reset this "+type +" to normal product?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Yes, do it!",
	            closeOnConfirm: true
	        	}, 
	        	function(action){
					if (action){
						var prodId = $(itm).parents('.product[data-id]').attr('data-id');
						
						setProductType(itm, 'generic', {});

						var data = searchData(dataSets.data, 'id', prodId);

						if (data){
    						$('#main-list_filter input').val(data.sku);
    						$('#main-list_filter input').trigger('keyup');
    
    						if ($('#main-list tbody td.dataTables_empty').length == 1){
    							$('#product_filter .active').removeClass('active');
    							$('#main-list_filter input').trigger('keyup');
    						}
						}
					}
		        }
	        );
		}
			
		$(document).ready(function(){

			$('#prod-image .media-tile .remove-indicator').each(function(){
				replaceRemoveButton(this);
			});
		});

		var prefix_field = "cart.extras";
		
		var JSParams = null;
		var codeM = null;
		function loadExtra(itm){
			$("#varientDialog .param-field").val("");
			$('#varientDialog [name="enable"]').prop("checked", true);
			$('#varientDialog [name="enable"]').iCheck('update');
			var field = "";
			if (codeM != null){
				codeM.toTextArea();
				codeM= null;
			}
			codeM = CodeMirror.fromTextArea($("#varientDialog .code-editor").get(0), {
				lineNumbers: true,
				matchBrackets: true,
				htmlMode: true,
				mode: "application/x-httpd-php",
				indentUnit: 4,
				indentWithTabs: true,
				theme: "eclipse"
				});
			codeM.on("blur", (cm, evt) => {
				cm.save();
			});
			
			$('#varientDialog').modal('show');
			if (itm == null){
				codeM.setValue($('#varientDialog [name="blade_default"]').val());
				return;
			}
			var params = $(itm).next().val();
			if (params != ""){
				JSParams = $.parseJSON(params);

				$.each(JSParams, (index, val)=>{
					field = index.substr(index.lastIndexOf(".")+1);
					$('#varientDialog [name="field"]').val(field);
					//prefix_field = index.substr(0,index.lastIndexOf("."));

					$.each(val, (f, v )=>{
						if ($('#varientDialog [name="'+f+'"]').length){
							if (f == "enable"){
								$('#varientDialog [name="'+f+'"]').prop("checked", v);
								$('#varientDialog [name="enable"]').iCheck('update');
							}
							else if (f == "blade_script"){
								codeM.setValue(v);
							}else{
								$('#varientDialog [name="'+f+'"]').val(v);
							}
						}
					});
				});
			}
		}
		function deleteExtra(itm){
			var tempObj = {
				
			};
			tempObj[prefix_field+"."+$('#varientDialog [name="field"]').val()] = "";
			$('#dialog-extra').val(JSON.stringify(tempObj));
			updateProduct($('#dialog-extra').get(0),'setparam');
		}
		function updateExtra(itm){
			var tempObj = {
				
			};
			codeM.save();
			var tempObjValue = {
				label:$('#varientDialog [name="label"]').val(),
				value:$('#varientDialog [name="value"]').val(),
				info:$('#varientDialog [name="info"]').val(),
				blade_script:$('#varientDialog [name="blade_script"]').val(),
			};

			if (!$('#varientDialog [name="enable"]').prop("checked"))
				tempObjValue["enable"] = false;
				
			tempObj[prefix_field+"."+$('#varientDialog [name="field"]').val()] = tempObjValue;
			$('#dialog-extra').val(JSON.stringify(tempObj));
			updateProduct($('#dialog-extra').get(0),'setparam');
		}
    </script>
    @endscript
</div>
</div>
<div id="varientDialog" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Cart Varient</h4>
      </div>
      <div class="modal-body">


		{!! Form::text('field')->addClass('param-field') !!}
		{!! Form::text('label')->addClass('param-field') !!}
		{!! Form::text('value','Value (default)')->addClass('param-field') !!}
		
		{!! Form::text('info')->addClass('param-field') !!}

		{!! Form::textarea('blade_script')->addClass('code-editor')->addClass('param-field') !!}

		

		<div class="checkbox"><input class="param-check" type="checkbox" name="enable" value="1"> <Label for="checkbox-enable">Enable</Label></div>
		
		<?php		  
$defaultBlade = <<< EOF
<div class="input-group input-cart-extra">
	<span class="input-group-addon">{{\$label}}
	</span>
	{!! Form::text(\$name,"")->required()->value(\$value) !!}
</div>
<p class="help-block">{!! \$info !!}</p>
EOF;
?>
		<div class="hide">
		{!! Form::textarea('dialog-extra') !!}
		{!! Form::textarea('blade_default')->value($defaultBlade) !!}
		</div>
      </div>
      <div class="modal-footer">
	  	<button type="button" class="btn btn-warning" onclick="deleteExtra(this)" data-dismiss="modal">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="updateExtra(this)" data-dismiss="modal">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->