<style>
.child-list .prod.active{
    /*background-color: lightblue*/
}

</style>
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><strong>Childs</strong></h3>
        <div class="box-tools pull-right">
        	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" >
    	<table class="table child-list">
    		<thead>
    			<tr><th style="width: 50px">Action</th><th>SKU</th><th>Description</th><th class="hide">Panel Size <i style="cursor: pointer" class="fa fa-trash"> </i></th></tr>
    		</thead>
    		<tbody>
                <?php 
                    $products = app('ProductHandler')->findWhere(['page_id'=>$page->id]);
                    $productGroups = [];
                    $refList=[];
                    
                    foreach ($products as $item){
                        if ($item->type == 'reference'){
                            $refList[$item->getParams('reference_to')][$item->id] = $item;
                        }
                        else{
                            $productGroups[$item->id] = $item;
                        }
                    }
                    
                ?>
    			@foreach($productGroups as $prod)
    			
    			<tr class="child-row product {{$product->id == $prod->id?'info':''}}" style="cursor: pointer" data-id="{{$prod->getRouteKey()}}" class="prod {{$product->id==$prod->id?'info':''}}"><td style="width: 50px">

<button type="button" class="btn-detaching btn btn-xs btn-warning" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement='top' title="Remove this item from group?" data-html="true" data-content="<?php 
echo <<<EOF
<div class='text-center'>
<button class='btn btn-warning' type='button' onclick='removeProductFromPage(this)'> Yes </button>
<button class='btn btn-default' type='button'> No </button>
</div>
EOF;
?>"><i class="fa fa-chain-broken"></i></button>
    			</td>
    			
    			<td onclick="selectChild(this)" style="width: 160px;">{{$prod->sku}}</td><td onclick="selectChild(this)" >{{$prod->description}}</td><td class="hide"><select><option>45 "</option><option>55 "</option><option>65 "</option></select></td></tr>
        			@if(isset($refList[$prod->id]))
        			<tr>
        			<td colspan="3" style="padding-top: 0px">
        			<table class="table">
            			<tbody>
                			@foreach($refList[$prod->id] as $prodRef)
            					<tr class="child-row product {{$product->id == $prodRef->id?'info':''}}" style="cursor: pointer" data-id="{{$prodRef->getRouteKey()}}" class="prod {{$product->id==$prodRef->id?'info':''}}">
            						<td style="width: 65px; text-align:right"> - </td>
                        			
                        			<td onclick="selectChild(this)" style="width: 160px;">{{$prodRef->sku}}</td><td onclick="selectChild(this)" >{{$prodRef->description}}</td><td class="hide"><select><option>45 "</option><option>55 "</option><option>65 "</option></select></td></tr>
                            		
            				@endforeach
            			</tbody>
        			</table>
        			</td>
        			</tr>
        			
        				
        			@endif
    			@endforeach
    			
    		</tbody>
    	</table>
    </div>
    <div class="box-footer hide">
    	<div class="box-tools pull-right">
        	<button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-page' data-href=''><i class="fa fa-floppy-o"></i> Apply </button>
       	</div>
    </div>
</div>

@script
<script type="text/javascript">

	$(document).ready(function(){
		$('.btn-detaching').popover();
	});

	function selectChild(itm){
		var jParent = $(itm).parents('.child-row.product');
		
		$('#entry-right').load('{{URL::to('admin/shop/product')}}' + '/' + jParent.attr('data-id')+'/detail',function(){
			$('.child-list .product.info').removeClass('info');
			jParent.addClass('info');
		});
		
	}
</script>
@endscript