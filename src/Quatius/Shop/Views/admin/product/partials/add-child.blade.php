<?php 

$classname = strtolower(substr(get_class($model), strrpos(get_class($model),'\\')+1));
$class_id = $model->getRouteKey();

?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title"><strong>{{$page->id?$page->name:""}}</strong></h3>
        <div class="box-tools pull-right">
        	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" >
        <div class="panel panel-default">
  			<div class="panel-body">
        	{!! Form::textarea('content')
                    -> label(trans('page::page.label.content'))
                    -> value(e($page['content']))
                    -> disabled(true)!!}
         	</div>
		</div>
        <div class="panel panel-primary">
        	<div class="panel-heading">
        	Childs
        	</div>
  			<div class="panel-body">
            	<table class="table table-striped">
            		<thead>
            			<tr><th>SKU</th><th>Description</th></tr>
            		</thead>
            		<tbody>
            			@foreach(app('ProductHandler')->findWhere(['page_id'=>$page->id]) as $prod)
            			
            			<tr><td>{{$prod->sku}}</td><td>{{$prod->description}}</td></tr>
            			
            			@endforeach
            			
            		</tbody>
            		<tfoot>
            			<tr style="background-color: #337ab7; color: white"><td>{{$product->sku}}</td><td>{{$product->description}}</td></tr>
            		</<tfoot>
            	</table>
            </div>
        </div>
    </div>
    <div class="box-footer" >
    	<div class="box-tools pull-right">
        	<button type="button" class="btn btn-success btn-sm" onclick="assignProductToPage(this,'{{$page->getRouteKey()}}', '#box-area')"><i class="fa fa-floppy-o"></i> Apply </button>
       	</div>
    </div>
</div>