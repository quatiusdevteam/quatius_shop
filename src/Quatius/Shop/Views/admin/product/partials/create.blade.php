<?php 

$classname = strtolower(substr(get_class($product), strrpos(get_class($product),'\\')+1));
$class_id = $product->getRouteKey();

?>

<div class="box box-warning">
<div class="box-header with-border">
    <h3 class="box-title">{{$product->description}} ({{$product->sku}})</h3>
    <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
{!!Form::vertical_open()
    ->id('create-page')
    ->method('POST')
    ->files('true')
    ->class('form-compact')
    ->enctype('multipart/form-data')
    ->action(Trans::to('admin/shop/product/'.$product->getRouteKey().'/page'))!!}
    
	{!!Form::token()!!}
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">{{ trans('page::page.tab.page') }}</a></li>
            <li><a href="#metatags" data-toggle="tab">{{ trans('page::page.tab.meta') }}</a></li>
            <li class="hide"><a href="#settings" data-toggle="tab">{{ trans('page::page.tab.setting') }}</a></li>
        </ul>
        @include('Shop::admin.page.partials.entry')
    </div>
    {{--
    	Form::multiselect('category[]')
                ->required()
                ->options(app('CategoryHandler')->getSelectionOptions())--}}
</div>

	{!!Form::close()!!}
<div class="box-footer" >
	<div class="box-tools pull-right">
		<button type="button" class="btn btn-primary btn-sm" onclick="updateTableData('{{$product->getRouteKey()}}', 'status', 1);" data-action='CREATE' data-form='#create-page'  data-load-to='#box-area'><i class="fa fa-floppy-o"></i> Save </button>
    </div>
</div>
</div>