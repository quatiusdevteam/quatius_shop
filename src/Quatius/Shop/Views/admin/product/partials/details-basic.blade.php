<div class="box box-primary product {{$product->status == 2?'status-published': 'status-unpublish'}}" data-id="{{$product->getRouteKey()}}">
<div class="box-header with-border">
    <h3 class="box-title">Settings </h3>
    <div class="box-tools pull-right">
		@if($product->exists && $product->status==2)
			<a href="{{url($product->url)}}" class="show-on-published prod-preview btn btn-primary btn-sm" target="prod-preview"><i class="fa fa-eye"></i> View</a>
		@else
			<button type="button" disabled class="show-on-unpublish prod-preview btn btn-primary btn-sm" title="Published First"><i class="fa fa-eye"></i> View</button>
		@endif
		<button type="button" class="btn btn-success btn-sm" data-action="UPDATE" data-form="#create-product" data-load-to="#box-area" data-datatable="#main-list"><i class="fa fa-floppy-o"></i> Save</button>
		{{-- <button class="btn btn-default btn-sm" onclick="updateStatus(this,'2')"><i class="icon-unpublish" aria-hidden="true"></i> Unpublish</button> 
		<button class="show-on-published btn btn-default btn-sm" onclick="updateStatus(this,'1')"><i class="icon-published" aria-hidden="true"></i> Published</button>
		<button class="show-on-unpublish btn btn-default btn-sm" onclick="updateStatus(this,'2')"><i class="icon-unpublish" aria-hidden="true"></i> Unpublish</button> --}}
    	<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" style="position: relative;">
	<label class="form-group" style="font-size: 16px; position: absolute; top: 8px; right: 40px;"><input type="checkbox" class="form-control" value="2" {{ $product->status==2?'checked="checked"':'' }} id="product-publish" name="product-publish"> <b>Published</b></label>
					
	<div class="nav nav-tabs-custom">
      <ul class="nav nav-tabs primary" role="tablist">
        <li role="presentation" class="active"><a href="#prod-image" aria-controls="prod-image" role="tab" data-toggle="tab">Info</a></li>
        <li role="presentation"><a href="#category" aria-controls="category" role="tab" data-toggle="tab">Category</a></li>
      	{{-- <li role="presentation"><a href="#price-stock" aria-controls="detail" role="tab" data-toggle="tab">Price/Stock</a></li> --}}
      </ul>
    
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="prod-image">
			<div class="row">
				<div class="col-sm-6">
					
					<div class="form-group input-group">
						<span class="input-group-addon"><b>Stock Available</b></span>
						<input type="number" class="form-control" value="{{ $product->stock }}" id="product-stock" name="product-stock">
					</div>
					
					<div class="input-group">
						<span class="input-group-addon"><b>Price</b></span>
						<input type="text" class="form-control" value="{{ number_format($product->priceRrp, 2) }}" id="product-price" name="price">
					</div>

					<div class="input-group">
						<span class="input-group-addon"><b>Discount Price</b></span>
						<input type="text" class="form-control" value="{{ number_format($product->priceRrp == $product->price?0:$product->price, 2)}}" id="discount-price" name="discount-price">
					</div>
				</div>
				<div class="col-sm-6">
					<label class="control-label">Images</label>
           			 @include('Media::admin.selection-dialog',['medias'=>$product->getMedias(),'mime_types' => 'image/png,image/jpeg,image/gif' ,'path'=>$product->getParams('files.base_path','products/').$product->sku, 'attributes'=>['data-multi-select'=>'true']])
				</div>
			</div>
			
		</div>
        <div role="tabpanel" class="tab-pane" id="category" data-selected='{!!json_encode(isset($assignedIds)?$assignedIds:[])!!}'>
			<button type="button" class="btn btn-success btn-sm" onclick="createCategory(this)" style="position: absolute;right: 25px;"> <i class="fa fa-star"></i> New Category</button>
			@php
				$categories = app('CategoryHandler')->getFromRoot(0,['name'], null)->toTree();
			@endphp
			
			<div id="category-tree">
				@include("Shop::admin.category.partials.tree",['categories'=>$categories, 'assignedIds'=>$product->categories->pluck('id'), 'onSelect'=>''])
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="price-stock">
			<div class="input-group">
              <span class="input-group-addon">Product Name</span>
              <input onchange="updateProduct(this,'description')" type="text" class="form-control" value="{{$product->description}}" id="product-name">
            </div>
		</div>
      </div>
	</div>
	@script
	<script type="text/javascript">
		$(document).ready(function(){
			$('#prod-image .media-tile .remove-indicator').each(function(){
				replaceRemoveButton(this);
			});
		});
    </script>
    @endscript
</div>
</div>