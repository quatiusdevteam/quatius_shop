<div class="box-header with-border">
    <h3 class="box-title"> View Customer Order : {{$order->order_number}} </h3>
       <div class="box-tools pull-right">
		@section('order-tools')
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-user' data-href=""><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        
	        @if(user('admin.web')->isSuperUser())
	        	<button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#entry-user' data-datatable='#main-list' data-href="" >
                    <i class="fa fa-times-circle"></i> {{ trans('cms.delete') }}
                </button>
	        @endif

        @show
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >   
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">Detail</a></li>
            <li><a href="#history" data-toggle="tab">History</a></li>
        </ul>
         <div class="tab-content">
            <div class="tab-pane active" id="details">
                 @include('Shop::order.partials.orderdetails')
            </div>
             <div class="tab-pane " id="history">
                <table class="table table-striped">
                    <tr> <th> Date </th> <th>Status </th> <th>Value</th> <th> Comments </th> </tr>
                    @foreach ($order->histories as $history)
                <tr data-params="{{$history->params}}"> <td>{{date("d/m/Y",strtotime($history->created_at))}}</td> 
                    <td>{{$history->status}}</td>
                    <td>{{$history->value}}</td>
                <td onclick="loadParam(this)"><i class="fa fa-server"></i> {{$history->comments}}</td></tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>
    
</div>
<div class="box-footer" >
    &nbsp;
</div>