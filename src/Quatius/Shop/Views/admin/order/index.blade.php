@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i>   Manage ORDERS
@stop
@section('title')
ORDERS
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Orders</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-user'>
	
</div>
@stop
@section('tools')
<h4>

</h4>
@stop
@section('content')
<table id="main-list" class="table table-striped table-bordered">
    <thead>
        <th>Date</th>
        <th>Order Number</th>
        <th>Name</th>
        <th>Email</th>
        <th>Status</th>
        <th>Total</th>
    </thead>
</table>

  <div class="modal fade" id="ViewDetails" tabindex="-1" role="dialog" aria-labelledby="tleViewDetails">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="tleViewDetails">View Details</h4>
        </div>
        <div class="modal-body">
            <table class="table table-striped" id="data-params"></table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

@stop
@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
    
    oTable = $('#main-list').DataTable( {
        "columns": [
        { "data": "created_at" },
        { "data": "order_number" },
        { "data": "name" },
        { "data": "email" },
        { "data": "status" },
        { "data": "total" },],

        "order": [[ 0, "desc" ]],
        serverSide: true,
        ajax: {
            url: "{{ URL::to('/admin/shop/order') }}",
            "dataType": "json",
            "type": "GET",
            "data":{
                extra_fields : [
                  "user_id","id"
                ],
                // hash:{
                //     from: "id",
                //     to: "eid"
                // }
            }
        }
    }); 
    $('#main-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('admin/shop/order')}}' + '/' + d.id);
    });
});

function loadParam(itm){
    var tbl = $('#data-params');
    tbl.empty();
    var jItm = $(itm).parents('tr');
    try{
        var params = jItm.data('params');
        $.each(params, function(key, val){
            tbl.append('<tr><td>'+key+'</td><td>'+val+'</td></tr>');
        });
        
        $('#ViewDetails').modal('show');
    }catch( e ){
        
    }
}
</script>
@stop
@section('style')
@stop


