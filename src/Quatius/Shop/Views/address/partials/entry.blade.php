
	{!! Form::text('first_name')                   
	-> placeholder(trans('user::user.user.placeholder.first_name'))!!}
	{!! Form::text('last_name')                    
	-> placeholder(trans('user::user.user.placeholder.last_name'))!!}
	
    @if(isset($email) && $email)
    {!! Form::email('email')			                    
    	-> placeholder(trans('user::user.user.placeholder.email'))!!}
	@endif
	
	{!! Form::tel('phone_1', 'Phone')					        
	-> placeholder(trans('user::user.user.placeholder.phone')) !!}
		
	{!! Form::text('address_1')      
	-> placeholder(trans('user::user.user.placeholder.address_1')) !!}
        					
	{!! Form::text('address_2')					        
	-> placeholder(trans('user::user.user.placeholder.address_2')) !!}
						        
	{!! Form::text('city')				        
	-> placeholder(trans('user::user.user.placeholder.city')) !!}
	<div class="row">
	<div class="col-xs-6">
		{!! Form::select('state')                 
		-> options(trans('user::user.user.options.states'))
		-> placeholder(trans('user::user.user.placeholder.state'))!!}
	</div>
	<div class="col-xs-6">
		{!! Form::text('postcode')		        
		-> placeholder(trans('user::user.user.placeholder.postcode')) !!}
	</div>
	</div>
	
	{!! Form::select('country')       
	-> options(['AU'=>'Australia'])->select('AU')
	-> placeholder(trans('user::user.user.placeholder.country'))!!}
	{!! Form::hidden('address_type') !!}