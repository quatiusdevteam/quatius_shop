{!!Form::horizontal_open()	
	->action('shop/address')
    ->method('POST')
	->class('form-horizontal')
	->rules([
        'first_name'           => 'required|max:64',
        'last_name'            => 'required|max:64',
        'phone_1' 			   => 'required|min:8|max:15',
        'address_1'			   => 'required|max:64',
        'city'				   => 'required|max:64',
        'state'				   => 'required|not_in:0',
        'postcode' 			   => 'required|max:4|min:4',
        'country'			   => 'required|not_in:0',
	])
!!}

@include('Shop::address.partials.entry')

{!! Form::close() !!}