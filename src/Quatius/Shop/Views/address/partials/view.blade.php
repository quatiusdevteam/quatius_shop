<div class="text" data-id="{{$address->getRouteKey()}}" data-type="{{$address->address_type}}">
	<span data-name="first_name">{{$address->first_name}} </span>
	<span data-name="last_name">{{$address->last_name}} </span><br />
	<span data-name="address_1">{{$address->address_1}} </span>{!!$address->address_2 != ""?"<br />":""!!}
	<span data-name="address_2">{{$address->address_2}} </span><br />
	<span data-name="city">{{$address->city}}</span>,
	<span data-name="state">{{$address->state}} </span>
	<span data-name="postcode">{{$address->postcode}} </span><br />
	<span data-name="country">{{ trans('Shop::address.country-codes')[$address->country]}} </span><br />
	<span data-name="phone_1">Phone: {{$address->phone_1}} </span><br />
	<span data-name="email">Email: {{$address->email}} </span>
</div>