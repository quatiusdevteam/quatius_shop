@includeScript('js/shop/shop-user.js')
@includeScript('css/shop/shop-user.css')
@script
<script type="text/javascript">
var shopBaseUrl = '{{url("shop")}}/';
</script>
@endscript
<div id="addresses">
<!-- billing address -->
	<div id="billing">
			@php
				$billingAddress = \Quatius\Shop\Models\UserDetail::whereUserId($user->id)->billings()->first();
			@endphp
			
			@if ($billingAddress) 
				<div class="col-xs-12 section">
					<span class="pull-right small"><button class="btn btn-default btn-xs" onclick="editAddress(this)"><i class="fa fa-edit"></i></button></span>

					@include("Shop::address.partials.view",["address"=>$billingAddress])
				</div>
			@else
				<button onclick="newAddress('BT')">Set Billing Address</button>
			@endif 

	</div>
	<!-- end billing address -->
	<hr style="border:1px dotted #ccc;" />
	
	<!-- shipping address -->
	<div class="page-header">
		<h2>Shipping Address <button onclick="newAddress('ST')"><i class="fa fa-plus"></i></button></h2>
	</div>
	<div id="shippings">
		@php
			$shipmentAddresses = \Quatius\Shop\Models\UserDetail::whereUserId($user->id)->shipments()->get();
		@endphp
			
		@foreach($shipmentAddresses as $shipping_address)
		
		<div class="col-xs-12 section">
			<span class="pull-right small"><button class="btn btn-default btn-xs" onclick="editAddress(this)"><i class="fa fa-edit"></i></button> <button class="btn btn-default btn-xs" onclick="removeAddress(this)"><i class="fa fa-trash"></i></button></span>

			@include("Shop::address.partials.view",["address"=>$shipping_address])
			<hr style="border:1px dotted #ccc;" />
		</div>		
		@endforeach
	</div>
	<div>
	
	</div>
</div>

<div id="addressModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Address</h4>
      </div>
      <div class="modal-body">
      		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="$(this).parents('.modal-content').find('form').submit()">Save</button>
      </div>
	</div>
	</div>
</div>