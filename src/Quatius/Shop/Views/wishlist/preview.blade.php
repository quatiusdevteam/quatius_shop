@php
$order = app('OrderSession');
$repo = app('ProductHandler');
$models = app('WishListHandler')->getList();
$prods = $repo->getBy('sku', array_keys($models), $order->getShopperGroupIds());
$modelOrder = array_flip(array_keys($models));

$products = $repo->preloadViewingProducts($prods, $order->getShopperGroupIds()[0]);

$products = $products->sortBy(function($item) use($modelOrder){
    return isset($modelOrder[$item->sku])?$modelOrder[$item->sku]:"";
})->values();

@endphp

<ul class="dropdown-menu wish-preview">
    @foreach($products as $product)
       <li class="wish-item" data-sku="{{$product->sku}}">
        <span class="remove-wrapper" onclick="toggleWishList(this)"> x </span>
		<a href="{{url($product->url)}}">
		
		<img class="thumb" src="{!!$product->getThumbnailUrl()!!}">
		<span class="title-wrapper">
			<span class="title">{{$product->description}}</span>
		</span>
		</a>
    </li>
    @endforeach
    
	@if (count($products) == 0)
	<li class="wish-item-empty">
		<a><span class=""><i>No item(s) Saved</i></span></a>
	</li>
	@endif
</ul>

@script
<script>
$(document).ready(()=>{
	$(document).on( "wishlist-updated", (e, itm, jsonRespose) =>{
		var newItems = $(jsonRespose.view);
		
		$('.wish-menu-item .dropdown-menu').empty();
		$('.wish-menu-item .dropdown-menu').append(newItems.find('li'));
		$('.wish-menu-item .wish-list-count').text(jsonRespose.size);
		
		if(parseInt(jsonRespose.size) > 0){
			$('.wish-menu-item.product-unsaved').removeClass('product-unsaved').addClass('product-saved');
		}else{
			$('.wish-menu-item.product-saved').removeClass('product-saved').addClass('product-unsaved');
		}
	});
});
</script>
@endscript