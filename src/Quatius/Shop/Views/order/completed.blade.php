<div class="col-xs-12 col-md-offset-2 col-md-8 ">
<div class="row">
	<div class="col-xs-12 text-center">
		<H1 style="color: #193889; font-weight: bold;">Thank You</H1>
		<H4>Order number: {{$order->order_number}}</H4>
		<p>The order has been placed successfully.</p>
		
		@if($order->getParams('notes', []))
			@foreach($order->getParams('notes', []) as $note)
				<p>{!!$note!!}</p>
			@endforeach
    	@endif
		
		<a class="btn btn-xs btn-primary" target="_blank" href="{{url(config('quatius.shop.cart.url_prefix','order/').'pdf/'.$order->order_number)}}"><i class="fa fa-file-pdf-o"></i> View Tax Invoice PDF</a>
		
		<br><br><br><H3>YOUR ORDER</H3>
		
		<br>
	</div>
	
	<div class="col-xs-12">
	
	@include('Shop::order.partials.orderdetails')
	
	</div>
	
	<div class="col-xs-12"><br><br><a href="{{url('/')}}" class="btn btn-primary">Continue Shopping</a></div>
	
</div>
</div>

<iframe src="about:blank" name="print-order" onload="if('about:blank' != this.contentWindow.location.href ){ this.contentWindow.print(); this.contentWindow.location='about:blank'}" style="display: none"></iframe>
