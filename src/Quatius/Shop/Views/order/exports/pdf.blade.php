<div style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif;">

@include('Shop::order.exports.partials.pdf-header', ['order'=>$order])
<div class="row">
    @include('Shop::order.exports.partials.header', ['order'=>$order])
    @include('Shop::order.partials.orderdetails', ['order'=>$order])
</div>
