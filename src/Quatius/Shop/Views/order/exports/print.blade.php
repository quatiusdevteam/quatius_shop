<!DOCTYPE html>
<html lang="en">
	<head>
		{!! Theme::asset()->styles() !!}
		<style>
		.container{
			width:690px
		}
		body{
          -webkit-print-color-adjust:exact;
        }
		</style>
	</head>
	<body style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif;">
		<div class="container">
		
		@include('Shop::order.exports.partials.print-header', ['order'=>$order])
		@include('Shop::order.exports.partials.header', ['order'=>$order])
		<div class="row">
			@include('Shop::order.partials.orderdetails')
		</div>
		</div>
		{!! Theme::asset()->scripts() !!}
    </body>
</html>

