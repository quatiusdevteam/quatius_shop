<table style="width: 100%">
    <tr><th colspan="3" style="text-align: center"><h1>TAX INVOICE</h1></th></tr>
    <tr>
    	<td style="width: 55%"></td>
    	<th style="text-align: right">INVOICE: </th><th style="text-align: right"> &nbsp; {{$order->order_number}}</th></tr>
    <tr>
    	<td style="width: 55%"></td>
    	<th style="text-align: right">DATE: </th><th style="text-align: right"> &nbsp; {{ date('d/m/Y', strtotime($order->created_at))}}</th></tr>
    <tr>
    	<td style="width: 55%"></td>
    	<th style="text-align: right">SHIPMENT: </th><th style="text-align: right"> &nbsp; {{ $order->getParams('shipment.label', '')}}</th></tr>
    
    
</table>
<br><br>