<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    
    <title>Confirmed Order</title>
</head>

<style media="screen" type="text/css">

	* { 
		margin:0;
		padding:0;
	}
	
	html {
		background: #eee;
	}
	
	a {
		text-decoration: none;	
	    color:#337ab7;
	}
	
	a:hover, a:active {
		text-decoration: none;	
	    color: #23527c;
	}	

</style>

<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background: #eee; -webkit-font-smoothing:antialiased; -webkit-text-size-adjust:none; padding: 20px 0;">
		<tr> 
			<td> 
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="background: #fff; border-collapse: collapse; font-family: 'Arial', sans-serif; font-size: 14px; line-height: 1.5em; width: 600px; height: 100%; margin: 0 auto;"> 
					
					<!-- HEADER -->
					<tr> 
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td align="center" style="padding: 10px 20px 0 20px;">
										<a class="navbar-brand" href="{{url('/')}}" style="text-decoration: none; color:#337ab7;">
											<img src="{{ config('quatius.shop.url.logo','') }}">
										</a>
									</td>
								</tr>
							</table>
						</td> 
					</tr> 
					
					
					<!-- YOUR ORDER CONFIRMATIONT -->
					<tr> 
						<td align="center" style="padding: 58px 0 30px 0;">							
							<h1>Your order confirmation</h1>
							Thank you for your purchase from <a href="{{url('/')}}">{{config('app.name','Online Store')}}</a><br /><br /><br />
						</td>
					</tr> 
					
					<!-- WHAT WE SHIPPED -->
					<tr> 
						<td style="padding: 0 20px;">
						<table width="100%" class="item-table" style="border: 1px solid #ddd;">
								<tr>
									<td>
										<table width="100%" class="order-details" style="padding: 20px; font-family: 'Arial', sans-serif;">
											<tbody>
												<tr>
													<td style="text-transform: uppercase; text-align: left;"><h3>WHAT'S ORDER</h3><br><span><strong>Shipping To:</strong></span></td>					
													<td style="text-transform: uppercase; text-align: right;"><table style="font-family: 'Arial', sans-serif;display: inline-block;">
															<tr><th>Invoice: </th><td style="text-align: right; font-weight: normal;"> &nbsp; {{$order->order_number}}</td></tr>
															<tr><th>Date: </th><td style="text-align: right; font-weight: normal"> &nbsp; {{ date('d/m/Y', strtotime($order->created_at)) }}</td></tr>
															<tr><th>Shipment: </th><td style="text-align: right; font-weight: normal"> &nbsp; {{$order->getParams('shipment.label', '')}}</td></tr>
														</table></td>
												</tr>
												<tr>
													<td colspan="2" style="line-height: 1.5em; font-size: 14px;">
														
														<span style="text-transform: uppercase; ">{{ $order->getShipmentDetail()->getName()}}</span><br><br>
																					
														{{$order->getShipmentDetail()->address_1}}{!! $order->getBillingDetail()->address_2 !=""?"<br>":"" !!}
														{{$order->getShipmentDetail()->address_2}}<br />
														{{$order->getShipmentDetail()->city}}&nbsp;
														{{$order->getShipmentDetail()->state}}&nbsp;
														{{$order->getShipmentDetail()->postcode}}<br />
														<strong>Phone:</strong> {{$order->getShipmentDetail()->phone_1}}
													</td>					
													<td valign="top" style="text-align: right;">
														
													</td>
												</tr>
												@if($order->getParams('notes', []))
                                            		<tr>
    													<td colspan="2" style="line-height: 1.5em; font-size: 14px;">
    													<br>
    													<hr>
    													@foreach($order->getParams('notes', []) as $note)
    														<p>{{$note}}</p>
    													@endforeach
    													</td>
    												</tr>
                                            	@endif
											</tbody>
										</table>
										
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" style="border-collapse: collapse; margin: 0; padding: 0; font-family: 'Arial', sans-serif; font-size: 14px;">
											<thead style="background: #eee;">
												<tr>
													<!--<td class="t_15"></td>	-->
													<td style="border: 1px solid #ddd; width: 65%; padding: 8px 10px; text-transform: uppercase; font-weight: bold;">Description</td>								
													<td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Price</td>			
													<td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Qty</td>			
													<td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Total</td>
												</tr>
											</thead>
											<tbody>
												@foreach ($order->getProducts() as $product)
												<tr>
													<td style="border: 1px solid #ddd; padding: 8px 10px;">{{$product->description}}</td>
													<td style="border: 1px solid #ddd; padding: 8px 10px;">$<?php echo sprintf("%.2f",$product->price * 1.1)?></td>
													<td style="border: 1px solid #ddd; padding: 8px 10px;">{{$product->qty}}</td>
													<td style="border: 1px solid #ddd; padding: 8px 10px;">{{floatval($product->total) > 0?format_currency($product->total):($product->getParams('price-label','')?:format_currency($product->total))}}</td>
												</tr>
												@endforeach	
												<tr style="border: none!important;">
													<td colspan="3" style=" padding: 8px 10px; font-weight: bold; text-align:right">Subtotal</td>
													<td style="padding: 8px 10px; font-weight: bold;">$<?php echo sprintf("%.2f",$order->subtotal * 1.1)?></td>
												</tr>				
												@if ($order->discount > 0)
												<tr style="border: none!important;">
													<td colspan="3" style="padding: 8px 10px; font-weight: bold; text-align:right">Discount</td>
													<td style="padding: 8px 10px; font-weight: bold;">-$<?php echo sprintf("%.2f",$order->discount)?></td>
												</tr>				
												@endif
												<tr style="border: none!important;">
													
													<td colspan="3" style=" padding: 8px 10px; font-weight: bold; text-align:right">Shipping Cost</td>
													<td style="padding: 8px 10px; font-weight: bold;">$<?php echo sprintf("%.2f",$order->shipping_cost)?></td>
												</tr>
												<tr style="border: none!important;">
													
													<td colspan="3" style="padding: 8px 10px; font-weight: bold; text-align:right; font-size: 16px">Total (Inc. GST)</td>
													<td style="padding: 8px 10px; font-weight: bold; font-size: 16px">$<?php echo sprintf("%.2f",$order->total)?></td>
												</tr>

												<tr style="border: none!important;">
													
													<td colspan="3" style="padding: 8px 10px; font-weight: bold; text-align:right; font-size: 16px">GST</td>
													<td style="padding: 8px 10px; font-weight: bold; font-size: 16px">$<?php echo sprintf("%.2f",($order->total / 11)) ?></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</table>							
						</td>
					</tr>
					
					<tr>
						<td align="center"style="font-size: 12px; padding: 15px 0;">
							If you have any question about your order, please <a style="text-decoration: none; color:#337ab7" href="{{url('/')}}/contact-us">contact us</a>.
						</td>
					</tr>
				</table>
			</td> 
		</tr> 
	</table> 
</body>
</html>
