<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Confirmed Order</title>
</head>

<style media="screen" type="text/css">

    * {
        margin:0;
        padding:0;
    }


    .no-margin {
        margin: 0 !important;
    }
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }

    table {
        border-spacing: 0;
        display: table;
        border-collapse: collapse;
        border-spacing: 2px;
        border-color: grey;
        background-color: transparent;

    }
    thead {
        display: table-header-group;
        vertical-align: middle;
        border-color: inherit;
    }
    tbody {
        display: table-row-group;
        vertical-align: middle;
        border-color: inherit;
    }
    tr {
        display: table-row;
        vertical-align: inherit;
        border-color: inherit;
    }
    .table > thead > tr > th {
        border-bottom: 2px solid #bbb;
        vertical-align: bottom;
        text-align: center;
    }
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: 1px solid #bbb;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

</style>

<body style="margin: 0; padding: 0;">
<p>Dear Online Order team,</p>
<p>Below are pending orders.</p>
<br />
<div class="table-responsive">
    <table class="table no-margin table-fixed" id="table-pending-order">
        <thead>
        <tr>
            <th>Date</th>
            <th class="hidden-xs">Customer</th>
            <th class="hidden-xs">Email</th>
            <th class="hidden-xs">Phone</th>
            <th>Order number</th>
            <th>Attempt</th>
            <th>SKU</th>
            <th>Qty</th>
            <th class="hidden-xs">Price</th>
            <th class="hidden-xs">Shipping</th>
            <th class="hidden-xs">Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pending_orders as $pending_order)
            <tr>
                <td>{{$pending_order->order_date}}</td>
                <td class="hidden-xs"> {{$pending_order->customer_name}}</td>
                <td class="hidden-xs">{{$pending_order->email}}</td>
                <td class="hidden-xs">{{$pending_order->phone_1}}</td>
                <td>{{$pending_order->order_number}}</td>
                <td>{{$pending_order->attempt}}</td>
                <td>{{$pending_order->sku}}</td>
                <td>{{$pending_order->qty}}</td>
                <td class="hidden-xs">{{format_currency($pending_order->price)}}</td>
                <td class="hidden-xs">{{format_currency($pending_order->shipping_cost)}}</td>
                <td class="hidden-xs">{{format_currency($pending_order->total)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<br />
<p>Kind regards,</p>
<p>Developer team</p>
<br />
<p><small>This is an automate email. Please do not reply.</small></p>
</body>
</html>