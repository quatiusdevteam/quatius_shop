<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    
    <title>Confirmed Order</title>
</head>

<style media="screen" type="text/css">

	* { 
		margin:0;
		padding:0;
	}
	
	html {
		background: #eee;
	}
	
	a {
		text-decoration: none;	
	    color:#337ab7;
	}
	
	a:hover, a:active {
		text-decoration: none;	
	    color: #23527c;
	}	
	
	.order > tbody > tr > td {
        border: 1px solid #ddd; 
    }

</style>


<body style="margin: 0; padding: 0;">
<br>
	<table class="order" border="0" cellpadding="0" cellspacing="0" width="600" style="background: #fff; border-collapse: collapse; font-family: 'Arial', sans-serif; font-size: 14px; line-height: 1.5em; width: 600px; height: 100%; margin: 0 auto;"> 
		<tbody>
		<tr>
			<td style="text-align: center;padding: 30px;">
				<h1 style="color: #193889; font-weight: bold; font-size: 18px;">PURCHASE ORDERED</h1>
				<strong>INVOICE:</strong> {{$order->order_number}}<br>
				<strong>DATE:</strong> {{ date('d/m/Y', strtotime($order->created_at)) }}<br>
				<strong>Shipment:</strong> {{$order->getParams('shipment.label', '')}}
			</td>
		</tr>
		<tr style="font-size:13px;">
			<td style="padding: 20px 20px 10px 20px; ">
				<table style="width:100%">
				<tr><td style="width:50% ;padding: 0px 5px 0px 0px">
					<h3>ABOUT CUSTOMER:</h3>
						<span style="text-transform: uppercase; ">{{ $order->getBillingDetail()->getName()}}</span><br><br>
																		
						{{$order->getBillingDetail()->address_1}}{!! $order->getBillingDetail()->address_2 !=""?"<br>":"" !!}
						{{$order->getBillingDetail()->address_2}}<br />
						{{$order->getBillingDetail()->city}}&nbsp;
						{{$order->getBillingDetail()->state}}&nbsp;
						{{$order->getBillingDetail()->postcode}}<br />					   				
					<strong>Phone:</strong> {{$order->getBillingDetail()->phone_1}}<br /></td>
					
					<td style="width:50% ;padding: 0px 0px 0px 5px">
					<h3> SHIP TO:</h3>			
						<span style="text-transform: uppercase; ">{{ $order->getShipmentDetail()->getName()}}</span><br><br>
																		
						{{$order->getShipmentDetail()->address_1}}{!! $order->getShipmentDetail()->address_2 !=""?"<br>":"" !!}
						{{$order->getShipmentDetail()->address_2}}<br />
						{{$order->getShipmentDetail()->city}}&nbsp;
						{{$order->getShipmentDetail()->state}}&nbsp;
						{{$order->getShipmentDetail()->postcode}}<br />					   				
					<strong>Phone:</strong> {{$order->getShipmentDetail()->phone_1}}
					</td>
					</tr>
				<tr><td colspan="2"><strong>Email:</strong> {{$order->getBillingDetail()->email}}</td></tr>
					@if($order->getParams('notes', []))
                		<tr>
							<td colspan="2" style="line-height: 1.5em; font-size: 14px;">
							<br>
							<hr>
							@foreach($order->getParams('notes', []) as $note)
								<p>{{$note}}</p>
							@endforeach
							</td>
						</tr>
                	@endif
				</table>
			</td>
		</tr>
		<tr style="padding: 20px 20px 40px 20px;">
			<td>
				<table width="100%" style="border-collapse: collapse; margin: 0; padding: 0; font-family: 'Arial', sans-serif; font-size: 14px;">
					<thead style="background: #eee;">
						<tr>
							<!--<td class="t_15"></td>	-->
							<td style="border: 1px solid #ddd; width: 65%; padding: 8px 10px; text-transform: uppercase; font-weight: bold;">Description</td>								
							<td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Price</td>			
							<td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Qty</td>			
							<td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Total</td>
						</tr>
					</thead>
					<tbody>
						@foreach ($order->getProducts() as $product)
						<tr>
							<td style="border: 1px solid #ddd; padding: 8px 10px;">{{$product->description}}</td>
							<td style="border: 1px solid #ddd; padding: 8px 10px;">$<?php echo sprintf("%.2f",$product->price * 1.1)?></td>
							<td style="border: 1px solid #ddd; padding: 8px 10px;">{{$product->qty}}</td>
							<td style="border: 1px solid #ddd; padding: 8px 10px;">{{floatval($product->total) > 0?format_currency($product->total):($product->getParams('price-label','')?:format_currency($product->total))}}</td>
						</tr>
						@endforeach	
						<tr style="border: none!important;">
							<td colspan="3" style="border: 1px solid #ddd; padding: 8px 10px; font-weight: bold; text-align:right">Subtotal</td>
							<td style="border: 1px solid #ddd; padding: 8px 10px; font-weight: bold;">$<?php echo sprintf("%.2f",$order->subtotal * 1.1)?></td>
						</tr>				
						@if ($order->discount > 0)
						<tr style="border: none!important;">
							<td colspan="3" style="border: 1px solid #ddd; padding: 8px 10px; font-weight: bold; text-align:right">Discount</td>
							<td style="border: 1px solid #ddd;padding: 8px 10px; font-weight: bold;">-$<?php echo sprintf("%.2f",$order->discount)?></td>
						</tr>				
						@endif
						<tr style="border: none!important;">
							<td colspan="3" style="border: 1px solid #ddd; padding: 8px 10px; font-weight: bold; text-align:right">Shipping Cost</td>
							<td style="border: 1px solid #ddd; padding: 8px 10px; font-weight: bold;">$<?php echo sprintf("%.2f",$order->shipping_cost)?></td>
						</tr>
						<tr style="border: none!important;">
							<td colspan="3" style="border: 1px solid #ddd; padding: 8px 10px; font-weight: bold; text-align:right; font-size: 16px">Total (Inc. GST)</td>
							<td style="border: 1px solid #ddd; padding: 8px 10px; font-weight: bold; font-size: 16px">$<?php echo sprintf("%.2f",$order->total)?></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</body>
</html>
