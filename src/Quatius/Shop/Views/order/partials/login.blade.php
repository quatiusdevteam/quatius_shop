@include('public::notifications')
{!!Form::horizontal_open()
->id('login')
->method('POST')
->class('form-horizontal white-row')
->action('login')!!}

{!! Form::email('email')
-> placeholder(trans('user::user.user.placeholder.email'))!!}

{!! Form::password('password')
-> placeholder(trans('user::user.user.placeholder.password'))!!}

{!! Form::hidden(config('user.params.type'))!!}
<div class="col-lg-9 col-lg-offset-3 col-sm-6 col-sm-offset-6">
    <div class="row">
    {!! Form::checkboxes('')
            ->checkboxes([' Remember me' => ['name'=>'rememberme', 'value'=>'1']])->inline()!!}
    </div>
</div>
<br />
{!! Form::submit('Login')->class('btn btn-primary btn-block')!!}
{!! Form::close() !!}

<!-- {{url('/')}}/order/address_signin -->
  