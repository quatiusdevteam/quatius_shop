	<div class="label-contact">
		<table style="width: 100%">
		<tr>
		<td>
			@if($order->hasShipmentDetail())
				<span style="font-weight:bold">Billing Address</span>
			@else
				<span style="font-weight:bold">Billing / Shipment Address</span>
			@endif
			<div class="output-billto">
				@include("Shop::address.partials.view",["address"=>$order->getBillingDetail(), "email"=>true])
			</div>
		</td>
		<td class="text-right">
    		@if($order->hasShipmentDetail())
    			<span style="font-weight:bold">Shipment Address</span>
    			
    			<div class="output-shipto">
    				@include("Shop::address.partials.view",["address"=>$order->getShipmentDetail()])
    			</div>
			@endif
		</td>
		</tr>
		</table>
	</div>