@script
<script>
	function updateToInputQty(itm){
		if ($(itm).val() == "-1"){
			var nextInput = $(itm).next();
			nextInput.val($(itm).attr("data-qty"));
			nextInput.attr("name", $(itm).attr("name")).attr('type','text');
			$(itm).remove();
			nextInput.focus();
		}
		else{
			$(itm).attr("data-qty",$(itm).val());
			updateOrderQty(itm);
		}
	}
</script>
@endscript

<div class="well">
<table class="order-table">
	<thead>
		<tr class="hide">
			<th style="border-right: none;" class="col-md-6 col-xs-6">ITEM DESCRIPTION</th>
			<th class="text-center hidden-xs col-md-2">COST</th>
			<th class="text-center col-xs-2">QUANTITY</th>
			<th class="text-center col-xs-2">TOTAL</th>
		</tr>
	</thead>	
	<tbody>

	@foreach($order->getProducts() as $prod_id=>$item)
	<tr class="prod-row product-order order-item {{$order->hasError($prod_id)?'has-error':''}} {{(isset($order->hasError($prod_id)['stock']))?'error-outofstock':''}} {{(isset($order->hasError($prod_id)['destination']))?'error-destination':''}}" data-id="{{$item->getProduct()->getRouteKey()}}">
		<td valign="middle" style="border-right: none;" class="col-md-6 col-xs-6">
			<img class="prod-thumb" style="width:100px" src="{!!$item->getProduct()->getThumbnailUrl()!!}">
			<div class="prod-wrapper">
				<a class="prod-name" href="{{url($item->getProduct()->url)}}">{!!$item->description!!}</a><br>
				<span class="prod-code">Model: {{$item->sku}}</span><br/>
				@include('Shop::product.partials.cart_extras', ['product'=>$product, 'order'=>$order])
				<input type="hidden" name="prod_ids[]" value="{{$item->getProduct()->getRouteKey()}}" />
				<a class="prod-remove visible-xs" href="#" onclick="removeProduct(this);"><i class="fa fa-times" aria-hidden="true"></i> Remove</a>
			</div>			
		</td>
		
		<td class="hidden-xs text-center col-md-2">
			<a class="prod-remove" href="#" onclick="removeProduct(this);"><i class="fa fa-times" aria-hidden="true"></i> Remove</a>
		</td>	
			
		<td class="col-xs-2">
			<div class="row">
			@php
				$qtys = [];
				$stock = $item->getProduct()->stock;
				for ($q=1; $q <=$stock && $q <= 10; $q++) {
					$qtys["$q"] = "$q";
				}
				$qtys["-1"] = "More ..";
			@endphp
			
			@if($item->qty <= 10)
				{!! Form::select('prod_qty[]','')->options($qtys)->value($item->qty)->attributes(["class"=>"form-control","max"=>$item->getProduct()->stock,"onchange"=>"updateToInputQty(this)"]); !!}
				<input type="hidden" class="form-control" name="temp_qty" pattern="[0-9\/]*" min="0" onchange="updateOrderQty(this)" max="{{$item->getProduct()->stock}}" value="{{$item->qty}}" />
			@else
				<input type="number" class="form-control" name="prod_qty[]" pattern="[0-9\/]*" min="0" onchange="updateOrderQty(this)" max="{{$item->getProduct()->stock}}" value="{{$item->qty}}" />
			@endif

			</div>
		</td>	
						
		<td class="text-right col-md-1 total-price">
			<div class="row">
				<strong>{!!format_currency($item->total,'Shop::currency.default')!!}</strong>
			</div>
		</td>
	</tr>
	@endforeach
	</tbody>
	<tfoot>
		<tr><td style="height: 80px;" class="empty hidden-xs"></td> </tr>
		<tr>
			<td colspan="4">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-5">
						{!! Theme::place('cart-extra')!!}
					</div>
					<div class="col-xs-12 col-sm-6 col-md-7">
						<div class="row">
							<div class="col-xs-9 col-md-10"><span class="text-right total-price pull-right"><strong>Sub Total</strong></span></div>
							<div class="col-xs-3 col-md-2"><div class="row"><span class="order-sub-total-val total-price text-right pull-right"><strong>{!!format_currency($order->subtotal,'Shop::currency.default')!!}</strong></span></div></div>
						</div>
						<div class="discount-list">
						@if($order->getParams('discounts', NULL))
							@foreach($order->getParams('discounts', []) as $type=>$discount)
								<div class="row discount-{{$type}} {{$discount['warning']?'discount-warning':''}}">
									<div class="col-xs-9 col-md-10 text-right"><strong><span class="discount-name">{{$discount['name']}}</span></strong>
										<span class="discount-text-warning">{!! $discount['warning']?:"" !!}</span>
									</div>
									<div class="col-xs-3 col-md-2"><div class="row"><span class="text-right pull-right"><strong>-{!!format_currency($discount['value'],'Shop::currency.default')!!}</strong></div></div>
								</div>
							@endforeach
						@endif
						</div>
						<div class="row">
							<div class="col-xs-9 col-md-10 text-right"><span class="text total-price"><strong>Shipping Cost</strong></span></div>
						<div class="col-xs-3 col-md-2"><span class="order-shipping-val total-price text-right pull-right">
							<div class="row">
								@if($order->hasBillingDetail())
									<strong>{!!format_currency($order->shipping_cost,'Shop::currency.default')!!}</strong>
								@else
									<strong title="No Address entered!">N/A</strong>
									<strong style="display:none">{!!format_currency($order->shipping_cost,'Shop::currency.default')!!}</strong>
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-xs-9 col-md-10 text-right"><span class="text total-price"><strong>Total <span class="gst">(Inc. {!!config('quatius.shop.tax.label', 'TAX')!!})</span></strong></span></div>
						<div class="col-xs-3 col-md-2"><span class="order-total-val total-price text-right pull-right"><strong>{!!format_currency($order->total,'Shop::currency.default')!!}</strong></span></div>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</tfoot>
</table>
</div>