@include('public::notifications')		

@php
Form::populate($order->getBillingDetail());
@endphp
							
	@if($order->allowGuestCheckout())
		{!! Form::horizontal_open()			                    
			->id('register')
			->action(url(config('quatius.shop.cart.url_prefix','order/').'address'.($session_name?'/'.$session_name:'')))
			->method('POST')
			->class('form-horizontal white-row')
			->rules([
						'first_name'           => 'required|max:64',
						'last_name'            => 'required|max:64',
						'email'                => 'required|email|max:255',
						'phone_1' 			   => 'required|min:8|max:15',
						'address_1'			   => 'required|max:64',
						'city'				   => 'required|max:64',
						'state'				   => 'required|not_in:0',
						'postcode' 			   => 'required|max:4|min:4',
						'country'			   => 'required|not_in:0',
				]);
		!!}

	@else
		{!! Form::horizontal_open()			                    
			->id('register')
			->action(url(config('quatius.shop.cart.url_prefix','order/').'address'.($session_name?'/'.$session_name:'')))
			->method('POST')
			->class('form-horizontal white-row')
			->rules([
						'first_name'           => 'required|max:64',
						'last_name'            => 'required|max:64',
						'email'                => 'required|email|max:255',
						'phone_1' 			   => 'required|min:8|max:15',
						'address_1'			   => 'required|max:64',
						'city'				   => 'required|max:64',
						'state'				   => 'required|not_in:0',
						'postcode' 			   => 'required|max:4|min:4',
						'country'			   => 'required|not_in:0',
						'password'				=> 'required|max:64',
						'password_confirmation'=> 'required|max:64',
				]);
		!!}
	@endif

	@if(!(Auth::guest()))
		@include('Shop::address.partials.entry',['address'=>$order->getBillingDetail()])
	@else
		@include('Shop::address.partials.entry',['address'=>$order->getBillingDetail(),'email'=>true])
	
		@position('order_address_extra')
		
		@if($order->allowGuestCheckout())
			<div class="col-xs-12 col-md-6 ">
				<div class="row">
				{!! Form::checkboxes('')
				->checkboxes([' Sign up with this information' => ['name'=>'signup','onchange'=>'onSignup(this)', 'value'=>'1']])->inline()!!}
										
				</div>
			</div>
			<div id="signup_ext" class="col-xs-12" style="display: none;">
		@else
			<div style="display: none;">
				{!! Form::checkboxes('')
					->checkboxes([' Sign up with this information' => ['name'=>'signup','onchange'=>'onSignup(this)', 'value'=>'1', "checked"=>"checked"]])->inline()!!}
			</div>
			<div id="signup_ext" class="col-xs-12">
			
		@endif
		
        
        
        <div class="row">	     	
        	<div class="col-md-6">
				@position('register_extra')
				
            	{!! Form::password('password')			                    
        		-> placeholder(trans('user::user.user.placeholder.password'))!!}
        		{!! Form::password('password_confirmation')			                    
        		-> placeholder(trans('user::user.user.placeholder.password_confirmation'))!!}
        		
            </div>
        	<div class="col-md-6">
        		{!! Captcha::render() !!}
        	</div>
    	</div>
        <br />
        <input type='hidden' name='mode' value='2'>
        <input type='hidden' name='login' value='1'>
        </div>
    
	@endif
	{!! Form::submit('Submit')->class('btn btn-primary')->style('width:100%')!!}
	
	{!! Form::close() !!}



@script
<script type="text/javascript">
$(document).ready(function(){

	if ($('input[name="signup"]').is(':checked')){
		$('#signup_ext').show();
	}
	else{
		$('#signup_ext').hide();
	}
});

function onSignup(itm){
	if($(itm).is(":checked")){
		$("#signup_ext").slideDown( "slow", function(){
			$('#signup_ext').show();
		});
	}else{
		$("#signup_ext").slideUp( "slow", function(){
			$("#signup_ext").hide();
			$("#password-2").val('');
			$("#password_confirmation").val('');
		});
	}
}

</script>

@endscript	
	          		