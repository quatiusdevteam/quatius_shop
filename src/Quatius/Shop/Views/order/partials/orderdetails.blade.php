<div id="invoice" data-ordernumber="{{isset($order->order_number)?$order->order_number:''}}" data-id="{{isset($order->id)?$order->id:''}}">
	@include('Shop::order.partials.address_details')
	
	<br>
	<table class="table table-bordered table-striped" style="width: 100%; background-color: #ffffff;">
	<thead>
	
	<tr style="border: 1px solid #081433; background: #081433; color: white; text-align:left"><th>DESCRIPTION</th><th style="text-align: center">QTY</th><th style="text-align: right">PRICE</th></tr>
	
	</thead>
	<tbody>
	<?php $rowNo = 0;?>
	@foreach ($order->getProducts() as $product)
		<?php $rowNo ++;?>
		<tr {!! $rowNo%2?'':'style="background-color: #f9f9f9;"'!!}><td>{{$product->description}}<br>
			@foreach($product->getParams('cart.extras',[]) as $field => $valObj)
				{{$product->getParams('cart.extras.'.$field.'.name', $field)}} : {{$product->getParams('cart.extras.'.$field.'.value', '')}}<br>
			@endforeach
		</td><td style="text-align: center">{{$product->qty}}</td><td style="text-align: right">{{floatval($product->total) > 0?format_currency($product->total):($product->getParams('price-label','')?:format_currency($product->total))}}</td></tr>	
	@endforeach
	</tbody>
	<tfoot>	
	<tr><td colspan="3">&nbsp;</td></tr>
	<tr><th colspan="2" style="text-align: right">Sub Total</th><td style="text-align: right">{{format_currency($order->subtotal)}}</td></tr>
	<tr><th colspan="2" style="text-align: right">Shipping Cost</th><td style="text-align: right">{{format_currency($order->shipping_cost)}}</td></tr>
	@if (isset($order->discount) && $order->discount > 0)
		<tr><th colspan="2" style="text-align: right">Discount <br></th><td style="text-align: right">-{{format_currency($order->discount)}}</td></tr>
	@endif
	
	<tr><td colspan="2" style="text-align: right"><span style="font-weight: bold;font-size: 20px">Total </span> (Inc. {!!config('quatius.shop.tax.label', 'TAX')!!})</td><td style="font-weight: bold;font-size: 20px;text-align: right">{{format_currency($order->total)}}</td></tr>
	<tr><td colspan="2" style="text-align: right"><span style="font-weight: bold;font-size: 20px">{!!config('quatius.shop.tax.label', 'TAX')!!}</span></td><td style="font-weight: bold;font-size: 20px;text-align: right">{{format_currency($order->tax_total)}}</td></tr>
	
	</tfoot>
	</table>
	
	@if (isset($order->comments)?($order->comments!=""?true:false):false)
		<div><label>Customer Reference:</label><br>{{$order->comments}}</div>
	@endif
	
	@if (isset($order->delivery_date)?($order->delivery_date!=""?true:false):false)
		<div><label>Prefered Delivery Date:</label><br>{{date('d/m/Y', strtotime($order->delivery_date))}}</div>
	@endif
</div>