
<?php Theme::breadcrumb()
 		->add('Home', url('/'))
 		->add(config('quatius.shop.cart.name','My Cart'), url(config('quatius.shop.cart.url_prefix','order/')))
 		->add('Address', url(config('quatius.shop.cart.url_prefix','order/').'address'));

 		$hasAddress = $order->hasBillingDetail();
?>
@includeStyle('packages/icheck/skins/square/red.css')
@includeScript('packages/icheck/icheck.min.js')
	
@script
<script>
$(document).ready(function(){
	
	$('.register-login input').iCheck({
	    checkboxClass: 'icheckbox_square-red',
	    radioClass: 'iradio_square-red',
	    labelHover: true,
		labelHoverClass: 'hover',
	});
	  
	$('.register-login input').on('ifChanged', function (e) {
		$(this).trigger('change');
	});
		
	if(window.location.hash) {
	    var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
	    jQuery('[href="#'+hash+'"]').click();
	    clearValidation(false);
	    window.scrollTo(0, 0);
	} else {
		@if(Auth::guest() && $hasAddress)
			jQuery('[href="#guest"]').click();
		@else
    		jQuery('[href="#logintab"]').click();
    		clearValidation(false);
    		window.scrollTo(0, 0);
		@endif
	}


});

function clearValidation(set){
	if(set){
		$("div.alert-danger").hide();
	}else{
		$("div.alert-danger").show();
	}
}

</script>
@endscript
<br>
<div class="col-lg-10 col-lg-offset-1 register-login">
	<div class="well well-lg">
	  <ul class="nav nav-tabs" role="tablist">
		@if((Auth::guest()))
	    <li role="presentation"><a href="#logintab" aria-controls="logintab" onclick="clearValidation(false)" role="tab" data-toggle="tab" class="logintab">Return Customer</a></li>		    
	    <li role="presentation"><a href="#guest" aria-controls="guest" onclick="clearValidation(true)" role="tab" data-toggle="tab" class="guest">Checkout as Guest</a></li>
	    @else
	    <li role="presentation" class="active"><a href="#guest" aria-controls="guest" role="tab" data-toggle="tab" class="guest">Change address</a></li>
	    @endif		        
	  </ul>
	  <br />
	  <div class="tab-content">
	  	<div role="tabpanel" class="tab-pane {{Auth::guest()&&!$hasAddress?'active':''}}" id="logintab">
    	  	@include('Shop::order.partials.login')
	    </div>
	    <div role="tabpanel" class="tab-pane {{Auth::guest()&&!$hasAddress?'':'active'}}" id="guest">
	    	 @include('Shop::order.partials.address')
	    </div>
	  </div>
	</div>
</div>
