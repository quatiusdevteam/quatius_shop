@includeStyle('css/shop/shop-order.css')
@includeScript('js/shop/shop-cart.js')

@script
<script type="text/javascript">
$(function () {
	$('#clearOrder').popover();

	$('#checkout_btn').popover();
});
</script>

@endscript
        <form class="form-horizontal" action="{{url(config('quatius.shop.cart.url_prefix','order/'))}}/checkout{{$session_name?'/'.$session_name:''}}" method="post" id="shopping-cart-form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-xs-6">
                    <H3>MY ORDER</H3>
                </div>
               
                @if (count($order->getProducts()) > 0)
                    <div class="col-xs-12">
                        <div class="cart-contact-info">
                        </div>
                        @include('Shop::order.partials.list')
                    </div>
                    
                    @include('Shop::shipment.select',['shipList'=>$order->getSelectableShipments(), 'selected'=>$order->getShipmentMethod()])
                    <div class="col-xs-12"><br></div>
                    
                    <div class="hidden-xs col-sm-6">
						{!! Theme::place('cart-extra')!!}
                        <a href="{{url('/')}}" class="btn btn-default">CONTINUE SHOPPING</a>
                    </div>
                    <div class="col-xs-12 col-sm-6 text-right">
						<a id="clearOrder" tabindex="0" class="btn btn-default" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" title="{!!trans('Shop::order.empty-confirm')!!}" data-html="true" data-content='<div class="text-center"><button class="btn btn-xs btn-inline btn-default" type="button">{!!trans('Shop::order.buttons.no')!!}</button> <a class="btn btn-xs btn-inline btn-warning" href="{{url(config('quatius.shop.cart.url_prefix','order/'))}}/clear{{$session_name?'/'.$session_name:''}}" onlick="">{!!trans('Shop::order.buttons.yes')!!}</a></div>'><i class="fa fa-times" aria-hidden="true"></i> {!!trans('Shop::order.buttons.empty-cart')!!}</a>
						<button type="submit" name='method' value="update" onclick="jQuery(this.form).removeAttr('target');" class="btn btn-default"><i class="fa fa-cart-arrow-down"></i> Update Cart</button>
						<button type="submit" name='method' value="standard" onclick="jQuery(this.form).removeAttr('target');return analyticActions(this.form, 'checkout', function(itmForm){itmForm.submit();});" id="checkout_btn" class="btn btn-primary"><i class="fa fa-shopping-bag"></i> {!!trans('Shop::order.buttons.checkout')!!}</button>
						
						@foreach($order->getCheckoutButtons() as $payMethod)
    						<button type="submit" name='pay_checkout' value="{{$payMethod->getRouteKey()}}" onclick="jQuery(this.form).removeAttr('target');return analyticActions(this.form, 'checkout', function(itmForm){itmForm.submit();});" class="btn btn-link"><img src="{!!$payMethod->getConfig('checkout.button', '')!!}" /></button>
    					@endforeach
						
					</div>
					
                    <div class="col-xs-12"><br></div>
                @else
                    <div class="col-xs-12 text-center">
                        <h4><i>{!!trans('Shop::order.cart-empty')!!}</i></h4>
                        <br><br>
                    </div>
                    <div class="col-xs-12">
                        <a href="{{url('/')}}" class="btn btn-default">CONTINUE SHOPPING</a><BR>
                    </div>
                @endif
            </div>
        </form>
        <br><br>
