<?php Theme::breadcrumb()
 		->add('Home', url('/'))
 		->add('My Order', url(config('quatius.shop.cart.url_prefix','order/').($session_name?'/'.$session_name:'')));
 		
 		if(Gate::allows('user.edit_address'))
 		{
 		    Theme::breadcrumb()->add('Address', url(config('quatius.shop.cart.url_prefix','order/').'address'));
 		}
 		
 		Theme::breadcrumb()->add('Confirm Order', "#");
?>

@script

<script type="text/javascript">
	
	$(document).ready(function(){
		jQuery('[data-toggle="popover"]').popover();
		
		$('#payment-collapsible').on('show.bs.collapse', function () {
			if($('.btn-confirm').hasClass('btn-default')){
				$('.btn-confirm').removeClass('btn-default').addClass('btn-success');
			}
		});
		
	});

	/* function submitPayment(itm){
		var jItm = $(itm);
		
		if ($('#payment-collapsible .active').length == 0){
    		jItm.popover('show');
    		setTimeout(function(){jItm.popover('hide');}, 2000);
    		return;
		}

		$('#payment-collapsible .active form').submit();
	} */
</script>
@endscript

<div class="col-xs-12 col-md-12 ">
<!-- <br>
<h4>Online payment methods will adopt EFT, such as debit, credit card payments and PayPal.</h4>
<br>
 -->
@include('public::notifications')													
	
 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
 
<div class="row">
	<div class="col-xs-12 text-center">
		<H3>CONFIRM ORDER</H3>
	</div>
	
	<div class="col-xs-12">
	
	@include('Shop::order.partials.orderdetails')
	
	</div>
	<div class="col-xs-12"><br><br></div>
	<div class="col-xs-12 col-md-6">
			@if($order->getParams('notes', []))
		<strong>Notes:</strong><br>
			@foreach($order->getParams('notes', []) as $note)
				<p>{{$note}}</p>
			@endforeach
    	@endif
	</div>
	<div class="col-xs-12 col-md-6">
	
	@include('Shop::payment.selections')
	
	</div>
	<div class="col-xs-12">	<div class="hidden-xs col-md-6">
		<a href="#" onclick="$(this).attr('href',$('.breadcrumb li:last-child').prev().find('a').attr('href'))" class="btn btn-default">BACK</a>
	</div>
	</div>
</div>

</div>
