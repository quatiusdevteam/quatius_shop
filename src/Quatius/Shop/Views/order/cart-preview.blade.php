<ul class="dropdown-menu cart-preview">
	@foreach($order->getProducts() as $product)
	<li class="cart-item" data-id="{{hashids_encode($product->product_id)}}">
		<a href="{{url($product->getProduct()->url)}}">
		<span class="qty-wrapper">
			<span class="qty badge">{{$product->qty}}</span>
		</span> x 
		<img class="thumb" src="{!!$product->getProduct()->getThumbnailUrl()!!}">
		<span class="title-wrapper">
			<span class="title">{{$product->description}}</span>
		</span>
		</a>
    </li>
    @endforeach
    
	@if (count($order->getProducts()) == 0)
	<li class="cart-item-empty">
		<a><span class=""><i>{!!trans('Shop::order.cart-empty')!!}</i></span></a>
	</li>
	@endif
	
    <li class="cart-footer">
    	<a class="cart-view-btn bg-primary" href="{{url(config('quatius.shop.cart.url_prefix','order/'))}}">
    		<span class="">VIEW CART <i class="fa fa-shopping-cart"></i></span>
    		<span class="cart-total">Total {!!format_currency($order->subtotal)!!}</span>
    	</a>
    </li>
</ul>