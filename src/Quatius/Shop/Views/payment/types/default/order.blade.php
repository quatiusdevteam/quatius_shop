{!!
	Form::horizotal_open()
	->action(url(config('quatius.shop.cart.url_prefix','order/').'confirm'))
	->method('POST')
	->class('form-horizontal white-row')
!!}
<button class="btn-confirm btn btn-default btn-block" type="submit" role="button" >
	{!!$payment->getLogo()?'<img class="img-responsive" src="'.$payment->getLogo().'">':''!!}
	
	{!!Form::hidden('payment_id')->forceValue($payment->getRouteKey())!!}
	@if($order->isExpressCheckout())
		{!!Form::hidden('checkout')->value(1)!!}
	@endif
	
	<i class="fa fa-credit-card" aria-hidden="true"></i> PROCESS PAYMENT</button>
	
{!! Form::close() !!}