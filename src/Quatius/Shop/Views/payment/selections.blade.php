<div class="row payment-section-area">
	<div class="col-xs-12">
		@php 
			$sel_payment = $order->getPaymentMethod();
		@endphp
		
		@if ($order->getPaymentSelections($sel_payment, $order->getShopperGroupIds())->count() > 1)
            <h3>Please select a payment method</h3>
        @endif
        
        <div class="panel-group" id="payment-collapsible" role="tablist" aria-multiselectable="false">
        	@foreach($order->getPaymentSelections($sel_payment, $order->getShopperGroupIds()) as $payment)
        		
        	<div class="payment panel panel-default" data-id="{{$payment->getRouteKey()}}" >
            <div class="panel-heading" role="tab" id="heading-{{$payment->type}}">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#payment-collapsible" href="#payment-{{$payment->type}}" aria-expanded="false" aria-controls="payment-{{$payment->type}}">
                  {{$payment->name}}
                </a>
              </h4>
            </div>
            <div id="payment-{{$payment->type}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{$payment->type}}">
              <div class="panel-body">
        			@include($payment->getOrderForm(), ['order'=>$order, 'payment'=>$payment])
              </div>
            </div>
          	</div>

        	@endforeach
        </div>
    </div>
</div>

@script
<script type="text/javascript">
	$(document).ready(function(){
		var selectedPayment = '{{$sel_payment?$sel_payment->getRouteKey():''}}';
		
		jQuery('[data-toggle="popover"]').popover();
		$('#payment-collapsible .panel-collapse').on('show.bs.collapse', function () {
			var panel = $(this).parents('.panel');
			
			$('#payment-collapsible .panel.panel-primary')
				.removeClass('panel-primary')
				.removeClass('active')
				.addClass('panel-default');
			
			panel.addClass('panel-primary').addClass('active');

			if (panel.find('form').length > 0){
				if (panel.find('form input[name="payment_id"]').length == 0){
						panel.find('form').append('<input type="hidden" value="'+panel.attr('data-id')+'" name="payment_id" >');	
				}
			}	
				
			
		});
		$('#payment-collapsible a[data-toggle="collapse"]').click(function(e){
			  if (!$(this).hasClass('collapsed')){
				  return false;  
			    //e.stopPropagation();
			  }
		});
		$('#payment-collapsible [data-id="'+selectedPayment+'"] a.collapsed').click();
	});
</script>
@endscript