@php

$searchVer = app('ProductHandler')->searchVersion();
$searchFile = app('ProductHandler')->searchFileUrl($searchVer);
Theme::asset()->add(basename($searchFile), $searchFile);

Theme::asset()->add('shop-search.js', 'js/shop/shop-search.js');

@endphp

<form class="navbar-form navbar-left" action="{{url('shop/search')}}" data-version="{{$searchVer}}">
  	<div class="input-group">
      <input id="search-item-input" name="search" onclick="searchItems(this)" autocomplete="off" type="text" class="form-control " value="{{Request::get('search','')}}" placeholder="Search for...">
      <div class="search-item-result" style="display: none">
      </div>
      <span class="input-group-btn">
        <button type="submit" class="btn btn-default" type="button">Go!</button>
      </span>
    </div>
</form>

@script
<script type="text/javascript">
$( "#search-item-input" ).keyup(function( event ) {
    if ( event.which == 13 ) {
    	event.preventDefault();
    }
    searchItems(this);
});

$( "#search-item-input" ).blur(function( event ) {
	setTimeout(function(){
		$(".search-item-result").hide();
	}, 1000);
	
});
</script>
@endscript