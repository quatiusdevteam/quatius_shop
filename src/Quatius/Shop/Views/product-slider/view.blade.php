@if (count($products) > 0)

	@includeStyle('build/vendor/slick/slick.css')
	@includeStyle('build/vendor/slick/slick-theme.css')
	@includeScript('build/vendor/slick/slick.min.js')
	
	<div class="col-xs-12">
		<div class="col-xs-12 special-sale">
			<h2 class="text-center">{{$component->getData()->heading}}</h2>
			<a onclick="doSlickCarousel(jQuery(this).next());"class="pull-right show-more">VIEW ALL</a>
		
			<div class="slider six-col-slider">
				@foreach($products as $product)	
					<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 ">
						@include('Product::partials._tile', ['product' => $product])
					</div>
				@endforeach
			</div>
		</div>
	</div>
	
	@script
		<script type="text/javascript">
	
		jQuery(document).ready(function(){
			jQuery('.six-col-slider').each(function(){
				doSlickCarousel(jQuery(this));
			});
		});
	
		function doSlickCarousel(jItm)
		{
			if (jItm.hasClass('slick-initialized'))
			{
				jItm.slick('unslick')
			}
			else
			{
			jItm.slick({
			    dots: false,
			    speed: 300,
			    slidesToShow: 6,
			    slidesToScroll: 1,
			    autoplay: true,
			    infinite: true,
			    autoplaySpeed: 2000,
			    responsive: [
				{
				    breakpoint: 1200,
				    settings: {
				      slidesToShow: 5,
				      slidesToScroll: 1,
				    }
				  },
			      {
			        breakpoint: 992,
			        settings: {
			          slidesToShow: 4,
			          slidesToScroll: 1,
			        }
			      },
			      {
			        breakpoint: 768,
			        settings: {
			          slidesToShow: 3,
			          slidesToScroll: 1
			        }
			      },
			      {
			        breakpoint: 580,
			        settings: {
			          slidesToShow: 2,
			          slidesToScroll: 1
			        }
			      }
			      ,
			      {
			        breakpoint: 420,
			        settings: {
			          slidesToShow: 2,
			          slidesToScroll: 1
			        }
			      }
			    ]
			  });
			}
		}
		</script>
		@endscript
@endif
