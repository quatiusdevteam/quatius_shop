<div class="product-preview img-thumbnail" style="text-align: center;width:120px;margin-left:5px;margin-bottom:5px; float:left" data-id="{!!$product->id!!}" data-slug="{!!$product->getSlug()!!}" >
	<div class="spacer" style="position: relative;">
		<button style="position: absolute; right: 0;" class="btn-link remove-indicator" onclick="jQuery(this).parents('.product-preview').remove();" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
		
		<img style="display: inline" class="prod-thumb img-responsive"  src="{{$product->getThumbMedia()->getThumbUrl(120,90)}}">
		
		<div class="col-xs-12" style="height:40px; overflow:hidden">
			<div class="row product-name">
			{!!$product->name!!}
			</div>
		</div>
		<input name="components[{{$index}}][data][products][]" type="hidden" value="{{$product->id}}">
	</div>
</div>
 