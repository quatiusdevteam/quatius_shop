
	<div class="row">
                    <div class='col-md-6'>
                         {!! Form::text('components['.$index.'][data][heading]')
                        -> label("Product Heading")
                        -> value($component->getData()->heading)
                        -> placeholder("Heading")!!}
                        
                    </div>
                    <div class='col-md-6'>
						{!! Form::select('products')
                        -> options($selectProducts)
                        -> label("Add Product")
                        -> setAttributes(['onchange' => 'addProduct(this)'])
                        -> placeholder("Select a product")!!}
                    </div>
                    <div class='col-xs-12'>
                    <hr>
                    </div>
					
                    <div class='product-area col-xs-12'style=" max-height: 300px; overflow: auto;">
                    
                    	@foreach($products as $product)
                    		@include('Product::product-slider.admin.each-tile', ['index'=>$index,'product'=>$product])
                        @endforeach
                    
                    </div>
       </div>
	<script type="text/javascript">
	    
		function addProduct(itm){
			var jItm = jQuery(itm);
			var index =  jQuery(itm).parents('.edit-component').attr('data-index');
			jQuery.get('{{url("/admin/component/product-slider")}}/'+index+'/'+jItm.val(), function(content){
				
				jQuery(itm).parents('.edit-component').find('.product-area').append(content);
	
				jItm.val('');
			})
			
		}
	</script>
