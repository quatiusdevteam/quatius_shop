
	<div class="row">
	<div class='col-xs-12'><h2>{{$component->getData()->heading}}</h2></div>
        <div class='product-area col-xs-12'style=" max-height: 200px; overflow: auto;">
                    
        	@foreach($products as $product)
            	@include('Product::product-slider.admin.view-tile', ['product'=>$product])
            @endforeach
        </div>
    </div>