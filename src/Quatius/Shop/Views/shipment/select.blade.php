<div class="shipment-list col-xs-12">
@if ($shipList->count() > 1)
	<div class="well spacer options">
	<h3>Postage Options</h3>
    @foreach($shipList as $shipment)
    
          
    @if($selected && $selected->id==$shipment->id) 
        {!!Form::radios('shipment_method')->label('')
          ->radios([$shipment->label => ['value' => $shipment->getRouteKey(), 'checked'=>'checked']])->stacked()!!}
    @else
        {!!Form::radios('shipment_method')->label('')
          ->radios([$shipment->label => ['value' => $shipment->getRouteKey()]])->stacked()!!}
    @endif
    <div class="customer-note">
    {!!$shipment->customer_note!!}
    </div>
    @endforeach
    </div>
@else
	{!!Form::hidden('shipment_method')->value($selected?$selected->getRouteKey():0)!!}
	@if ($selected && trim($selected->customer_note))
		<div class="well spacer">
        	<h3>Postage Information</h3>
            <div class="customer-note">
            	{!!$selected->customer_note!!}
            </div>
        </div>
    @endif
@endif

</div>