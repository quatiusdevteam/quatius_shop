<ul class="dropdown-menu">
	@foreach ($categories as $category)
     	@include('Shop::menu.sub.category-item', ['category' => $category])
    @endforeach
</ul>
