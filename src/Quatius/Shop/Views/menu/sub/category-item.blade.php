    @if ($category->children->count())
<li class="dropdown {{ $category->active?'active':'' }}" data-products="{{$category->product_count}}">
        <a href="{{trans_url($category->url)}}" link-href="{{trans_url($category->url)}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <span>{{$category->page->name}}</span>
        	<span class="caret"></span>
        </a>
        @include('Shop::menu.sub.category-group', ['categories' => $category->children])
    </li>
    @else
    <li class="{{ $category->active?'active':'' }}" data-products="{{$category->product_count}}">
        <a href="{{trans_url($category->url)}}">
            <span>{{$category->page->name}}</span>
        </a>
    </li>
    @endif