@php
	/* Note: to be placed in menu-item override
	  	view('Shop::menu.category',compact(['menu'])) 
	*/
	
	$menu = isset($menu)?$menu:null;;
	$routeReq = request()->route();
	$categoryRoute = $routeReq?$routeReq->getParameter('category_slug'):null;

	$categoryRepo = app('CategoryHandler');
	$root_id = isset($root_id)?$root_id:0;

	$categories = $categoryRepo->getMenuTree($categoryRoute, $root_id);
	$catCacheMenu = "";
    foreach ($categories as $category){
		if ($category->status == 3) continue;	

		$catCacheMenu .= view('Shop::menu.sub.category-item', ['category' => $category]);
	}
@endphp

@if ($menu)
    <li class="dropdown {{$categoryRoute?'active':''}}"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
    <i class="{{$menu->icon}}"></i> <span>{{$menu->name}}</span> <span class="caret"></span>
    </a>
    	<ul class="dropdown-menu">
        	{!!$catCacheMenu!!}
        </ul>
    </li>
@else
	<ul class="nav navbar-nav">
	{!!$catCacheMenu!!}
    </ul>
@endif