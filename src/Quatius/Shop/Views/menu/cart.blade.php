@php
	/* Note: to be placed in menu-item override
	  	view('Shop::menu.cart',compact(['menu'])) 
	*/
	
	\Debugbar::startMeasure('Render cart menu');
	$order = app('OrderSession');
	
	$menu = isset($menu)?$menu:null;
	
	$numProds = $order->total_qty;
	
@endphp

<li class="dropdown cart-menu-item {{ $menu->active?'active':'' }}"><a href="{{url(config('quatius.shop.cart.url_prefix','order/'))}}"> 
<i class="{{$menu->icon}}"></i> {{$menu->name}}<span> <span onclick="$(this).parents('.dropdown').toggleClass('open')" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><span class="badge order-count">{{$order->total_qty}}</span> <span class="caret"></span></span>
</a>

	@include('Shop::order.cart-preview', compact(['order']))
</li>

@php
	\Debugbar::stopMeasure('Render cart menu');
@endphp