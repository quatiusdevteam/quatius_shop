<?php 
$hasFeature = !!trim($product->page->content);
?>
<div>
	
  <ul class="nav nav-tabs" role="tablist">
	@if(config('quatius.shop.admin.product.mode') != 'basic')
		@if($hasFeature)
			<li role="presentation" class="active"><a href="#features" aria-controls="features" role="tab" data-toggle="tab">Features</a></li>
			<li role="presentation"><a href="#specifications" aria-controls="specifications" role="tab" data-toggle="tab">Specifications</a></li>
		@else
			<li role="presentation" class="active"><a href="#specifications" aria-controls="specifications" role="tab" data-toggle="tab">Specifications</a></li>
		@endif
		<li role="presentation"><a href="#warranty" aria-controls="warranty" role="tab" data-toggle="tab">Warranty</a></li>
		@if($product->hasMedia('support'))
			<li role="presentation"><a href="#supports" aria-controls="supports" role="tab" data-toggle="tab">Supports/Manuals</a></li>
		@endif
	@else
		<li role="presentation" class="active"><a href="#features" aria-controls="features" role="tab" data-toggle="tab">Details</a></li>	
	@endif	
	
  </ul>

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane {{$hasFeature?'active':''}}" id="features">
    	@if ($product->page->compiler && ($product->page->compiler == 'blade' || $product->page->compiler == 'php'))
        	{!! renderBlader($product->page->content, ['page' => $page]) !!}
    	@else
    		{!!$product->page->content!!}
    	@endif
    </div>
	@if(config('quatius.shop.admin.product.mode') != 'basic')
		<div role="tabpanel" class="tab-pane {{$hasFeature?'':'active'}}" id="specifications">
			@include('Shop::product.partials.sub.specs')
		</div>
		<div role="tabpanel" class="tab-pane" id="warranty">See Warranty Terms and Conditions</div>
		
		<div role="tabpanel" class="tab-pane" id="supports">
			
			@foreach($product->getMedias('support') as $media)
			
				View <a href="{{media_url($media)}}" target="_blank">{{$media->title}}</a><br>
			@endforeach
		</div>
	@endif
</div>
</div>