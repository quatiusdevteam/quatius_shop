<div class="prod-price prod-price-range" style="width:100%">
@foreach ($product->getPriceRanges() as $rangePrice)
<div class="col-xs-3" style="padding-left: 0px;">
    <h6 class="range"style="font-weight:normal">{{$rangePrice->qty_start}}{{$rangePrice->qty_end?"-".$rangePrice->qty_end:"+"}} Pieces</h6>
    <h5 class="each">
        <span class="price-range" data-start="{{$rangePrice->qty_start}}" data-end="{{$rangePrice->qty_end}}"style="padding-left: 0px;">
        {!! format_currency($rangePrice->getPrice(), 'Shop::currency.format')!!}
        </span>
    </h5>
</div>
@endforeach
@if ($product->getPriceRanges()->last() && $product->getPriceRanges()->last()->qty_end > 0)
    <div class="col-xs-3" style="padding-left: 0px;">
        <h6 class="range" style="font-weight:normal">{{$rangePrice->qty_end}}+ Pieces</h6>
        <h5 class="each" style="font-weight:normal"><a href="{{url('contact-us')}}" target="contact-us">Contact Us</a></h5>
    </div>
@endif
<div class="clearfix"></div>
</div>

@script
<script>
$(document).ready(function(){

    $(document).on( "product-qty-updating", function(evnt, itm){
        var newQty = parseInt($(itm).val());

        var jPrice = $('.prod-price-range .price-range').filter(function() {
            return $(this).attr("data-start") <= newQty && ($(this).attr("data-end") >= newQty || $(this).attr("data-end") == 0);
        });
        
        $(".prod-price .now-price .price-range").remove();

        if (newQty > 1 && jPrice.length){
            $(".prod-price .now-price .sale-price").hide();
            $(".prod-price .now-price").append(jPrice.clone());
        }else{
            $(".prod-price .now-price .sale-price").show();
        }

    });

    $(document).trigger( "product-qty-updating", [$('.cart-btn-area input[name="prod_qty"]')]);
});

</script>

@endscript