@foreach($specList as $spec)
	<div class="input-group col-lg-12 col-md-12 col-xs-12" style="padding: 0px 5px px 0px;">
		<span class="input-group-addon pull-left" style="width: 40%;" >{{$spec->name}}</span>
		@if (count($spec->values) == 1 && array_values($spec->values)[0] == $numOfProds)
			<select class="spec-filter form-control pull-right" disabled style="width: 60%;" onchange="filterSpec(this)" >
					<?php $spec->value = array_keys($spec->values)[0];?>
					<option value="{{$spec->value}}" selected data-amt="{{array_values($spec->values)[0]}}">{{$spec->label}}</option>
			</select>
		@else
		<select class="spec-filter form-control pull-right" style="width: 60%;" onchange="filterSpec(this)" >
			<option></option>
			@foreach($spec->values as $value=>$amt)
				<?php $spec->value = $value;?>
				<option value="{{$value}}" data-amt="{{$amt}}">{{$spec->label}}</option>
			@endforeach
		</select>
		@endif
    </div>
@endforeach