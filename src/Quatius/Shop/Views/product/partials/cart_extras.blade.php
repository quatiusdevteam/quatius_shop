<?php
$order = isset($order)?$order:app('OrderSession');
$orderItem = $order->getProductLineById($product->id);



?>


<div class="cart-extra_area col-xs-12">
@foreach($product->getParams('cart.extras',[]) as $field => $valObj)
    <?php
    if (!$product->getParams('cart.extras.'.$field.'.enable',true)) continue;

    $product->setParams('cart.extras.'.$field.'.value', $orderItem?$orderItem->getParams('cart.extras.'.$field.'.value',''):'');
    
$defaultBlade = <<< EOF
    <div class="input-group input-cart-extra">
        <span class="input-group-addon">{{\$label}}
        </span>
        {!! Form::text(\$name,"")->required()->value(\$value) !!}
    </div>
    <p class="help-block">{!! \$info !!}</p>
EOF;
    $viewParams = json_decode(json_encode($product->getParams('cart.extras.'.$field,[])), true);
    $viewParams["product"] = $product;
    $viewParams["orderItem"] = $orderItem;
    $viewParams["field"] = $field;
    $viewParams["name"]="cart_extras[".$product->getRouteKey()."][".$field."]";
    ?>
    <div class="extra-field" data-field="{{$field}}">
    {!!renderBlade($product->getParams('cart.extras.'.$field.'.blade_script',$defaultBlade),$viewParams)!!}
    
    <div class="clearfix"><br></div>
    </div>
@endforeach
</div>
