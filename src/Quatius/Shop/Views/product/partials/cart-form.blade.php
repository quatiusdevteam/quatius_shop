@if(View::exists('Shop::product.partials.'.$product->type.'.cart-form'))
	@include('Shop::product.partials.'.$product->type.'.cart-form')
@else
	@include('Shop::product.partials.generic.cart-form')
@endif
