<div class="row">
	<div class="col-xs-12 cnr-positions">
		@include('Shop::product.partials.image_extras', ['product'=>$product, 'order'=>$order])
		
	<img class="prod-image img-responsive" src="{!!$product->getImageUrl(615,461,false)!!}">
	</div>
	@if($product->getImages()->count() > 1)
    	@foreach($product->getImages() as $mediaThumb)
    	<div class="col-xs-4 col-md-3 ">
    		<img class="prod-thumb img-responsive img-thumbnail"
    			src="{!!$mediaThumb->mime_type=='image/gif'?$mediaThumb->getImageUrl(615,461,false):$mediaThumb->getThumbnailUrl()!!}">
    	</div>
    	@endforeach
	@endif
</div>
