	<div class="row">
		<div class="col-xs-6">
		<input type="hidden" name="prod_ids[]" value="{{$product->getRouteKey()}}" data-stock="{{$product->stock}}" />
						
		</div>
		
		<div class="col-xs-6">
		<label for="select-quantity">Quantity: </label>
			@if($product->stock <=0)
				<i>Out of stock</i>
			@else
			<br>
			<select id="select-quantity" autocomplete="off" class="form-control" name="prod_qty[]" value="1" required="required">
				<option selected="selected" value="1">1</option>
				@for($q=2; $q < 13; $q++)
					<option value="{{$q}}">{{$q}}</option>
				@endfor
			</select>
			@endif	
		</div>
	</div>