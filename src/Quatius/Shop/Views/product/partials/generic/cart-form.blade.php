@can("order.add_to_cart")
	@if($product->stock <=0 && $product->hasStockControl)
	
		
        <div class="row">
    		<div class="col-md-6 text-right">
    			<div class="input-group">
                  <span class="input-group-addon" for="prod_qty">Quantity: </span>
                  	{!!Form::number('prod_qty')->label('')->disabled()->value('Out of stock')!!}
                </div>
    		</div>
    		<div class="col-md-6 text-right">
    			<button type="submit" disabled class="btn btn-default">Out Of Stock</button>
    		</div>
       	</div>	
    @else
        <form action="" method="post" onsubmit="overrideCartProduct(this); return false;">
        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
        	<input type="hidden" name="prod_id" value="{{$product->getRouteKey()}}" data-stock="{{$product->getMaxPurchase()}}" />
        	<div class="row">
        		<div class="col-md-6 text-right">
        			<div class="input-group">
                      <span class="input-group-addon" for="prod_qty">Quantity: </span>
                            @php
                            	$order = isset($order)?$order:app('OrderSession');
                            @endphp
                      		
                      		<input 	class="form-control" type="number" name="prod_qty"
                          		 	value="{{$order->getProductQty($product->id)}}"
                          		 	pattern="[0-9\/]*" min="0" max="{{$product->getMaxPurchase()}}"/>
                    		
                    </div>
        		</div>
        		<div class="col-md-6 text-right">
        			<button type="submit" data-loading-text="Sending ..." class="btn btn-primary btn-addtocart">ADD TO CART</button>
        		</div>
        	</div>	
        </form>	
    @endif
            	
@endcan