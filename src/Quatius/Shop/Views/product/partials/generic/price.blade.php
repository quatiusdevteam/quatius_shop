@php
	$hasDiscounted = !!($product->price_rrp > $product->price)
@endphp
<span
	class="prod-price {{$hasDiscounted?'discounted':''}}">
	<span class="now-price">{!! format_currency($product->price, 'Shop::currency.format')!!}</span>
	<span class="was-price">

	@if ($hasDiscounted)
    	{!! format_currency($product->price_rrp, 'Shop::currency.format') !!}
	@endif
	</span>
</span>