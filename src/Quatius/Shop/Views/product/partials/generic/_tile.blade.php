<div class="row cnr-positions">
	@include('Shop::product.partials.image_extras', ['product'=>$product, 'order'=>$order])
	<a href="{{trans_url($product->url)}}">
	<img class="prod-thumb img-responsive prod-animate-image" 
		data-failed-img onload="this.removeAttribute('data-failed-img')" style="" 
		src="{{$product->thumb_url}}"></a>
</div>
<div class="row prod-name-area">
<a class="prod-name" href="{{trans_url($product->url)}}">{!!$product->description!!}</a>
</div>
<div class="row text-center">
	<span class="prod-code">{{$product->sku}}</span>
	@can("order.add_to_cart")
		@include('Shop::product.partials.price')
		<div class="btn-area">
		@if($product->stock <=0 && $product->hasStockControl)
			<span class="btn-tile outofstock">Out Of Stock</span>
    	@else
    		<button type="button" onclick="addToCart(this)" data-loading-text="Sending ..." class="btn btn-primary btn-addtocart btn-tile">ADD TO CART</button>
		
    	@endif
		</div>
	@endcan
</div>