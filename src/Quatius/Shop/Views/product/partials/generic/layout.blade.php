<div class="well well-lg spacer">
	<div class="col-sm-6 col-md-6 col-lg-7">

	@include('Shop::product.partials.image')
	
	@include('Shop::product.partials.feature_spec')
	
	
	</div>
	
	
	<div class="col-sm-6 col-md-6 col-lg-5">
		<div style=" float: left; width: 100%;">
		@if($product->stock > 0 && $product->getPriceRanges()->count() > 0)
			@include('Shop::product.partials.price-range')
		@endif
		<h3><span class="prod-name">{!!$product->page->name!!}</span> 
			@if($product->stock > 0 || !$product->hasStockControl)
				@include('Shop::product.partials.price')
			@endif
		</h3>

		<div class="prod-description"><p>
        		{!!$product->page->description!!}
        </p></div>

		@include('Shop::product.partials.cart-form')
		
		<br>
		<div class="text-right">
		<div id="fb-root"></div>
		@script
		<script>
			$(document).ready(function(){
				$('.fb-like').attr('data-width',$('#fb-root').width());
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			});
		</script>
		@endscript
		<div class="fb-like" data-href="{!!Request::fullUrl()!!}" data-width="300" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
		
		</div>
		</div>
	</div>
</div>
