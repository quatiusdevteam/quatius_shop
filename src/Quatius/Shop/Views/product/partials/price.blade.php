@if(View::exists('Shop::product.partials.'.$product->type.'.price'))
	@include('Shop::product.partials.'.$product->type.'.price')
@else
	@include('Shop::product.partials.generic.price')
@endif