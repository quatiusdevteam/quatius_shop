<?php
$showIds = isset($showIds)?$showIds:[];
$setAttrSrc = isset($setAttrSrc)?$setAttrSrc:"src";

$tilePath = view('Shop::product.partials.tile')->getPath();
$tileBlade = file_get_contents($tilePath); // optimus for loading
\Debugbar::startMeasure('Product Views');
?>

@foreach($products as $product)	
	<div class="listed-prod col-xs-6 col-sm-4 col-md-3" {!! ($showIds && array_search($product->id,$showIds)===false)?'style="display:none"' :""!!}>
		{!! renderBlade($tileBlade, ['product' => $product, 'setAttrSrc' => $setAttrSrc]) !!}
	</div>
@endforeach

<?php \Debugbar::stopMeasure('Product Views'); ?>