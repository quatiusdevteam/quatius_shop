@if(View::exists('Shop::product.partials.'.$product->type.'.layout'))
	@include('Shop::product.partials.'.$product->type.'.layout')
@else
	@include('Shop::product.partials.generic.layout')
@endif