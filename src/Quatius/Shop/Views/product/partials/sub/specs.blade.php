@if ($product->specifications(true)->count() > 0)
<table class="table table-striped spec-table" style="width:100% ;margin-bottom: 0px;">

	@foreach($product->specifications(true, true) as $spec)
		@php
			if ($spec->getParams('visible', '1') == 0) continue;
			$spec->labelSeparator = "<br>";
		@endphp
		
		@if($spec->label != "")
    	<tr>
    		<th style="width: 30%">{!! $spec->name !!}</th>
    		<td>{!! $spec->label !!}</td>
    	</tr>
    	@endif
	@endforeach

	<tr>
		<td colspan="2"></td>
	</tr>
</table>
@endif