@if(View::exists('Shop::product.partials.'.$product->type.'.order_selection'))
	@include('Shop::product.partials.'.$product->type.'.order_selection', ['product'=>$product, 'order'=>$order])
@else
	@include('Shop::product.partials.generic.order_selection', ['product'=>$product, 'order'=>$order])
@endif
