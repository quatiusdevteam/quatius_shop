@php
	$order = isset($order)?$order:app('OrderSession');
	$setAttrSrc = isset($setAttrSrc)?$setAttrSrc:"src";
@endphp

<div class="product-tile product-order {{$order->hasProduct($product->id)?'has-ordered':'not-ordered'}} {{$product->isSaved()?'product-saved':'product-unsaved'}}" 
	data-id="{!!$product->getRouteKey()!!}" data-slug="{!!$product->getSlug()!!}" data-sku="{{$product->sku}}"
	data-price="{{$product->price}}"
	data-keyword="{{$product->keyword}}"
	data-spec-values="{{$product->specs}}">
	<div class="well well-sm spacer">
		<div class="row cnr-positions">
    	<a class="product-link" href="{{trans_url($product->url)}}">
    	<img class="prod-thumb img-responsive prod-animate-image" 
    		data-failed-img onload="this.removeAttribute('data-failed-img')" style="" 
    		{{$setAttrSrc}}="{{$product->thumb_url}}"></a>
    </div>
    <div class="row prod-name-area">
    <a class="prod-name product-link" href="{{trans_url($product->url)}}">{!!$product->description!!}</a>
    </div>
    <div class="row text-center">
    	<span class="prod-code">{{$product->sku}}</span>
    	@can("order.add_to_cart")
    		@php
            	$hasDiscounted = !!($product->price_rrp > $product->price)
            @endphp
            <span
            	class="prod-price {{$hasDiscounted?'discounted':''}}">
            	<span class="now-price">
            	
            	<span class="priceSym">{{config('quatius.shop.price.symbol', "$")}}</span><span class="dollars">{{number_format($product->price, 2)}}</span>
            	
            	<span class="was-price">
            
            	@if ($hasDiscounted)
                	<span class="priceSym">{{config('quatius.shop.price.symbol', "$")}}</span><span class="dollars">{{number_format($product->price_rrp, 2)}}</span>
            	@endif
            	</span>
            </span>
    		<div class="btn-area">
    		@if($product->stock <=0 && $product->hasStockControl)
    			<span class="btn-tile outofstock">Out Of Stock</span>
        	@else
        		<button type="button" onclick="addToCart(this)" data-loading-text="Sending ..." class="btn btn-primary btn-addtocart btn-tile">ADD TO CART</button>
    		
        	@endif
    		</div>
    	@endcan
    </div>
	</div>
</div>