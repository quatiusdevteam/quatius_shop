@includeStyle('css/shop/shop-product.css')
@includeScript('js/shop/shop-cart.js')

@php
	$order = isset($order)?$order:app('OrderSession');
@endphp

<div class="product-details product-order {{$order->hasProduct($product->id)?'has-ordered':'not-ordered'}}" data-id="{{$product->getRouteKey()}}" data-slug="{!!$product->getSlug()!!}"
	data-sku="{{$product->sku}}"
	data-price="{{$product->price}}"
	data-keyword="{{$product->keyword}}"
	data-spec-values="{{$product->specs}}">

@include('Shop::product.partials.layout')

</div>