@includeStyle('css/shop/shop-product.css')
@includeScript('js/shop/shop-cart.js')
@php
	if ($products->count() > 0){
	/** Analytics - start */
		Event::fire('analytics.order',[['items'=>productAnalyticsDetails($products,[
				'list'=>$name,
				])],'view', 'impressions']);
	/** Analytics - end */
	}
@endphp
<div class="row">
	<div class="col-xs-12">
		<h1 class="category-name text-center">{{$name}}</h1>
		<div class="category-description text-center well">{{$description}} {!! isset($content)&&trim($content)&&$description?'<br> <br>':''!!} {!!isset($content)&&trim($content)?$content:""!!}</div>
	</div>
	<div class="col-xs-12">
		<div class="col-xs-4">
    		<div class="row">
        		<div id="filterList">
        			<div class="input-group col-lg-12 col-md-12 col-xs-12" style="padding: 0px 5px px 0px;">
    						<span class="input-group-addon pull-left" style="width: 40%;" >Search</span>
    							
    						<input class="search-filter form-control pull-right" style="width: 60%;" onchange="filterSpec(this)" value="{{$search}}"/>
    						<span class="input-group-addon"><i class="fa fa-search"></i></span>
    						
                    </div>
                        
        			{!!$specFilter!!}
        		</div>
    		</div>
    	</div>
		<div class="col-xs-8">
    		<div class="row">
        		<div class="product-list" id="prodList">
        			@include('Shop::product.partials.list', compact('products'))
        		</div>
    		</div>
    	</div>
	</div>
</div>