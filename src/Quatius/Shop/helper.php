<?php
 /**
 *	Shop Helper  

 */

if (!function_exists('productAnalyticsDetails'))
{
    function productAnalyticsDetails($products, $extra = [])
    {
        $callback = function ($prod, $extra){
            $datas = [];
            $posIndex = 0;
            foreach ($prod as $product)
            {
                $posIndex++;

                $analData = [
                    'sku' => $product->sku,
                    'name' => $product->description,
                    'price' => $product->price,
                ];

                if (isset($product->qty))
                    $analData['quantity'] = $product->qty;
                
                if ($product instanceof \Quatius\Shop\Models\OrderItem)
                    $product = $product->getProduct();
                    
                $brand = $product->specifications()->where('id',app('SpecificationHandler')->getSpecIdByName('Brand'))->first();
                
                if ($brand)
                    $analData['brand'] = $brand->label;

                $cateNames = [];

                foreach(app('ProductHandler')->getParentCategory($product) as $cate){
                    $cateNames[] = $cate->page->name;
                }
                
                $analData['category'] = implode("/", $cateNames);
                
                if ($extra){
                    $analData = array_merge($analData, $extra);
                }

                $datas[] = $analData;
            }
            return $datas;
        };
        
        if (collect($products)->first() instanceof \Quatius\Shop\Models\Product)
        {
            $datas = app('ProductHandler')->cache('productAnalyticsDetails', $callback, [$products, $extra]);
        }
        else{ // if orderitem object no cache needed.
            $datas = $callback($products, $extra);
        }

		return $datas;
	}
}


if (!function_exists('orderAnalyticsDetails'))
{
    function orderAnalyticsDetails($order)
    {
		if (!config('quatius.analytics.tracking_id', '')) return [];

		$datas = [
			"transaction_id"=> isset($order->order_number)?$order->order_number:'',
			"description"=> '',
			"currency_code"=> 'AUD',
			"revenue"=> round($order->total,2),
			"shipping"=> round($order->shipping_cost,2),
			"tax"=> round($order->tax_total,2)
		];
		
        if ($order instanceof \Quatius\Shop\Models\Order) 
			$datas['checkout_step_option'] = $order->payment_method;
			
        if (floatval($datas['tax']) == 0) 
            $datas['tax'] = $datas['revenue']/11;
        
        if ($order->getUser()->id)
			$datas['user_id'] = $order->getUser()->id;
		
		
		$datas["items"] = [];
        
        $coupon_code = $order->getParams('discounts.coupon.code','');
        
        if ($coupon_code)
                $datas['coupon']=$coupon_code;
                
		foreach ($order->getProducts() as $orderItem)
		{
			
			$analData = [
                'sku' => $orderItem->sku,
                'name' => $orderItem->description,
                'price' => $orderItem->getPrice(),
                'quantity' => $orderItem->qty,
            ];

            $product = $orderItem->getProduct();

            $brand = $product->specifications()->where('id',app('SpecificationHandler')->getSpecIdByName('Brand'))->first();
            if ($brand)
                $analData['brand'] = $brand->label;

            $cateNames = [];

            foreach(app('ProductHandler')->getParentCategory($product) as $cate){
                $cateNames[] = $cate->page->name;
            }
            
            $analData['category'] = implode("/", $cateNames);

			$datas["items"][] = $analData;
        }
        
		return $datas;
    }
}



if (!function_exists('get_stock'))
{
    
    /**
     * @param integer $product_id
     * @return integer
     */
    function get_stock($product_id){
        
        $qty = app('WarehouseHandler')->getAvailable(config('quatius.shop.domain','AU'))->get($product_id);
        
        return $qty?:0;
    }
}

if (!function_exists('checkBrowser'))
{
    
    /**
     * @param array $onlySupports [['browser_name'=>version]]
     * @return boolean
     */
    function checkBrowser($onlySupports = [], $greaterThan = true){
        
        $browser = new Browser();
        
        foreach ($onlySupports as $name=> $version){
            if ($greaterThan){
                if ($browser->getBrowser() == $name  && $browser->getVersion() >= $version){
                    return true;
                }
            }
            else{
                if ($browser->getBrowser() == $name  && $browser->getVersion() <= $version){
                    return true;
                }
            }
        }
        
        return false;
    }
}

if (!function_exists('plus_tax'))
{
    function plus_tax($price)
    {
        return $price + ($price * config('quatius.shop.tax.percentage', 0));
    }
}

if (!function_exists('tax_amount'))
{
    function tax_amount($price)
    {
        if (config('quatius.shop.tax.percentage', 0) == 0.1)
            return $price/11;
        elseif(config('quatius.shop.tax.percentage', 0) == 0.15)
            return $price * 3 / 23;
        elseif (config('quatius.shop.tax.percentage', 0) == 0)
            return 0;
        else{
            throw new \Exception('TAX '.config('quatius.shop.tax.percentage', 0).' Amount Cannot be calcuated');
        }

        return 0;
    }
}


if (!function_exists('format_currency'))
{
    function format_currency($price, $view='', $options=[])
    {
        $options['symbol'] = config('quatius.shop.price.symbol', "$");
        $options['price'] = number_format($price, 2);
        if (View::exists($view)) {
            return view($view, $options);
        }
        elseif($view){
            return renderBlade($view, $options);
        }
        
        return $price >= 0?$options['symbol'].$options['price']:'-'.$options['symbol'].number_format(abs($price), 2);
    }
}

if (!function_exists('merge_collect'))
{
    function merge_collect($first, $second, $id = 'id')
    {
        $first = $first->groupBy($id);
        $second = $second->groupBy($id);
        
        foreach ($second as $SpecId => $specGrp){
            if ($first->has($SpecId)){
                $first->forget($SpecId);
            }
        }
        
        return collect([$first->collapse(),$second->collapse()])->collapse()->values();
    }
}

if (!function_exists('generateInvoiceNumber'))
{
    function generateInvoiceNumber($prefix = '', $length = 8)
    {
        if ($prefix == ""){
            $prefix = config('quatius.shop.order.prefix', 'OR');
        }
        $token = $prefix.strtoupper(substr(md5(uniqid(rand(), true)),0,$length));
        
        if (\Quatius\Shop\Models\Order::whereOrderNumber($token)->count()) {
            return generateInvoiceNumber($prefix, $length);
        }
        
        return $token;
    }
}

if (!function_exists('lookup'))
{
    function lookup($group, $value=null, $default=""){
        if ($value == null){
            return \Quatius\Shop\Models\Dictionary::group("$group");
        }
        $lookup = \Quatius\Shop\Models\Dictionary::lookup("$group", "$value");
        
        return $lookup?$lookup->label:$default;
    }
}

if (!function_exists('lookup_save'))
{
    function lookup_save($group, $value, $label="", $comment=""){
        if ($value == null || $group == null){
            return false;
        }
        
        \Quatius\Shop\Models\Dictionary::saveDictionary($id.'_'.$value, $attributes["label"], $group.'_'.$id, (isset($attributes["comment"])?$attributes["comment"]:null));
        
        \Quatius\Shop\Models\Dictionary::set($group, $value, $label, $comment);
    }
}

if (!function_exists('category_breadcrumb'))
{
    function category_breadcrumb($breadcrumb, $category_id){
        $categories = app('CategoryHandler')->getParentPaths($category_id);
        $breadcrumb->add('Home', url('/'));
        
        foreach ($categories as $upperCategory)
        {
            $breadcrumb->add($upperCategory->page->name, url($upperCategory->url));
        }
    }
}

if (!function_exists('email_order_confirmaion_to_user'))
{
    function email_order_confirmaion_to_user($order)
    {
        $isAttachedInvoice=config('quatius.shop.email.on-confirm.attach-pdf',true);

        if ($order->getBillingDetail()) {
            if(trim($order->getBillingDetail()->email) != ""){
                Event::fire('email.send-view',["Order No." . $order->order_number . " has been Confirmed",
                    'Shop::order.emails.confirm_to_user',
                    compact('order'),
                    $order->getBillingDetail()->email,
                    config('quatius.shop.email.from'),
                    ($isAttachedInvoice)?$order->getPdfFile():""
                ]);
            }
        }
    }
}

if (!function_exists('email_order_confirmaion_to_admin'))
{
    function email_order_confirmaion_to_admin($order)
    {
        if ($order->getBillingDetail()) {
            Event::fire('email.send-view',[
                "[" . $order->order_number . "], confirmed order by " . $order->getBillingDetail()->getName(),
                'Shop::order.emails.confirm_to_admin',
                compact('order'),
                config('quatius.shop.email.address')
            ]);
        }
    } 
}

if (!function_exists('email_dispatch_to_user'))
{
    function email_dispatch_to_user($order)
    {
        if ($order->getBillingDetail()) {
            if(trim( $order->getBillingDetail()->email) != ""){
                Event::fire('email.send-view',[
                    "Order No." . $order->order_number . " has been Dispatched",
                    'Shop::order.emails.dispatch',
                    compact('order'),
                    $order->getBillingDetail()->email,
                    config('quatius.shop.email.from')
                ]);
            }
        }
    }
}

if (!function_exists('deactiveCoupon'))
{
    function deactiveCoupon($order)
    {
        //retrieve order_number from event OrderStatusChange
        $order_number = $order->order_number;
        
        //retrieve the coupons used for this order
        $coupons = DB::table('orders_coupons')->select('coupon_code')->where('order_number', '=', $order_number)->get();
        
        //for each coupon
        //update the coupon status to expired if this coupon with an extend of 'single use'
        foreach ($coupons as $coupon) {
            try {
                //retrieve extend info according to coupon_code
                $extend_info = DB::table('coupons')->select('extend')->where('coupon_code', '=', $coupon->coupon_code)->first();
                if($extend_info->extend=='single use'){
                    //update the coupon status to expired
                    $updated = DB::table('coupons')->where('coupon_code', '=', $coupon->coupon_code)->update(['status' => 'expired']);
                    if($updated){
                    } else {
                        //if update fails, throw error message
                        throw new Exception('Not able to update coupon status in coupons');
                    }
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}

if (!function_exists('updateProductWarehouse'))
{
    function updateProductWarehouse($order)
    {
        //retrieve order items from order
        $order_item = $order->getProducts();
        foreach($order_item as $item){
            try {
                $selected = DB::table('product_warehouse')->select('ordered')->where('product_id', '=', $item->product_id)->first();
                if($selected){
                    $ordered = $selected->ordered;
                    $ordered = $ordered + $item->qty;
                }else{
                    throw new Exception('Not able to retrieve ordered in product_warehouse');
                }
                $updated = DB::table('product_warehouse')->where('product_id', '=', $item->product_id)->update(['ordered'=>$ordered]);
                if($updated){
                    
                }else{
                    throw new Exception('Not able to update ordered in product_warehouse');
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}

if (!function_exists('email_pending_order_to_manager')){
    function email_pending_order_to_manager($pending_orders){
		if (sizeof($pending_orders)>0 && config('quatius.shop.to.order_team', '') != '') {
            Event::fire('email.send-view',[
                "Pending orders",
                'Shop::order.emails.pending_notification',
                compact('pending_orders'),
                config('quatius.shop.to.order_team'),                config('quatius.shop.email.from')
            ]);
        }
    }
}

/* 
if (!function_exists('addProductTags'))
{
	function addProductTags($products)
	{
		$prodAndTags = [];
		$prodAndTags['products'] = [];
		
		$tags_list = [];
		foreach ($products as $prod){
			$spec1 = $prod->specifications->find(1)->pivot->value;
			$productIds = DB::table('product_specification')->whereSpecificationId('1')->whereValue($spec1)->pluck('product_id');
			
			$filter_tags = DB::table('product_specification')
			->whereIn('product_id', $productIds)
			->selectRaw("DISTINCT(CONCAT(specification_id,'-',value)) as tags_value")
			->pluck('tags_value');
			$tags_list = array_merge($tags_list, array_flip($filter_tags));
			if (Gate::denies('order.bulk_ordering'))
			{
				$spec2 = $prod->specifications->find(2)->pivot->value;
				$filter_tags = array_filter($filter_tags,function($elm) use($spec2){
					if (substr($elm,0,strpos($elm,'-'))=='2')
					{
						if ($elm == "2-".$spec2)
							return true;
						
						return false;
					}
					return true;
				});
			}
			$prod->filter_tags = implode(' ', $filter_tags);
			$prodAndTags['products'][] = $prod;
		}
		
		$search_tags = [];
		foreach (array_keys($tags_list) as $tag)
		{
			
			$keyVal = explode('-', $tag);
			
			$key = $keyVal[0];
			$value = $keyVal[1];
			if (!isset($search_tags[$key]))
				$search_tags[$key]=[];
				
				$search_tags[$key][$value]=$value;
		}
		
		foreach ($search_tags as $key=>$values)
		{
			if ($key == 1)
			{
				$search_tags[$key] = Dictionary::whereGroup('style')->whereIn('value',$values)->orderby('label')->pluck('label','value')->toArray();
			}
			if ($key == 2)
			{
				$search_tags[$key] = Dictionary::whereGroup('colour')->whereIn('value',$values)->orderby('label')->pluck('label','value')->toArray();
			}
			if ($key == 3)
			{
				$search_tags[$key] = Dictionary::whereGroup('size')->whereIn('value',$values)->orderby('value')->pluck('label','value')->toArray();
			}
		}
		$prodAndTags['search_tags'] =$search_tags;
		return $prodAndTags;
	}
}


if (!function_exists('getProductsFromCate'))
{
	function getProductsFromCate($cate = null)
	{
		if (!$cate)
			$cate = new Category(["name"=>"All", "id"=>0]); // root
			
			$rememberKey = "Product::list".$cate->id."-".implode('_',User::getLoginShoppers()).'-'.User::getPriceGroup();
			
			$filterList = Cache::rememberForever($rememberKey, function() use($cate) {
				if ($cate->exists)
					$products = $cate->products(true)->currentShoppers()->status(2)->parentProducts()->with('prices')->orderBy('name')->get();
				else
					$products = Product::currentShoppers()->status(2)->parentProducts()->with('prices')->orderBy('name')->get();
						
				foreach ($products as $product)
				{
					$product->getImage();
				}
				
				return addProductTags($products);
			});
			
		return $filterList;
	}
} */