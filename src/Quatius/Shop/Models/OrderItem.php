<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;
use Quatius\Framework\Traits\ParamSetter;

class OrderItem extends Model
{
	use ParamSetter;
	/* The attributes that are mass assignable.
	 *
	 * @var array
	 */
	private $product = null;

	public $timestamps = false;

	protected $fillable = [
		'product_id',
		'description',
		'sku',
		'barcode',
		'qty',
		'shipped_qty',
		'price',
		'price_ex_tax',
		"discount",
		//"reduce_percentage",
		//"discount_info",
		"tax",
		"total",
		"params"
	];

	public function getPrice($type = '', $display = true)
	{
		return $this->price;
	}

	public function setProduct($product, $qty = 0, $shoppergroups = [])
	{
		$priceObj = $product->getPriceByQty($qty, $shoppergroups);
		$itemdatas = [
			"product_id" => $product->id,
			"description" => $product->description,
			'sku' => $product->sku,
			'barcode' => $product->barcode,
			"qty" => intval($qty),
			"price_ex_tax" => $priceObj->getPrice('no-tax')//$product->getPrice('no-tax', $shoppergroups)
		];
		
		$this->fill($itemdatas);

		$this->price = sprintf("%.2f", plus_tax($this->price_ex_tax));
		$this->tax = round(($this->price - $this->price_ex_tax) * $this->qty, 4);
		$this->total = $this->price * $this->qty;
		
		$this->setParams('price-type', ["price_group"=>$priceObj->price_group, "qty_start"=>$priceObj->qty_start, "qty_start"=>$priceObj->qty_end, "updated_at"=>$priceObj->updated_at]);
		
		if ($product->hasParams('order-item-data', [])) {

			foreach ($product->getParams('order-item-data', []) as $key => $value) {
				$this->setParams($key, $value);
			}
		}
		$this->product = $product;
	}

	public function getProduct()
	{
		if ($this->product == null) {
			$this->product = app('ProductHandler')->find($this->product_id);
		}

		return $this->product;
	}

	public function order()
	{
		return $this->belongsTo(Order::class);
	}

	public function product()
	{
		return $this->belongsTo(app('ProductHandler')->model());
	}
}
