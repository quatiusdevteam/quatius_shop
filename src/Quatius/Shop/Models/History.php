<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{	
    protected $table = 'shop_histories';
    
	protected $fillable = [
	    'status',
	    'value',
	    'comments',
	    'params'
	];
}
