<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;

use Quatius\Framework\Traits\PublishStatus;
use Quatius\Shop\Traits\UseDictionary;
use Quatius\Media\Traits\MediaLink;
use Quatius\Framework\Traits\ParamSetter;

class Specification extends Model
{
    use PublishStatus, UseDictionary, MediaLink, ParamSetter;
    
    protected $fillable = ['name', 'label', 'unit_display', 'search_by', 'unit_type','comment', 'params']; // store in dictionaries table
    
    private static $tempAttr = [];
    
    public $labelSeparator = ', ';

    public static function boot()
    {
        parent::boot();
        
        static::creating(function ($spec) {
            if (!isset($spec->ordering)){
                $spec->ordering =  Specification::max('ordering') + 1;
            }
        });
        
        static::created(function ($spec) {
            if (isset(self::$tempAttr["name"])){
                $spec->name = self::$tempAttr["name"];
            }
        });
        
        static::saving(function($spec){
            
            self::$tempAttr = json_decode(json_encode($spec->getAttributes()),true);
            unset($spec->name);
            unset($spec->label);
            unset($spec->comment);
        });
        
        static::saved(function($spec){
            self::updateValue(self::$tempAttr, $spec->id);
        });
        
        static::deleted(function($spec){
            self::removeDictionary($spec->id, self::getGroupName()."_name");
            self::removeDictionary(null, self::getGroupName().'_'.$spec->id);
        });
    }
    
    public function getValues(){
        $group = self::getGroupName();
        return lookup($group.'_'.$this->id);
    }
    
    public static function updateValue($attributes, $id){
        if (isset($attributes["label"])){
            $group = self::getGroupName();
            $values = lookup($group.'_'.$id);
            
            if (array_search($attributes["label"],$values)!==false)
            {
                $index = array_search($attributes["label"],$values);
                if (isset($attributes["comment"])){
                    self::saveDictionary($index, $attributes["label"], $group.'_'.$id, $attributes["comment"]);
                }

                return $index;
            }
            
            if (count($values) == 0){
                $value = 1;
            }
            else {
                $spec_val = explode('_',array_keys($values)[count($values)-1]);
                $value = intval(count($spec_val)==2?$spec_val[1]:$spec_val[0])+1;
            }

            $valCode = $id.'_'.$value;

            self::saveDictionary($valCode, $attributes["label"], $group.'_'.$id, (isset($attributes["comment"])?$attributes["comment"]:null));
            
            return $valCode;
        }
        
        return null;
    }
    
    public function getIsMultipleAttribute(){
        $attributes = $this->getAttributes();
        if (isset($attributes["value"])){
            return strpos($attributes["value"], ' ') !==false;
        }
        
        return false;
    }
    
    public function setNameAttribute($value){
        if ($this->id){
            self::saveDictionary($this->id, $value, self::getGroupName()."_name");
        }
        $this->attributes['name'] = $value;
    }
    
    public function getNameAttribute(){
        return self::lookupLabel($this->id, self::getGroupName()."_name");
    }
    
    public function getLabelAttribute(){
        $attributes = $this->getAttributes();
        if (isset($attributes["value"])){
            if ($this->isMultiple){
                $valueList = explode(' ', $attributes["value"]);
                $labels = [];
                foreach ($valueList as $val){
                    if ($val == ""){
                        continue;
                    }
                    $labels[] = self::lookupLabel($val, self::getGroupName().'_'.$this->id). ($this->unit_display!=""?$this->unit_display:"");
                }
                
                return implode($this->labelSeparator, $labels);
            }
            if ($attributes["value"] != ""){
                return self::lookupLabel($attributes["value"], self::getGroupName().'_'.$this->id). ($this->unit_display!=""?$this->unit_display:"");
            }
        }
        return "";
    }
    
    public function removeValue($value){
        self::removeDictionary($value, self::getGroupName().'_'.$this->id);
    }
}
