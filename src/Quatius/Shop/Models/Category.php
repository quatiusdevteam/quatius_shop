<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;

use Kalnoy\Nestedset\NodeTrait;
use Quatius\Framework\Traits\PublishStatus;
use Quatius\Media\Traits\MediaLink;
use Quatius\Shop\Traits\LinkPage;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Shop\Traits\ShopperAssignability;
use Quatius\Shop\Traits\HasSpecifications;

class Category extends Model
{
    use LinkPage, ShopperAssignability, ModelFinder, NodeTrait, MediaLink, PublishStatus, HasSpecifications
    {
        specifications as eachSpecifications;
    }
    
	protected $attributes  = [
		'parent_id' => 0,
		'status' => 2 // published
	];
	
	protected $fillable = [
		'status',
	    'slug',
	    'page_id',
		'parent_id'
	];

	public static function boot()
	{
		parent::boot();
		
		static::saving(function($cate){
		    $slugSql = Category::whereSlug($cate->slug);
		    
		    if ($cate->id) {    
		        $slugSql->where('id', '!=' , $cate->id);
			}
			
			if (config('quatius.shop.category.shoppers', [])){
				$slugSql->ofShoppers(config('quatius.shop.category.shoppers', []));
			}

		    $slugExist = $slugSql->count();
		    
		    if ($cate->parent_id != 0)
		    {
		        $parent_slugs = Category::whereId($cate->parent_id)->pluck('slug');
		        if (count($parent_slugs) > 0 && !!$slugExist)
		        {
		            $cate->slug = $parent_slugs[0] .'-'. $cate->slug;
		        }
		    }
		    else if(!!$slugExist)
		    {
		        $cate->slug .= "-".($slugExist + 1);
		    }
		    
		});
	}
	
	public function getUrlAttribute($value)
	{
	    return config('quatius.shop.category.url_prefix','category/').$this->slug;
	}
	
	public function specifications(){
		return $this->eachSpecifications();
	   /*  $cateSpec = app('CategoryHandler')->getSpecs($categories)->groupBy('id');
	    
	    $pageSpecValues = $this->eachSpecifications(false, $this->page)->groupBy('id');
	    
	    $pageSpecValues = $this->eachSpecifications(false, $this->page)->groupBy('id');
	    
	    $prodSpecValues = $this->eachSpecifications(false)->groupBy('id');
	    
	    foreach ($prodSpecValues as $SpecId => $specGrp){
	        if ($pageSpecValues->has($SpecId)){
	            $pageSpecValues->forget($SpecId);
	        }
	    } */
	}
	
	public function addProduct($product_id){
	    $this->products()->sync([$product_id],false);
	}
	
	public function products(){
	    return $this->belongsToMany(Product::class);
	}
	
	public function getAllProducts($status = 2){
	   $query = Product::withCategory()->whereIn('category_id',$this->getSubIds($status))->status($status);
	   return $query->get();
	}
	
	public function getSubIds($status = null){
		$results = $this->descendants;
	    if ($status != null){
	        $results = $results->where('status', $status);
		}
		
		if (config('quatius.shop.category.shoppers', [])){
			$results = app('ShopperHandler')->skipCache()->filterShoppers($results,config('quatius.shop.category.shoppers', []));
		}

		$subCatIds = $results->pluck('id')->toArray();
		
	    $subCatIds[] = $this->id;
	    
	    return $subCatIds;
	}
	
	public function getParentIds($status = null){
		$results = $this->ancestors;
	    if ($status != null){
	        $results = $results->where('status', $status);
		}
		
		if (config('quatius.shop.category.shoppers', [])){
			$results = app('ShopperHandler')->skipCache()->filterShoppers($results,config('quatius.shop.category.shoppers', []));
		}

		$parentCatIds = $results->pluck('id')->toArray();

	    $parentCatIds[] = $this->id;
	    
	    return $parentCatIds;
	}

	public function scopePublished($query){
		$query->where('status', '>', 1);

		return $query;
	}
	
	public function getProductCountAttribute($value)
	{
		// Prefer to be loaded from: app('CategoryHandler')->getFromRoot()->toTree()

		$prodWithin = 0;

		if(isset($this->attributes['product_count'])){
			$prodWithin = $this->attributes['product_count'];
		}
		
		if(isset($this->getRelations()['children'])){
			$prodWithin += $this->children->sum('product_count');
		}

	    return $prodWithin;
	}
}
