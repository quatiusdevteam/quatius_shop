<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    public static function boot()
    {
        parent::boot();
        static::creating(function (Warehouse $item) {
            if (!isset($item->ordering)){
                $item->ordering =  Warehouse::max('ordering') + 1;
            }
        });
    }
    
    protected $fillable = ['name','code','destinations'];
	
	protected $dates = ['created_at','updated_at'];
    
	function products(){
		return $this->belongsToMany(Product::class);
	}
}
