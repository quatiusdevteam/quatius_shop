<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;

use Quatius\Shop\Traits\AddressTypes;
use App\User;
use Litepie\Hashids\Traits\Hashids;

class UserDetail extends Model
{
    use AddressTypes, Hashids;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'company',
		'first_name',
		'middle_name',
		'last_name',
		'phone_1',
		'phone_2',
		'address_1',
		'address_2',
		'city',
		'postcode',
		'state',
		'country',
	];
	
private $overrideSave = null;
	
	public function user(){
		return $this->belongsTo(User::class);
	}
	
	public function overrideSave(User $user)
	{
		$this->overrideSave = $user;
	}
	
	public function isValid(){
		return (!empty($this->address_1));
	}
	
	public function update(array $attributes = [], array $options = [])
	{
		if ($this->overrideSave != null)
			$this->overrideSave->update($attributes ,$options);
		else
			parent::update($attributes ,$options);
	}
	
	public function save(array $options = [])
	{
		if ($this->overrideSave != null)
			$this->overrideSave->save($options);
		else 
			parent::save($options);
	}
}
