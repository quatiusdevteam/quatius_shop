<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Quatius\Media\Traits\MediaLink;
use Illuminate\Database\Eloquent\SoftDeletes;
use Quatius\Framework\Traits\PublishStatus;
use Prettus\Repository\Traits\TransformableTrait;
use Prettus\Repository\Contracts\Transformable;
use Quatius\Shop\Traits\LinkPage;
use Litepie\Revision\Traits\Revision;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Shop\Traits\HasSpecifications;
use Quatius\Shop\Traits\ShopperAssignability;
use DB;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use Event;
use Quatius\Framework\Traits\ParamSetter;
use Quatius\Shop\Traits\UseDictionary;


/**
 * @author seyla.keth
 *
 */
class Product extends Model implements Transformable
{
    use ParamSetter, SingleTableInheritanceTrait, Revision, HasSpecifications, LinkPage, TransformableTrait, MediaLink, ModelFinder, ShopperAssignability, SoftDeletes, PublishStatus, UseDictionary
    {
        specifications as eachSpecifications;
    }
    
    protected $table = "products";
    
    protected static $singleTableTypeField = 'type';
    
    //Can be added in a service provider
    public static $singleTableSubclasses = [ProductReference::class];
    
    protected static $singleTableType = 'generic';
    
    protected $attributes  = array(
        'status' => 3,
    );
    
	protected $fillable = [
		'parent_id',
	    'page_id',
	    'description',
		'type',
		'slug',
		'sku',
		'barcode',
	    'keyword',
	    'specs',
		'params',
		'manufacturer_id',
		'status',
	];
	
	protected $dates = ['deleted_at','created_at','updated_at'];
	
	public static $disableSubclassUpdate = false;
	
	public static function boot()
	{
	    parent::boot();
	    
	    /* Notify the changes to subclasses */
	    static::saved(function($item){
	        if (!self::$disableSubclassUpdate){
    	        foreach (Product::getSingleTableTypeMap() as $class){
    	            app($class)->baseUpdate($item);
    	        }
	        }
	    });
	}
	
	/* use for subclasses to be overriden when base class changes.   
	*/
	public static function baseUpdate($item)
	{
	    
	}
	
	private $display_name = null;
	private static $incReserveStock = false;
	
	public function getDisplayName()
	{
		if (!$this->display_name)
		{
			$this->display_name = $this->description;
		}
		
		return $this->display_name;
	}
	
	public function changeType($type='generic'){
	    if ($this->type != $type && (isset (Product::getSingleTableTypeMap()[$type]))){
	        
	        $this->cleanProductType();
	        
	        DB::table($this->table)
	        ->where('id', $this->id)
	        ->update(['type' => $type]);
	        
	        $fromType = $this->type;
	        
	        $product = Product::find($this->id);
	        $product = $product?:$this;
	        Event::fire('shop.product-type-change',[$product, Product::getSingleTableTypeMap()[$fromType], Product::getSingleTableTypeMap()[$type]]);

	        return $product;
	    }
	    return $this;
	}

	public function syncProductType($data = null, $save=true){
	
    }
    
    public function cleanProductType($save=true){
        
    }
	
	public function getSlug(){
		return $this->slug;
		//return str_replace(['/',' '],'-',strtolower($this->name));
	}
	
	public function getUrlAttribute($value)
	{
	    return config('quatius.shop.product.url_prefix','/product')
	               .$this->getSlug()
	               .config('quatius.shop.product.url_suffix','');
	}
	
	public static function getProdFromSlug($slug){
		return Product::whereSlug($slug)->whereStatus(2)->first();
	}
	
    public function manufacturer(){
        return $this->belongsTo(Manufacturer::class);
    }
    
    public function categories(){
        return $this->belongsToMany(Category::class, 'category_product', 'product_id');
    }
    
    public function scopeWithCategory($query){
    	$query->leftJoin('category_product as cp','id','=','cp.product_id');
    	return $query;
    }
    
    public function specifications($groupByValues = false, $visibleOnly = false){
        
		$cateIdsMap = app('ProductHandler')->getCategoryIdMap(null)->groupBy('id');//ignore shopper group
		
		$combineSpecs = collect([]);
		if ($cateIdsMap->get($this->id)){
			$categories = app('CategoryHandler')->getParentCategories($cateIdsMap->get($this->id)->pluck('category_id')->toArray());
			$combineSpecs = app('CategoryHandler')->getSpecs($categories);
		}
		
		$combineSpecs = merge_collect($combineSpecs, $this->eachSpecifications(false, $this->page));
		$combineSpecs = merge_collect($combineSpecs,  $this->eachSpecifications(false));

		if ($visibleOnly)
			foreach($combineSpecs as $key => $spec)
				if ($spec->getParams('visible', '1') == 0) $combineSpecs->forget($key);

        if (!$groupByValues){
            return $combineSpecs;
        }
        
        return app('SpecificationHandler')->groupValues($combineSpecs);
    }
    
    public function isSaved(){
        return app('WishListHandler')->check($this->sku);
    }
    
    public function getThumbnail(){
        if (isset($this->thumbnail))
            $mediaThumb = $this->thumbnail;
        else
            $mediaThumb = $this->getImages()->first();
        
        return $mediaThumb;
    }
    
    public function getThumbnailUrl($size='thumbnail'){
	    $mediaThumb = $this->getThumbnail();
	    
	    if ($mediaThumb){
			if ($mediaThumb->mime_type == 'image/gif')
				return media_url($mediaThumb);
	        elseif ($size == 'tile'){
	            return media_url($mediaThumb->resizeTo(
					config('quatius.shop.product.thumbnail.width',320),
					config('quatius.shop.product.thumbnail.height',240),
					false, null, false));
	        }
	        else
	            return media_url($mediaThumb->getSize($size));
	    }
	    return "";
	}
	
    public function prices(){
    	return $this->hasMany(ProductPrice::class, 'product_id');
    }
    
    public function warehouses(){
        return $this->belongsToMany(Warehouse::class, 'product_warehouse', 'product_id')->withPivot(['stock', 'low_stock', 'reserve', 'ordered'])->withTimestamps();;
    }
    
	public function getHasStockControlAttribute($value){
	    return true;
	}
	
    public function getStockAttribute($value)
    {
        return isset($this->attributes['stock'])?$this->attributes['stock']:$this->getMaxPurchase();
    }
    
    public function getPriceAttribute($value)
    {
        if(isset($this->attributes['price'])){
            return $this->attributes['price'];
		}
		
		if(isset($this->attributes['price_group']) && isset($this->price_list[$this->attributes['price_group']])){
            return $this->price_list[$this->attributes['price_group']];
		}
		
        return round($this->getPrice(),2);
    }
    
    public function getPriceRrpAttribute($value)
    {
        if(isset($this->attributes['price_rrp'])){
            return $this->attributes['price_rrp'];
		}
		
		$rrpGroup = config('quatius.shop.price.compared-to', 'RRP');
		if(isset($this->price_list[$rrpGroup])){
            return $this->price_list[$rrpGroup];
		}
		
        return round($this->getPriceByGroup($rrpGroup),2);
    }
	
	public function getGroupPrice($group)
    {
		if(isset($this->price_list[$group])){
            return $this->price_list[$group];
		}
		
        return round(0,2);
	}
	
    public function getThumbUrlAttribute($value)
    {
        if(isset($this->attributes['thumb_url'])){
            return $this->attributes['thumb_url'];
        }
        $mediaThumb = $this->getThumbnail();
        if ($mediaThumb)
            return media_url($mediaThumb->resizeTo(config('quatius.shop.product.thumbnail.width',320),config('quatius.shop.product.thumbnail.height',240), false, null, false));
        else
            return "";
    }
    
    public function getPriceObject($shopperGroups = null)
    {
    	if (!$this->exists)
    		return new ProductPrice();
    	
    	if (!$shopperGroups){
    	    $shopperGroups = app('OrderSession')->getShopperGroups();
    	}
    	
    	$price = ProductPrice::getPriceByGroups($this->prices, $shopperGroups);
    	
    	if (!$price){
    		return new ProductPrice();
	    	//throw new \Exception("No price available for '{$this->name}({$this->id})' with '$price_group'");
    	}
    	return $price;
    }
    
    public function getPriceByGroup($price_group, $type='final')
    {
        $price = $this->prices->keyBy('price_group');
        if ($price->has($price_group)){
            return $price->get($price_group)->getPrice($type);
        }
    }
    
    public function getPrice($type='final', $shopperGroups = null)
    {
        return $this->getPriceObject($shopperGroups)->getPrice($type);
    }

	public function getPriceRanges($shopperGroups = null)
    {
		if(config('quatius.shop.price.use-cheapest',true)){
			return $this->prices->where("price_group", "PRICE-RANGE" )->where('price', '<', $this->getPrice('no-tax', $shopperGroups) )->sortBy('qty_start');
		}
		
        return $this->prices->where("price_group", "PRICE-RANGE" )->sortBy('qty_start');
	}

	public function getPriceByQty($qty = 1, $shopperGroups = null)
    {
		if ($qty > 1){
			if ($this->getPriceRanges()->where("qty_start","<=",$qty)->count()){
				$selectedPrice = $this->getPriceRanges()->where("qty_start","<=",$qty)->filter(function($value) use($qty){
					return $value->qty_end > $qty || $value->qty_end == 0;
				})->first();
				
				if ($selectedPrice){
					return $selectedPrice;
				}
			}
		}
		return $this->getPriceObject($shopperGroups);
    }

	public function getMaxPurchase()
    {
		$stock = isset($this->attributes['stock'])?$this->attributes['stock'] : get_stock($this->id);

		$priceRange = $this->getPriceRanges()->sortByDesc('qty_start')->first();
		if ($priceRange && $priceRange->qty_end > 0) return $stock < $priceRange->qty_end-1 ? $stock: $priceRange->qty_end -1;

		return $stock;
    }

    public function getCreatedAtAttribute($date){
    	$date = $date ?: date('Y-m-d H:i:s'); 
    	return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d-m-Y H:i:s');
    }	

    public function getUpdatedAtAttribute($date){
    	if (empty($date)) return;
    	return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d-m-Y H:i:s');
    }
    
    public function scopeSearch($query, $search){
    	$search = trim($search);
    	if(!empty($search)){
    		$query->where('name','like', "%".trim($search)."%")
    			->orWhere('sku','like',"%".trim($search)."%")
    			->orWhere('label_1','like',"%".trim($search)."%");
    	}
    	return $query;
    }
    
    //TODO: implements
    public function scopeParentProducts($query){
        return $query;
    }
    
    public function scopeChildProducts($query){
        return $query;
    }
    
    public function queryChildProducts()
    {
    	
    }
    
    
    public function queryRelatedProducts($specName = 'style')
    {
    	
    }
    
    public function scopeCategoryId($query)
    {
        return $query->leftJoin('category_product', 'id', 'category_product.product_id');
    }
    
    public function scopeCategoryIds($query){
        
		$query->leftJoin(
		    DB::raw('(SELECT GROUP_CONCAT(category_id) as category_ids, product_id FROM category_product group by product_id) AS categs'),
		    'id','categs.product_id'
		);
		
    	return $query;
    }
    
   /*  public function scopeJoinSpecificationValue($query, $specIds=0, $withLabel=false){
    	//Need to Select cos of ambiguous status column.
    	$specList = [];
    	 
    	if (is_array($specIds))
    		$specList = $specIds;
    	else
    		$specList = array($specIds);
    	 
    	foreach ($specList as $specId)
    	{
    		$valueLabel = 'value_'.$specId;
    		$specLabels[] = $valueLabel;
    	
    		$query->join(DB::raw('(select product_id, value as '.$valueLabel.' FROM product_specification WHERE specification_id = '.$specId.') AS spec'.$specId), function($join) use ($specId)
    		{
    			$join->on('id','=','spec'.$specId.'.product_id');
    		});
    		
    		if ($withLabel)
    		{
    			$dataLabel = 'label_'.$specId;
    			$query->leftJoin(DB::raw("(select value as dist_".$valueLabel." ,label as ".$dataLabel." FROM dictionaries) AS dict".$specId),
    					'dist_'.$valueLabel, '=', $valueLabel);
    		}
    	}
    	return $query;
    }
    
    public function scopeGroupBySpecificationValue($query, $specIds=0, $withLabel=false){
    	
    	$this->scopeJoinSpecificationValue($query, $specIds, $withLabel);
    	
    	$specList = [];
    	
    	if (is_array($specIds))
    		$specList = $specIds;
    	else
    		$specList = array($specIds);
    	
    	$specLabels = [];
    	foreach ($specList as $specId)
    		$specLabels[] = 'value_'.$specId;
    	
    	$query->groupBy($specLabels);
    	
    	return $query;
    } */
    public function specSizeValue(){
        $specs = explode(" ", trim($this->specs));
        foreach($specs as $spec){
            if(explode("_", $spec)[0] == 2){
                return self::lookupLabel($spec, 'specification_2');
            }
        }
        return "0";
    }

}
