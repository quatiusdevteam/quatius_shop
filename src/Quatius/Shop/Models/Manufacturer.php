<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    protected $fillable = [
        'name',
        'description',
        'contact_name',
        'contact_email',
        'contact_phone',
        'url'
    ];
    
	public function products(){
		return $this->hasMany(Product::class);
	}
}
