<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Framework\Traits\ParamSetter;
use Quatius\Framework\Traits\PublishStatus;
use Quatius\Shop\Traits\ShopperAssignability;
use Quatius\Shop\Models\Shipments\ShipmentPickUp;
use Quatius\Shop\Models\Shipments\ShipmentCustom;

/**
 * @author seyla.keth
 *
 */
class ShipmentMethod extends Model
{
    use ParamSetter,  SingleTableInheritanceTrait, ModelFinder, ShopperAssignability, SoftDeletes, PublishStatus;
    
    protected $table = "shop_shipment_methods";
    
    protected static $singleTableTypeField = 'type';
    
    //Can be added in a service provider
    public static $singleTableSubclasses = [ShipmentPickUp::class, ShipmentCustom::class];
    
    protected static $singleTableType = '';
    
    protected $attributes  = array(
        'name' => "Shipment",
        'status' => 1,
        'params' => "{}"
    );
    
    protected $fillable = [
        'name',
        'label',
        'description',
        'customer_note',
        'order_note',
        'type',
        'params',
        'status',
        'selectable_by',
        'ordering'
    ];
    
    protected $dates = ['deleted_at','created_at','updated_at'];
    
    protected $mIsHalted = false;
    
    public $mInfoDatas =[];
    
    public static function boot()
    {
        parent::boot();
        
        static::creating(function (ShipmentMethod $shipment) {
            if (!isset($shipment->ordering) || !$shipment->ordering){
                $shipment->ordering =  ShipmentMethod::max('ordering') + 1;
            }
        });
    }
    
    public static function getCalculateDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $unit="M"){
        //ref: https://www.codexworld.com/distance-between-two-addresses-google-maps-api-php/
        
        //Calculate distance from latitude and longitude
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        
        if ($unit == "K") {
            return round($miles * 1.609344, 2);//.' km';
        }
        else if ($unit == "M") {
            return round(($miles * 1.609344) * 1000, 2);//.' m';
        } else if ($unit == "N") {
            return round($miles * 0.8684, 2);//.' nm';
        } else {
            return round($miles, 2);//.' mi';
        }
    }
    
    public static function getDistancePostcode($from, $to, $country='Australia'){
        return self::getDistance(self::getLocation($from, $country), self::getLocation($to, $country));
    }
    
    public static function getDistance($from, $to){
        return self::getCalculateDistance($from['lat'], $from['lng'], $to['lat'], $to['lng']);
    }
    
    private static function getPosLatLng($addressStr="")
    {
        $key = config('quatius.shop.api.google_map','');
        
        $response = self::fetchData(
            "https://maps.googleapis.com/maps/api/geocode/json?".($key?"key=$key&":""),
            'sensor=true&address='. urlencode($addressStr));
        
        $json_a = json_decode($response, true);
        
        if (isset($json_a['results'][0])){
            return $json_a['results'][0]['geometry']['location'];
        }
        else{
            return false;
        }
    }
    
    public static function fetchData($url, $params=""){
        $full_url = $url.$params;
        
        $client = new \GuzzleHttp\Client();
        $response = $client->get($full_url);
        
        return (string) $response->getBody();
    }
    
    public static function getLocation($postcode, $country='Australia'){
        $found = null;
        
        if ($country == 'Australia'){
            //TODO: not yet implemented
        }
        elseif($country == 'New Zealand'){
            $postcodes = trans('Shop::postcode-nz',[], 'messages','en');
            $postcodes = collect($postcodes)->keyBy('postcode');
            $found = $postcodes->get(intval($postcode));
        }
        
        if ($found){
            return $found;
        }
        else{
            return self::getPosLatLng("$postcode, $country");
        }
    }
    
    public static function getDrivingDistance($lat1, $long1, $lat2, $long2)
    {
        //https://maps.googleapis.com/maps/api/distancematrix/json?origins=-37.0011,174.8672&destinations=-39.5002,176.9031&mode=driving&language=pl-PL
        
        $key = config('quatius.shop.api.google_map','');
        
        $response = self::fetchData(
            "https://maps.googleapis.com/maps/api/distancematrix/json?".($key?"key=$key&":""),
            "origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2);
        
        $response_a = json_decode($response, true);
        
        $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
        
        return $dist;
    }
    /**
     * 
     * @return boolean
     */
    
    public function isHalted(){
        return $this->mIsHalted;
    }
    
    public function initalise($order){
        $this->mIsHalted = false;
    }
    
    public function getItemCost($item, $order){
        return null;
    }
    
    public function getTotalCost($order){
        return null;
    }
    
    public function resetItemCost($order){
        
        foreach ($order->getProducts() as $item){
            unset($item->shipCost);
        }
    }
    
    public function getInfoDatas(){
        return $this->mInfoDatas;
    }
    
    public function getVolumetric($product, $checkNull = false)//($length, $width, $height){
    {
        $datas = $product->getParams('pronto_data');
        
        if ($checkNull && (!isset($datas['Width(m)']) || !isset($datas['Height(m)']) || !isset($datas['Length(m)']))){
            return null;
        }
        
        //Length(m), Width(m), Height(m) and Weight(kg)
        $width = isset($datas['Width(m)'])?$datas['Width(m)']:0;
        $height = isset($datas['Height(m)'])?$datas['Height(m)']:0;
        $length = isset($datas['Length(m)'])?$datas['Length(m)']:0;
        
        return round($width * $height * $length, 3);
    }
    
    public function getOrderVolumetric($order, $checkNull = false){
        $totalVol = 0;
        foreach ($order->getProducts() as $item)
        {
            $product = $item->getProduct();
            $prodVol = $this->getVolumetric($product , $checkNull);
            if ($prodVol === null) return null;
            
            $totalVol += $prodVol * $item->qty;
        }
        
        return $totalVol;
    }
    
    
    public function reference($request, $link=""){
        $ext = substr(strtolower($link), strpos($link, '.'), strlen($link));
        
        if ($ext == '.csv' || $ext == '.json'){
            $response = response(file_get_contents(base_path('docs/') . $link), 200);
            $response->header('Content-Type', 'application/json');
            return $response;
        }
        
        return response()->download(base_path('docs/') . $link);
    }
    public function fillRequest($request){
        //$this->setParams('fields', $request->get('fields',[]));
    }
    
    public function editView(){
        return "Shop::admin.shipment.partials.std-edit";
    }
}
