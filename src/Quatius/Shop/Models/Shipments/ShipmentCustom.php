<?php

namespace Quatius\Shop\Models\Shipments;

use Quatius\Shop\Models\ShipmentMethod;

/**
 * @author seyla.keth
 *
 */
class ShipmentCustom extends ShipmentMethod
{
    protected $table = "shop_shipment_methods";
    
    protected static $singleTableType = 'custom';
    
    protected $attributes  = array(
        'name' => "Custom Shipment",
        'label' => 'Custom',
        'description' => "Custom Shipment it's calculations is on php code.",
        'customer_note'=>'',
        'order_note'=>'',
        'status' => 1,
        'type' => 'custom',
        'params' => "{}"
    );
    
    public function initalise($order){

        
        parent::initalise($order);
    }
    
    public function getItemCost($item, $order){
        $execute_item = $this->getParams('execute_item', '');
        $return = null;
        if ($execute_item){
            eval($execute_item);
        }
        
        return $return;
    }
    
    public function getTotalCost($order){
        $execute_total = $this->getParams('execute_total', '');
        $return = null;
        
        if ($execute_total){
            eval($execute_total);
        }
        
        return $return;
    }
    
    public function fillRequest($request){
        if($request->has('selectable_by')){
            $this->selectable_by = $request->get('selectable_by');
        }
        
        if($request->has('execute_item')){
            $this->setParams('execute_item', $request->get('execute_item',''));
        }
        
        if($request->has('execute_total')){
            $this->setParams('execute_total', $request->get('execute_total',''));
        }
        
        if($request->has('process_next')){
            $this->setParams('process_next', $request->get('process_next',''));
        }
        
        if($request->has('admin_edit')){
            $this->setParams('admin_edit', $request->get('admin_edit',''));
        }
        
        if($request->has('admin_save')){
            $this->setParams('admin_save', $request->get('admin_save',''));
        }
        
        $admin_save = $this->getParams('admin_save', $request->get('admin_save',''));
        if ($admin_save){
            eval($admin_save);
        }
    }
    
    public function editView(){
        return "Shop::admin.shipment.partials.custom-edit";
    }
}
    