<?php

namespace Quatius\Shop\Models\Shipments;

use Quatius\Shop\Models\ShipmentMethod;

/**
 * @author seyla.keth
 *
 */
class ShipmentPickUp extends ShipmentMethod
{
    protected $table = "shop_shipment_methods";
    
    protected static $singleTableType = 'pickup';
    
    protected $attributes  = array(
        'name' => "PickUp Shipment",
        'label' => 'Pickup',
        'description' => "Pick up at warehouse",
        'customer_note'=>'Please Note - Allow at least 2-7 business days before collection. Our staff will notify you by phone or email when your TV is ready for collection.',
        'order_note'=>'Pick up address at 178 Boundary Road Braeside VIC 3195',
        'status' => 1,
        'type' => 'pickup',
        'params' => "{}"
    );
    
    public function getItemCost($orderItem, $order){
        return 0;
    }
    
    public function getTotalCost($order){
        return 0;
    }
}