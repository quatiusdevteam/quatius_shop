<?php

namespace Quatius\Shop\Models;

use Quatius\Framework\Traits\HasModelGrouping;

class ProductReference extends Product
{
    use HasModelGrouping;
    
    protected $table = "products";
    
    protected static $singleTableType = 'reference';
    
    protected $syncProductData = [
        'page_id',
        //'manufacturer_id'
    ];
    
    public static function baseUpdate($item)
    {
        $refProd = self::getGroupQuery(ProductReference::class, self::$singleTableType, null, $item->id)->first();
        
        if ($refProd){
            $prodReference = Product::find($refProd->grouptable_id);
            if ($prodReference){
                $prodReference->syncProductType();
            }
        }
    }
    
    public function medias(){ // override Media link
        return app('MediaHandler')->getLinkFrom($this->getProductReference());
    }
    
    public function syncProductType($postData = null, $save=true){
        
        if ($postData && isset($postData['product_id'])){
            $this->setProductReference(hashids_decode($postData['product_id']), false);
        }
        
        $refProd = $this->getProductReference();
        if (!$refProd){
            return;
        }
        
        foreach ($this->syncProductData as $field) {
            $this->$field = $refProd->$field;
        }
        
        $cateIds = $refProd->categories->pluck('id')->toArray();
        $this->categories()->sync($cateIds);
        
        $repo = app('ProductHandler');
        $this->keyword = $repo->generateKeyword($this);
        $this->specs = $repo->getSpecList($this);
        if($save){
            $this->save();
        }
    }
    
    public function cleanProductType($save=true){
        $this->unsetParams('reference_to');
        $this->removeItemGroup(self::$singleTableType);
        $this->status = 3;
        foreach ($this->syncProductData as $field) {
            $this->$field = null;
        }
        $this->save();
    }

    public function specifications($groupByValues = false, $visibleOnly = false){
        if (!$this->getProductReference())
        {
            app('log')->error('Get Product Reference',[$this]);
            return collect();
        }
        $refereceSpecs = $this->getProductReference()->specifications(false);
        
        $repoRepo = app('SpecificationHandler');
        $refurbSpecValues = $this->eachSpecifications(false);
        $combineSpecs = merge_collect($refereceSpecs, $this->eachSpecifications(false));
        
        //check if exist in product class
        $specParent = $repoRepo->getLinksFrom(Product::class, $this->id);
        if ($specParent){
            $combineSpecs = merge_collect($combineSpecs, $specParent);
        }

        if ($visibleOnly)
			foreach($combineSpecs as $key=>$spec)
                if ($spec->getParams('visible', '1') == 0) $combineSpecs->forget($key);
                
        if (!$groupByValues){
            return $combineSpecs;
        }
        
        return $repoRepo->groupValues($combineSpecs);
    }
    
    function setProductReference($product_id, $save=true){
        if (!$product_id){
            return;
        }
        
        $this->setParams('reference_to', $product_id);
        $this->addItemGroup(self::$singleTableType, $product_id);
        if ($save){
            $this->save();
        }
    }
    
    function getProductReferenceId(){
        $refId = $this->getParams('reference_to');
        if (!$refId){
            $refId = $this->getItemGroupData(self::$singleTableType);
        }
        return $refId;
    }
    
    function getProductReference(){
        if (!isset($this->reference_to)){
            $refId = $this->getProductReferenceId();
            $product = app('ProductHandler')->findWhere(['id'=>$refId])->first();
            $this->setRelation('reference_to', $product);
        }
        
        return $this->reference_to;
    }
}
