<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
	protected $attributes  =[
		'price' => 0.00,
		'override' => 0,
		'override_price' => 0.00,
	];
	
	protected $fillable = [
		'price',
		'override',
		'override_price',
		'price_group',
	    'currency',
		'discount_group',
		'qty_start',
		'qty_end'
	];
	
    public function products(){
		return $this->belongsTo(Product::class);
	}
	
	public static function getPriceByGroups($prices , $shoppers)
	{
	    $prices = $prices->keyBy('price_group');
	    
        foreach ($shoppers as $group){
            if($prices->has($group->price_group)){
	            return $prices->get($group->price_group);
	        }
	    }
    	
	    return $prices->get(config('quatius.shop.price.default'));
	}
	
	public function getPrice($type='final')
	{
		$price = $this->price; //'no-tax'
		 
		if ($this->override && $type != 'base'){
			$price = $this->override_price;
		}
		
		if ($type == 'no-tax'){
		    $price = $price;
		}
		else if ($type == 'tax'){
		    $price = plus_tax($price) - $price ;
		}
		else if ($type == 'discount'){
		    $price = plus_tax($this->price - $price);
		}
		else {
		    $price = plus_tax($price);
		}
		return $price;
	}
}
