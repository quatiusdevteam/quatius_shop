<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;
use Quatius\Shop\Traits\AddressTypes;
use Litepie\Hashids\Traits\Hashids;

class OrderDetail extends Model
{
    use AddressTypes, Hashids;
	
	public $timestamps = false;
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	//'sex',
	'address_type',
	'company',
	'first_name',
	'last_name',
	'email',
	'phone_1',
	'address_1',
	'address_2',
	'city',
	'state',
	'country',
	'postcode',
	];
	
	public function getName()
	{
		if ($this->first_name == "" && $this->last_name == "")
			return $this->company;
		else if ($this->company != "")
			return $this->company. ' - '. $this->first_name." ".$this->last_name;
		else
			return $this->first_name." ".$this->last_name;
	}
	
	public function order(){
		return $this->belongsTo(Order::class);
	}
}
