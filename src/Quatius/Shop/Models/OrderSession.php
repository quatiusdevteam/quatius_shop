<?php

namespace Quatius\Shop\Models;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Quatius\Shop\Traits\HasAddressDetails;
use Quatius\Shop\Traits\HasOrderItems;
use Quatius\Payment\Traits\PerformPayment;
use App;
use Event;
use Quatius\Framework\Traits\ParamSetter;

class OrderSession
{
	use HasAddressDetails, HasOrderItems, PerformPayment, ParamSetter {
		getPaymentMethods as getParentPaymentMethods;
		setPaymentMethod as setParentPaymentMethod;
		getPaymentBilling as getParentPaymentBilling;
	}

	protected $sessionKey = 'user-order';
	protected $unsavedBillingDetail;
	protected $unsavedShipmentDetail;
	public $comments = "";
	public $delivery_date = "";
	public $total_qty = 0;
	public $subtotal = 0;
	public $discount = 0;
	public $tax_subtotal = 0;
	public $tax_total = 0;
	public $shipping_cost = 0;
	public $shipping_cost_ex_tax = 0;
	public $total = 0;
	public $currency = '';
	public $errors = [];
	public $shipment_method = null;
	public $params = '{}';

	public $isValidResponse = true;

	protected $fillable = [
		//'prefix_num',
		//'reduce_percentage',
		//'discount_info',
		'params',
		'order_desc',
		'pronto_order_id',
		'cust_reference',
		'delivery_date',
		'comments',
		'order_number',
		'payment_method',
		'subtotal',
		'shipping_cost',
		'shipping_cost_ex_tax',
		'total',
		'tax_total',
		'status',
		'discount',
		'currency'
	];

	protected $productLines = [];

	protected $user = null;
	protected $allowGuest = true;
	protected $allowAddressSaving = true;
	protected $shopperGroupIds = [];
	protected $shopperGroups = [];

	function __construct()
	{
		$this->sessionKey = config('quatius.shop.cart.session', 'user-order');
		$a = func_get_args();
		$i = func_num_args();
		if (method_exists($this, $f = '__construct' . $i)) {
			call_user_func_array(array($this, $f), $a);
		}
	}

	function __construct0()
	{
		$this->__construct1(new Order());
	}

	function __construct1(Order $order)
	{
		$shopperGroups = app('ShopperHandler')->getUserShopperGroups(user());
		$this->__construct2($order, $shopperGroups);
	}

	function __construct2(Order $order, $shopperGroups)
	{
		if ($order == null) {
			$order = new Order();
		}

		$this->setShopperGroups($shopperGroups);

		$this->loadFillableDatas($order->toArray());

		if ($order->exists) {
			$this->copy($order);
		}
	}

	function setShopperGroups($shopperGroups)
	{
		$this->shopperGroups = $shopperGroups->sortByDesc('ordering');
		$this->shopperGroupIds = $this->shopperGroups->pluck('id')->toArray();
	}

	function getShopperGroupIds()
	{
		return $this->shopperGroupIds;
	}

	function getShopperGroups()
	{
		return $this->shopperGroups;
	}

	function loadFillableDatas($data = [])
	{

		foreach ($data as $key => $value) {
			$this->$key = $value;
			$this->fillable[] = $key;
		}
	}

	function toArray()
	{
		$toArray = [];
		foreach ($this->fillable as $key) {
			$toArray[$key] = isset($this->$key) ? $this->$key : '';
		}

		$toArray['products'] = array_values($this->productLines);
		return $toArray;
	}

	public function isValid($response = false)
	{
		$this->errors = [];
		$this->save();
		$this->isValidResponse = true;
		
		if (count($this->productLines) == 0) {
			if ($response) {
				app('flash')->warning(trans('Shop::order.cart-is-empty'));
			}
			$this->isValidResponse = false;
		}else if (!$this->hasBillingDetail()) {
			if ($response) {
				app('flash')->warning(trans('Shop::order.address-required'));
			}
			$this->isValidResponse = false;
		}
		else if(!$this->hasCartExtraRequired($response)) {
			$this->isValidResponse = false;
		}
		else if(!$this->hasStock($response)) {
			$this->isValidResponse = false;
		}
		else if (!$this->isDeliverable($response)) {
			$this->isValidResponse = false;
		}
		else if (auth()->guest() && !$this->allowGuestCheckout()) {
			$this->isValidResponse = false;
		}
		Event::fire('shop.cart-check-valid', [$this, $response, false]);

		return $this->isValidResponse;
	}

	public function isDeliverable($response = false)
	{
		$hasDeliverError = $this->getParams('shipment.error', false);
		foreach ($this->productLines as $item) {
			if (!isset($item->shipCost) || $item->shipCost === null) {
				$this->errors[$item->product_id]['deliverable'] = false;
				$hasDeliverError = true;
			}
		}

		if ($this->hasError() || $hasDeliverError) {
			if ($response && $hasDeliverError) {
				app('flash')->message($this->getParams("shipment.error_msg",trans('Shop::order.product-not-deliverable')), $this->getParams("shipment.error_type", "warning"));
			}
			return false;
		}
		return true;
	}
public function hasStock($response = false)
	{
		$hasStockError = false;
		$hasDestinationError = false;
		$hasPriceError = false;
		$shipAddr = $this->getShipmentDetail();

		foreach ($this->getProducts() as $item) {
			$stock = $item->getProduct()->stock;
			if ($item->qty > $stock) {
				$hasStockError = true;
				$this->errors[$item->product_id]['stock'] = $stock;
			}

			if (!$item->hasParams('prices') && (!$item->getProduct()->getPriceObject($this->getShopperGroups())->exists
				|| $item->total <= 0)) // price cannot be zero
			{
				$hasPriceError = true;
				$this->errors[$item->product_id]['price'] = false;
			}

			if ($shipAddr && $shipAddr->country) {
				$wareDest = app('WarehouseHandler')->getProductQty($item->product_id, $shipAddr->country, $shipAddr->state);
				if ($wareDest->count() > 0) {
					if ($wareDest->sum('available') < $item->qty) {
						$hasDestinationError = true;
						$this->errors[$item->product_id]['destination'] = $shipAddr->state;
					}
				} else {
					$hasDestinationError = true;
					$this->errors[$item->product_id]['destination'] = $shipAddr->state;
				}
			}
		}

		if ($this->hasError()) {
			if ($response && $hasPriceError) {
				app('flash')->warning(trans('Shop::order.product-no-price'));
			}
			if ($response && $hasStockError) {
				app('flash')->warning(trans('Shop::order.product-out-of-stock'));
			}
			if ($response && $hasDestinationError) {
				app('flash')->warning(sprintf(trans('Shop::order.product-not-available-destination'), $shipAddr->state));
			}
			return false;
		}
		return true;
	}

	public function hasCartExtraRequired($response = false)
	{
		$hasExtraError = false;
		foreach ($this->getProducts() as $item) {
			$product = $item->getProduct();

			foreach($product->getParams('cart.extras',[]) as $field => $valObj){
				if (!$product->getParams('cart.extras.'.$field.'.enable',true)) continue;

				if ($product->getParams('cart.extras.'.$field.'.required', true) && !$item->getParams('cart.extras.'.$field.'.value')){
					$this->errors[$item->product_id]['extra-field'] = $field;
				}
			}
		}

		if ($this->hasError()) {
			if ($response && $hasExtraError) {
				app('flash')->warning(trans('Shop::order.product-field-require'));
			}
			return false;
		}
		return true;
	}

	public function hasError($ref = null)
	{
		if ($ref) {
			if (isset($this->errors[$ref])) {
				return $this->errors[$ref];
			}
			return false;
		}
		return !!$this->errors;
	}

	public function addProductById($id, $qty = 0, $recalcPrice = true, $appendQty = false, $extras = [])
	{
		return $this->addProduct($this->getProductById($id), $qty, $recalcPrice, $appendQty, $extras);
	}

	public function getProductCount()
	{
		return $this->total_qty;
	}

	public function getCurrency()
	{
		return $this->currency;
		//if (!$this->getParams('shipment.free',false)){
	}

	private function sumProducts()
	{
		$this->subtotal = 0;
		$this->total_qty = 0;
		$this->tax_subtotal = 0;

		foreach ($this->productLines as $pid => $orderItem) {
			$this->total_qty += $orderItem->qty;
			$this->subtotal += $orderItem->total;
			$this->tax_subtotal += $orderItem->tax;
		}
	}

	public function calculatePrice()
	{
		Event::fire('shop.cart-calculating-price', [$this]);

		$this->sumProducts();

		Event::fire('shop.cart-calculated-subtotal', [$this]);

		$this->calcTotal();

		$this->save();
	}

	public function calcDiscount()
	{
		Event::fire('shop.cart-calculating-discount', [$this]);
		$this->discount = 0;

		//{"name":"Discount (%5)","value":0,"amount":5,"type":"percentage","min_order":50,"warning":""}}

		foreach ($this->getParams('discounts', []) as $key => $discount) {
			$this->setParams("discounts.$key.warning", "");

			if ($this->getParams("discounts.$key.type", "fix") == "fix") {
				$this->setParams("discounts.$key.value", number_format(floatval($this->getParams("discounts.$key.amount", 0)), 2));
			} else if ($this->getParams("discounts.$key.type", "fix") == "percentage") {
				$this->setParams("discounts.$key.value", number_format($this->subtotal * (floatval($this->getParams("discounts.$key.amount", 0) / 100)), 2));
			} else {
				$this->setParams("discounts.$key.value", 0);
			}

			if ($this->subtotal < floatval($this->getParams("discounts.$key.min_order", 0))) {
				$this->setParams("discounts.$key.value", 0);
				$this->setParams("discounts.$key.warning", "Minimum purchase of $" . $this->getParams("discounts.$key.min_order", 0) . " required.");
			}

			if ($this->getParams("discounts.$key.value", 0) > $this->subtotal)
				$this->setParams("discounts.$key.value", $this->subtotal);

			$this->discount += floatval($this->getParams("discounts.$key.value", 0));
		}
	}

	public function calcTotal()
	{
		$this->calcDiscount();

		$this->shipping_cost_ex_tax = $this->getShippingCost();
		$this->shipping_cost = round(plus_tax($this->shipping_cost_ex_tax), 2);

		$this->total = ($this->subtotal - $this->discount + $this->shipping_cost);

		$this->tax_total = round(tax_amount($this->total), 2);

		Event::fire('shop.cart-calculated-total', [$this]);
	}

	public function getSubTotal($taxOnly = false)
	{
		if ($taxOnly) {
			if (!$this->tax_subtotal) {
				$this->sumProducts();
			}
			return $this->tax_subtotal;
		}

		return $this->subtotal;
	}

	public function getTotal($taxOnly = false)
	{
		if ($taxOnly) {
			return $this->tax_total;
		}

		return $this->total;
	}

	public function updateShippingCost($reset = false)
	{

		$this->shipping_cost_ex_tax = $this->getShippingCost($reset);
		$this->shipping_cost = round(plus_tax($this->shipping_cost_ex_tax), 2);

		$this->calcTotal();
	}

	public function getShippingCost($reset = false)
	{
		$shipRepo = app('ShipmentHandler');
		$this->unsetParams('shipment.error');
		if ($reset) $shipRepo->resetItemCost($this);

		$cost = $shipRepo->getCost($this);
		if (!$this->getParams('shipment.free', false)) {
			$this->unsetParams('shipment.free');
		}
		if ($this->shipment_method)
			$this->setParams('shipment.datas', $this->shipment_method->getInfoDatas());
		return $cost;
	}

	public function addProduct($product, $qty = 0, $recalcPrice = true, $appendQty = false, $extras = [])
	{
		if (!$product)
			return;

		$price = 0;

		try {
			$price = $product->getPrice('no-tax', $this->getShopperGroups());
		} catch (\Exception $e) {
			return;
		}
		if (!$this->currency) {
			$this->currency = $product->getPriceObject($this->getShopperGroups())->currency;
		}

		//only postive number
		$qty = $qty > 0 ? $qty : 0;

		if ($appendQty) {
			$qty = isset($this->productLines[$product->id]) ? $this->productLines[$product->id]->qty + $qty : $qty;
		}

		$qty = $qty > $product->stock ? $product->stock : $qty;

		$orderItem = new OrderItem();

		$orderItem->setProduct($product, $qty, $this->getShopperGroups());
		
		foreach($extras as $field=>$value){
			$orderItem->setParams('cart.extras.'.$field.'.label',  $product->getParams('cart.extras.'.$field.'.label', $field));
			$orderItem->setParams('cart.extras.'.$field.'.value', $value);
		}

		$this->productLines[$product->id] = $orderItem;

		if ($recalcPrice) {
			$this->calculatePrice();
		}
		Event::fire('shop.cart-add-product', [$this, $orderItem]);

		return $orderItem;
	}

	public function removeZeroQty()
	{
		foreach ($this->productLines as $pid => $orderItem) {
			if ($orderItem->qty == 0)
				unset($this->productLines[$pid]);
		}
	}

	public function removeProduct($id)
	{
		if (!isset($this->productLines[$id])) return null;

		$productLine = $this->productLines[$id];
		unset($this->productLines[$id]);
		$this->calculatePrice();

		Event::fire('shop.cart-remove-product', [$this, $productLine]);
		return $productLine;
	}

	public function hasProduct($id)
	{
		return isset($this->productLines[$id]) || isset($this->productGroup[$id]);
	}

	public function getProductLineById($id)
	{
		return isset($this->productLines[$id]) ? $this->productLines[$id] : null;
	}

	public function getProductQty($id, $default = 1)
	{
		return isset($this->productLines[$id]) ? $this->productLines[$id]->qty : $default;
	}

	public function getProductById($id)
	{
		$this->applyCustomer();

		$prod = null;
		if (isset($this->productLines[$id])) {
			$prod = $this->productLines[$id]->getProduct();
			if ($prod) {
				$prod->qty = $this->productLines[$id]->qty;
				return $prod;
			}
		}

		$prod = app('ProductHandler')->find($id);
		if (!$prod->isBelongToShopper($this->getShopperGroupIds())) {
			app('log')->warning('Adding product not in shoppergroup', [$id, $this->getShopperGroupIds()]);
			return null;
		}
		if ($prod)
			$prod->qty = 0;
		return $prod;
	}

	public function getProducts($includeSubs = true)
	{
		if (!$includeSubs) return $this->productLines;

		$allProducts = [];
		// $bundle = ["products"=>[["sku"=>"ABTS200BL","qty"=>1],["sku"=>"T2IM180GK"]],"price-label"=>"FREE","type"=>"Buy one get one free"]
		$repo  = app('ProductHandler');
		foreach ($this->productLines as $item) {
			$allProducts[] = $item;
			if ($item->hasParams('bundle')) {
				foreach ($item->getParams('bundle.products', []) as $iSub) {
					$product = $repo->findWhere(['sku' => $iSub["sku"]])->first();
					$orderItem = new OrderItem();
					$subQty = isset($iSub["qty"]) ? $iSub["qty"] : 1;
					$qty = $item->qty * $subQty;
					$orderItem->setProduct($product, $qty, $this->getShopperGroups());

					$orderItem->setParams("prices.total", $orderItem->total);
					$orderItem->setParams("prices.price", $orderItem->price);
					$orderItem->setParams("prices.tax", sprintf("%.4f", $orderItem->tax));
					$orderItem->setParams("prices.ex_tax", $orderItem->price_ex_tax);

					$orderItem->setParams("price-label", $item->getParams('bundle.price-label', ''));

					// set to zero price for free/bundle item
					$orderItem->setProduct($product, 0, $this->getShopperGroups());
					$orderItem->price = sprintf("%.2f", 0);
					$orderItem->tax = sprintf("%.2f", 0);
					$orderItem->price_ex_tax = sprintf("%.2f", 0);
					$orderItem->qty = $qty;

					$allProducts[] = $orderItem;
				}
			}
		}

		return $allProducts;
	}

	public function isGuest()
	{
		return !$this->getBillingDetail()->user_id;
	}

	public function details()
	{
		return [$this->unsavedBillingDetail, $this->unsavedShipmentDetail];
	}

	public function clearAddressDetails($shipOnly = false)
	{
		if (!$shipOnly)
			$this->unsavedBillingDetail = null;

		$this->unsavedShipmentDetail = null;
		$this->updateShippingCost(true);
	}

	public function hasBillingDetail()
	{
		return !!$this->unsavedBillingDetail;
	}

	public function hasShipmentDetail()
	{
		return !!$this->unsavedShipmentDetail;
	}

	public function getOrderSessionKey()
	{
		return $this->sessionKey;
	}

	public function setOrderSessionKey($sessionKey = 'user-order')
	{
		$this->sessionKey = $sessionKey;
	}

	public function getBillingDetail()
	{
		return $this->unsavedBillingDetail ?: new OrderDetail();
	}

	public function getShipmentDetail($address_id = 0)
	{
		return $this->unsavedShipmentDetail ?: $this->getBillingDetail();
	}

	public function setBillingDetail($detail)
	{
		if (is_array($detail)) {
			$detail = new OrderDetail($detail);
		}

		$detail->setAsBillingAddress();
		$detail->account = $this->getShopperGroups()->first()->account;

		if ($this->getCustomer()->getParams('account_code', '')) {
			$detail->account = $this->getCustomer()->getParams('account_code', '');
		}

		$tmpAddr = $this->unsavedBillingDetail;
		$this->unsavedBillingDetail = $detail;

		if ($this->unsavedShipmentDetail == null) {
			$this->updateShippingCost(true);
		}

		Event::fire('shop.cart-add-billing-addr', [$this, $tmpAddr]);
		$this->save();
		return $this;
	}

	public function setShipmentDetail($detail = null)
	{
		if ($detail == null) {
			if (!$this->unsavedShipmentDetail) {
				Event::fire('shop.cart-remove-shipping-addr', [$this]);
			}

			$this->unsavedShipmentDetail = null;
		} else {
			if ($detail instanceof Collection) {
				return false;
			}
			if (is_array($detail)) {
				$detail = new OrderDetail($detail);
			}

			if (!$this->hasBillingDetail()) {
				$this->setBillingDetail($detail);
				return $this;
			}

			$detail->setAsShippmentAddress();

			$tmpAddr = $this->unsavedShipmentDetail;
			$this->unsavedShipmentDetail = $detail;
			Event::fire('shop.cart-add-shipping-addr', [$this, $tmpAddr]);
		}
		$this->updateShippingCost(true);
		$this->save();
		return $this;
	}

	public function setShipmentParam($shipment) // for order on behalf of customer
	{
		$this->setParams('shipment.id', $shipment->id);
		$this->setParams('shipment.type', $shipment->type);
		$this->setParams('shipment.label', $shipment->label);
		if (trim($shipment->order_note)) {
			$this->setParams('notes.shipment_note', trim($shipment->order_note));
		} else {
			$this->unsetParams('notes.shipment_note');
		}
	}

	public function setShipmentMethod($shipment) // for order on behalf of customer
	{
		if (is_scalar($shipment)) {
			$shipmentList = app('ShipmentHandler')->getByShoppers($this->getShopperGroupIds());
			$shipment = $shipmentList->where('id', $shipment)->first();
		}

		if ($shipment) {
			$this->setShipmentParam($shipment);

			if ($this->shipment_method && ($this->shipment_method->id != $shipment->id)) {
				app('ShipmentHandler')->resetItemCost($this);
			}
			$this->shipment_method = $shipment;
		}
	}

	public function getSelectableShipments()
	{
		$shipmentList = app('ShipmentHandler')->getByShoppers($this->getShopperGroupIds());
		return $shipmentList->whereIn('selectable_by', ['default', 'user']);
	}

	public function getShipmentMethod() // for order on behalf of customer
	{
		if (!$this->shipment_method) {
			$shipmentList = app('ShipmentHandler')->getByShoppers($this->getShopperGroupIds());

			if ($this->getParams('shipment_id', 0)) {
				$shipmentMethod = $shipmentList->where('id', $this->getParams('shipment_id', 0))->first();
			} else { // autoselect
				$shipmentMethod = $shipmentList->where('selectable_by', 'default')->first();
				if (!$shipmentMethod) {
					$shipmentMethod = $shipmentList->where('selectable_by', 'user')->first();
				}
			}

			$this->setShipmentMethod($shipmentMethod);
		}

		return $this->shipment_method;
	}

	public function setUser($user, $shoppers = null) // for order on behalf of customer
	{
		$this->user = $user;

		if ($shoppers) {
			$this->setShopperGroups($shoppers);
		} else {
			$this->setShopperGroups(app('ShopperHandler')->getUserShopperGroups($user));
		}
	}

	public function setCustomer($user, $shoppers = null) // for order on behalf of customer
	{
		$this->setUser($user, $shoppers);

		$this->applyCustomer();
		$this->loadCustomerDetail($this->user);
		$this->reloadProducts();

		Event::fire('shop.cart-setting-customer', [$this, $user]);
		//User::setLoginUser(null);
	}

	public function getCustomer()
	{
		if (!$this->user) {
			$this->setCustomer(user());
		}

		return $this->user;
	}

	public function getCustomerId()
	{
		if ($this->user)
			return $this->user->id;
		return 0;
	}

	public function setAllowGuest($canCheckout = true)
	{
		$this->allowGuest = $canCheckout;
	}

	public function allowGuestCheckout()
	{
		return $this->allowGuest;
	}

	public function allowAddressSaving($canSave = null)
	{
		if ($canSave !== null)
			$this->allowAddressSaving = $canSave;

		return $this->allowAddressSaving;
	}

	public function applyCustomer()
	{
		Event::fire('shop.cart-apply-customer', [$this]);
	}

	public function loadCustomerDetail($user)
	{
		$billingAddress = $user->id == 0 ? null : \Quatius\Shop\Models\UserDetail::whereUserId($user->id)->billings()->first();
		$shipmentAddresses = $user->id == 0 ? null : \Quatius\Shop\Models\UserDetail::whereUserId($user->id)->shipments()->first();

		if ($billingAddress) {
			$this->unsavedBillingDetail = null;
			$this->setBillingDetail(new OrderDetail($billingAddress->toArray()));
			$this->getBillingDetail()->user_id = $user->id;
			$this->getBillingDetail()->email = $user->email;
		}
		if ($shipmentAddresses) {
			$this->unsavedShipmentDetail = null;
			$this->setShipmentDetail(new OrderDetail($shipmentAddresses->toArray()));
		}
		$this->save();
	}

	public function reloadProducts()
	{
		if ($this->productLines) {
			$tmpItems = $this->productLines;

			$this->productLines = [];
			$prods = Product::whereIn('id', array_keys($tmpItems))->ofShoppers($this->getShopperGroupIds())->get()->keyBy('id');
			foreach ($tmpItems as $id => $prodLine) {
				$newProd = $prods->get($id);
				if ($newProd)
					$this->addProduct($newProd, $prodLine->qty, false); // clear qty and disc
			}
		}
		$this->calculatePrice();
	}

	public function update(array $attributes = array(), array $options = array())
	{
		//$this->fill($attributes);
	}

	public function getOrderFromDatabase($status = null)
	{
		if (isset($this->order_number) && $this->order_number != "") {
			$orderQuery = Order::whereOrderNumber($this->order_number);
			if ($status) {
				$orderQuery->whereStatus($status);
			}

			return $orderQuery->first();
		}

		return null;
	}


	public function saveToDatabase($status = 'pending')
	{
		$this->storeToSession();
		$this->setParams('shopper_group_ids', $this->getShopperGroupIds());
		$order = $this->getOrderFromDatabase($status);

		if ($order) {
			if (count($this->getProducts()) < $order->items->count()) {
				for ($i = count($this->productLines); $i < $order->items->count(); $i++) {
					if ($order->items->get($i))
						$order->items->get($i)->delete();
				}
				$order->load('items'); // refresh
			}
		} else {
			$order = new Order();
		}

		$this->status = $status;

		foreach ($this->fillable as $key) {
			if (isset($this->$key)) {
				$order->$key = $this->$key;
			}
		}

		if ($order->delivery_date == "")
			$order->delivery_date = null;

		//total discount not in the fillable list
		//save total discount to order->discount seperately
		if (isset($this->discount))
			$order->discount = $this->discount;

		// don't save zero discount
		foreach ($order->getParams('discounts', []) as $key => $discount) {
			if ($order->getParams("discounts.$key.value", 0) <= 0)
				$order->unsetParams("discounts.$key");
		}

		Event::fire('shop.cart-saving', [$this, $order]);
		try {
			$order->save();
		} catch (\Exception $e) {
			$this->order_number = null;
			$order->order_number = null;
			$order->save();
		}

		if (isset($this->order_number) && $this->order_number != null) {
			$order->order_number = $this->order_number;
		} else {
			$this->order_number = $order->order_number;
		}

		$this->save();
		self::setSavedOrder($this->getOrderSessionKey());

		$index = 0;
		foreach ($this->getProducts() as $orderItem) {
			if ($order->items->get($index))
				$order->items->get($index)->update($orderItem->toArray());
			else {
				$order->items()->save(new OrderItem($orderItem->toArray()));
			}
			$index++;
		}

		if ($this->hasBillingDetail()) {
			$this->unsavedBillingDetail->user_id = $this->getCustomerId()?:null;
			$order->setBillingDetail($this->unsavedBillingDetail);
		}

		if ($this->hasShipmentDetail()) {
			$this->unsavedShipmentDetail->user_id = $this->getCustomerId()?:null;
			$order->setShipmentDetail($this->unsavedShipmentDetail);
		}

		Event::fire('shop.cart-saved', [$this, $order]);
		return $order;
	}

	public function getUser()
	{
		if (!$this->hasBillingDetail() || $this->getBillingDetail()->user_id == 0) {
			return guest_user();
		}

		$user = User::find($this->getBillingDetail()->user_id);
		if ($user)
			return $user;
		else
			return guest_user();
	}

	public function clearOrderHeader()
	{
		if (isset($this->id)) {
			$this->ref_id = $this->id;
			$this->id = null;
		}

		$this->order_number = null;
		$this->cust_reference = null;
		$this->status = 'pending';
		$this->comments = "";
		$this->delivery_date = "";
		$this->total_qty = 0;
		$this->subtotal = 0;
		$this->discount = 0;
		$this->tax_total = 0;
		$this->shipping_cost = 0;
		$this->total = 0;
		$this->shipment_method = null;
		$this->params = '{}';
		$this->calculatePrice();
		$this->save();
	}

	public function resetOrder($keepOrderNumber = true)
	{
		if (!$keepOrderNumber) {
			$this->removeFromSession();
			return;
		}

		$this->productLines = [];
		$keepOrderNo = isset($this->order_number) ? $this->order_number : null;
		$this->clearOrderHeader();
		if ($keepOrderNumber && $keepOrderNo) {
			$this->order_number = $keepOrderNo;
		}
		$this->setCustomer($this->getCustomer());

		$this->save();
	}

	public function copy($origOrder, $withItems = true, $withCust = true)
	{
		if ($withItems) {
			foreach ($origOrder->items()->with('product')->get() as $item) {
				$this->addGroupProduct($item->product);
				$this->addProduct($item->product, $item->qty, false);
			}
		}

		if ($withCust) {
			foreach ($origOrder->details as $detail) {
				if ($detail->isBillingAddress())
					$this->setBillingDetail(new OrderDetail($detail->toArray()));
				else
					$this->setShipmentDetail(new OrderDetail($detail->toArray()));
			}
		}
	}

	public function save($msg = "")
	{
		$this->storeToSession();
	}

	public function removeFromSession()
	{
		self::clearOrder($this->sessionKey);
	}

	protected function storeToSession()
	{
		Event::fire('shop.cart-saved', [$this]);
		session([$this->sessionKey => $this]);
		//Session::save(); // write to session immediately 
	}

	public static function hasOrder($sessionKey = 'user-order')
	{
		return session()->has($sessionKey);
	}

	public static function setSavedOrder($sessionKey = null)
	{
		session(['lastSavedOrder' => $sessionKey ?: 'user-order']);
		//Session::save(); // write to session immediately
	}

	public static function recentSavedOrder()
	{
		return self::getOrder(null, session('lastSavedOrder',  config('quatius.shop.cart.session', 'user-order')));
	}

	public static function getOrder($order = null, $sessionKey = null)
	{
		if (!$sessionKey) {
			$sessionKey = config('quatius.shop.cart.session', 'user-order');
		}

		$storeOrder = session($sessionKey, null);

		if ($storeOrder != null) {
			if ($storeOrder->getOrderSessionKey() == $sessionKey)
				return $storeOrder;
		}

		if ($order == null)
			$orderSession = new OrderSession();
		else
			$orderSession = new OrderSession($order);

		$orderSession->setOrderSessionKey($sessionKey);
		$orderSession->save();

		Event::fire('shop.cart-created', [$orderSession]);
		return session($sessionKey, null);
	}

	public static function clearOrder($sessionKey = null)
	{
		$selKey = $sessionKey ?: 'user-order';

		$storeOrder = session($selKey, null);
		Event::fire('shop.cart-clearing', [$selKey, $storeOrder]);

		session()->forget($selKey);
		//Session::forget($selKey);
		if (session('lastSavedOrder', 'user-order') == $selKey) {
			session()->forget('lastSavedOrder');
		}

		App::forgetInstance('OrderSession');
		//Session::forget('lastSavedOrder');
		//Session::save(); // write to session immediately
	}

	protected $productGroup = [];

	public function getProductGroups()
	{
		return $this->productGroup;
	}

	public function getProductGroup($id)
	{
		if (isset($this->productGroup[$id]))
			return $this->productGroup[$id];
		return null;
	}

	public function addGroupProduct($product)
	{
		$this->productGroup[$product->id] = $product;
		$this->save();
	}

	public function removeGroupProduct($product)
	{
		$childIds = $product->queryChildProducts()->pluck('id')->toArray();

		foreach ($childIds as $prodId) {
			$this->removeProduct($prodId);
		}

		$tmpList = $this->productGroup;
		$this->productGroup = [];
		foreach ($tmpList as $prod) {
			if ($product->id == $prod->id)
				continue;

			$this->addGroupProduct($prod);
		}

		$this->save();
	}

	protected $presenter = null;

	public function setPresenter($presenter)
	{
		$this->presenter = $presenter;
		return $this;
	}

	public function parserResult()
	{
		if ($this->presenter) {
			return $this->presenter->present($this);
		}

		return $this;
	}

	public function getPaymentMethods()
	{
		$shopperIds = $this->getShopperGroupIds();

		$payments = app('ShopperHandler')->filterShoppers($this->getParentPaymentMethods(), $shopperIds);

		if (!$this->_payment_method) {
			$this->setPaymentMethod($payments->first());
			if ($this->hasParams('payment.id')) {
				$this->setPaymentById($this->getParams('payment.id', 0));
			}
		}
		return $payments;
	}

	public function setPaymentMethod($payment)
	{
		$this->setParentPaymentMethod($payment);
		$this->updatePaymentKey();

		if ($this->getParams('payment.id', 0) == $payment->id) {
			return;
		}

		$this->setParams('payment.id', $payment->id);
		$this->setParams('payment.type', $payment->type);
		$this->setParams('payment.label', $payment->name);
		$this->save();
	}

	public function getPaymentDetails()
	{
		$items = [];
		foreach ($this->getProducts() as $itemLine) {
			$items[] = [
				'name' => $itemLine->description,
				"sku" => $itemLine->sku,
				'price' => $itemLine->getPrice('final'),
				'quantity' => $itemLine->qty
			];
		}
		if ($this->discount > 0) {
			foreach ($this->getParams('discounts', []) as $discount) {
				$items[] = [
					'name' => $discount['name'],
					"sku" => "",
					'price' => sprintf("%.2f", 0 - $discount['value']),
					'quantity' => 1
				];
			}
		}

		return [
			"reference" => isset($this->order_number) ? $this->order_number : '',
			"description" => '',
			"currency" => 'AUD',
			"total" => $this->total,
			"shipping" => $this->shipping_cost,
			"tax" => sprintf("%.2f", 0),
			"items" => $items
		];
	}

	public function onRedirectPayment($to = 'cart')
	{
		$routeOrder = config('quatius.shop.cart.url_prefix', 'order/');
		if ($to == 'cart')
			return url($routeOrder);

		if ($to == 'checkout')
			return url($routeOrder . 'checkout');

		if ($to == 'processing')
			return url($routeOrder . 'processing');

		if ($to == 'completed')
			return url($routeOrder . 'success/' . $this->order_number);
	}

	public function getPaymentBilling()
	{
		$datas = $this->getParentPaymentBilling();
		$datas['billingCountry'] = $datas['shippingCountry'] = 'AU';
		/*
		$datas['company'] =$this->getBillingDetail()->company;
		$datas["billingAddress1"] =$this->getBillingDetail()->address_1;
		$datas["billingAddress2"] =$this->getBillingDetail()->address_2;
		$datas["billingCity"] =$this->getBillingDetail()->city;
		$datas["billingPostcode"] =$this->getBillingDetail()->postcode;
		$datas["billingState"] =$this->getBillingDetail()->state;
		$datas["billingCountry"] =$this->getBillingDetail()->country;
		$datas["billingPhone"] =$this->getBillingDetail()->phone_1;
		$datas["email"] =$this->getBillingDetail()->email;
		*/
		return $datas;
	}
}
