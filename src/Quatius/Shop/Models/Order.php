<?php

namespace Quatius\Shop\Models;

use DB;
use Event;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Quatius\Shop\Traits\HasAddressDetails;
use Quatius\Shop\Events\OrderStatusChange;
use Illuminate\Database\Eloquent\Collection;
use Quatius\Shop\Traits\HasHistories;
use PDF;
use Quatius\Framework\Traits\ParamSetter;
use Quatius\Payment\Traits\PerformPayment;

class Order extends Model
{    
    use ParamSetter, HasAddressDetails, HasHistories, PerformPayment{
		getPaymentMethod as getPayment;
		setPaymentMethod as setParentPaymentMethod;
        getPaymentBilling as getParentPaymentBilling;
    }//, InvoiceReference;
	
	protected $attributes  = array(
			'total' => 0.00,
			'subtotal' => 0.00,
			'status' => 'pending',
			//'order_type' => 'R'
	);
	
	protected $fillable = [
	    //'prefix_num',
	    //'reduce_percentage',
	    //'discount_info',
	    //'order_type',
	    'params',
	    'order_desc',
	    'pronto_order_id',
	    'cust_reference',
	    'delivery_date',
	    'comments',
	    'order_number',
	    'payment_method',
	    'subtotal',
	    'shipping_cost',
	    'shipping_cost_ex_tax',
	    'total',
	    'tax_total',
	    'status',
	    'discount',
	    'currency'
	];
	
	public static function boot()
	{
		parent::boot();
		
		static::creating(function($item){
		    $item->getInvoiceNumber();
		});
		
		static::created(function($order){
			//Event::fire(new OrderCreated($order));
			Event::fire('shop.order.created',[$order]);
		});
	}
	
	public function getInvoiceNumber()
	{
	    if (!$this->order_number)
	    {
	        $this->order_number = generateInvoiceNumber();
		}
		
		return $this->order_number;
	}
	
	public function paymentApproved($status='confirmed')
	{
		$this->updateStatus($status);
		return true;
	}
	
	public function paymentRefunded($status='refunded')
	{
		$this->updateStatus($status);
		return true;
	}
	
	public function saveStatus($status='pending')
	{
		$this->updateStatus($status);
		return false;
	}
	
	public function getProducts()
	{
		return $this->items;
	}
	
	public function getShippingCost()
	{
		return $this->shipping_cost;
	}
	
	public function isGuest()
	{
		return !$this->getBillingDetail()->user_id;
	}
	
	public function getUser()
	{
		if ($this->isGuest()){
			return guest_user();
		}
		
		$user = User::find($this->getBillingDetail()->user_id);
		if ($user)
			return $user;
		else 
			return guest_user();
	}
	
	public function items()
	{
		return $this->hasMany(OrderItem::class);
	}
	
	public function details()
	{
		return $this->hasMany(OrderDetail::class);
	}
	
	public function updateStatus($newStatus='pending', $attributes=[])
	{
		$oldStatus =  $this->status;
		$attributes['status']=$newStatus;
		$history = $this->updateHistoryByStatus($newStatus, $attributes);
		$this->update(['status'=>$newStatus]);
		
		Event::fire(new OrderStatusChange($this, $oldStatus));
		Event::fire('shop.order-status-changed',[$this, $oldStatus]);
		
		return $history;
	}
	
	public function scopeByUser($query, $userId =0){

		if (!is_array($userId)){
			$userId = [$userId];
		}
		
		$query->join(DB::raw('(select user_id, order_id FROM order_details WHERE user_id in ('.implode(',', $userId).')) AS userdetail'), function($join) 
		{
			$join->on('id','=','userdetail.order_id');
		});
		
		return $query;
	}
	
	public function setShipmentDetail($detail)
	{
		if (is_array($detail) || $detail instanceof Collection)
			return false;
		
		$detail->setAsShippmentAddress();
		
		$getNumberOfShipmentDetails = $this->details()->shipments()->count();
		$detail->setAsShippmentAddress();
		if ($getNumberOfShipmentDetails == 1)
		{
			$oldShipmentDetail = $this->getShipmentDetail();
			
			$oldShipmentDetail->update($detail->toArray());
		}
		else
		{
			$this->details()->save($detail);
		}
		
		return $this;
	}
	
	function updateProductStock(){
	    $wareRepo = app('WarehouseHandler');
	    
	    $shipAdr = $this->getShipmentDetail();
	    
	    foreach ($this->items as $item){
	        $wareRepo->updateProductOrdered(
                $item->product_id, $item->qty,
                $shipAdr->country, $shipAdr->state);
	    }
	}
	
	public function getPdfFile($pdfPath = "")
	{
	    $pdfPath = $pdfPath==""?config('quatius.shop.invoice.path',storage_path('app/shop/invoices')):$pdfPath;
	    
	    if (!file_exists($pdfPath))
	        mkdir($pdfPath, 0755, true);
	        
	        $pdfFile = $pdfPath.'/'.$this->order_number.'.pdf';
	        
	        if (!file_exists($pdfFile)){
	            try{
	                $html = view("Shop::order.exports.pdf",['order'=>$this])
	                ->render();
	                
	                $pdf = PDF::loadHTML($html);
	                $pdf->save($pdfFile);
	            }
	            catch(\Exception $exception){
	                app('log')->error($exception->getFile(). "(" .$exception->getLine() . "): ".$exception->getMessage());
	            }
	        }
	        return $pdfFile;
	}

	public function getPaymentMethod(){
	    
	    if ($this->hasParams('payment.id')){
	        $this->setPaymentById($this->getParams('payment.id'));
	    }
	    
	    return $this->getPayment();
	}
	
	public function getPaymentDetails(){
		$items = [];
		foreach ($this->getProducts() as $itemLine){
			$items[] = [
				'name'=> $itemLine->description, 
				"sku" => $itemLine->sku,
				'price'=>$itemLine->getPrice('final'), 
				'quantity'=>$itemLine->qty
			];
        }
        if ($this->discount > 0){
            foreach($this->getParams('discounts',[]) as $discount){
				$items[] = [
					'name'=>$discount['name'],
					"sku" => "",
					'price'=>sprintf("%.2f",0-$discount['value']), 
					'quantity'=>1
				];
            }
		}
		
        return [
            "reference"=> isset($this->order_number)?$this->order_number:'',
            "description"=> '',
            "currency"=> 'AUD',
            "total"=> $this->total,
            "shipping"=> $this->shipping_cost,
            "tax"=> sprintf("%.2f",0),
            "items"=>$items
        ];
	}
	
	public function onRedirectPayment($to = 'cart'){
        $routeOrder = config('quatius.shop.cart.url_prefix','order/');
        if ($to == 'cart')
            return url($routeOrder);

        if ($to == 'checkout')
            return url($routeOrder.'checkout');

        if ($to == 'processing')
            return url($routeOrder.'processing');

        if ($to == 'completed')
            return url($routeOrder.'success/'.$this->order_number);
            
	}
	
	public function getPaymentBilling(){
        $datas = $this->getParentPaymentBilling();
		$datas['billingCountry'] = $datas['shippingCountry'] = 'AU';
		/*
		$datas['company'] =$this->getBillingDetail()->company;
		$datas["billingAddress1"] =$this->getBillingDetail()->address_1;
		$datas["billingAddress2"] =$this->getBillingDetail()->address_2;
		$datas["billingCity"] =$this->getBillingDetail()->city;
		$datas["billingPostcode"] =$this->getBillingDetail()->postcode;
		$datas["billingState"] =$this->getBillingDetail()->state;
		$datas["billingCountry"] =$this->getBillingDetail()->country;
		$datas["billingPhone"] =$this->getBillingDetail()->phone_1;
		$datas["email"] =$this->getBillingDetail()->email;
		*/
        return $datas;
    }
}
