<?php

namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ShopperGroup extends Model
{
	public $timestamps = false;
	protected $fillable = ['name', 'account', 'price_group', 'reference', 'ordering'];
	
	public static function boot()
	{
	    parent::boot();
	    static::creating(function (ShopperGroup $shopper) {
	        if (!isset($shopper->ordering)){
	            $shopper->ordering =  ShopperGroup::max('ordering') + 1;
	        }
	    });
	   
	}
	    
	public function scopeDefaultShoppers($query)
	{
		return $query->where('default', '=', 1);
	}
	
	public static function getDefault(){
		if (ShopperGroup::defaultShoppers()->count() == 0)
		{
			throw new \Exception("Default guest Shopper group not set.");
			return null;
		}
		
		return ShopperGroup::defaultShoppers()->first();
	}
	
	public function setDefault()
	{
		ShopperGroup::defaultShoppers()->update(['default'=>0]);
		$this->default = 1;
		$this->save();
	}

	public function scopeRegisteredDefaultShoppers($query)
	{
		return $query->where('default', '=', 2);
	}
	
	public static function getRegisteredDefault(){
		if (ShopperGroup::registeredDefaultShoppers()->count() == 0)
		{
			throw new \Exception("Default Registered Shopper group not set.");	
			return null;
		}
		
		return ShopperGroup::registeredDefaultShoppers()->first();
	}
	
	public function setRegisteredDefault()
	{
		ShopperGroup::registeredDefaultShoppers()->update(['default'=>0]);
		$this->default = 2;
		$this->save();
	}
	
    public function users()
    {
    	return $this->morphedByMany(User::class, 'shopper_assignable');
    }
    
    public function assignedClass($classPath="")
    {
    	return $this->morphedByMany($classPath, 'shopper_assignable');
    }
}
