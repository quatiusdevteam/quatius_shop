<?php
namespace Quatius\Shop\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;


class Dictionary extends Model
{
    protected $fillable = ['group','value','label','comment'];
    
    protected $dates = ['created_at','updated_at'];
    
    public static function boot()
    {
    	parent::boot();
    	static::saved(function($dic){
    	    self::clearCache($dic);
    	});
    	
    	static::deleted(function($dic){
    	    self::clearCache($dic);
    	});
    }
    
    static function group($group, $options=array()){
    	$items = self::lookupGroup($group);
    	
    	if (!$items->first() instanceof Dictionary){
    	    $items = collect();
    	}
    	
    	$result = array();
    	$key = isset($options['key']) ? $options['key'] : 'value';
    	$value = isset($options['value']) ? $options['value'] : 'label';
    	$extra_fields = isset($options['extras']) ? $options['extras'] : array();
    	foreach($items as $item){
    		$extras = array();
    		foreach($extra_fields as $ext){
    			$extras[] = $item->$ext;
    		}
    		$extra_values = count($extras)>0 ? ' ('.implode(' ',$extras).') ' : '';
    		$result[$item->$key] = $item->$value . $extra_values;
    	}
    	return $result;
    }
    
    static function custom($group, $callback=null){
    	
    	$items = self::lookupGroup($group);
    	
    	if(!is_string($callback) || !function_exists($callback)) {
    		return $items;
    	}
    	if(!is_object($callback)||!is_callable($callback)){
    		return $items;
    	}
    	return call_user_func_array($callback, array($items));
    }
    
    static function set($group, $value, $label, $comment = null, $update = true){
        $dic = self::lookup($group, $value);
        
        $datas = [
            'group'=>$group,
            "value"=>$value,
            "label"=>$label
        ];
        
        if ($comment) {
            $datas["comment"] = $comment;
        }
        
        if ($dic && $update){
            $dic->update($datas);
        }
        else{
            $dic = Dictionary::create($datas);
        }
    }
    
    static function remove($group, $value=null){
        if ($value != null){
            Dictionary::where('group',$group)->where('value',$value)->delete();
        }
        else{
            Dictionary::where('group',$group)->delete();
        }
        Cache::forget('dictionary-lookup-'.$group);
    }
    
    static function lookup($group, $value){
    	return self::lookupGroup($group)->where('value',$value)->first();
    }
    
    static function lookupGroup($group){
        $lookup = Cache::rememberForever('dictionary-lookup-'.$group, function() use($group){
            return Dictionary::where('group',$group)->get();
    	});
        
        if (!$lookup->first() instanceof Dictionary){
            $lookup = collect();
        }
            
    	return $lookup;
    }
    
    static function clearCache($dic){
        Cache::forget('dictionary-lookup-'.$dic->group);
    }
}
